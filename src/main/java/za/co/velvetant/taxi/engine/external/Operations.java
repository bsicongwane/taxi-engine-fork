package za.co.velvetant.taxi.engine.external;

import za.co.velvetant.taxi.ops.notifications.Notification;

public interface Operations {

    <T> void notify(Notification<T> notification);

}
