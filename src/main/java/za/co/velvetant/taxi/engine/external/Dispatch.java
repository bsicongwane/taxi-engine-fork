package za.co.velvetant.taxi.engine.external;

import java.util.Collection;

import za.co.velvetant.taxi.api.common.TaxiDistance;
import za.co.velvetant.taxi.dispatch.api.driver.FullDriverShift;
import za.co.velvetant.taxi.engine.api.Fare;

public interface Dispatch {

    Collection<TaxiDistance> fare(Fare fare, int closest, int radius, int window);

    Collection<FullDriverShift> expired(Long expiration);

    void removeFare(String uuid);

    void makeDriverBusy(String shiftIdentifier);

    void makeDriverAvailable(String shiftIdentifier);

    void endDriverShift(String shiftIdentifier);
}
