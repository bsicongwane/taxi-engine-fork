package za.co.velvetant.taxi.engine.activiti.delegates;

import static com.google.common.base.Optional.fromNullable;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.CVV;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_OVERRIDDEN;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_AMOUNT;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_METHOD;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_SUCCESSFUL;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.USE_ACCOUNT;

import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.base.Optional;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.delegates.payments.PaymentsFactory;
import za.co.velvetant.taxi.engine.model.exceptions.PaymentException;
import za.co.velvetant.taxi.engine.models.PaymentMethod;

@Named
public class ProcessPayment extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(ProcessPayment.class);

    @Inject
    private PaymentsFactory payments;

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        log.debug("Processing payment with [{}] for R: {}", execution.getVariable(PAYMENT_METHOD), execution.getVariable(PAYMENT_AMOUNT));
        final PaymentMethod paymentMethod = getVariable(execution, PAYMENT_METHOD, PaymentMethod.class);

        boolean paymentSuccessful = false;
        try {
            String tripUuid = execution.getProcessBusinessKey();
            String cvv = getVariable(execution, CVV, String.class);
            Boolean useAccount = fromNullable(getVariable(execution, USE_ACCOUNT, Boolean.class)).or(false);

            payments.processFor(paymentMethod.name()).process(tripUuid, cvv, useAccount);
            paymentSuccessful = true;
        } catch (PaymentException e) {
            log.error("Error processing payment", e);
        }

        execution.setVariable(PAYMENT_SUCCESSFUL, paymentSuccessful);
        execution.setVariable(OPS_OVERRIDDEN, false);
    }
}
