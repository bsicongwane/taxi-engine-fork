package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.DRIVER_RATING;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.api.Rating;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.ops.notifications.RatingDriverNotification;

@Named
public class RatingDriver extends ExecutionDelegate {

    @Inject
    private Operations operations;

    @Override
    public void process(final DelegateExecution execution) {
        final Rating rating = getVariable(execution, DRIVER_RATING, Rating.class);

        operations.notify(new RatingDriverNotification(execution.getProcessBusinessKey(), RatingDriver.class, rating));
    }
}
