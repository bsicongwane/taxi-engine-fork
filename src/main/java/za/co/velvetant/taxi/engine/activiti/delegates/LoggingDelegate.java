package za.co.velvetant.taxi.engine.activiti.delegates;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.delegate.VariableScope;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

public abstract class LoggingDelegate implements JavaDelegate, TaskListener, ExecutionListener {

    private static final Logger log = LoggerFactory.getLogger(LoggingDelegate.class);

    @Override
    public void execute(final DelegateExecution execution) throws IOException {
        log.debug("Process [{} - {}] - Processing delegate for task {}", execution.getProcessInstanceId(), execution.getProcessBusinessKey(), execution.getCurrentActivityName());
        log.debug("Process variables BEFORE processing delegate: {}", formatVariables(execution.getVariables()));

        process(execution);

        log.debug("Process variables AFTER processing delegate: {}", formatVariables(execution.getVariables()));
    }

    @Override
    public void notify(final DelegateExecution execution) throws Exception {
        execute(execution);
    }

    @Override
    public void notify(final DelegateTask task) {
        try {
            log.debug("Process [{} - {}] - Processing listener for event {}", task.getProcessInstanceId(), task.getName(), task.getEventName());
            log.debug("Process variables BEFORE processing listener: {}", formatVariables(task.getVariables()));

            process(task);

            log.debug("Process variables AFTER processing listener: {}", formatVariables(task.getVariables()));
        } catch (IOException ioe) {
            Throwables.propagate(ioe);
        }
    }

    public abstract void process(final DelegateExecution execution);

    public abstract void process(final DelegateTask task);

    protected <T> T getVariable(final VariableScope scope, final String name, final Class<T> clazz) {
        T castVariable = null;
        final Object variable = scope.getVariable(name);
        if (variable != null) {
            castVariable = clazz.cast(variable);
        }

        return castVariable;
    }

    private String formatVariables(final Map<String, Object> variables) throws IOException {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            MapUtils.debugPrint(new PrintStream(outputStream, true, CharEncoding.UTF_8), null, variables);

            return new String(outputStream.toByteArray(), CharEncoding.UTF_8);
        }
    }
}
