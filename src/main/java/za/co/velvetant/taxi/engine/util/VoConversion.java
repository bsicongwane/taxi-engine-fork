package za.co.velvetant.taxi.engine.util;

import static com.google.common.base.Optional.fromNullable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import za.co.velvetant.taxi.api.common.AffiliateLocation;
import za.co.velvetant.taxi.driverbox.api.fare.FareCustomer;
import za.co.velvetant.taxi.engine.api.Customer;
import za.co.velvetant.taxi.engine.api.Driver;
import za.co.velvetant.taxi.engine.api.Fare;
import za.co.velvetant.taxi.engine.api.FareState;
import za.co.velvetant.taxi.engine.api.PaymentMethod;
import za.co.velvetant.taxi.engine.api.Rating;
import za.co.velvetant.taxi.engine.api.Role;
import za.co.velvetant.taxi.engine.api.Taxi;
import za.co.velvetant.taxi.engine.api.User;
import za.co.velvetant.taxi.engine.api.coupon.Coupon;
import za.co.velvetant.taxi.engine.api.coupon.CustomerCoupon;
import za.co.velvetant.taxi.engine.api.transactions.TransactionType;
import za.co.velvetant.taxi.engine.models.DriverBox;
import za.co.velvetant.taxi.engine.models.DriverStatus;
import za.co.velvetant.taxi.engine.models.Location;
import za.co.velvetant.taxi.engine.models.RatingLevel;
import za.co.velvetant.taxi.engine.models.TaxiCompany;
import za.co.velvetant.taxi.engine.models.transactions.Transaction;

public final class VoConversion {

    private VoConversion() {
    }

    public static User convert(final za.co.velvetant.taxi.engine.models.User user) {
        User vo = null;
        if (user != null) {
            vo = new User(user.getFirstName(), user.getLastName(), user.getCellphone(), user.getEmail());
            for (final za.co.velvetant.taxi.engine.models.Role role : user.getGroups()) {
                vo.getUserRoles().add(Role.valueOf(role.getRole().name()));
            }

        }

        return vo;
    }

    public static za.co.velvetant.taxi.engine.models.User convert(final User user) {
        if (user == null) {
            return null;
        }

        return new za.co.velvetant.taxi.engine.models.User(user.getFirstName(), user.getLastName(), user.getCellphone(), user.getEmail(), user.getPassword());
    }

    public static List<User> convertUsers(final Collection<za.co.velvetant.taxi.engine.models.User> users) {
        final List<User> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.User user : users) {
            vos.add(convert(user));
        }
        return vos;
    }

    public static Driver convert(final za.co.velvetant.taxi.engine.models.driver.Driver driver) {
        if (driver == null) {
            return null;
        }
        final Driver vo = new Driver(driver.getFirstName(), driver.getLastName(), driver.getCellphone(), driver.getEmail(), driver.getIdNumber(), driver.getUserId(), convert(driver.getTaxiCompany()));
        if (driver.getStatus() != null) {
            vo.setStatus(driver.getStatus().toString());
        }

        return vo;
    }

    public static za.co.velvetant.taxi.engine.models.driver.Driver convert(final Driver driver) {
        za.co.velvetant.taxi.engine.models.driver.Driver vo = null;
        if (driver != null) {
            vo = new za.co.velvetant.taxi.engine.models.driver.Driver(driver.getFirstName(), driver.getLastName(), driver.getCellphone(), driver.getEmail(), driver.getIdNumber(), driver.getUserId(),
                    driver.getPassword());
            vo.setStatus(DriverStatus.valueOf(driver.getStatus()));
        }

        return vo;
    }

    public static List<Driver> convertDrivers(final Collection<za.co.velvetant.taxi.engine.models.driver.Driver> drivers) {
        final List<Driver> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.driver.Driver driver : drivers) {
            vos.add(convert(driver));
        }

        return vos;
    }

    public static List<Fare> convertFares(final List<za.co.velvetant.taxi.engine.models.Fare> fares) {
        final List<Fare> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Fare fare : fares) {
            vos.add(convert(fare));
        }
        return vos;
    }

    public static za.co.velvetant.taxi.api.common.Location convert(final Location location) {
        if (location == null) {
            return null;
        }
        return new za.co.velvetant.taxi.api.common.Location(location.getLatitude(), location.getLongitude(), location.getAddress());
    }

    public static Location convert(final za.co.velvetant.taxi.api.common.Location location) {
        if (location == null) {
            return null;
        }
        final Location location2 = new Location();
        if (location.getLatitude() != null) {
            location2.setLatitude(location.getLatitude().doubleValue());
        }
        if (location.getLongitude() != null) {
            location2.setLongitude(location.getLongitude().doubleValue());

        }
        location2.setAddress(location.getAddress());
        return location2;
    }

    public static Rating convert(final za.co.velvetant.taxi.engine.models.Rating rating) {
        if (rating == null) {
            return null;
        }

        final Rating rating2 = new Rating(nullSafeAssign(rating.getTripRating()), nullSafeAssign(rating.getDriverRating()), rating.getFare().getUuid());
        rating2.setCustomer(rating.getFare().getCustomer().getFirstName() + " " + rating.getFare().getCustomer().getLastName());
        rating2.setDate(rating.getFare().getDropOffTime());
        rating2.setFeedback(rating.getFeedback());
        return rating2;
    }

    public static int nullSafeAssign(final RatingLevel level) {
        int rating = 0;
        if (level != null) {
            rating = level.getRating();
        }

        return rating;
    }

    public static za.co.velvetant.taxi.engine.models.Rating convert(final za.co.velvetant.taxi.engine.api.Rating rating) {
        if (rating == null) {
            return null;
        }
        final za.co.velvetant.taxi.engine.models.Rating rating2 = new za.co.velvetant.taxi.engine.models.Rating(RatingLevel.getRating(rating.getTripRating()), RatingLevel.getRating(rating
                .getDriverRating()));
        rating2.setFeedback(rating.getFeedback());
        return rating2;
    }

    public static Customer convert(final za.co.velvetant.taxi.engine.models.Customer customer) {
        if (customer == null) {
            return null;
        }

        return new Customer(customer.getFirstName(), customer.getLastName(), customer.getCellphone(), customer.getEmail(), customer.getCreatedDate());
    }

    public static Customer convert(final za.co.velvetant.taxi.engine.models.Customer customer, final BigDecimal balance) {
        if (customer == null) {
            return null;
        }

        Customer voCustomer = new Customer(customer.getFirstName(), customer.getLastName(), customer.getCellphone(), customer.getEmail(), customer.getCreatedDate());
        voCustomer.setBalance(balance);

        return voCustomer;
    }

    public static za.co.velvetant.taxi.engine.models.Customer convert(final Customer customer) {
        if (customer == null) {
            return null;
        }

        return new za.co.velvetant.taxi.engine.models.Customer(customer.getFirstName(), customer.getLastName(), customer.getCellphone(), customer.getEmail(), null);
    }

    public static List<Customer> convertCustomers(final List<za.co.velvetant.taxi.engine.models.Customer> customers) {
        final List<Customer> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Customer customer : customers) {
            vos.add(convert(customer));
        }
        return vos;
    }

    public static Taxi convert(final za.co.velvetant.taxi.engine.models.Taxi taxi) {
        if (taxi == null) {
            return null;
        }

        final Taxi voTaxi = new Taxi();
        voTaxi.setCapacity(taxi.getCapacity());
        voTaxi.setColor(taxi.getColor());
        voTaxi.setMake(taxi.getMake());
        voTaxi.setModel(taxi.getModel());
        voTaxi.setRegistration(taxi.getRegistration());
        voTaxi.setStatus(taxi.getStatus().name());
        voTaxi.setTaxiId(taxi.getTaxiId());

        DriverBox taxiDriverBox = fromNullable(taxi.getDriverBox()).or(new DriverBox());
        voTaxi.setDriverBox(new za.co.velvetant.taxi.engine.api.DriverBox(taxiDriverBox.getSerialNumber(), taxiDriverBox.getVersion()));

        return voTaxi;
    }

    public static Fare convert(final za.co.velvetant.taxi.engine.models.Fare fare) {
        if (fare == null) {
            return null;
        }

        za.co.velvetant.taxi.api.common.Location from = null;
        if (fare.getPickup() != null) {
            from = new za.co.velvetant.taxi.api.common.Location(fare.getPickup().getLatitude(), fare.getPickup().getLongitude(), fare.getPickup().getAddress());
        }
        za.co.velvetant.taxi.api.common.Location to = null;
        if (fare.getDropOff() != null) {
            to = new za.co.velvetant.taxi.api.common.Location(fare.getDropOff().getLatitude(), fare.getDropOff().getLongitude(), fare.getDropOff().getAddress());
        }

        Rating rating = null;
        if (fare.getRating() != null) {
            rating = VoConversion.convert(fare.getRating());
        }

        final Fare api = new Fare(fare.getUuid(), 
                convert(fare.getCustomer()), 
                convert(fare.getDriver()), 
                convert(fare.getPaymentMethod()), 
                rating, 
                fare.getAcceptedTime(), 
                fare.getRequestTime(),
                fare.getDropOffTime(), 
                fare.getPickUpTime(), 
                fare.getAmount(), 
                convert(fare.getTaxi()), 
                from, 
                to, 
                String.valueOf(fromNullable(fare.getDistance()).or(0L)), 
                String.valueOf(fromNullable(fare.getDuration()).or(0L)));
        
        api.setState(FareState.valueOf(fare.getState().name()));
        api.setReference(fare.getReference());
        if (fare.getRewardPointLog() != null) {
            api.setRewards(fare.getRewardPointLog().getAmount());
        }
        if (fare.getDriver() != null && fare.getDriver().getTaxiCompany() != null) {
            api.setDriverAffiliate(fare.getDriver().getTaxiCompany().getCompanyName());
        }
        api.setTip(fare.getTip());
        if (api.getTip() == null) {
            api.setTip(0.0);
        }

        return api;
    }

    public static za.co.velvetant.taxi.driverbox.api.fare.Fare convertHack(final za.co.velvetant.taxi.engine.models.Fare fare) {
        if (fare == null) {
            return null;
        }

        za.co.velvetant.taxi.engine.models.Customer customer = fare.getCustomer();
        FareCustomer fareCustomer = null;
        if (customer != null) {
            fareCustomer = new FareCustomer(customer.getFirstName(), customer.getLastName(), customer.getUserId(), customer.getCellphone());
        }
        za.co.velvetant.taxi.api.common.Location from = null;
        if (fare.getPickup() != null) {
            from = new za.co.velvetant.taxi.api.common.Location(fare.getPickup().getLatitude(), fare.getPickup().getLongitude(), fare.getPickup().getAddress());
        }
        za.co.velvetant.taxi.api.common.Location to = null;
        if (fare.getDropOff() != null) {
            to = new za.co.velvetant.taxi.api.common.Location(fare.getDropOff().getLatitude(), fare.getDropOff().getLongitude(), fare.getDropOff().getAddress());
        }

        za.co.velvetant.taxi.driverbox.api.fare.Fare driverFare = new za.co.velvetant.taxi.driverbox.api.fare.Fare(fare.getUuid(), za.co.velvetant.taxi.engine.api.FareState.valueOf(fare.getState()
                .name()), fare.getReference(), fareCustomer, PaymentMethod.valueOf(fare.getPaymentMethod().name()), from, to, String.valueOf(fromNullable(fare.getDistance()).or(0L)),
                String.valueOf(fromNullable(fare.getDuration()).or(0L)));

        return driverFare;
    }

    public static Fare convert(final za.co.velvetant.taxi.engine.models.Fare fare, final List<String> affiliates) {
        final Fare convertedFare = convert(fare);
        convertedFare.addAffiliates(affiliates);

        return convertedFare;
    }

    public static PaymentMethod convert(final za.co.velvetant.taxi.engine.models.PaymentMethod paymentMethod) {
        if (paymentMethod == null) {
            return null;
        }

        return PaymentMethod.valueOf(paymentMethod.name());
    }

    public static CustomerCoupon convert(final za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon customerCoupon) {
        CustomerCoupon voCustomerCoupon = new CustomerCoupon();

        if (customerCoupon != null) {
            voCustomerCoupon = new CustomerCoupon(customerCoupon.getUuid(), convert(customerCoupon.getCoupon()), convert(customerCoupon.getCustomer()), customerCoupon.getRedemptionDate());
            voCustomerCoupon.setActive(!customerCoupon.hasExpired());
            voCustomerCoupon.setUsageCount(customerCoupon.getCoupon().getCustomerCoupons().size());
        }

        return voCustomerCoupon;
    }

    public static CustomerCoupon convert(final za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon customerCoupon, final BigDecimal balance) {
        CustomerCoupon voCustomerCoupon = convert(customerCoupon);
        voCustomerCoupon.setCustomer(convert(customerCoupon.getCustomer(), balance));

        return voCustomerCoupon;
    }

    public static Coupon convert(final za.co.velvetant.taxi.engine.models.coupons.Coupon coupon) {

        if (coupon == null) {
            return null;
        }

        final Coupon voCoupon = new Coupon(coupon.getUuid(), coupon.getCouponNumber(), coupon.getDescription(), coupon.getValidFrom(), coupon.getValidTo(), coupon.getMonetaryLimit(),
                coupon.getUsageLimit());

        voCoupon.setUsageCount(coupon.getCustomerCoupons().size());
        voCoupon.setActive(coupon.getActive());

        return voCoupon;
    }

    public static za.co.velvetant.taxi.engine.api.TaxiCompany convert(final TaxiCompany taxiCompany) {
        if (taxiCompany == null) {
            return null;
        }

        final za.co.velvetant.taxi.engine.api.TaxiCompany affiliate = new za.co.velvetant.taxi.engine.api.TaxiCompany(taxiCompany.getCompanyName(), taxiCompany.getBillingAddress1(),
                taxiCompany.getBillingAddress2(), taxiCompany.getBillingAddress3(), taxiCompany.getBillingPostalCode(), taxiCompany.getPostalAddress1(), taxiCompany.getPostalAddress2(),
                taxiCompany.getPostalAddress3(), taxiCompany.getPostalPostalCode(), taxiCompany.getRegistration(), taxiCompany.getVat(), taxiCompany.getTaxExempt());
        affiliate.setAdministratorEmail(taxiCompany.getAdministratorEmail());
        affiliate.setCabCount(taxiCompany.getDrivers().size());
        affiliate.setActive(taxiCompany.getActive());

        return affiliate;
    }

    public static TaxiCompany convert(final za.co.velvetant.taxi.engine.api.TaxiCompany taxiCompany) {
        if (taxiCompany == null) {
            return null;
        }

        final TaxiCompany affiliate = new TaxiCompany(taxiCompany.getCompanyName(), taxiCompany.getBillingAddress1(), taxiCompany.getBillingAddress2(), taxiCompany.getBillingAddress3(),
                taxiCompany.getBillingPostalCode(), taxiCompany.getPostalAddress1(), taxiCompany.getPostalAddress2(), taxiCompany.getPostalAddress3(), taxiCompany.getPostalPostalCode(),
                taxiCompany.getRegistration(), taxiCompany.getVat(), taxiCompany.getTaxExempt());
        affiliate.setAdministratorEmail(taxiCompany.getAdministratorEmail());
        if (taxiCompany.getLocations() != null) {
            for (AffiliateLocation l : taxiCompany.getLocations()) {
                affiliate.getLocations().add(
                        new za.co.velvetant.taxi.engine.models.AffiliateLocation(l.getLocation().getLatitude().doubleValue(), l.getLocation().getLongitude().doubleValue(), l.getLocation()
                                .getAddress(), l.getRadius()));
            }
        }

        return affiliate;
    }

    public static za.co.velvetant.taxi.engine.api.transactions.Transaction convert(final Transaction transaction) {
        za.co.velvetant.taxi.engine.api.transactions.Transaction voTransaction = new za.co.velvetant.taxi.engine.api.transactions.Transaction();
        if (transaction != null) {
            voTransaction.setUuid(transaction.getUuid());
            voTransaction.setDate(transaction.getDate());
            voTransaction.setType(TransactionType.valueOf(transaction.getType().name()));
            voTransaction.setAmount(transaction.getAmount());
            voTransaction.setCustomer(convert(transaction.getCustomer()));
            voTransaction.setTrip(convert(transaction.getTrip()));
        }

        return voTransaction;
    }

    public static <T> T convert(final Object from, final T to) {
        return ReflectionUtil.copySameNameFields(from, to);
    }
}
