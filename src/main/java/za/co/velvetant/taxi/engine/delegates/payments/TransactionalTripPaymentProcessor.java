package za.co.velvetant.taxi.engine.delegates.payments;

import static za.co.velvetant.taxi.engine.models.PaymentMethod.CREDIT_CARD;

import javax.inject.Inject;

import za.co.velvetant.taxi.engine.delegates.Transactions;
import za.co.velvetant.taxi.engine.model.Payment;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

public abstract class TransactionalTripPaymentProcessor implements PaymentProcessor {

    @Inject
    protected Transactions transactions;

    @Inject
    protected FareRepository fareRepository;

    protected abstract Payment createPayment(Fare trip, String cvv);

    protected abstract void process(Payment payment, Boolean useAccount);

    @Override
    public void process(final String tripUuid, final String cvv, final Boolean useAccount) {
        Fare trip = fareRepository.findByUUID(tripUuid);
        Payment payment = createPayment(trip, cvv);
        if(trip.getPaymentMethod() == CREDIT_CARD && useAccount) {
            payment.setInvoiceAmount(transactions.determinePayInGiven(payment));
        } else {
            payment.setInvoiceAmount(payment.getTripAmount());
        }

        process(payment, useAccount);
        record(payment);
    }

    protected void record(final Payment payment) {
        transactions.record(payment);
    }
}
