package za.co.velvetant.taxi.engine.rest.exception;

import static org.apache.commons.lang3.StringUtils.isBlank;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class MissingValueException extends WebApplicationException {

    public MissingValueException(final String message) {
        super(Response.status(422).entity(message).type(MediaType.TEXT_PLAIN_TYPE).build());
    }

    public static void assertNotEmpty(final Object o, final String notEmptyMessage) {
        if (isBlank(o.toString())) {
            throw new MissingValueException(notEmptyMessage);
        }

    }
}
