package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.ops.notifications.PaymentFailedNotification;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class PaymentFailed extends PaymentFailureDelegate {

    private Operations operations;

    @Inject
    protected PaymentFailed(final Operations operations, final FareRepository fareRepository, final MyGateTransactionRepository transactionRepository, final PaymentService paymentService) {
        super(fareRepository, transactionRepository, paymentService);
        this.operations = operations;
    }

    PaymentFailed() {
    }

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        operations.notify(new PaymentFailedNotification(execution.getProcessBusinessKey(), PaymentFailed.class, buildTransaction(execution)));
    }
}
