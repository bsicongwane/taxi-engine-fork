package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Group;
import za.co.velvetant.taxi.engine.models.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRole(Group role);
}
