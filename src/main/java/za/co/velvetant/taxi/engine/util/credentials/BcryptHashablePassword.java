package za.co.velvetant.taxi.engine.util.credentials;

import org.mindrot.jbcrypt.BCrypt;

public abstract class BcryptHashablePassword implements Password {

    @Override
    public String hash(final String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }
}
