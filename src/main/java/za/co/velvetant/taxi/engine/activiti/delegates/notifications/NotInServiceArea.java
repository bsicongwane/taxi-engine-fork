package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import static za.co.velvetant.taxi.engine.models.FareState.OUT_OF_SERVICE_AREA;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.messaging.mymessages.MessageService;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.ops.notifications.FareNotInServiceAreaNotification;

@Named
public class NotInServiceArea extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(NotInServiceArea.class);

    @Inject
    private Operations operations;

    @Inject
    private MessageService messageService;

    @Inject
    private FareRepository fareRepository;

    @Override
    public void process(final DelegateExecution execution) {
        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        log.debug("This fare [{}] is out of the service area", fare.getUuid());
        fare.setState(OUT_OF_SERVICE_AREA);
        fareRepository.save(fare);

        messageService.addNoServiceMessage(fare.getCustomer());

        operations.notify(new FareNotInServiceAreaNotification(execution.getProcessBusinessKey(), NotInServiceArea.class));
    }
}
