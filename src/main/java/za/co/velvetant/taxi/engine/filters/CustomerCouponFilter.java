package za.co.velvetant.taxi.engine.filters;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.coupons.Coupon;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon_;
import za.co.velvetant.taxi.persistence.coupons.CouponRepository;
import za.co.velvetant.taxi.persistence.coupons.CustomerCouponRepository;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class CustomerCouponFilter implements Filter<CustomerCoupon, za.co.velvetant.taxi.engine.api.coupon.CustomerCoupon, CustomerCouponFilter> {

    private static final Logger log = LoggerFactory.getLogger(CustomerCouponFilter.class);

    @Inject
    private CustomerCouponRepository customerCouponRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private CouponRepository couponRepository;

    @Inject
    private TransactionRepository transactionRepository;

    private CustomerCoupon example = new CustomerCoupon();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(DESC, "redemptionDate"));

    private Long total;
    private List<CustomerCoupon> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.coupon.CustomerCoupon> vos = new ArrayList<>();

    @Override
    public CustomerCouponFilter sort(Integer page, Integer size, String sort, Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));
        return this;
    }

    @Override
    public CustomerCouponFilter filter() {
        Page<CustomerCoupon> results = customerCouponRepository.findAll(new CustomerCouponSpecification(example), sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public CustomerCouponFilter andConvert() {
        for (final CustomerCoupon customerCoupon : filtered) {
            vos.add(convert(customerCoupon, transactionRepository.findCustomersBalance(customerCoupon.getCustomer())));
        }

        return this;
    }

    @Override
    public List<CustomerCoupon> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.coupon.CustomerCoupon> vos() {
        return vos;
    }

    public CustomerCouponFilter with(final String userid, final String couponNumber) {
        return withCoupon(couponNumber).withUserId(userid);
    }

    private CustomerCouponFilter withUserId(String userId) {
        Customer customer = null;
        if (isNotBlank(userId)) {
            customer = customerRepository.findByUserId(userId);
        }

        example.setCustomer(customer);

        return this;
    }

    private CustomerCouponFilter withCoupon(String couponNo) {
        Coupon coupon = null;
        if (isNotBlank(couponNo)) {
            coupon = couponRepository.findByCouponNumber(couponNo);
        }
        example.setCoupon(coupon);

        return this;
    }

    class CustomerCouponSpecification implements Specification<CustomerCoupon> {

        public CustomerCoupon customerCoupon;

        CustomerCouponSpecification(final CustomerCoupon customerCoupon) {
            this.customerCoupon = new LikableCoupon(customerCoupon);
        }

        @Override
        public Predicate toPredicate(Root<CustomerCoupon> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

            List<Predicate> predicates = new ArrayList<>();

            Path<Customer> customerPath = root.get(CustomerCoupon_.customer);
            if (example.getCustomer() != null) {
                predicates.add(cb.or(cb.equal(customerPath, customerCoupon.getCustomer())));
            }

            if (example.getCoupon() != null) {
                predicates.add(cb.or(cb.equal(root.get(CustomerCoupon_.coupon), customerCoupon.getCoupon())));
            }

            query.where(predicates.toArray(new Predicate[predicates.size()]));
            return query.getRestriction();
        }

        class LikableCoupon extends CustomerCoupon {
            LikableCoupon(CustomerCoupon customerCoupon) {
                this.setCoupon(fromNullable(customerCoupon.getCoupon()).orNull());
                this.setCustomer(fromNullable(customerCoupon.getCustomer()).orNull());
            }

            @Override
            public String getUuid() {
                return makeLikeable(super.getUuid());
            }

            private String makeLikeable(final String value) {
                String likeable = format("%%%s%%", fromNullable(value).or(""));

                log.trace("Made this Coupon value likeable: {} -> {}", value, likeable);
                return likeable;
            }
        }
    }
}
