package za.co.velvetant.taxi.engine.util.credentials;

import javax.inject.Named;
import java.util.Random;

@Named("SimpleDigitPassword")
public class SimpleDigitPassword extends BcryptHashablePassword {

    private static final Random random = new Random();
    private static final int DEFAULT_DIGITS = 4;

    @Override
    public String generate() {
        return buildPassword(DEFAULT_DIGITS);
    }

    @Override
    public String generate(final int digits) {
        return buildPassword(digits);
    }

    private String buildPassword(int digits) {
        StringBuilder builder = new StringBuilder(digits);
        for (int i = 0; i < digits; i++) {
            builder.append((char) ('0' + random.nextInt(10)));
        }

        return builder.toString();
    }
}
