package za.co.velvetant.taxi.engine.messaging.mail.mandrill;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {

    private String subject;
    @JsonProperty("from_email")
    private String fromEmail;
    @JsonProperty("from_name")
    private String fromName;

    @JsonProperty("track_opens")
    private boolean trackOpens = true;
    @JsonProperty("track_clicks")
    private boolean trackClicks = true;
    @JsonProperty("auto_text")
    private boolean autoText = true;
    @JsonProperty("preserve_recipients")
    private boolean preserveRecipients = true;

    private List<To> to = new ArrayList<>();
    @JsonProperty("merge_vars")
    private List<Merge> merge = new ArrayList<>();
    @JsonProperty("google_analytics_domain")
    private List<String> analyticsDomains = new ArrayList<>();
    @JsonProperty("google_analytics_campaign")
    private List<String> analyticsCampaign = new ArrayList<>();

    public String getSubject() {
        return subject;
    }

    public void setSubject(final String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(final String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(final String fromName) {
        this.fromName = fromName;
    }

    public boolean isTrackOpens() {
        return trackOpens;
    }

    public void setTrackOpens(final boolean trackOpens) {
        this.trackOpens = trackOpens;
    }

    public boolean isTrackClicks() {
        return trackClicks;
    }

    public void setTrackClicks(final boolean trackClicks) {
        this.trackClicks = trackClicks;
    }

    public boolean isAutoText() {
        return autoText;
    }

    public void setAutoText(final boolean autoText) {
        this.autoText = autoText;
    }

    public boolean isPreserveRecipients() {
        return preserveRecipients;
    }

    public void setPreserveRecipients(final boolean preserveRecipients) {
        this.preserveRecipients = preserveRecipients;
    }

    public List<To> getTo() {
        return to;
    }

    public void setTo(final List<To> to) {
        this.to = to;
    }

    public List<Merge> getMerge() {
        return merge;
    }

    public void setMerge(final List<Merge> merge) {
        this.merge = merge;
    }

    public List<String> getAnalyticsDomains() {
        return analyticsDomains;
    }

    public void setAnalyticsDomains(final List<String> analyticsDomains) {
        this.analyticsDomains = analyticsDomains;
    }

    public List<String> getAnalyticsCampaign() {
        return analyticsCampaign;
    }

    public void setAnalyticsCampaign(final List<String> analyticsCampaign) {
        this.analyticsCampaign = analyticsCampaign;
    }

}
