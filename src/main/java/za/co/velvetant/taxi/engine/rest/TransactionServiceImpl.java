package za.co.velvetant.taxi.engine.rest;

import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.api.transactions.Transaction;
import za.co.velvetant.taxi.engine.delegates.Transactions;
import za.co.velvetant.taxi.engine.filters.CustomerTransactionsFilter;
import za.co.velvetant.taxi.engine.filters.TransactionsFilter;
import za.co.velvetant.taxi.engine.models.transactions.TransactionType;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.engine.rest.exception.NotAllowedException;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

@Named
@Path("/transactions.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TransactionServiceImpl implements TransactionService {

    @Inject
    private Transactions delegate;

    @Inject
    private Provider<TransactionsFilter> filter;

    @Inject
    private Provider<CustomerTransactionsFilter> filterCustomerTransaction;

    @Inject
    private UserRepository userRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Context
    private HttpHeaders headers;

    @Override
    public Response findAll(final String uuid,
                            final Long date,
                            final TransactionType transactionType,
                            final String userid,
                            final String reference,
                            final Integer page,
                            final Integer size,
                            final String sort,
                            final Sort.Direction order) {
        final TransactionsFilter filteredTransaction = filter.get().with(uuid, date, transactionType, userid, reference).sort(page, size, sort, order).filter();
        List<Transaction> vos = filteredTransaction.andConvert().vos();
        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredTransaction.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }
        return response.build();
    }

    @Override
    public Response findAllForCustomer(final String uuid,
                                       final Long date,
                                       final TransactionType transactionType,
                                       final String userid,
                                       final String reference,
                                       final Integer page,
                                       final Integer size,
                                       final String sort,
                                       final Sort.Direction order) {
        checkAllowed(uuid);
        final CustomerTransactionsFilter filteredTransaction = filterCustomerTransaction.get().with(uuid, date, transactionType, userid, reference).sort(page, size, sort, order).filter();
        List<Transaction> vos = filteredTransaction.andConvert().vos();
        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredTransaction.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response find(final String uuid) {
        Transaction transaction = convert(delegate.find(uuid));

        return ok().entity(transaction).build();
    }

    private void checkAllowed(final String userId) {
        final String userid = CustomerRoutines.getUserId(headers);
        if (userid.equalsIgnoreCase(userId)) {
            final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userId);
            NotFoundException.assertNotNull(customer, String.format("Customer with userId %s does not exist", userId));
        } else {
            throw new NotAllowedException("You are not allowed to view these transactions.");
        }
    }
}
