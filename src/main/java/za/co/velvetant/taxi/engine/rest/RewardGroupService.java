package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_400;
import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_409;
import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.M_SECTOR_NAME;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import za.co.velvetant.taxi.engine.api.RewardGroup;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/rewardGroups")
@Api(value = "/rewardGroups", description = "Services for managing rewardGroups.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({ "CUSTOMER", "DRIVER" })
public interface RewardGroupService {

    String REWARD_GROUP_CLASS = "za.co.velvetant.taxi.engine.api.RewardGroup";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Registers a new reward group in the system.", responseClass = REWARD_GROUP_CLASS)
    @ApiErrors({ @ApiError(code = 400, reason = E_400), @ApiError(code = 409, reason = E_409 + " RewardGroup with same group already exists") })
    Response create(@ApiParam(value = "RewardGroup to create", required = true) RewardGroup rewardGroup);

    @PUT
    @Path("/{groupName}")
    @ApiOperation(value = "Updates a rewardGroup's details.", responseClass = REWARD_GROUP_CLASS)
    Response edit(@ApiParam(value = M_SECTOR_NAME, required = true) @PathParam("groupName") String groupName, @ApiParam(value = "RewardGroup details update", required = true) RewardGroup rewardGroup);

    @GET
    @Path("/{groupName}")
    @ApiOperation(value = "Finds a rewardGroup given the rewardGroup's name.", responseClass = REWARD_GROUP_CLASS)
    Response find(@ApiParam(value = M_SECTOR_NAME, required = true) @PathParam("groupName") String groupName);

    @GET
    @ApiOperation(value = "Finds all the rewardGroups in the system.", multiValueResponse = true, responseClass = REWARD_GROUP_CLASS)
    Response findAll();

}
