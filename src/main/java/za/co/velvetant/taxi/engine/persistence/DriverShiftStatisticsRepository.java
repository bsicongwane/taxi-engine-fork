package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.driver.DriverShift;
import za.co.velvetant.taxi.engine.models.driver.DriverShiftState;

import java.util.Date;

@Repository
public interface DriverShiftStatisticsRepository extends CrudRepository<DriverShift, Long> {

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state = :state and ended is null")
    Long countByState(@Param("state") DriverShiftState state);

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state = :state and ended is null and (started between :from and :to)")
    Long countByState(@Param("state") DriverShiftState state, @Param("from") Date from, @Param("to") Date to);

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state = :state and ended is null and driver.taxiCompany.registration = :registration")
    Long countByState(@Param("state") DriverShiftState state, @Param("registration") String registrationo);

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state = :state and ended is null and driver.taxiCompany.registration = :registration and (started between :from and :to)")
    Long countByState(@Param("state") DriverShiftState state, @Param("registration") String registration, @Param("from") Date from, @Param("to") Date to);

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state != :state and ended is null")
    Long countByStateNot(@Param("state") DriverShiftState state);

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state != :state and ended is null and (started between :from and :to)")
    Long countByStateNot(@Param("state") DriverShiftState state, @Param("from") Date from, @Param("to") Date to);

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state != :state and ended is null and driver.taxiCompany.registration = :registration")
    Long countByStateNot(@Param("state") DriverShiftState state, @Param("registration") String registration);

    @QueryHints(forCounting = true)
    @Query("select count(ds) from DriverShift ds where state != :state and ended is null and driver.taxiCompany.registration = :registration and (started between :from and :to)")
    Long countByStateNot(@Param("state") DriverShiftState state, @Param("registration") String registration, @Param("from") Date from, @Param("to") Date to);
}
