package za.co.velvetant.taxi.engine.external;

import java.math.BigDecimal;
import java.util.Collection;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import za.co.velvetant.taxi.api.common.TaxiDistance;
import za.co.velvetant.taxi.dispatch.api.driver.FullDriverShift;
import za.co.velvetant.taxi.engine.api.Fare;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;
import za.co.velvetant.taxi.persistence.taxi.TaxiLocationRepositoryCustom;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DispatchMock implements Dispatch {

    private static final Logger log = LoggerFactory.getLogger(DispatchMock.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Inject
    @Qualifier("FakeTaxiLocationRepository")
    private TaxiLocationRepositoryCustom taxiLocationRepository;

    @Inject
    private TaxiCompanyRepository affiliatesRepository;

    @Override
    public Collection<TaxiDistance> fare(final Fare fare, final int closest, final int radius, final int window) {
        String json = null;
        try {
            json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(fare);
        } catch (final JsonProcessingException e) {
            log.error("Could not marshal fare", e);
        }

        log.debug("[MOCK] Dispatching fare for the closest {} taxis: {}", closest, json);
        final Collection<TaxiDistance> taxisInVicinity = taxiLocationRepository.findInVicinity(new BigDecimal(fare.getPickup().getLatitude().doubleValue()), new BigDecimal(fare.getPickup()
                .getLongitude().doubleValue()), 5, fare.getAffiliates(), radius);

        log.debug("[MOCK] Dispatched to {} taxis: {}", taxisInVicinity.size(), taxisInVicinity);
        return taxisInVicinity;
    }

    @Override
    public Collection<FullDriverShift> expired(final Long expiration) {
        log.debug("[MOCK] Getting expired driver shifts with expiration of: {}", expiration);

        return null;
    }

    @Override
    public void removeFare(final String uuid) {
        log.debug("[MOCK] Removing fare {} from dispatch", uuid);
    }

    @Override
    public void makeDriverBusy(final String shiftIdentifier) {
        log.debug("[MOCK] Making driver shift {} as busy", shiftIdentifier);
    }

    @Override
    public void makeDriverAvailable(final String shiftIdentifier) {
        log.debug("[MOCK] Making driver shift {} available", shiftIdentifier);
    }

    @Override
    public void endDriverShift(final String shiftIdentifier) {
        log.debug("[MOCK] Ending driver shift {}", shiftIdentifier);
    }
}
