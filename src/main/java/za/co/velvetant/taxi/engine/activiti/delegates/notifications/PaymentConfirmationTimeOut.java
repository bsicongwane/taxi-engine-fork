package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.ops.notifications.PaymentConfirmationTimedOutNotification;

@Named
public class PaymentConfirmationTimeOut extends ExecutionDelegate {

    @Inject
    private Operations operations;

    @Override
    public void process(final DelegateExecution execution) {
        operations.notify(new PaymentConfirmationTimedOutNotification(execution.getProcessBusinessKey(), PaymentConfirmationTimeOut.class));
    }
}
