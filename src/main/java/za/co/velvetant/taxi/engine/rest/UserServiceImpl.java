package za.co.velvetant.taxi.engine.rest;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.api.ChangePasswordDetails;
import za.co.velvetant.taxi.engine.api.Role;
import za.co.velvetant.taxi.engine.api.User;
import za.co.velvetant.taxi.engine.filters.UserFilter;
import za.co.velvetant.taxi.engine.messaging.mail.MailService;
import za.co.velvetant.taxi.engine.models.Group;
import za.co.velvetant.taxi.engine.persistence.RoleRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.engine.util.VoConversion;
import za.co.velvetant.taxi.engine.util.credentials.Password;

@Named
@Path("/users.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class UserServiceImpl implements UserService {

    @Inject
    private UserRepository userRepository;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private Provider<UserFilter> filter;

    @Inject
    private MailService mailService;

    @Inject
    @Qualifier("AlphaNumericPassword")
    private Password password;

    @Override
    public Response create(final User user) {
        if (userRepository.findByEmailAndActive(user.getEmail()) == null) {
            za.co.velvetant.taxi.engine.models.User newUser = convert(user);
            newUser.setPassword(password.hash(user.getPassword()));
            newUser = userRepository.save(newUser);

            for (final Role role : user.getUserRoles()) {
                newUser.addGroup(roleRepository.save(new za.co.velvetant.taxi.engine.models.Role(Group.valueOf(role.name()), newUser)));
            }

            userRepository.save(newUser);

            return Response.status(CREATED).build();
        } else {
            throw new ConflictException(format("User %s already exists", user.getEmail()));
        }
    }

    @Override
    public Response edit(final String email, final User user) {
        za.co.velvetant.taxi.engine.models.User existingUser = userRepository.findByEmailAndActive(email);
        if (existingUser != null) {
            existingUser.setFirstName(user.getFirstName());
            existingUser.setLastName(user.getLastName());
            existingUser.setEmail(user.getEmail());
            existingUser.setCellphone(user.getCellphone());

            roleRepository.deleteInBatch(existingUser.getGroups());
            roleRepository.flush();
            existingUser.setGroups(new ArrayList<za.co.velvetant.taxi.engine.models.Role>());
            existingUser = userRepository.save(existingUser);

            for (final Role role : user.getUserRoles()) {
                existingUser.addGroup(roleRepository.save(new za.co.velvetant.taxi.engine.models.Role(Group.valueOf(role.name()), existingUser)));
            }

            userRepository.saveAndFlush(existingUser);
        } else {
            throw new PreconditionFailedException(format("User %s does no exist", user.getEmail()));
        }

        return ok().build();
    }

    @Override
    public Response remove(final String email) {
        final za.co.velvetant.taxi.engine.models.User existingUser = userRepository.findByEmailAndActive(email);
        if (existingUser != null) {
            existingUser.setActive(false);
            userRepository.save(existingUser);
        } else {
            throw new PreconditionFailedException(format("User %s does no exist", email));
        }

        return ok().build();
    }

    @Override
    public Response activate(final String email) {
        final za.co.velvetant.taxi.engine.models.User existingUser = userRepository.findByEmail(email);
        if (existingUser != null) {
            if (!existingUser.getActive()) {
                existingUser.setActive(true);
                userRepository.save(existingUser);
            } else {
                throw new PreconditionFailedException(format("User %s is not deactivated", email));
            }
        } else {
            throw new PreconditionFailedException(format("User %s does no exist", email));
        }

        return ok().build();
    }

    @Override
    public User find(final String email) {
        return convert(userRepository.findByEmailAndActive(email));
    }

    @Override
    public Response findAll(final String firstName, final String lastName, final String cellphone, final String email, final Boolean active, final Integer page, final Integer size, final String sort,
            final Sort.Direction order) {
        final UserFilter filteredUsers = filter.get().with(firstName, lastName, cellphone, email, active).sort(page, size, sort, order).filter();
        final List<User> vos = filteredUsers.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredUsers.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public List<User> findRange(final Integer from, final Integer to) {
        return VoConversion.convertUsers(userRepository.findAll());
    }

    @Override
    public String count() {
        return String.valueOf(userRepository.count());
    }

    @Override
    public Response changePassword(final String userid, final ChangePasswordDetails details) {
        // TODO verify that verifying old password is not required
        final za.co.velvetant.taxi.engine.models.User existingUser = userRepository.findByUserId(userid);
        if (details.getNewPassword() != null && details.getConfirmNewPassword() != null && details.getNewPassword().equals(details.getConfirmNewPassword())) {
            existingUser.setPassword(password.hash(details.getConfirmNewPassword()));
            userRepository.save(existingUser);
            return ok().build();
        }
        throw new PreconditionFailedException("Invalid new password and confirm password combination");
    }

    @Override
    public Response resetPassword(final String email) {
        final za.co.velvetant.taxi.engine.models.User existingUser = userRepository.findByEmailAndActive(email);

        final String newPassword = password.generate();
        existingUser.setPassword(password.hash(newPassword));
        userRepository.save(existingUser);
        mailService.sendPasswordResetEmail(existingUser, newPassword);

        return ok().build();
    }

}
