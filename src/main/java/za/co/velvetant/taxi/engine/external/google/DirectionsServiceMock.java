package za.co.velvetant.taxi.engine.external.google;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.api.common.Directions;
import za.co.velvetant.taxi.api.common.Distance;
import za.co.velvetant.taxi.api.common.Duration;
import za.co.velvetant.taxi.api.common.GoogleGeoCodeResponse;
import za.co.velvetant.taxi.api.common.Leg;
import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.api.common.Routes;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DirectionsServiceMock implements DirectionsService {

    private static final String GOOGLE_GEOCODE_URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng=:olatlng&sensor=true";
    private static final Logger log = LoggerFactory.getLogger(DirectionsServiceMock.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Directions get(final Location from, final Location to) {
        log.debug("Using test directions service to get directions from {} to {}", from, to);

        final Directions directions = new Directions();
        final List<Routes> routes = new ArrayList<>();
        final Routes route = new Routes();

        final List<Leg> legs = new ArrayList<>();
        final Leg leg = new Leg(new Distance("far far away", -1), new Duration("forever", -1), "start of space", "end of space");
        legs.add(leg);

        route.setLegs(legs);
        routes.add(route);
        directions.setRoutes(routes);

        return directions;
    }

    @Override
    public String getAddress(final Location location) {
        try {
            log.debug("Starting Google request for address");
            final String customUrlString = GOOGLE_GEOCODE_URL.replace(":olatlng", location.getLatitude().toString() + "," + location.getLongitude().toString());
            log.debug("Google directions request url: {}", customUrlString);
            final URL urlGoogleDirService = new URL(customUrlString);

            final HttpURLConnection urlGoogleDirCon = (HttpURLConnection) urlGoogleDirService.openConnection();
            urlGoogleDirCon.setRequestMethod("GET");
            urlGoogleDirCon.setRequestProperty("Accept", "application/json");

            if (urlGoogleDirCon.getResponseCode() != 200) {
                throw new IOException(String.format("Could not create connection to google directions service: %s", urlGoogleDirCon.getResponseMessage()));
            }

            final BufferedReader rd = new BufferedReader(new InputStreamReader(urlGoogleDirCon.getInputStream(), Charset.forName("UTF-8")));
            final StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();

            urlGoogleDirCon.disconnect();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            final GoogleGeoCodeResponse response = mapper.readValue(sb.toString(), GoogleGeoCodeResponse.class);
            if (response.getResults() != null && response.getResults().length > 0) {
                return response.getResults()[0].getFormatted_address();

            }
            log.debug(sb.toString());
            return null;
        } catch (final Exception e) {
            log.error("Error getting directions", e);
            throw new PreconditionFailedException(e, String.format("3rd party [google.directions] service not available: %s", e.getMessage()));
        }

    }
}
