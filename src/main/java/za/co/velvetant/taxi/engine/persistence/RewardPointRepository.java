package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.velvetant.taxi.engine.models.RewardPoint;

import java.util.List;

@Repository
public interface RewardPointRepository extends CrudRepository<RewardPoint, Long> {

    List<RewardPoint> findByCustomerUserId(String customerId);
}
