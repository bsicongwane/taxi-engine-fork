package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.models.driver.DriverShift;
import za.co.velvetant.taxi.ops.notifications.DriverAssignedNotification;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.driver.DriverShiftRepository;

@Named
public class DriverAssigned extends ExecutionDelegate {

    @Inject
    private DriverShiftRepository driverShiftRepository;

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private Operations operations;

    @Override
    public void process(final DelegateExecution execution) {
        final Driver driver = driverRepository.findByUserId(getVariable(execution, "driver", String.class));
        final DriverShift shift = driverShiftRepository.findActiveDriverShiftWithDriverUserId(driver.getUserId());

        operations.notify(new DriverAssignedNotification(execution.getProcessBusinessKey(), DriverAssigned.class, new za.co.velvetant.taxi.engine.api.DriverShift(driver.getUserId(), shift
                .getTaxiRegistration(), shift.getShiftIdentifier())));
    }
}
