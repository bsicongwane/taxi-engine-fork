package za.co.velvetant.taxi.engine.activiti.delegates;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.PaymentMethod;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class ChangeToCash extends ExecutionDelegate {

    @Inject
    private FareRepository fareRepository;

    private static final Logger log = LoggerFactory.getLogger(ChangeToCash.class);

    @Override
    public void process(final DelegateExecution execution) {
        Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        fare.setPaymentMethod(PaymentMethod.CASH);
        fareRepository.save(fare);
        log.debug("Changing fare: [{}] to cash payment method.", execution.getProcessBusinessKey());

    }
}
