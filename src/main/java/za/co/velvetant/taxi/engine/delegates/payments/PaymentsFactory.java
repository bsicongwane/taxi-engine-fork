package za.co.velvetant.taxi.engine.delegates.payments;

public interface PaymentsFactory {

    PaymentProcessor processFor(String paymentType);
}
