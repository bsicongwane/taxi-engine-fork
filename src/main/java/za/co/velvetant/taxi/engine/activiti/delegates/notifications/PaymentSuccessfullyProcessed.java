package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.ops.notifications.PaymentSuccessfullyProcessedNotification;

@Named
public class PaymentSuccessfullyProcessed extends ExecutionDelegate {

    @Inject
    private Operations operations;

    @Override
    public void process(final DelegateExecution execution) {
        operations.notify(new PaymentSuccessfullyProcessedNotification(execution.getProcessBusinessKey(), PaymentSuccessfullyProcessed.class, getVariable(execution, "driver", String.class)));
    }
}
