package za.co.velvetant.taxi.engine.persistence;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.RewardPointLog;

@Repository
public interface RewardPointLogRepository extends CrudRepository<RewardPointLog, Long> {


    @Query("select h from RewardPointLog h join fetch h.fare where h.fare.customer.userId = ?1")
    List<RewardPointLog> findByFareCustomerUserId(String userId);

    @Query("select h from RewardPointLog h join fetch h.fare where h.fare.customer.userId = ?1 order by h.transactionTime desc ")
    List<RewardPointLog> findLatest(String userId);

    List<RewardPointLog> findByFareCustomerUserIdAndTransactionTimeGreaterThan(String userId, Date startTime);

    List<RewardPointLog> findByFareCustomerUserIdAndTransactionTimeLessThan(String userId, Date endTime);

    List<RewardPointLog> findByFareCustomerUserIdAndTransactionTimeBetween(String userId, Date startTime, Date endTime);
}
