package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_AMOUNT;

import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.PaymentMethod;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.ops.notifications.AccountTransaction;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

public abstract class PaymentFailureDelegate extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(PaymentFailureDelegate.class);

    private FareRepository fareRepository;
    private MyGateTransactionRepository transactionRepository;
    private PaymentService paymentService;

    protected PaymentFailureDelegate(final FareRepository fareRepository, final MyGateTransactionRepository transactionRepository, final PaymentService paymentService) {
        this.fareRepository = fareRepository;
        this.transactionRepository = transactionRepository;
        this.paymentService = paymentService;
    }

    PaymentFailureDelegate() {
    }

    protected AccountTransaction buildTransaction(final DelegateExecution execution) {
        final AccountTransaction accountTransaction = new AccountTransaction();

        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());

        accountTransaction.setPaymentMethod(PaymentMethod.valueOf(fare.getPaymentMethod().name()));
        accountTransaction.setAmount(getVariable(execution, PAYMENT_AMOUNT, Double.class));
        accountTransaction.setTip(fare.getTip());

        final List<MyGateTransaction> transactions = transactionRepository.findLastTransaction(fare);
        if (transactions != null && !transactions.isEmpty()) {
            final MyGateTransaction transaction = transactions.get(0);
            za.co.velvetant.taxi.engine.models.CreditCard cardUsed = null;
            switch (accountTransaction.getPaymentMethod()) {
                case CREDIT_CARD:
                    cardUsed = transaction.getCustomer().getCreditCards().get(0);
                    break;
                case FAMILY_ACCOUNT:
                    cardUsed = transaction.getFamily().getCreditCards().get(0);
                    break;
                case CORPORATE_ACCOUNT:
                    cardUsed = transaction.getCorporate().getCreditCards().get(0);
                    break;
            }

            if (cardUsed != null) {
                try {
                    CreditCard cardDetails = paymentService.getMaskedCardDetails(cardUsed.getGateWayId());
                    accountTransaction.setCreditCard(new CreditCard(cardDetails.getCardNumber(), cardDetails.getCardHolder(), cardDetails.getExpiryMonth(), cardDetails.getExpiryYear(), null));
                } catch (Exception e) {
                    log.error("Could not get credit card details for token: {}", cardUsed.getGateWayId());
                }
            }

            accountTransaction.setAuthorisationId(transaction.getAuthorisationId());
            accountTransaction.setErrorMessage(transaction.getErrorMessage());
            accountTransaction.setStatus(transaction.getStatus());
            accountTransaction.setTransactionIndex(transaction.getTransactionIndex());
            accountTransaction.setTransactionTime(transaction.getTransactionTime());
        }

        return accountTransaction;
    }
}
