package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_400;
import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_409;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import za.co.velvetant.taxi.engine.api.TaxiCompany;

import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/taxiCompanies")
// @Api(value = "/taxiCompanies", description =
// "Services for managing taxi companies.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({ "CUSTOMER", "DRIVER" })
public interface TaxiCompanyService {

    String TAXI_COMPANY_CLASS = "za.co.velvetant.taxi.engine.api.TaxiCompany";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Registers a new taxi company in the system.", responseClass = TAXI_COMPANY_CLASS)
    @ApiErrors({ @ApiError(code = 400, reason = E_400), @ApiError(code = 409, reason = E_409 + " TaxiCompany with same registration already exists") })
    Response create(@ApiParam(value = "TaxiCompany to create", required = true) TaxiCompany sector);

    @PUT
    @Path("/{registration}")
    @ApiOperation(value = "Updates a taxi company details.", responseClass = TAXI_COMPANY_CLASS)
    Response edit(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = "TaxiCompany details to update", required = true) TaxiCompany sector);

    @GET
    @Path("/{registration}")
    @ApiOperation(value = "Finds a taxi company given the company registration.", responseClass = TAXI_COMPANY_CLASS)
    Response find(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration);

    @GET
    @ApiOperation(value = "Finds all the taxiCompany in the system.", multiValueResponse = true, responseClass = TAXI_COMPANY_CLASS)
    Response findAll();

}
