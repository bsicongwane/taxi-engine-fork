package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Sector;

@Repository
public interface SectorRepository extends CrudRepository<Sector, Long> {

    Sector findByName(String name);
}
