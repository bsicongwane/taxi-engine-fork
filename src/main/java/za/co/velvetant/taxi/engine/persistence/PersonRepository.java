package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Person;
import za.co.velvetant.taxi.persistence.repositories.auditing.AuditingRepository;
import za.co.velvetant.taxi.persistence.repositories.metamodel.MetaModelRepository;

@Repository
public interface PersonRepository extends AuditingRepository<Person, Long>, MetaModelRepository<Person, Long> {

    @Query("SELECT u FROM Person u where u.email = ?1 and u.active = true")
    Person findByEmailAndActive(String email);

    Person findByEmail(String email);

    Person findByUserId(String userId);

    Person findByCellphone(String cellphone);
}
