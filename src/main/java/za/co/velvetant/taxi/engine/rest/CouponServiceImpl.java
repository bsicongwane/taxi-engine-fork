package za.co.velvetant.taxi.engine.rest;

import static javax.ws.rs.core.Response.noContent;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.api.coupon.Coupon;
import za.co.velvetant.taxi.engine.api.coupon.CustomerCoupon;
import za.co.velvetant.taxi.engine.delegates.Coupons;
import za.co.velvetant.taxi.engine.filters.CouponFilter;
import za.co.velvetant.taxi.engine.filters.CustomerCouponFilter;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

@Named
@Path("/coupons.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class CouponServiceImpl implements CouponService {

    @Inject
    private Coupons coupons;

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private Provider<CouponFilter> filter;

    @Inject
    private Provider<CustomerCouponFilter> filterCustomerCoupon;

    @Override
    public Response create(final Coupon coupon) throws URISyntaxException {
        Coupon newCoupon = convert(coupons.create(coupon));

        return Response.created(new URI(newCoupon.getCouponNumber())).entity(newCoupon).build();
    }

    @Override
    public Response redeem(final String couponNumber, final String customer) {
        return Response.ok().entity(convertWithBalance(coupons.redeem(couponNumber, customer))).build();
    }

    @Override
    public Response findAll(final String couponNumber,
                            final Long validFrom,
                            final Long validTo,
                            final BigDecimal monetaryLimit,
                            final Boolean activated,
                            final Integer page,
                            final Integer size,
                            final String sort,
                            final Sort.Direction order) {
        final CouponFilter filteredCoupon = filter.get().with(couponNumber, validFrom, validTo, monetaryLimit, activated).sort(page, size, sort, order).filter();
        final List<Coupon> vos = filteredCoupon.andConvert().vos();
        Response.ResponseBuilder response = Response.ok().entity(vos).header("X-Total-Entities", filteredCoupon.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response find(final String couponNumber) {
        return Response.ok().entity(convert(coupons.findByCouponNumber(couponNumber))).build();
    }

    @Override
    public Response findCustomerCoupons(final String couponNumber,
                                        final String userid,
                                        final Boolean activated,
                                        final Integer page,
                                        final Integer size,
                                        final String sort,
                                        final Sort.Direction order) {
        final CustomerCouponFilter filteredCoupon = filterCustomerCoupon.get().with(userid, couponNumber).sort(page, size, sort, order).filter();
        final List<za.co.velvetant.taxi.engine.api.coupon.CustomerCoupon> vos = filteredCoupon.andConvert().vos();
        Response.ResponseBuilder response = Response.ok().entity(vos).header("X-Total-Entities", filteredCoupon.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response findCustomerCoupon(final String couponNumber, final String uuid) {
        return Response.ok().entity(convertWithBalance(coupons.findCustomerCouponByUuid(uuid))).build();
    }

    @Override
    public Response assign(final String couponNumber, final String userid) {
        return Response.ok().entity(convert(coupons.assign(couponNumber, userid))).build();
    }

    @Override
    public Response assignAll(final String couponNumber, final List<String> customers) {
        return Response.ok().entity(convert(coupons.assignAll(couponNumber, customers))).build();
    }

    @Override
    public Response unasssign(final String couponNumber, final String userid) {
        return Response.ok().entity(convert(coupons.unassign(couponNumber, userid))).build();
    }

    @Override
    public Response expire(final String couponNumber) {
        return Response.ok().entity(convert(coupons.expire(couponNumber))).build();
    }

    @Override
    public Response expireCustomerCoupon(final String couponNumber, final String uuid) {
        return Response.ok().entity(convert(coupons.expireCustomerCoupon(couponNumber, uuid))).build();
    }

    @Override
    public Response upload(final InputStream csvStream) {
        return ok();
    }

    private Response ok() {
        return Response.ok().entity("{}").build();
    }

    private CustomerCoupon convertWithBalance(final za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon customerCoupon) {
        return convert(customerCoupon, transactionRepository.findCustomersBalance(customerCoupon.getCustomer()));
    }
}
