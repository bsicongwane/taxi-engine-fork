package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.User;
import za.co.velvetant.taxi.persistence.repositories.auditing.AuditingRepository;
import za.co.velvetant.taxi.persistence.repositories.metamodel.MetaModelRepository;

@Repository
public interface UserRepository extends AuditingRepository<User, Long>, MetaModelRepository<User, Long> {

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.groups where u.email = ?1 and u.active = true")
    User findByEmailAndActive(String email);

    @Query("SELECT u FROM User u LEFT JOIN FETCH u.groups where u.userId = ?1 and u.active = true")
    User findByUserIdAndActive(String userId);

    User findByEmail(String email);

    User findByUserId(String userId);
}
