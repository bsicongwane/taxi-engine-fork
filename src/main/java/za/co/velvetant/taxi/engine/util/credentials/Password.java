package za.co.velvetant.taxi.engine.util.credentials;

public interface Password {

    String generate();

    String generate(int digits);

    String hash(String password);
}
