package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.api.RewardGroup;
import za.co.velvetant.taxi.engine.persistence.RewardGroupRepository;

@Named
@Path("/rewardGroups.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class RewardGroupServiceImpl implements RewardGroupService {

    @Inject
    private RewardGroupRepository rewardGroupRepository;

    @Override
    public Response create(final RewardGroup rewardGroup) {
        za.co.velvetant.taxi.engine.models.RewardGroup entity = convert(rewardGroup, new za.co.velvetant.taxi.engine.models.RewardGroup());
        entity = rewardGroupRepository.save(entity);
        return Response.status(Response.Status.CREATED).entity(convert(entity, new RewardGroup())).build();
    }

    @Override
    public Response edit(final String name, final RewardGroup rewardGroup) {
        final za.co.velvetant.taxi.engine.models.RewardGroup currentRewardGroup = rewardGroupRepository.findByGroupName(name);
        currentRewardGroup.setGroupName(rewardGroup.getGroupName());
        currentRewardGroup.setAmount(rewardGroup.getAmount());
        final za.co.velvetant.taxi.engine.models.RewardGroup updatedRewardGroup = rewardGroupRepository.save(currentRewardGroup);

        return Response.ok().entity(convert(updatedRewardGroup, new RewardGroup())).build();
    }

    @Override
    public Response find(final String name) {
        return Response.ok().entity(convert(rewardGroupRepository.findByGroupName(name), new RewardGroup())).build();
    }

    @Override
    public Response findAll() {
        final List<RewardGroup> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.RewardGroup rewardGroup : rewardGroupRepository.findAll()) {
            vos.add(convert(rewardGroup, new RewardGroup()));
        }
        return Response.ok().entity(vos).build();
    }
}
