package za.co.velvetant.taxi.engine.messaging.mail.mandrill;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import za.co.velvetant.taxi.engine.rest.exception.EngineException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

public class Mailer {

    private final String template;
    private final String email;
    private final String name;
    private final List<Var> mergeVars = new ArrayList<>();
    private final MandrillConfig configuration;
    private final String subject;

    public Mailer(final String template, final String email, final String name, final String subject, final MandrillConfig configuration) {

        this.template = template;
        this.email = email;
        this.name = name;
        this.configuration = configuration;
        this.subject = subject;
    }

    public void addMergeVar(final String name, final String content) {

        mergeVars.add(new Var(name, content));
    }

    public void send() {

        final Message message = buildMessage();

        final To to = new To();
        to.setEmail(email);
        to.setName(name);
        message.getTo().add(to);

        final Merge merge = new Merge();
        merge.setRecipient(email);
        merge.setVars(mergeVars);

        message.getMerge().add(merge);

        final SendTemplate sendTemplate = new SendTemplate();
        sendTemplate.setKey(configuration.getMandrillKey());
        sendTemplate.setTemplate(template);
        sendTemplate.setMessage(message);

        final Client client = Client.create();

        try {
            final String json = new ObjectMapper().writeValueAsString(sendTemplate);
            final ClientResponse response = client.resource(configuration.getMandrillTemplateUrl()).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, json);

            if (response.getStatus() != javax.ws.rs.core.Response.Status.OK.getStatusCode()) {
                final String error = String.format("Sending Mandrill mail template failed[%d]: %s", response.getStatus(), response.toString());
                throw new EngineException(error);
            }
        } catch (final JsonProcessingException e) {
            throw new EngineException(e);
        }

    }

    private Message buildMessage() {
        final Message message = new Message();
        message.setSubject(subject);
        message.setFromEmail(configuration.getMailFrom());
        message.setFromName(configuration.getMailFromName());
        message.getAnalyticsDomains().add(configuration.getAnalyticsDomain());
        message.getAnalyticsCampaign().add(message.getSubject());
        return message;
    }

}
