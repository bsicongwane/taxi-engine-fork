package za.co.velvetant.taxi.engine.rest;

import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/health")
public interface HeathService {

    @OPTIONS
    Response check();

    @GET
    @Path("/monitoring")
    /**
     * Ued for monitoring tools that do not support OPTIONS
     */
    Response monitoring();

}
