package za.co.velvetant.taxi.engine.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.transactions.Transaction;
import za.co.velvetant.taxi.engine.models.transactions.TransactionType;
import za.co.velvetant.taxi.engine.models.transactions.Transaction_;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
@Scope(SCOPE_PROTOTYPE)
public class TransactionsFilter implements Filter<Transaction, za.co.velvetant.taxi.engine.api.transactions.Transaction, TransactionsFilter> {

    private static final Logger log = LoggerFactory.getLogger(TransactionsFilter.class);

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private FareRepository fareRepository;

    private String uuid;
    private String reference;
    private Date date;
    private TransactionType transactionType;
    private Transaction example = new Transaction();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(DESC, "type"));

    private Long total;
    private List<Transaction> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.transactions.Transaction> vos = new ArrayList<>();

    @Override
    public TransactionsFilter sort(Integer page, Integer size, String sort, Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));
        return this;
    }

    @Override
    public TransactionsFilter filter() {
        log.debug("Transaction example: {}", example.getType());
        Page<Transaction> results = transactionRepository.findAll(new TransactionSpecification(example), sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    @Transactional
    public TransactionsFilter andConvert() {
        for (final Transaction transaction : filtered) {
            vos.add(convert(transaction));
        }

        return this;
    }

    @Override
    public List<Transaction> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.transactions.Transaction> vos() {
        return vos;
    }

    public TransactionsFilter with(final String uuid, final Long date, final TransactionType type, final String userId, final String reference) {
        if (date != null) {
            this.date = new Date(date);
        }

        this.uuid = uuid;
        this.reference = reference;
        this.transactionType = type;
        return withCustomer(userId).withReference(reference);
    }

    public TransactionsFilter withCustomer(String userId) {
        Customer customer;
        if (isNotBlank(userId)) {
            customer = customerRepository.findByUserId(userId);
            example.setCustomer(customer);
        }

        return this;
    }

    public TransactionsFilter withReference(String reference) {
        Fare fare;
        if (isNotBlank(reference)) {
            fare = fareRepository.findByReference(reference);
            example.setTrip(fare);
        }
        return this;
    }

    class TransactionSpecification implements Specification<Transaction> {

        public Transaction transaction;

        TransactionSpecification(final Transaction transaction) {
            this.transaction = new LikableTransaction(transaction);
        }

        @Override
        public Predicate toPredicate(Root<Transaction> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            List<Predicate> predicates = new ArrayList<>();
            if (date != null) {
                predicates.add(cb.or(cb.equal(root.get(Transaction_.date), date)));
            } else {
                predicates.add(cb.or(cb.isNotNull(root.get(Transaction_.date))));
            }

            if (transactionType != null) {
                predicates.add(cb.or(cb.equal(root.get(Transaction_.type), transactionType)));
            } else {
                predicates.add(cb.or(cb.isNotNull(root.get(Transaction_.type))));
            }

            if (example.getTrip() != null) {
                predicates.add(cb.equal(root.get(Transaction_.trip), transaction.getTrip()));

            } else {
                predicates.add(cb.or(cb.isNull(root.get(Transaction_.trip))));
            }

            if (example.getCustomer() != null) {
                predicates.add(cb.equal(root.get(Transaction_.customer), transaction.getCustomer()));

            } else {
                predicates.add(cb.or(cb.isNotNull(root.get(Transaction_.customer))));
            }

            if (uuid != null) {
                predicates.add(cb.or(cb.like(root.get(Transaction_.uuid), uuid)));

            } else {
                predicates.add(cb.or(cb.isNotNull(root.get(Transaction_.uuid))));
            }

            query.where(predicates.toArray(new Predicate[predicates.size()]));

            return query.getRestriction();
        }

        class LikableTransaction extends Transaction {

            LikableTransaction(Transaction transaction) {
                this.setCoupon(fromNullable(transaction.getCoupon()).orNull());
                this.setCustomer(fromNullable(transaction.getCustomer()).orNull());
                this.setTrip(fromNullable(transaction.getTrip()).orNull());
            }

            @Override
            public String getUuid() {
                return makeLikeable(super.getUuid());
            }

            private String makeLikeable(final String value) {
                String likeable = format("%%%s%%", fromNullable(value).or(""));

                log.trace("Made this Coupon value likeable: {} -> {}", value, likeable);
                return likeable;
            }

        }
    }
}
