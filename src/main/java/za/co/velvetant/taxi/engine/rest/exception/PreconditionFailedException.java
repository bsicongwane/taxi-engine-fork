package za.co.velvetant.taxi.engine.rest.exception;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PreconditionFailedException extends WebApplicationException {

    public PreconditionFailedException(final String message) {
        super(Response.status(Response.Status.PRECONDITION_FAILED).entity(message).type(MediaType.TEXT_PLAIN_TYPE).build());
    }

    public PreconditionFailedException(final Exception e, final String message) {
        super(Response.status(Response.Status.PRECONDITION_FAILED).entity(format("%s - %s", message, e.getMessage())).type(MediaType.TEXT_PLAIN_TYPE).build());
    }

    public static void assertNotEmpty(final String value, final String emptyMessage) {
        if (isBlank(value)) {
            throw new PreconditionFailedException(emptyMessage);
        }
    }

    public static void assertNotNull(final Object object, final String emptyMessage) {
        if (object == null) {
            throw new PreconditionFailedException(emptyMessage);
        }
    }
}
