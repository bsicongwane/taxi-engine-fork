package za.co.velvetant.taxi.engine.rest;

import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static za.co.velvetant.taxi.engine.models.Group.DRIVER;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;
import static za.co.velvetant.taxi.engine.util.VoConversion.convertDrivers;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.api.ChangePasswordDetails;
import za.co.velvetant.taxi.engine.api.Driver;
import za.co.velvetant.taxi.engine.api.Rating;
import za.co.velvetant.taxi.engine.filters.DriverFilter;
import za.co.velvetant.taxi.engine.filters.RatingFilter;
import za.co.velvetant.taxi.engine.messaging.mail.MailService;
import za.co.velvetant.taxi.engine.models.DriverStatus;
import za.co.velvetant.taxi.engine.models.Role;
import za.co.velvetant.taxi.engine.models.TaxiCompany;
import za.co.velvetant.taxi.engine.persistence.PersonRepository;
import za.co.velvetant.taxi.engine.persistence.RatingRepository;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.engine.util.Bytes;
import za.co.velvetant.taxi.engine.util.Strings;
import za.co.velvetant.taxi.engine.util.credentials.Password;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

import com.sun.jersey.api.ConflictException;

//import org.mindrot.jbcrypt.BCrypt;

@Named
@Path("/drivers.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class DriverServiceImpl implements DriverService {

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private PersonRepository personRepository;

    @Inject
    private RatingRepository ratingRepository;

    @Inject
    private Provider<DriverFilter> driverFilter;

    @Inject
    private Provider<RatingFilter> ratingFilter;

    @Inject
    private MailService mailService;

    @Inject
    private TaxiCompanyRepository taxiCompanyRepository;

    @Inject
    @Qualifier("AlphaNumericPassword")
    private Password password;

    @Override
    public Driver create(final Driver driver) {
        CustomerRoutines.checkUserId(personRepository, driver.getUserId());
        checkCellphone(driver.getCellphone());
        za.co.velvetant.taxi.engine.models.driver.Driver entity = new za.co.velvetant.taxi.engine.models.driver.Driver();
        entity.setUserId(driver.getUserId());
        entity.setPassword(password.hash(driver.getPassword()));
        entity.setCellphone(driver.getCellphone());
        entity.setEmail(driver.getEmail());
        entity.setFirstName(driver.getFirstName());
        entity.setIdNumber(driver.getIdNumber());
        entity.setLastName(driver.getLastName());
        entity.setActive(true);
        entity.setStatus(DriverStatus.OFFLINE);

        final Role role = new Role(DRIVER, entity);
        entity.addGroup(role);
        entity = driverRepository.save(entity);

        final Driver to = new Driver();

        return convert(entity, to);
    }

    @Override
    public Driver find(final String userId) {
        final za.co.velvetant.taxi.engine.models.driver.Driver driver = loadDriver(userId);
        final Driver vo = convert(driver);
        vo.setRating(findRating(userId));
        return vo;
    }

    @Override
    public List<Driver> findAll(final String affiliate) {
        List<za.co.velvetant.taxi.engine.models.driver.Driver> drivers;

        if (StringUtils.isNotBlank(affiliate)) {
            drivers = driverRepository.findByTaxiCompanyRegistration(affiliate);
        } else {
            drivers = driverRepository.findAll();
        }

        return convertDrivers(drivers);
    }

    @Override
    public Response findAll(final String firstName, final String lastName, final String cellphone, final String email, final Boolean active, final String affiliate, final Boolean affiliated,
            final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        final DriverFilter filteredDrivers = driverFilter.get().with(firstName, lastName, cellphone, email, active, affiliate, affiliated).sort(page, size, sort, order).filter();
        final List<za.co.velvetant.taxi.engine.api.Driver> vos = filteredDrivers.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredDrivers.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response edit(final String userid, final Driver driver) {
        za.co.velvetant.taxi.engine.models.driver.Driver loadDriver = loadDriver(userid);
        if (driver.getCellphone() != null && loadDriver.getCellphone() != null && !driver.getCellphone().equals(loadDriver.getCellphone())) {
            checkCellphone(driver.getCellphone());
            loadDriver.setCellphone(Strings.findKeeper(loadDriver.getCellphone(), driver.getCellphone()));
        }

        loadDriver.setFirstName(Strings.findKeeper(loadDriver.getFirstName(), driver.getFirstName()));
        loadDriver.setIdNumber(Strings.findKeeper(loadDriver.getIdNumber(), driver.getIdNumber()));
        loadDriver.setLastName(Strings.findKeeper(loadDriver.getLastName(), driver.getLastName()));
        loadDriver.setEmail(Strings.findKeeper(loadDriver.getEmail(), driver.getEmail()));
        loadDriver.setCellphone(Strings.findKeeper(loadDriver.getCellphone(), driver.getCellphone()));
        loadDriver.setUserId(Strings.findKeeper(loadDriver.getUserId(), driver.getUserId()));
        loadDriver = driverRepository.save(loadDriver);
        final Driver vo = convert(loadDriver);
        vo.setPassword(null);
        return Response.ok(vo).build();

    }

    @Override
    public String count() {
        return String.valueOf(driverRepository.count());
    }

    @Override
    public List<Rating> findRatings(final String userid) {
        final List<Rating> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Rating r : ratingRepository.findByFareDriverUserId(userid)) {
            vos.add(convert(r));
        }
        return vos;
    }

    @Override
    public Response findRatings(final String userid, final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        final RatingFilter filteredRatings = ratingFilter.get().with(userid).sort(page, size, sort, order).filter();
        final List<Rating> vos = filteredRatings.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredRatings.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public String findRating(final String userid) {
        return getRating(userid, ratingRepository);
    }

    public static String getRating(final String userid, final RatingRepository repository) {
        double count = 0.0;
        final List<za.co.velvetant.taxi.engine.models.Rating> ratings = repository.findByFareDriverUserId(userid);
        if (ratings.isEmpty()) {
            return StringUtils.EMPTY;
        }
        for (final za.co.velvetant.taxi.engine.models.Rating r : ratings) {
            if (r.getDriverRating() != null) {
                count += r.getDriverRating().getRating();
            }
        }
        return new DecimalFormat("##0.00").format(count / ratings.size());
    }

    @Override
    public Response uploadImage(final String userid, final InputStream inputStream, final String photoFormat) {
        final za.co.velvetant.taxi.engine.models.driver.Driver existing = loadDriver(userid);
        existing.setPhoto(Bytes.getBytes(inputStream));
        existing.setPhotoFormat(photoFormat);
        driverRepository.save(existing);

        return ok().build();
    }

    @Override
    public Response getPhoto(final String userid) {
        final za.co.velvetant.taxi.engine.models.driver.Driver existing = loadDriver(userid);
        if (existing.getPhoto() != null) {
            return Response.ok(new ByteArrayInputStream(existing.getPhoto())).build();
        }
        throw new NotFoundException("Driver with userid " + userid + " has no image loaded");

    }

    @Override
    public Response remove(final String userid) {
        final za.co.velvetant.taxi.engine.models.driver.Driver driver = loadDriver(userid);
        driver.setStatus(DriverStatus.OFFLINE);
        driver.setActive(false);
        TaxiCompany taxiCompany = driver.getTaxiCompany();
        if (taxiCompany != null) {
            if (taxiCompany.getDrivers().remove(driver)) {
                taxiCompanyRepository.save(taxiCompany);
            }
        }
        driver.setTaxiCompany(null);
        driverRepository.save(driver);
        return ok().build();
    }

    @Override
    public Response activate(final String userid) {
        final za.co.velvetant.taxi.engine.models.driver.Driver driver = loadDriver(userid);
        driver.setActive(true);
        driverRepository.save(driver);
        return ok().build();
    }

    private za.co.velvetant.taxi.engine.models.driver.Driver loadDriver(final String userId) {
        final za.co.velvetant.taxi.engine.models.driver.Driver driver = driverRepository.findByUserId(userId);
        NotFoundException.assertNotNull(driver, String.format("Driver with userId %s was not found", userId));
        return driver;
    }

    @Override
    public Response resetPassword(final String userid) {
        final za.co.velvetant.taxi.engine.models.driver.Driver driver = loadDriver(userid);

        final String newPassword = password.generate();
        driver.setPassword(password.hash(newPassword));
        driverRepository.save(driver);
        mailService.sendPasswordResetEmail(driver, newPassword);
        return ok().build();
    }

    @Override
    public Response getEncodedPhoto(final String userid) {
        final za.co.velvetant.taxi.engine.models.driver.Driver customer = loadDriver(userid);
        if (customer.getPhoto() != null) {
            return ok(new String(Base64.encodeBase64(customer.getPhoto()))).build();
        }

        throw new NotFoundException("Driver with userid " + userid + " has no image loaded");

    }

    @Override
    public Response changePassword(final String userid, final ChangePasswordDetails details) {
        // TODO verify that verifying old password is not required
        final za.co.velvetant.taxi.engine.models.driver.Driver driver = loadDriver(userid);
        if (details.getNewPassword() != null && details.getConfirmNewPassword() != null && details.getNewPassword().equals(details.getConfirmNewPassword())) {
            driver.setPassword(password.hash(details.getConfirmNewPassword()));
            driverRepository.save(driver);
            return ok().build();
        }
        throw new PreconditionFailedException("Invalid new password and confirm password combination");
    }

    private void checkCellphone(final String cellphone) {

        final za.co.velvetant.taxi.engine.models.driver.Driver existing = driverRepository.findByCellphone(cellphone);

        if (existing != null) {
            throw new ConflictException(String.format("A driver with cellphone %s already exists.", cellphone));
        }

    }

}
