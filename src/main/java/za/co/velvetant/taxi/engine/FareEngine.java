package za.co.velvetant.taxi.engine;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.api.FareTasks;
import za.co.velvetant.taxi.engine.models.FareState;

public interface FareEngine {

    /**
     * Start a fare based on the supplied request details identified by the uuid
     *
     * @param uuid
     * @param request
     * @return uuid of the started fare
     */
    String start(String uuid, FareRequest request);

    /**
     * Return the next task to be completed for the current fare for the specified person
     *
     *
     * @param uuid
     * @param person
     * @return the tasks to be completed by the specified person for the current fare
     */
    FareTasks nextTasks(String uuid, String person);

    /**
     * Complete the current task for the current fare for the specified person
     *
     * @param uuid
     * @param person
     */
    void completeTask(String uuid, String person);

    /**
     * Complete the current task with data for the current fare for the specified person
     *
     * @param uuid
     * @param person
     * @param data
     */
    void completeTask(String uuid, String person, Map<String, Object> data);

    /**
     * Complete the task of specified fare state for the current fare for the specified person
     *
     * @param uuid
     * @param person
     */
    void completeTask(String uuid, FareState type, String person);

    /**
     * Complete the task of specified fare state with data for the current fare for the specified person
     *
     * @param uuid
     * @param fareState
     * @param person
     * @param data
     */
    void completeTask(String uuid, FareState fareState, String person, Map<String, Object> data);

    /**
     * Send the specified signal to the fare
     *
     * @param signal
     */
    void sendSignal(String uuid, String signal);

    /**
     * Get all current active fares for the customer
     *
     * @param customer
     * @return current active fares
     */
    List<String> current(String customer);

    /**
     * Render a graphical representation of the fare process
     *
     * @param uuid
     * @return image representing the fare process
     */
    InputStream diagram(String uuid);

    /**
     * Get all fares waiting for task
     * @return current active fares
     */
    List<String> getFaresAwaitingFormTask(FareState fareState);

    List<String> completed(final Date start, final Date end, final String customer);

    void removeFare(String uuid, String reason);
}
