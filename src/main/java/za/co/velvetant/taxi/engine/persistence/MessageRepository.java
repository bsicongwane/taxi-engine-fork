package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Message;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

    Message findByUuid(String uuid);

    List<Message> findByCustomerUserId(String userId);

}
