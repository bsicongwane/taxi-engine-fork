package za.co.velvetant.taxi.engine.activiti.delegates;

import static com.google.common.base.Optional.fromNullable;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.DRIVER_OFFLINE_INTERVAL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.base.Optional;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.dispatch.api.driver.FullDriverShift;
import za.co.velvetant.taxi.engine.external.Dispatch;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

@Named
public class AllExpiredShifts extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(AllExpiredShifts.class);

    @Inject
    private Dispatch dispatch;

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    @Override
    public void process(final DelegateExecution execution) {
        log.debug("Getting all expired shifts");

        final List<String> expiredShifts = new ArrayList<>();
        try {
            final List<FullDriverShift> shifts = new ArrayList<>(fromNullable(dispatch.expired(Long.valueOf(systemPropertyHelper.getProperty(DRIVER_OFFLINE_INTERVAL)))).or(Collections.<FullDriverShift>emptyList()));
            expiredShifts.addAll(Lists.transform(shifts, new Function<FullDriverShift, String>() {

                @Override
                public String apply(final FullDriverShift driverShift) {
                    return driverShift.getShiftId();
                }
            }));

            log.debug("Found [{}] expired shifts", expiredShifts.size());
        } catch(PreconditionFailedException e) {
            log.error("Error getting expired shifts", e);
        }

        execution.setVariable("expiredShifts", expiredShifts);
    }
}
