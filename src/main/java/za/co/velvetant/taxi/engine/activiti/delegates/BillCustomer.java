package za.co.velvetant.taxi.engine.activiti.delegates;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_OVERRIDDEN;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_AMOUNT;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_METHOD;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.PaymentMethod;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class BillCustomer extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(BillCustomer.class);

    @Inject
    private FareRepository fareRepository;

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        log.debug("Billing customer[{}] with payment method [{}] for amount of R: {}", execution.getVariable("customer"), execution.getVariable(PAYMENT_METHOD), execution.getVariable(PAYMENT_AMOUNT));

        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        final Double amount = getVariable(execution, PAYMENT_AMOUNT, Double.class);
        fare.setAmount(amount);
        final PaymentMethod paymentMethod = getVariable(execution, PAYMENT_METHOD, PaymentMethod.class);
        fare.setPaymentMethod(paymentMethod);
        fare.setDropOffTime(new Date());
        fare.setDuration(new Duration(fare.getAcceptedTime().getTime(), fare.getDropOffTime().getTime()).toDuration().getMillis());
        fareRepository.save(fare);
        
        // reset Ops overridden
        execution.setVariable(OPS_OVERRIDDEN, false);
    }
}
