package za.co.velvetant.taxi.engine.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/messages")
@Api(value = "/messages", description = "Services for managing messages.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({ "CUSTOMER", "DRIVER" })
public interface MessageService {

    String MESSAGE_CLASS = "za.co.velvetant.taxi.engine.api.Message";

    @GET
    @Path("/{userid}/{uuid}")
    @ApiOperation(value = "Finds a customer message.", responseClass = MESSAGE_CLASS)
    Response find(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = DocumentationMessages.M_UUID, required = true) @PathParam("uuid") String uuid);

    @GET
    @Path("/{userid}")
    @ApiOperation(value = "Finds all the messages for a customer.", multiValueResponse = true, responseClass = MESSAGE_CLASS)
    Response findAll(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @DELETE
    @Path("/{userid}/{uuid}")
    @ApiOperation(value = "Removes a message")
    Response remove(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = DocumentationMessages.M_UUID, required = true) @PathParam("uuid") String uuid);

}
