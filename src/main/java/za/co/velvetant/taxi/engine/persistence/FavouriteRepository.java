package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Favourite;

@Repository
public interface FavouriteRepository extends CrudRepository<Favourite, Long> {

    List<Favourite> findByCustomerUserId(String customerEmail);

    Favourite findByNameAndCustomerUserId(String name, String customerEmail);
}
