package za.co.velvetant.taxi.engine.rest;

import com.sun.jersey.multipart.FormDataParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiParamImplicit;
import com.wordnik.swagger.annotations.ApiParamsImplicit;
import org.springframework.data.domain.Sort;
import za.co.velvetant.taxi.engine.api.coupon.Coupon;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;

@Path("/coupons")
@Api(value = "/coupons", description = "Service for managing coupons")
@RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT"})
public interface CouponService {

    String COUPON_CLASS = "za.co.velvetant.taxi.coupon.api.Coupon";
    String CUSTOMER_COUPON_CLASS = "za.co.velvetant.taxi.coupon.api.CustomerCoupon";

    @POST
    @ApiOperation(value = "Create a new coupon.", responseClass = COUPON_CLASS)
    Response create(@ApiParam(value = "Coupon to create", required = true) final Coupon coupon) throws URISyntaxException;

    @POST
    @Path("/{couponNumber}")
    @ApiOperation(value = "Redeem a coupon", responseClass = CUSTOMER_COUPON_CLASS)
    @RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER"})
    Response redeem(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber, @QueryParam("customer") String customer);

    @GET
    @ApiOperation(value = "Find all coupons", multiValueResponse = true, responseClass = COUPON_CLASS)
    Response findAll(@ApiParam(value = "Filter by coupon number.") @QueryParam("couponNumber") String couponNumber,
            @ApiParam(value = "Filter by date valid from.") @QueryParam("validFrom") Long validFrom,
            @ApiParam(value = "Filter by date valid to.") @QueryParam("validTo") Long validTo,
            @ApiParam(value = "Filter by monetary limit.") @QueryParam("monetaryLimit") BigDecimal monetaryLimit,
            @ApiParam(value = "Filter by activated/not activated coupons.", defaultValue = "true") @QueryParam("activated") @DefaultValue("true") Boolean activated,
            @ApiParam(value = "Page number of coupons to return.", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number coupons to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "validFrom", allowableValues = "validFrom,validTo") @QueryParam("sort") @DefaultValue("validFrom") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "desc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);

    @GET
    @Path("/{couponNumber}")
    @ApiOperation(value = "Find a coupon given the coupon number", responseClass = COUPON_CLASS)
    Response find(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber);

    @GET
    @Path("/customers")
    @ApiOperation(value = "Find all customer coupons", multiValueResponse = true, responseClass = CUSTOMER_COUPON_CLASS)
    @RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER"})
    Response findCustomerCoupons(@ApiParam(value = "Filter for coupon.") @QueryParam("couponNumber") String couponNumber,
            @ApiParam(value = "Filter by customer.") @QueryParam("userid") String userid,
            @ApiParam(value = "Filter by activated/not activated customer coupons.", defaultValue = "true") @QueryParam("activated") @DefaultValue("true") Boolean activated,
            @ApiParam(value = "Page number of customer coupons to return.", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number customer coupons to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "redemptionDate", allowableValues = "redemptionDate") @QueryParam("sort") @DefaultValue("redemptionDate") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);

    @GET
    @Path("/{couponNumber}/coupon/{uuid}")
    @ApiOperation(value = "Find a customer coupon given the coupon's uuid", responseClass = CUSTOMER_COUPON_CLASS)
    @RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER"})
    Response findCustomerCoupon(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber,
            @ApiParam(value = "Customer coupon uuid", required = true) @PathParam("uuid") String uuid);

    @POST
    @Path("/{couponNumber}/customers/{userid}")
    @ApiOperation(value = "Assign coupon to customer", responseClass = CUSTOMER_COUPON_CLASS)
    Response assign(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber,
            @ApiParam(value = "Customer", required = true) @PathParam("userid") String userid);

    @POST
    @Path("/{couponNumber}/customers")
    @ApiOperation(value = "Assign/unassign a coupon to/from multiple customers", responseClass = COUPON_CLASS)
    Response assignAll(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber,
            @ApiParam(value = "Customers", required = true, allowMultiple = true) List<String> customers);

    @DELETE
    @Path("/{couponNumber}/customers/{userid}")
    @ApiOperation(value = "Unassign a coupon assigned to the customer", responseClass = COUPON_CLASS)
    Response unasssign(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber,
            @ApiParam(value = "Customer", required = true) @PathParam("userid") String userid);

    @DELETE
    @Path("/{couponNumber}")
    @ApiOperation(value = "Expire coupon")
    Response expire(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber);

    @DELETE
    @Path("/{couponNumber}/coupon/{uuid}")
    @ApiOperation(value = "Expire customer coupon")
    Response expireCustomerCoupon(@ApiParam(value = "Coupon number", required = true) @PathParam("couponNumber") String couponNumber,
            @ApiParam(value = "Customer Coupon uuid", required = true) @PathParam("uuid") String uuid);

    @POST
    @Path("/upload")
    @ApiOperation(value = "Upload coupons csv file.")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiParamsImplicit(@ApiParamImplicit(dataType = "file", name = "file", paramType = "body"))
    Response upload(@FormDataParam("file") InputStream csvStream);

}
