package za.co.velvetant.taxi.engine.filters;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Family;
import za.co.velvetant.taxi.engine.persistence.FamilyRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class FamilyFilter implements Filter<Family, za.co.velvetant.taxi.engine.api.Family, FamilyFilter> {

    private static final Logger log = LoggerFactory.getLogger(FamilyFilter.class);

    @Inject
    private FamilyRepository familyRepository;

    private Family example = new Family();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "familyName"));

    private Long total;
    private List<Family> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Family> vos = new ArrayList<>();

    public FamilyFilter with(final String uuid, final String familyName) {
        return withUuid(uuid).withName(familyName);
    }

    @Override
    public FamilyFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public FamilyFilter filter() {
        Page<Family> results = familyRepository.findWithExample(example, sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public FamilyFilter andConvert() {
        for (final Family family : filtered) {
            final za.co.velvetant.taxi.engine.api.Family vo = new za.co.velvetant.taxi.engine.api.Family(family.getFamilyName(), family.getUuid());
            if (family.getAdministrators() != null) {
                final List<String> administrators = new ArrayList<>();
                for (final Customer customer : family.getAdministrators()) {
                    administrators.add(customer.getEmail());
                }
                vo.setAdministratorEmails(administrators);
            }

            vos.add(vo);
        }

        return this;
    }

    @Override
    public List<Family> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Family> vos() {
        return vos;
    }

    private FamilyFilter withUuid(final String uuid) {
        example.setUuid(uuid);

        return this;
    }

    private FamilyFilter withName(final String familyName) {
        example.setFamilyName(familyName);

        return this;
    }
}
