package za.co.velvetant.taxi.engine.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.api.common.AffiliateLocation;
import za.co.velvetant.taxi.engine.api.Driver;
import za.co.velvetant.taxi.engine.api.TaxiCompany;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/affiliates")
@Api(value = "/affiliates", description = "Services for managing affiliates.")
@RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT " })
public interface AffiliatesService {

    @POST
    @ApiOperation(value = "Registers a new taxi company in the system.", responseClass = "za.co.velvetant.taxi.engine.api.TaxiCompany")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 409, reason = DocumentationMessages.E_409 + " TaxiCompany with same name already exists") })
    Response create(@ApiParam(value = "TaxiCompany to create", required = true) TaxiCompany taxiCompany);

    @Path("/{registration}/driver")
    @POST
    @ApiOperation(value = "Add a driver to an affiliate", responseClass = "za.co.velvetant.taxi.engine.api.Driver")
    Response addDriver(@ApiParam(value = "Registration of affiliate", required = true) @PathParam("registration") String registration,
            @ApiParam(value = "Operator to create", required = true) Driver driver);

    @DELETE
    @Path("/{registration}/drivers/{driverId}")
    @ApiOperation(value = "Remove the driver from the affiliate")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_412) })
    Response removeDriver(@ApiParam(value = "Registration of affiliate", required = true) @PathParam("registration") String registration,
            @ApiParam(value = "Driver identifier", required = true) @PathParam("driverId") String driverId);

    @PUT
    @Path("/{registration}")
    @ApiOperation(value = "Updates a taxiCompany's details.")
    Response edit(@ApiParam(value = "Registration of affiliate", required = true) @PathParam("registration") String registration,
            @ApiParam(value = "TaxiCompany to update", required = true) TaxiCompany taxiCompany);

    @POST
    @Path("/{registration}/locations")
    @ApiOperation(value = "Add a taxiCompany's location")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_412) })
    Response addLocation(@ApiParam(value = "Registration of affiliate", required = true) @PathParam("registration") String registration,
            @ApiParam(value = "TaxiCompany to update", required = true) AffiliateLocation location);

    @PUT
    @Path("/{registration}/locations/{address}")
    @ApiOperation(value = "Add a taxiCompany's location")
    Response changeLocation(@ApiParam(value = "Registration of affiliate", required = true) @PathParam("registration") String registration,
            @ApiParam(value = "Name of location", required = true) @PathParam("address") String address, @ApiParam(value = "TaxiCompany to update", required = true) AffiliateLocation location);

    @DELETE
    @Path("/{registration}/locations/{address}")
    @ApiOperation(value = "Remove the location from the affiliate")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_412) })
    Response removeLocation(@ApiParam(value = "Registration of affiliate", required = true) @PathParam("registration") String registration,
            @ApiParam(value = "Name of location", required = true) @PathParam("address") String address);

    @ApiOperation(value = "Find close to given location.",  multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.TaxiCompany")
    @Path("/closeTo")
    @GET
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "FAMILY", "DRIVER" })
    Response closeTo(@ApiParam(value = "Location latitude", required = true) @QueryParam("latitude") String latitude,
            @ApiParam(value = "Location longitude", required = true) @QueryParam("longitude") String longitude);

    @DELETE
    @Path("/{registration}")
    @ApiOperation(value = "Remove the affiliate", notes = "Affiliate details are not permanently removed and can still be retrieved")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400) })
    Response remove(@ApiParam(value = "registration", required = true) @PathParam("registration") String registration);

    @HEAD
    @Path("/{registration}")
    @ApiOperation(value = "Activate a deactivated affiliate.")
    Response activate(@ApiParam(value = "registration", required = true) @PathParam("registration") String registration);

    @GET
    @Path("/{registration}")
    @ApiOperation(value = "Finds a taxiCompany given the taxiCompany's name.", responseClass = "za.co.velvetant.taxi.engine.api.TaxiCompany")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "FAMILY", "DRIVER" })
    TaxiCompany find(@ApiParam(value = "Registration number of affiliate", required = true) @PathParam("registration") String registration);

    @GET
    @ApiOperation(value = "Finds all the affiliates in the system.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.GetAffiliatesResponse")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "FAMILY", "DRIVER" })
    Response findAll();

    @GET
    @Path("/all")
    @ApiOperation(value = "Find all affiliates.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.TaxiCompany", notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    Response findAll(
            @ApiParam(value = "Filter by company name. Partial filtering is allowed.") @QueryParam("companyName") String companyName,
            @ApiParam(value = "Filter by registration. Partial filtering is allowed.") @QueryParam("registration") String registration,
            @ApiParam(value = "Filter by contact email. Partial filtering is allowed.") @QueryParam("contactEmail") String contactEmail,
            @ApiParam(value = "Filter by active/inactive affiliates.", defaultValue = "true") @QueryParam("active") @DefaultValue("true") Boolean active,
            @ApiParam(value = "Page number of affiliates to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number of affiliates to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "companyName", allowableValues = "companyName,registration,contactEmail") @QueryParam("sort") @DefaultValue("companyName") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

    @GET
    @Path("/count")
    @Produces("text/plain")
    String count();

}
