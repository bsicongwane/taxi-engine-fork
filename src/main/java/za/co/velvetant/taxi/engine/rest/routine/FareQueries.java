package za.co.velvetant.taxi.engine.rest.routine;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.PaymentMethod;

public final class FareQueries {

    private FareQueries() {

    }

    public static boolean customerFare(final Fare fare) {
        return (fare.getPaymentMethod() != null && fare.getPaymentMethod() == za.co.velvetant.taxi.engine.models.PaymentMethod.CASH)
                || (fare.getPaymentMethod() != null && fare.getPaymentMethod() == za.co.velvetant.taxi.engine.models.PaymentMethod.CREDIT_CARD);
    }

    public static boolean customerOnlyQuery(final String customer, final String registration, final String family) {
        return customer != null && registration == null && family == null;
    }

    public static boolean everyThingQuery(final String customer, final String registration, final String family) {
        final boolean allMissing = customer == null && registration == null && family == null;
        final boolean allThere = customer != null && registration != null && family != null;
        return allMissing || allThere;
    }

    public static boolean corporateOnlyQuery(final String customer, final String registration, final String family, final Fare fare) {
        final boolean corporateMatches = fare.getCustomer().getCorporate() != null && registration != null && registration.equals(fare.getCustomer().getCorporate().getRegistration());
        final boolean corporateOnly = registration != null && family == null;
        return corporateOnly && fare.getPaymentMethod() != null && fare.getPaymentMethod() == PaymentMethod.CORPORATE_ACCOUNT && corporateMatches;
    }

    public static boolean familyOnlyQuery(final String customer, final String registration, final String family, final Fare fare) {
        final boolean familyMatches = fare.getCustomer().getFamily() != null && family != null && family.equals(fare.getCustomer().getFamily().getUuid());
        final boolean onlyFamily = registration == null && family != null;
        return onlyFamily && fare.getPaymentMethod() != null && fare.getPaymentMethod() == PaymentMethod.FAMILY_ACCOUNT && familyMatches;
    }

    public static boolean customerOnly(final String customer, final String registration, final String family, final Fare fare) {
        return FareQueries.customerOnlyQuery(customer, registration, family) && FareQueries.customerFare(fare);
    }

    public static boolean shouldAddCompletedFare(final String customer, final String registration, final String family, final Fare fare) {
        return (customerOnly(customer, registration, family, fare) || everyThingQuery(customer, registration, family) || corporateOnlyQuery(customer, registration, family, fare) || familyOnlyQuery(
                customer, registration, family, fare));
    }

    public static boolean shouldAddActiveFare(final String customer, final String registration, final String family, final Fare fare) {
        final boolean customerOnlyQuery = customerOnlyQuery(customer, registration, family);
        final boolean customerOnly = customerOnlyQuery && fare.getCustomer().getUserId().equals(customer);
        return (customerOnly || everyThingQuery(customer, registration, family) || corporateOnlyQuery(customer, registration, family, fare) || familyOnlyQuery(customer, registration, family, fare));
    }
}
