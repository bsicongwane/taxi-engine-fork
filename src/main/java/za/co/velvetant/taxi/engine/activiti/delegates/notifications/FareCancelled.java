package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.api.FareState;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.ops.notifications.FareCancelledNotification;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class FareCancelled extends ExecutionDelegate {

    @Inject
    private Operations operations;

    @Inject
    private FareRepository fareRepository;

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        operations.notify(new FareCancelledNotification(execution.getProcessBusinessKey(), FareCancelled.class, FareState.valueOf(fare.getState().name())));
    }
}
