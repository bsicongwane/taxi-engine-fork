package za.co.velvetant.taxi.engine.util.credentials;

import javax.inject.Named;
import java.util.Random;

@Named("AlphaNumericPassword")
public class AlphaNumericPassword extends BcryptHashablePassword {

    private static final Random random = new Random();
    private static final String CHARACTER_SET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&1234567890";
    private static final int LENGTH = 10;

    @Override
    public String generate() {
        return generateString(random, CHARACTER_SET, LENGTH);
    }

    @Override
    public String generate(final int digits) {
        return generateString(random, CHARACTER_SET, digits);
    }

    private static String generateString(final Random random, final String characters, final int length) {
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(random.nextInt(characters.length()));
        }

        return new String(text);
    }
}
