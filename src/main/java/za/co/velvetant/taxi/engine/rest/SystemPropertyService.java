package za.co.velvetant.taxi.engine.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;
import za.co.velvetant.taxi.engine.api.SystemProperty;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/systemProperties")
@Api(value = "/systemProperties", description = "Services for managing systemProperties.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({ "ADMINISTRATOR", "SUPERVISOR" })
public interface SystemPropertyService {

    String SYSTEM_PROPERTY_CLASS = "za.co.velvetant.taxi.engine.api.SystemProperty";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Registers a new systemProperty in the system.", responseClass = SYSTEM_PROPERTY_CLASS)
    Response create(@ApiParam(value = "SystemProperty to create", required = true) SystemProperty systemProperty);

    @PUT
    @Path("/{name}")
    @ApiOperation(value = "Updates a systemProperty's details.", responseClass = SYSTEM_PROPERTY_CLASS)
    Response edit(@ApiParam(required = true) @PathParam("name") String name, @ApiParam(value = "SystemProperty details update", required = true) SystemProperty systemProperty);

    @GET
    @Path("/{name}")
    @ApiOperation(value = "Finds a systemProperty given the systemProperty's name.", responseClass = SYSTEM_PROPERTY_CLASS)
    Response find(@ApiParam(required = true) @PathParam("name") String name);

    @DELETE
    @Path("/{name}")
    @ApiOperation(value = "Remove.", responseClass = "za.co.velvetant.taxi.engine.api.SystemProperty")
    Response remove(@ApiParam(required = true) @PathParam("name") String name);

    @HEAD
    @Path("/{name}")
    @ApiOperation(value = "Activate a deactivated system property.")
    Response activate(@ApiParam(value = "name", required = true) @PathParam("name") String name);

    @GET
    @ApiOperation(value = "Find all properties.", multiValueResponse = true, responseClass = SYSTEM_PROPERTY_CLASS,
            notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    Response findAll(@ApiParam(value = "Filter by name. Partial filtering is allowed.") @QueryParam("name") String name,
            @ApiParam(value = "Filter by active/inactive properties.", defaultValue = "true") @QueryParam("active") @DefaultValue("true") Boolean active,
            @ApiParam(value = "Page number of properties to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number of properties to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "name", allowableValues = "name,value,startTime") @QueryParam("sort") @DefaultValue("name") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

}
