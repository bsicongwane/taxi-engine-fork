package za.co.velvetant.taxi.engine.activiti.delegates;

import org.activiti.engine.delegate.DelegateExecution;

public abstract class ListenerDelegate extends LoggingDelegate {

    @Override
    public void process(final DelegateExecution execution) {
        throw new UnsupportedOperationException("This is not a execution delegate, cannot process this delegate");
    }
}
