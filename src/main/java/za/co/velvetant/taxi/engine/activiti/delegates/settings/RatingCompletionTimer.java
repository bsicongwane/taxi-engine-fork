package za.co.velvetant.taxi.engine.activiti.delegates.settings;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.RATING_COMPLETION_DURATION;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

@Named
public class RatingCompletionTimer {

    private static final Logger log = LoggerFactory.getLogger(RatingCompletionTimer.class);

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    public String duration() {
        String duration = systemPropertyHelper.getProperty(RATING_COMPLETION_DURATION, "PT5M");

        log.debug("Using rating completion duration: {}", duration);
        return duration;
    }
}
