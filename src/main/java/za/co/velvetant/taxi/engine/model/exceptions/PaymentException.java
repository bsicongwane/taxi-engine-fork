package za.co.velvetant.taxi.engine.model.exceptions;

import static java.lang.String.format;

public class PaymentException extends RuntimeException {

    public PaymentException(final String message) {
        super(message);
    }

    public PaymentException(final String message, final Object... args) {
        super(format(message, args));
    }
}
