package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.ops.notifications.PaymentDeclinedNotification;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class PaymentDeclined extends PaymentFailureDelegate {

    private static final Logger log = LoggerFactory.getLogger(PaymentDeclined.class);

    private Operations operations;

    @Inject
    protected PaymentDeclined(final Operations operations, final FareRepository fareRepository, final MyGateTransactionRepository transactionRepository, final PaymentService paymentService) {
        super(fareRepository, transactionRepository, paymentService);
        this.operations = operations;
    }

    PaymentDeclined() {
    }

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        operations.notify(new PaymentDeclinedNotification(execution.getProcessBusinessKey(), PaymentDeclined.class, buildTransaction(execution)));
    }
}
