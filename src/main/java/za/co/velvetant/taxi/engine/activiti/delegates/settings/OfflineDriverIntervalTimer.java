package za.co.velvetant.taxi.engine.activiti.delegates.settings;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.DRIVER_OFFLINE_POLLING_INTERVAL;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

@Named
public class OfflineDriverIntervalTimer {

    private static final Logger log = LoggerFactory.getLogger(OfflineDriverIntervalTimer.class);

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    public String duration() {
        String interval = systemPropertyHelper.getProperty(DRIVER_OFFLINE_POLLING_INTERVAL, "R/PT2M");

        log.debug("Using driver offline polling interval: {}", interval);
        return interval;
    }
}
