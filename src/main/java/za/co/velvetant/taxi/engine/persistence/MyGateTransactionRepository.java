package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;

@Repository
public interface MyGateTransactionRepository extends CrudRepository<MyGateTransaction, Long> {

    @Query("select t from MyGateTransaction t where fare = ?1 order by transactionTime desc")
    List<MyGateTransaction> findLastTransaction(Fare fare);
}
