package za.co.velvetant.taxi.engine.external.google;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.api.common.Directions;
import za.co.velvetant.taxi.api.common.GoogleGeoCodeResponse;
import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DirectionsServiceHTTP implements DirectionsService {

    private static final String GOOGLE_MAPS_URL = "http://maps.googleapis.com/maps/api/directions/json?origin=:oLat,:oLong&destination=:dLat,:dLong&region=za&sensor=true&units=metric";
    private static final String GOOGLE_GEOCODE_URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng=:olatlng&sensor=true";

    private static final Logger log = LoggerFactory.getLogger(DirectionsServiceHTTP.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Directions get(final Location from, final Location to) {
        try {
            log.debug("Starting Google request for directions");
            String customUrlString = GOOGLE_MAPS_URL.replace(":oLat", from.getLatitude().toString());
            customUrlString = customUrlString.replace(":oLong", from.getLongitude().toString());
            customUrlString = customUrlString.replace(":dLat", to.getLatitude().toString());
            customUrlString = customUrlString.replace(":dLong", to.getLongitude().toString());
            log.debug("Google directions request url: {}", customUrlString);

            final URL urlGoogleDirService = new URL(customUrlString);

            final HttpURLConnection urlGoogleDirCon = (HttpURLConnection) urlGoogleDirService.openConnection();
            urlGoogleDirCon.setRequestMethod("GET");
            urlGoogleDirCon.setRequestProperty("Accept", "application/json");

            if (urlGoogleDirCon.getResponseCode() != 200) {
                throw new IOException(String.format("Could not create connection to google directions service: %s", urlGoogleDirCon.getResponseMessage()));
            }

            final BufferedReader rd = new BufferedReader(new InputStreamReader(urlGoogleDirCon.getInputStream(), Charset.forName("UTF-8")));
            final StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();

            urlGoogleDirCon.disconnect();
            log.debug(sb.toString());
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            final Directions directions = mapper.readValue(sb.toString(), Directions.class);

            return directions;
        } catch (final Exception e) {
            log.error("Error getting directions", e);
            throw new PreconditionFailedException(e, String.format("3rd party [google.directions] service not available: %s", e.getMessage()));
        }
    }

    @Override
    public String getAddress(final Location location) {
        try {
            log.debug("Starting Google request for address");
            final String customUrlString = GOOGLE_GEOCODE_URL.replace(":olatlng", location.getLatitude().toString() + "," + location.getLongitude().toString());
            log.debug("Google directions request url: {}", customUrlString);
            final URL urlGoogleDirService = new URL(customUrlString);

            final HttpURLConnection urlGoogleDirCon = (HttpURLConnection) urlGoogleDirService.openConnection();
            urlGoogleDirCon.setRequestMethod("GET");
            urlGoogleDirCon.setRequestProperty("Accept", "application/json");

            if (urlGoogleDirCon.getResponseCode() != 200) {
                throw new IOException(String.format("Could not create connection to google directions service: %s", urlGoogleDirCon.getResponseMessage()));
            }

            final BufferedReader rd = new BufferedReader(new InputStreamReader(urlGoogleDirCon.getInputStream(), Charset.forName("UTF-8")));
            final StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();

            urlGoogleDirCon.disconnect();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            final GoogleGeoCodeResponse response = mapper.readValue(sb.toString(), GoogleGeoCodeResponse.class);
            if (response.getResults() != null && response.getResults().length > 0) {
                return response.getResults()[0].getFormatted_address();

            }
            log.debug(sb.toString());
            return null;
        } catch (final Exception e) {
            log.error("Error getting directions", e);
            throw new PreconditionFailedException(e, String.format("3rd party [google.directions] service not available: %s", e.getMessage()));
        }

    }
}
