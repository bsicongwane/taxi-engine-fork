package za.co.velvetant.taxi.engine.delegates;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.api.CreateCustomer;
import za.co.velvetant.taxi.engine.messaging.mymessages.MessageService;
import za.co.velvetant.taxi.engine.messaging.sms.SmsService;
import za.co.velvetant.taxi.engine.models.Role;
import za.co.velvetant.taxi.engine.persistence.PersonRepository;
import za.co.velvetant.taxi.engine.persistence.RoleRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.engine.util.credentials.Password;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.UUID;

import static za.co.velvetant.taxi.engine.models.Group.CUSTOMER;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
public class Customers {

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private RoleRepository roleRepository;

    @Inject
    private MessageService messageService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private SmsService smsMessageService;

    @Inject
    private PersonRepository personRepository;

    @Inject
    @Qualifier("SimpleDigitPassword")
    private Password password;

    @Transactional
    public CreateCustomer create(final CreateCustomer customer) {

        CustomerRoutines.checkUserId(personRepository, customer.getEmail());
        CustomerRoutines.checkCellphone(customerRepository, customer.getCellphone());

        za.co.velvetant.taxi.engine.models.Customer entity = new za.co.velvetant.taxi.engine.models.Customer();
        entity.setCellphone(customer.getCellphone());
        entity.setEmail(customer.getEmail());
        entity.setUserId(customer.getEmail());
        entity.setFirstName(customer.getFirstName());
        entity.setLastName(customer.getLastName());
        entity.setSendNewsLetter(customer.getSendNewsLetter());
        entity.setActive(false);
        entity.setPin(password.generate(5));
        entity.setPassword(password.hash(customer.getPassword()));

        entity.setHash(UUID.randomUUID().toString());
        final Role role = new Role(CUSTOMER, entity);
        entity.addGroup(role);
        entity = customerRepository.save(entity);

        roleRepository.save(role);

        final CreateCustomer vo = convert(entity, new za.co.velvetant.taxi.engine.api.CreateCustomer());
        vo.setUserId(customer.getEmail());
        vo.setPassword(null);

        messageService.addNewUserMessage(entity);
        smsMessageService.addNewUserMessage(entity);

        return vo;
    }
}
