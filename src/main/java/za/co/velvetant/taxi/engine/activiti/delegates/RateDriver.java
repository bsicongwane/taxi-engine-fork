package za.co.velvetant.taxi.engine.activiti.delegates;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.LoggerFactory;
import za.co.velvetant.taxi.engine.api.Rating;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

import javax.inject.Inject;
import javax.inject.Named;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.DRIVER_RATING;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
public class RateDriver extends ExecutionDelegate {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(RateDriver.class);

    @Inject
    private FareRepository fareRepository;

    @Override
    public void process(final DelegateExecution execution) {
        Rating rating = getVariable(execution, DRIVER_RATING, Rating.class);
        log.debug("Rating driver [{}]: {}", execution.getVariable("driver"), rating);

        za.co.velvetant.taxi.engine.models.Rating fareRating = convert(rating);
        Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        fareRating.setFare(fare);
        fare.setRating(fareRating);

        fareRepository.save(fare);
    }

}
