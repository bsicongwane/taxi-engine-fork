package za.co.velvetant.taxi.engine.activiti.delegates;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.activiti.util.FareReferenceGenerator;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.external.google.DirectionsService;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.Location;
import za.co.velvetant.taxi.engine.models.PaymentMethod;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;

import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.MULTI_LINE_STYLE;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.FARE_REQUEST;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_OVERRIDDEN;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_CONFIRMATION_TIMED_OUT;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_METHOD;

@Named
public class CreateFare extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(CreateFare.class);

    @Inject
    private DirectionsService directionsService;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private FareRepository fareRepository;

    @Inject
    private FareReferenceGenerator referenceGenerator;

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        final FareRequest fareRequest = getVariable(execution, FARE_REQUEST, FareRequest.class);
        final Customer customer = customerRepository.findByUserId(fareRequest.getCustomer());

        if (fareRequest.getPickup() == null || fareRequest.getPickup().getLatitude() == null || fareRequest.getPickup().getLongitude() == null) {
            throw new PreconditionFailedException("Fare pickup location must be provided and contain latitude and longitude values");
        }

        final Location from = new Location(fareRequest.getPickup().getLatitude().doubleValue(), fareRequest.getPickup().getLongitude().doubleValue());
        from.setAddress(fareRequest.getPickup().getAddress());

        final Fare fare = new Fare(customer, new Date(), new Date(fareRequest.getPickupTime()), from, execution.getProcessBusinessKey());
        fare.setPaymentMethod(PaymentMethod.valueOf(fareRequest.getPaymentMethod()));

        if (fare.getPickUpTime() == null) {
            fare.setPickUpTime(new Date(System.currentTimeMillis()));
        }

        if (fareRequest.getDropOff() != null) {
            za.co.velvetant.taxi.api.common.Location dropOff = fareRequest.getDropOff();
            fare.setDropOff(new Location(dropOff.getLatitude().doubleValue(), dropOff.getLongitude().doubleValue(), dropOff.getAddress()));
        }

        final Fare saved = fareRepository.save(fare);
        saved.setReference(referenceGenerator.generate(saved));
        fareRepository.save(fare);

        execution.setVariable(PAYMENT_METHOD, fare.getPaymentMethod());
        execution.setVariable(PAYMENT_CONFIRMATION_TIMED_OUT, false);
        execution.setVariable(OPS_OVERRIDDEN, false);
        log.debug("Created new fare: {}", reflectionToString(fare, MULTI_LINE_STYLE));
    }

}
