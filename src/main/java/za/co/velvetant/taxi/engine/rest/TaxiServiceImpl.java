package za.co.velvetant.taxi.engine.rest;

import static com.google.common.base.Optional.fromNullable;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.api.Taxi;
import za.co.velvetant.taxi.engine.filters.TaxiFilter;
import za.co.velvetant.taxi.engine.models.DriverBox;
import za.co.velvetant.taxi.engine.models.TaxiStatus;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.util.VoConversion;
import za.co.velvetant.taxi.persistence.taxi.TaxiRepository;

@Named
@Path("/taxis.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class TaxiServiceImpl implements TaxiService {

    @Inject
    private TaxiRepository taxiRepository;

    @Inject
    private Provider<TaxiFilter> filter;

    @Override
    public Response create(final Taxi taxi) {
        if (taxiRepository.findByRegistration(taxi.getRegistration()) == null) {
            za.co.velvetant.taxi.engine.models.Taxi entity = new za.co.velvetant.taxi.engine.models.Taxi(taxi.getTaxiId(), taxi.getRegistration(), taxi.getMake(), taxi.getModel(), taxi.getColor(),
                    taxi.getCapacity());
            entity.setActive(true);

            if (taxi.getDriverBox() != null) {
                entity.setDriverBox(new DriverBox(taxi.getDriverBox().getSerialNumber()));
            }

            entity = taxiRepository.save(entity);
            return Response.status(Response.Status.CREATED).entity(convert(entity)).build();
        } else {
            throw new ConflictException(String.format("Taxi with registration %s already exists", taxi.getRegistration()));
        }
    }

    @Override
    public Response edit(final String registration, final Taxi taxi) {
        za.co.velvetant.taxi.engine.models.Taxi currentTaxi = taxiRepository.findByRegistration(registration);
        currentTaxi = VoConversion.convert(taxi, currentTaxi);
        currentTaxi.setDriverBox(new DriverBox(fromNullable(taxi.getDriverBox()).or(new za.co.velvetant.taxi.engine.api.DriverBox()).getSerialNumber()));
        final za.co.velvetant.taxi.engine.models.Taxi updatedTaxi = taxiRepository.save(currentTaxi);

        return ok().entity(convert(updatedTaxi, new Taxi())).build();
    }

    @Override
    public Response find(final String registration) {
        final za.co.velvetant.taxi.engine.models.Taxi findByRegistration = taxiRepository.findByRegistration(registration);
        NotFoundException.assertNotNull(findByRegistration, String.format("Taxi with registration %s was not found", registration));

        return ok().entity(convert(findByRegistration)).build();
    }

    @Override
    public Response findAll() {
        final List<Taxi> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Taxi taxi : taxiRepository.findAll()) {
            vos.add(convert(taxi));
        }

        return ok().entity(vos).build();
    }

    @Override
    public Response findAll(final String registration, final String make, final String model, final TaxiStatus status, final Boolean active, final Integer page, final Integer size, final String sort,
                            final Sort.Direction order) {
        final TaxiFilter filteredTaxis = filter.get().with(registration, make, model, status, active).sort(page, size, sort, order).filter();
        final List<Taxi> vos = filteredTaxis.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredTaxis.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response remove(final String registration) {
        final za.co.velvetant.taxi.engine.models.Taxi taxi = taxiRepository.findByRegistration(registration);
        NotFoundException.assertNotNull(taxi, String.format("Taxi with registration %s was not found", registration));
        taxi.setStatus(TaxiStatus.OFFLINE);
        taxi.setActive(false);
        taxiRepository.save(taxi);

        return ok().build();
    }

}
