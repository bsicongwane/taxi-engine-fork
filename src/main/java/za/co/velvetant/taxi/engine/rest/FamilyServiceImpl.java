package za.co.velvetant.taxi.engine.rest;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.mygate.MyGateResponse;
import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.api.CreateCustomer;
import za.co.velvetant.taxi.engine.api.CreateFamily;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.Family;
import za.co.velvetant.taxi.engine.filters.FamilyFilter;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Group;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.models.Role;
import za.co.velvetant.taxi.engine.models.User;
import za.co.velvetant.taxi.engine.persistence.CreditCardRepository;
import za.co.velvetant.taxi.engine.persistence.FamilyRepository;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.MissingValueException;
import za.co.velvetant.taxi.engine.rest.exception.NotAllowedException;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.google.common.collect.Lists.transform;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;
import static org.apache.commons.collections.CollectionUtils.containsAny;
import static za.co.velvetant.taxi.engine.models.Group.ADMINISTRATOR;
import static za.co.velvetant.taxi.engine.models.Group.AGENT;
import static za.co.velvetant.taxi.engine.models.Group.SUPERVISOR;
import static za.co.velvetant.taxi.engine.models.Group.SYSTEM;
import static za.co.velvetant.taxi.engine.models.MyGateTransaction.Type.STORE_CARD;
import static za.co.velvetant.taxi.engine.models.MyGateTransaction.Type.UPDATE_CARD;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
@Path("/families.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class FamilyServiceImpl implements FamilyService {

    private static final Logger log = LoggerFactory.getLogger(FamilyServiceImpl.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private FamilyRepository familyRepository;

    @Inject
    private CreditCardRepository creditCardRepository;

    @Inject
    private MyGateTransactionRepository myGateTransactionRepository;

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private PaymentService paymentService;

    @Inject
    private Provider<FamilyFilter> filter;

    @Context
    private HttpHeaders headers;

    @Override
    public Response create(final CreateFamily createFamily) {
        za.co.velvetant.taxi.engine.models.Family family = new za.co.velvetant.taxi.engine.models.Family();
        family.setFamilyName(createFamily.getFamilyName());
        family.setUuid(UUID.randomUUID().toString());
        family = familyRepository.save(family);
        final String userId = CustomerRoutines.getUserId(headers);

        family = assign(family.getUuid(), userId, false);
        final Customer customer2 = customerRepository.findByUserId(userId);
        customer2.setFamily(family);
        customerRepository.save(customer2);
        final Family vo = convert(family, new Family());
        if (family.getAdministrators() != null) {
            final List<String> administrators = new ArrayList<>();
            for (final za.co.velvetant.taxi.engine.models.Customer customer : family.getAdministrators()) {
                administrators.add(customer.getEmail());
            }
            vo.setAdministratorEmails(administrators);
        }

        return status(CREATED).entity(vo).build();
    }

    @Override
    public Response createAdministratively(final String familyHead, final CreateFamily createFamily) {
        za.co.velvetant.taxi.engine.models.Family family = new za.co.velvetant.taxi.engine.models.Family();
        family.setFamilyName(createFamily.getFamilyName());
        family.setUuid(UUID.randomUUID().toString());
        family = familyRepository.save(family);

        family = assign(family.getUuid(), familyHead, false);
        final Customer familyHeadCustomer = customerRepository.findByUserId(familyHead);
        familyHeadCustomer.setFamily(family);
        customerRepository.save(familyHeadCustomer);
        final Family vo = convert(family, new Family());
        if (family.getAdministrators() != null) {
            final List<String> administrators = new ArrayList<>();
            for (final Customer customer : family.getAdministrators()) {
                administrators.add(customer.getEmail());
            }
            vo.setAdministratorEmails(administrators);
        }

        return status(CREATED).entity(vo).build();
    }

    @Override
    public Response edit(final Family family, final String uuid) {
        checkAllowed(uuid);
        MissingValueException.assertNotEmpty(family.getFamilyName(), "You must provide a family name");
        final za.co.velvetant.taxi.engine.models.Family familyEntity = familyRepository.findByUuid(uuid);
        familyEntity.setFamilyName(family.getFamilyName());
        familyRepository.save(familyEntity);
        return Response.ok().build();
    }

    @Override
    public za.co.velvetant.taxi.engine.api.CreateCustomer addFamilyMember(final String uuid, final za.co.velvetant.taxi.engine.api.CreateCustomer customer) {
        checkAllowed(uuid);
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.findByUuid(uuid);
        // TODO move to generic location
        final Customer exists = customerRepository.findByUserIdOrCellphone(customer.getEmail(), customer.getCellphone());
        if (exists == null) {
            Customer entity = new Customer();
            entity.setCellphone(customer.getCellphone());
            entity.setEmail(customer.getEmail());
            entity.setUserId(customer.getEmail());
            entity.setFirstName(customer.getFirstName());
            entity.setLastName(customer.getLastName());
            entity.setSendNewsLetter(customer.getSendNewsLetter());

            entity.setActive(true);
            entity.setPassword(BCrypt.hashpw(entity.getPassword(), BCrypt.gensalt()));
            entity.setFamily(family);
            entity = customerRepository.save(entity);
            final CreateCustomer vo = convert(entity, new za.co.velvetant.taxi.engine.api.CreateCustomer());
            vo.setPassword(null);
            return vo;
        } else {
            final boolean cell = exists.getCellphone() != null && exists.getCellphone().equals(customer.getCellphone());
            String fieldName = "Cellphone";
            String field = customer.getCellphone();
            if (!cell) {
                fieldName = "Email";
                field = customer.getEmail();
            }
            throw new ConflictException(String.format("%s %s is already registered", fieldName, field));
        }

    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Customer> findMembers(final String uuid) {

        final List<za.co.velvetant.taxi.engine.api.Customer> vos = new ArrayList<>();
        final List<Customer> family = customerRepository.findByFamilyUuid(uuid);
        NotFoundException.assertNotNull(family, String.format("Family with uuid %s was not found", uuid));
        for (final Customer customer : family) {
            vos.add(CustomerRoutines.buildVo(customerRepository.fetchWithCreditCard(customer.getUserId()), creditCardRepository, transactionRepository.findCustomersBalance(customer)));
        }
        return vos;
    }

    @Override
    public Family find(final String uuid) {
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.fetchWithAdministrators(uuid);
        NotFoundException.assertNotNull(family, String.format("Family with uuid %s was not found", uuid));
        final Family vo = convert(family, new Family());
        if (family.getAdministrators() != null) {
            final List<String> administrators = new ArrayList<>();
            for (final za.co.velvetant.taxi.engine.models.Customer customer : family.getAdministrators()) {
                administrators.add(customer.getEmail());
            }
            vo.setAdministratorEmails(administrators);
        }
        return vo;
    }

    @Override
    public List<Family> findAll() {
        final List<za.co.velvetant.taxi.engine.api.Family> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Family f : familyRepository.fetchAllWithAdministrators()) {
            final Family vo = new Family(f.getFamilyName(), f.getUuid());
            if (f.getAdministrators() != null) {
                final List<String> administrators = new ArrayList<>();
                for (final za.co.velvetant.taxi.engine.models.Customer customer : f.getAdministrators()) {
                    administrators.add(customer.getEmail());
                }
                vo.setAdministratorEmails(administrators);
            }
            vos.add(vo);
        }
        return vos;
    }

    @Override
    public Response findAll(final String uuid, final String familyName, final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        final FamilyFilter filteredFamilies = filter.get().with(uuid, familyName).sort(page, size, sort, order).filter();
        final List<Family> vos = filteredFamilies.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredFamilies.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response addFamilyMember(final String uuid, final String userid) {
        checkAllowed(uuid);
        final Customer user = customerRepository.findByUserId(userid);
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.findByUuid(uuid);
        if (user != null && family != null) {
            user.setFamily(family);
            customerRepository.save(user);
            return Response.ok().build();
        }

        if (user == null) {
            throw new ConflictException(String.format("Customer with userid %s has not been created yet", userid));
        }
        throw new ConflictException(String.format("Family has not been created yet"));
    }

    @Override
    public Response assignAdministrator(final String uuid, final String userid) {
        checkAllowed(uuid);
        final za.co.velvetant.taxi.engine.models.Family assign = assign(uuid, userid, true);
        return Response.ok().entity(convert(assign, new za.co.velvetant.taxi.engine.api.Family())).build();
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Customer> findAdministrators(final String uuid) {
        final List<za.co.velvetant.taxi.engine.api.Customer> administrators = new ArrayList<>();
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.fetchWithAdministrators(uuid);
        NotFoundException.assertNotNull(family, String.format("Family with uuid %s was not found", uuid));

        if (family.getAdministrators() != null) {
            for (final za.co.velvetant.taxi.engine.models.Customer customer : family.getAdministrators()) {
                administrators.add(CustomerRoutines.buildVo(customerRepository.fetchWithCreditCard(customer.getUserId()), creditCardRepository, transactionRepository.findCustomersBalance(customer)));
            }
        }
        return administrators;
    }

    @Override
    public Response removeAdministrator(final String uuid, final String userid) {
        checkAllowed(uuid);
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found or already administers another company");
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.fetchWithAdministrators(uuid);

        if (family.getAdministrators().size() == 1) {
            throw new PreconditionFailedException("There must be at least two administrators for a remove to be allowed");
        }
        NotFoundException.assertNotNull(family, String.format("Family with uuid %s was not found", uuid));
        customer.setHeaded(null);
        customerRepository.save(customer);
        return Response.ok().build();
    }

    @Override
    public Response removeMember(final String uuid, final String userid) {
        checkAllowed(uuid);
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found or already administers another family");
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.fetchWithAdministrators(uuid);
        NotFoundException.assertNotNull(family, String.format("Family with uuid %s was not found", uuid));
        customer.setFamily(null);
        customerRepository.save(customer);
        return Response.ok().build();
    }

    @Override
    public Response uploadCreditCard(final String uuid, final CreditCard card) {
        Response.ResponseBuilder response = ok();
        za.co.velvetant.taxi.engine.models.CreditCard creditCard = new za.co.velvetant.taxi.engine.models.CreditCard();
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.fetchWithCreditCard(uuid);
        if (family.getCreditCards() != null && !family.getCreditCards().isEmpty()) {
            creditCard = family.getCreditCards().get(0);
        }

        creditCard.setFamily(family);
        MyGateResponse myGateREsponse = null;
        MyGateTransaction transaction;
        try {
            if (creditCard.getId() == null) {
                final String tokenId = UUID.randomUUID().toString().replaceAll("-", "");
                creditCard.setGateWayId(tokenId);
                myGateREsponse = paymentService.storeCard(tokenId, card);
                transaction = new MyGateTransaction(myGateREsponse.getDateTime(), null, STORE_CARD, myGateREsponse.getTransactionIndex(), myGateREsponse.getStatus());
            } else {
                myGateREsponse = paymentService.updateCard(creditCard.getGateWayId(), card);
                transaction = new MyGateTransaction(myGateREsponse.getDateTime(), null, UPDATE_CARD, myGateREsponse.getTransactionIndex(), myGateREsponse.getStatus());
            }

            transaction.setFamily(family);
            transaction.setErrorMessage(myGateREsponse.getErrorCode());
            myGateTransactionRepository.save(transaction);
        } catch (final Exception e) {
            log.error("Error calling MyGate", e);
        }

        if (myGateREsponse != null && myGateREsponse.isSuccess()) {
            creditCard.setCvvReference(card.getCvv());
            creditCard = creditCardRepository.save(creditCard);

            family.addCreditCard(creditCard);
            familyRepository.save(family);

            response = response.entity(card);
        } else {
            response = status(PRECONDITION_FAILED).entity("There was an error saving your card details with payment gateway.");
        }

        return response.build();
    }

    @Override
    public Response getCreditCard(final String uuid) {
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.fetchWithCreditCard(uuid);
        NotFoundException.assertNotNull(family, String.format("Family with registration %s does not exist", uuid));

        final List<za.co.velvetant.taxi.engine.models.CreditCard> creditCards = family.getCreditCards();
        if (creditCards != null && !creditCards.isEmpty()) {

            final za.co.velvetant.taxi.engine.models.CreditCard creditCard = family.getCreditCards().get(0);
            try {
                final CreditCard cc = paymentService.getMaskedCardDetails(creditCard.getGateWayId());
                cc.setCvv(creditCard.getCvvReference());
                return Response.ok().entity(cc).build();
            } catch (RemoteException | ServiceException | ParseException e) {
                log.error(e.getMessage(), e);
            }

        }
        throw new NotFoundException(String.format("Family %s has no credit card loaded", uuid));
    }

    private za.co.velvetant.taxi.engine.models.Family assign(final String uuid, final String userid, final boolean check) {
        if (check) {
            checkAllowed(uuid);
        }

        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findAllowedFamilyHead(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found or already heads another family");
        final za.co.velvetant.taxi.engine.models.Family family = familyRepository.findByUuid(uuid);
        NotFoundException.assertNotNull(family, String.format("Family with uuid %s was not found", uuid));
        customer.setHeaded(family);
        customerRepository.save(customer);

        return familyRepository.fetchWithAdministrators(uuid);
    }

    private void checkAllowed(final String uuid) {
        final String userId = CustomerRoutines.getUserId(headers);
        User user = userRepository.findByUserIdAndActive(userId);
        if (user == null || !containsAny(getGroups(user.getGroups()), ImmutableList.of(SYSTEM, ADMINISTRATOR, SUPERVISOR, AGENT))) {
            final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userId);
            NotFoundException.assertNotNull(customer, String.format("Customer with userId %s does not exist", userId));
            if (customer.getHeaded() == null || (!customer.getHeaded().getUuid().equals(uuid))) {
                throw new NotAllowedException("You are not allowed to modify this family.");
            }
        }
    }

    private List<Group> getGroups(final List<Role> roles) {
        return transform(roles, new Function<Role, Group>() {

            @Override
            public Group apply(final za.co.velvetant.taxi.engine.models.Role role) {
                return role.getRole();
            }
        });
    }
}
