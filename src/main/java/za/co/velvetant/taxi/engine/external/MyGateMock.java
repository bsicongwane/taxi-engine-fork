package za.co.velvetant.taxi.engine.external;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.startsWith;

import java.rmi.RemoteException;
import java.text.ParseException;

import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.mygate.BillingDetails;
import za.co.velvetant.mygate.MyGateResponse;
import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.api.CreditCard;

import com.google.common.collect.ImmutableList;

public class MyGateMock implements PaymentService {

    private static final Logger log = LoggerFactory.getLogger(MyGateMock.class);

    @Override
    public MyGateResponse storeCard(final String clientToken, final CreditCard creditCard) throws RemoteException, ServiceException, ParseException {
        return null;
    }

    @Override
    public MyGateResponse updateCard(final String clientToken, final CreditCard creditCard) throws RemoteException, ServiceException, ParseException {
        return null;
    }

    @Override
    public MyGateResponse processPayment(final String clientToken, final BillingDetails billingDetails) throws RemoteException, ServiceException, ParseException {
        log.debug("[MOCK] Processing MyGate payment for: {}", clientToken);

        String result = "0";
        if (startsWith(clientToken, "VA_TEST_FAILURE")) {
            log.debug("[MOCK] Declining payment");
            result = "-1";
        }

        return new MyGateResponse(ImmutableList.of(format("Result||%s", result)).toArray(new Object[1]));
    }

    @Override
    public CreditCard getMaskedCardDetails(final String clientToken) throws RemoteException, ServiceException, ParseException {
        return null;
    }

    @Override
    public MyGateResponse removeCard(final String clientToken) throws RemoteException, ServiceException, ParseException {
        return null;
    }

    @Override
    public MyGateResponse verifyExists(final String clientToken) throws RemoteException, ServiceException, ParseException {
        return null;
    }
}
