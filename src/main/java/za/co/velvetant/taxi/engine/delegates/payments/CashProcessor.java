package za.co.velvetant.taxi.engine.delegates.payments;

import static za.co.velvetant.taxi.engine.model.Payment.PaymentBuilder.cashPayment;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.model.Payment;
import za.co.velvetant.taxi.engine.models.Fare;

@Named("CASH")
public class CashProcessor extends TransactionalTripPaymentProcessor {

    private static final Logger log = LoggerFactory.getLogger(CashProcessor.class);

    @Override
    public Payment createPayment(final Fare trip, final String cvv) {
        return cashPayment(trip).forAmount(trip.getAmount());
    }

    @Override
    public void process(final Payment payment, final Boolean useAccount) {
        log.debug("Processing CASH payment: {}", payment);

        Fare fare = fareRepository.findByUUID(payment.getTrip().getUuid());
        fare.setTip(null);

        fareRepository.save(fare);
    }
}
