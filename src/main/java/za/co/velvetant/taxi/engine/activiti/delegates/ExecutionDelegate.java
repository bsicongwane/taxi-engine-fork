package za.co.velvetant.taxi.engine.activiti.delegates;

import org.activiti.engine.delegate.DelegateTask;

public abstract class ExecutionDelegate extends LoggingDelegate {

    @Override
    public void process(final DelegateTask task) {
        throw new UnsupportedOperationException("This is not a listener, cannot process this delegate");
    }
}
