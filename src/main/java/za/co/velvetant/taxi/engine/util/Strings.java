package za.co.velvetant.taxi.engine.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public final class Strings {

    private Strings() {

    }

    public static String findKeeper(final String current, final String newOne) {
        if (newOne != null && !newOne.trim().equals("")) {
            return newOne;
        }
        return current;
    }

    public static String noNullString(final Object object) {
        if (object == null) {
            return "";
        }
        return object.toString();
    }

    public static String decode(final String encoded) {
        if (encoded == null) {
            return encoded;
        }
        try {
            return URLDecoder.decode(encoded, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
