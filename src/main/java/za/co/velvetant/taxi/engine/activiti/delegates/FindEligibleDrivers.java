package za.co.velvetant.taxi.engine.activiti.delegates;

import static java.lang.Integer.parseInt;
import static org.joda.time.DateTime.now;
import static org.joda.time.Seconds.secondsBetween;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.ELIGIBLE_DRIVERS;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.FARE_REQUEST;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.FIND_DRIVER_RETRY_THRESHOLD;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.TAXIS_IN_SERVICE_RADIUS;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.DRIVER_FIRST_NOTIFIED_COUNT;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.ELIGIBLE_DRIVER_ACCEPTANCE_DURATION;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.OUT_OF_SERVICE_AREA_DISTANCE;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.VariableScope;
import org.activiti.engine.impl.calendar.DurationHelper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.api.common.TaxiDistance;
import za.co.velvetant.taxi.engine.activiti.delegates.processing.Affiliates;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.external.Dispatch;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;
import za.co.velvetant.taxi.persistence.driver.DriverShiftRepository;
import za.co.velvetant.taxi.persistence.taxi.TaxiLocationRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

import com.google.common.base.Throwables;

@Named
public class FindEligibleDrivers extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(FindEligibleDrivers.class);

    @Inject
    private FareRepository fareRepository;

    @Inject
    private DriverShiftRepository driverShiftRepository;

    @Inject
    private Dispatch dispatch;

    @Inject
    private TaxiLocationRepository taxiLocationRepository;

    @Inject
    private Affiliates affiliates;

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    @Override
    public void process(final DelegateExecution execution) {
        final FareRequest fareRequest = getVariable(execution, FARE_REQUEST, FareRequest.class);

        final List<String> drivers = new ArrayList<>();
        Boolean serviceableFare = false;
        List<String> affiliateIds = affiliates.filterAffiliates(fareRequest);
        if (!affiliateIds.isEmpty()) {
            final Integer threshold = determineDispatchThreshold(execution);
            final Integer distance = parseInt(systemPropertyHelper.getProperty(OUT_OF_SERVICE_AREA_DISTANCE));
            final Integer window = determineDispatchWindow(systemPropertyHelper.getProperty(ELIGIBLE_DRIVER_ACCEPTANCE_DURATION), threshold);
            final Integer initialDrivers = parseInt(systemPropertyHelper.getProperty(DRIVER_FIRST_NOTIFIED_COUNT));
            final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());

            final Collection<TaxiDistance> taxis = dispatch.fare(convert(fare, affiliateIds), (threshold * initialDrivers), distance, window);

            for (final TaxiDistance taxi : taxis) {
                final Driver driver = driverShiftRepository.findDriverByRegistration(taxi.getTaxi().getRegistration());
                if (driver != null) {
                    drivers.add(driver.getUserId());
                    log.debug("\t\t !! Adding eligible driver: {}", driver.getUserId());
                } else {
                    log.error("Taxi [{}] has no Driver!", taxi.getTaxi().getRegistration());
                }
            }

            serviceableFare = checkIfDriverInServiceArea(execution, drivers, fareRequest.getPickup(), distance);
        }

        execution.setVariable(ELIGIBLE_DRIVERS, drivers);
        execution.setVariable(TAXIS_IN_SERVICE_RADIUS, serviceableFare);
    }

    private Integer determineDispatchThreshold(final VariableScope execution) {
        Integer threshold = getVariable(execution, FIND_DRIVER_RETRY_THRESHOLD, Integer.class);
        if (threshold == null) {
            threshold = 0;
        }

        execution.setVariable(FIND_DRIVER_RETRY_THRESHOLD, ++threshold);
        log.debug("Driver retry threshold currently at: {}", threshold);

        return threshold;
    }

    private Integer determineDispatchWindow(final String acceptanceDuration, final Integer threshold) {
        Integer window = 0;
        try {
            Integer duration = secondsBetween(now(), new DateTime(new DurationHelper(acceptanceDuration).getDateAfter())).getSeconds();
            window = (3 - threshold) * duration;
        } catch (Exception e) {
            Throwables.propagate(e);
        }

        log.debug("Determined that the fare acceptance window is: {}", window);
        return window;
    }

    private Boolean checkIfDriverInServiceArea(final VariableScope execution, final List<String> drivers, final Location pickup, final int distance) {
        Boolean serviceableFare = getVariable(execution, TAXIS_IN_SERVICE_RADIUS, Boolean.class);
        if (serviceableFare == null) {
            // first assume serviceable.
            serviceableFare = true;
        }

        if (drivers.isEmpty()) {
            final Long inRadius = taxiLocationRepository.countBusyInRadius(new BigDecimal(pickup.getLatitude().doubleValue()), new BigDecimal(pickup.getLongitude().doubleValue()), distance);
            serviceableFare = inRadius > 0;
            log.debug("There are [{}] taxis in this service area [{}]", inRadius, distance);
        }

        return serviceableFare;
    }

}
