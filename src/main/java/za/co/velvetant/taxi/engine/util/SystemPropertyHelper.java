package za.co.velvetant.taxi.engine.util;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.IllegalFormatConversionException;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.models.SystemProperty;
import za.co.velvetant.taxi.persistence.repositories.SystemPropertyRepository;

@Named
public class SystemPropertyHelper {

    private static final Logger log = LoggerFactory.getLogger(SystemPropertyHelper.class);

    @Inject
    private SystemPropertyRepository systemPropertyRepository;

    public String getProperty(final SystemProperty.Name name, final String defaultValue) {
        String propertyValue = defaultValue;

        SystemProperty property = systemPropertyRepository.findActiveProperty(name.getDatabaseName());
        if (property != null) {
            propertyValue = property.getValue();
        }

        return propertyValue;
    }

    public String getProperty(final SystemProperty.Name name) {
        return getProperty(name, EMPTY);
    }

    public String getAndFormatProperty(final SystemProperty.Name name, final Object... value) {
        String property = getProperty(name);
        if (isNotBlank(property)) {
            try {
                property = format(property, value);
            } catch (IllegalFormatConversionException e) {
                log.error("Error formatting property", e);
            }
        }

        return property;
    }
}
