package za.co.velvetant.taxi.engine.rest;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static za.co.velvetant.taxi.engine.models.DriverStatus.OFFLINE;
import static za.co.velvetant.taxi.engine.models.Group.DRIVER;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.api.common.AffiliateLocation;
import za.co.velvetant.taxi.engine.api.Driver;
import za.co.velvetant.taxi.engine.api.GetAffiliatesResponse;
import za.co.velvetant.taxi.engine.filters.AffiliateFilter;
import za.co.velvetant.taxi.engine.models.Role;
import za.co.velvetant.taxi.engine.models.TaxiCompany;
import za.co.velvetant.taxi.engine.persistence.AffiliateLocationRepository;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

@Named
@Path("/affiliates.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class AffiliateServiceImpl implements AffiliatesService {

    @Inject
    private TaxiCompanyRepository taxiCompanyRepository;

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private AffiliateLocationRepository affiliateLocationRepository;

    @Inject
    private Provider<AffiliateFilter> filter;

    @Override
    public Response create(final za.co.velvetant.taxi.engine.api.TaxiCompany taxiCompany) {
        TaxiCompany entity = convert(taxiCompany);
        entity.setAdministratorEmail(taxiCompany.getAdministratorEmail());
        entity = taxiCompanyRepository.save(entity);

        for (za.co.velvetant.taxi.engine.models.AffiliateLocation location : entity.getLocations()) {
            location.setTaxiCompany(entity);
            affiliateLocationRepository.save(location);
        }

        return Response.status(CREATED).entity(convert(entity)).build();
    }

    @Override
    public Response edit(final String registration, final za.co.velvetant.taxi.engine.api.TaxiCompany taxiCompany) {
        TaxiCompany existingAffiliate = taxiCompanyRepository.findByRegistrationAndActive(registration);
        if (existingAffiliate != null) {
            existingAffiliate = convert(taxiCompany, existingAffiliate);

            taxiCompanyRepository.saveAndFlush(existingAffiliate);

        } else {
            throw new PreconditionFailedException(format("Affiliate %s does no exist", taxiCompany.getCompanyName()));
        }

        return ok().build();
    }

    @Override
    public Response remove(final String registration) {
        final TaxiCompany existingAffiliate = taxiCompanyRepository.findByRegistrationAndActive(registration);
        if (existingAffiliate != null) {
            existingAffiliate.setActive(false);
            taxiCompanyRepository.save(existingAffiliate);
        } else {
            throw new PreconditionFailedException(format("Affiliate %s does no exist", registration));
        }

        return ok().build();
    }

    @Override
    public Response activate(final String registration) {
        final TaxiCompany existingAffiliate = taxiCompanyRepository.findByRegistration(registration);
        if (existingAffiliate != null) {
            if (!existingAffiliate.getActive()) {
                existingAffiliate.setActive(true);
                taxiCompanyRepository.save(existingAffiliate);
            } else {
                throw new PreconditionFailedException(format("Affiliate %s is not deactivated", registration));
            }
        } else {
            throw new PreconditionFailedException(format("Affiliate %s does no exist", registration));
        }

        return ok().build();
    }

    @Override
    public za.co.velvetant.taxi.engine.api.TaxiCompany find(final String registration) {
        za.co.velvetant.taxi.engine.api.TaxiCompany convert = convert(taxiCompanyRepository.findByRegistration(registration));
        List<za.co.velvetant.taxi.engine.models.AffiliateLocation> findByTaxiCompanyRegistration = affiliateLocationRepository.findByTaxiCompanyRegistration(registration);
        List<AffiliateLocation> locations = new ArrayList<>();
        for (za.co.velvetant.taxi.engine.models.AffiliateLocation location : findByTaxiCompanyRegistration) {
            locations.add(new AffiliateLocation(location.getLocation().getLatitude(), location.getLocation().getLongitude(), location.getLocation().getAddress(), location.getRadius()));
            convert.setLocations(locations);
        }

        return convert;
    }

    @Override
    public Response findAll() {
        final AffiliateFilter affiliateFilter = filter.get().with(null, null, true).sort(1, 100, "companyName", Sort.Direction.ASC).filter();
        final List<za.co.velvetant.taxi.engine.api.TaxiCompany> vos = affiliateFilter.andConvert().vos();

        final List<GetAffiliatesResponse> affiliatesResponses = new ArrayList<>(vos.size());
        for (final za.co.velvetant.taxi.engine.api.TaxiCompany taxiCompany : vos) {
            final GetAffiliatesResponse response = new GetAffiliatesResponse(taxiCompany.getCompanyName(), taxiCompany.getCabCount(), System.currentTimeMillis(), taxiCompany.getRegistration());
            affiliatesResponses.add(response);
        }

        Response.ResponseBuilder response = ok().entity(affiliatesResponses).header("X-Total-Entities", affiliateFilter.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response findAll(final String companyName, final String registration, final String contactEmail, final Boolean active, final Integer page, final Integer size, final String sort,
            final Sort.Direction order) {
        final AffiliateFilter affiliateFilter = filter.get().with(companyName, registration, active).sort(page, size, sort, order).filter();
        final List<za.co.velvetant.taxi.engine.api.TaxiCompany> vos = affiliateFilter.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", affiliateFilter.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public String count() {
        return String.valueOf(taxiCompanyRepository.count());
    }

    @Override
    public Response addDriver(final String registration, final Driver driver) {
        final TaxiCompany taxiCompany = taxiCompanyRepository.findByRegistrationAndActiveWithDrivers(registration);

        za.co.velvetant.taxi.engine.models.driver.Driver entity = null;
        if (isNotBlank(driver.getUserId())) {
            entity = driverRepository.findByUserId(driver.getUserId());

        }
        if (entity != null) {
            entity.setTaxiCompany(taxiCompany);
            entity = driverRepository.save(entity);

            taxiCompany.getDrivers().add(entity);
            taxiCompanyRepository.save(taxiCompany);
        } else {
            entity = new za.co.velvetant.taxi.engine.models.driver.Driver();
            entity.setCellphone(driver.getCellphone());
            entity.setUserId(driver.getUserId());
            entity.setEmail(driver.getEmail());
            entity.setFirstName(driver.getFirstName());
            entity.setIdNumber(driver.getIdNumber());
            entity.setLastName(driver.getLastName());
            entity.setStatus(OFFLINE);
            entity.setTaxiCompany(taxiCompany);
            entity.setPassword(BCrypt.hashpw(driver.getPassword(), BCrypt.gensalt()));
            entity = driverRepository.save(entity);
            final Role role = new Role(DRIVER, entity);
            entity.addGroup(role);
            entity = driverRepository.save(entity);
        }

        final Driver convertedDriver = convert(entity, new Driver());
        convertedDriver.setPassword(null);

        return ok().entity(convertedDriver).build();
    }

    @Override
    public Response removeDriver(final String registration, final String driverId) {
        final TaxiCompany taxiCompany = taxiCompanyRepository.findByRegistrationAndActiveWithDrivers(registration);
        final za.co.velvetant.taxi.engine.models.driver.Driver driver = driverRepository.findByUserId(driverId);
        if (driver != null) {
            if (taxiCompany.getDrivers().remove(driver)) {
                taxiCompanyRepository.save(taxiCompany);
            }

            driver.setTaxiCompany(null);
            driverRepository.save(driver);
        } else {
            throw new PreconditionFailedException(format("Driver %s does not exist", driverId));
        }

        return ok().build();
    }

    @Override
    public Response addLocation(final String registration, final AffiliateLocation location) {
        if (isBlank(location.getLocation().getAddress())) {
            throw new PreconditionFailedException("Affiliate location address cannot be empty");
        }

        final TaxiCompany taxiCompany = taxiCompanyRepository.findByRegistrationAndActiveWithDrivers(registration);
        final za.co.velvetant.taxi.engine.models.AffiliateLocation entity = new za.co.velvetant.taxi.engine.models.AffiliateLocation(location.getLocation().getLatitude().doubleValue(), location
                .getLocation().getLongitude().doubleValue(), location.getLocation().getAddress(), location.getRadius());
        entity.setTaxiCompany(taxiCompany);
        affiliateLocationRepository.save(entity);

        return Response.status(CREATED).build();
    }

    @Override
    public Response changeLocation(final String registration, final String address, final AffiliateLocation location) {
        final za.co.velvetant.taxi.engine.models.AffiliateLocation changing = affiliateLocationRepository.findByTaxiCompanyRegistrationAndLocationAddress(registration, address);
        if (changing != null) {
            changing.getLocation().setAddress(location.getLocation().getAddress());
            changing.getLocation().setLatitude(location.getLocation().getLatitude().doubleValue());
            changing.getLocation().setLongitude(location.getLocation().getLongitude().doubleValue());
            changing.setRadius(location.getRadius());
            affiliateLocationRepository.save(changing);
            return ok().build();
        }
        throw new PreconditionFailedException(format("Location %s does not exist", address));

    }

    @Override
    public Response removeLocation(final String registration, final String address) {
        final za.co.velvetant.taxi.engine.models.AffiliateLocation removing = affiliateLocationRepository.findByTaxiCompanyRegistrationAndLocationAddress(registration, address);
        if (removing != null) {
            affiliateLocationRepository.delete(removing);
            return ok().build();
        }
        throw new PreconditionFailedException(format("Location %s does not exist", address));

    }

    @Override
    public Response closeTo(final String latitude, final String longitude) {
        final Response.ResponseBuilder response;

        final List<TaxiCompany> closestAffiliates = taxiCompanyRepository.findAllCloseTo(new BigDecimal(latitude), new BigDecimal(longitude));
        if (!closestAffiliates.isEmpty()) {
            final List<za.co.velvetant.taxi.engine.api.TaxiCompany> affiliates = new ArrayList<>();
            for (TaxiCompany affiliate : closestAffiliates) {
                affiliates.add(convert(affiliate));
            }

            response = ok().entity(affiliates);
        } else {
            response = noContent();
        }

        return response.build();
    }
}
