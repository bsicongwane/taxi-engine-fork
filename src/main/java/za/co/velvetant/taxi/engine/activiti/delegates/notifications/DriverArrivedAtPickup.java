package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.messaging.sms.SmsService;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.driver.DriverShift;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.ops.notifications.DriverArrivedAtPickupNotification;
import za.co.velvetant.taxi.persistence.driver.DriverShiftRepository;

@Named
public class DriverArrivedAtPickup extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(DriverArrivedAtPickup.class);

    @Inject
    private Operations operations;
    @Inject
    private SmsService smsMessageService;

    @Inject
    private FareRepository fareRepository;

    @Inject
    private DriverShiftRepository driverShiftRepository;

    @Override
    public void process(final DelegateExecution execution) {
        operations.notify(new DriverArrivedAtPickupNotification(execution.getProcessBusinessKey(), DriverArrivedAtPickup.class, getVariable(execution, "driver", String.class)));
        try {
            final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
            final DriverShift shift = driverShiftRepository.findActiveDriverShiftWithDriverUserId(getVariable(execution, "driver", String.class));
            smsMessageService.sendDriverArrivedMessage(fare.getCustomer(), fare.getDriver(), shift.getTaxi());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
