package za.co.velvetant.taxi.engine.activiti.delegates;

import static za.co.velvetant.taxi.engine.models.FareState.CANCELLED;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.external.Dispatch;
import za.co.velvetant.taxi.engine.messaging.sms.SmsService;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class CancelFare extends ExecutionDelegate {

    @Inject
    private FareRepository fareRepository;

    @Inject
    private Dispatch dispatch;

    @Inject
    private SmsService smsService;

    private static final Logger log = LoggerFactory.getLogger(CancelFare.class);

    @Override
    public void process(final DelegateExecution execution) {
        Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        switch (fare.getState()) {
            case CANCELLED_BY_ADMIN:
            case CANCELLED_BY_CUSTOMER:
            case CANCELLED_BY_DRIVER:
            case TIMED_OUT:
            case OUT_OF_SERVICE_AREA:
                break;
            default: {
                log.debug("Fare has no external cancelled state, defaulting to CANCELLED");
                fare.setState(CANCELLED);
                fareRepository.save(fare);
            }
        }
        log.debug("Cancelling fare: [{}] with reason: {}", execution.getProcessBusinessKey(), fare.getState());

        dispatch.removeFare(execution.getProcessBusinessKey());
        smsService.sendCancelledMessage(fare);
    }
}
