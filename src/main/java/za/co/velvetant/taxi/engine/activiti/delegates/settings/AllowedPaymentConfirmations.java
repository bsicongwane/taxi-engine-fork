package za.co.velvetant.taxi.engine.activiti.delegates.settings;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.ALLOWED_PAYMENT_CONFIRMATIONS;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

@Named
public class AllowedPaymentConfirmations {

    private static final Logger log = LoggerFactory.getLogger(AllowedPaymentConfirmations.class);

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    public String value() {
        String value = systemPropertyHelper.getProperty(ALLOWED_PAYMENT_CONFIRMATIONS, "6");

        log.debug("Using allowed payment confirmation value: {}", value);
        return value;
    }
}
