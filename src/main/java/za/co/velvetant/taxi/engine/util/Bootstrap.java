package za.co.velvetant.taxi.engine.util;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.DRIVER_BOX_VERSION;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.ALLOWED_PAYMENT_CONFIRMATIONS;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.DRIVER_FIRST_NOTIFIED_COUNT;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.DRIVER_OFFLINE_INTERVAL;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.DRIVER_OFFLINE_POLLING_INTERVAL;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.ELIGIBLE_DRIVER_ACCEPTANCE_DURATION;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MAIL_FROM;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MAIL_FROM_NAME;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MANDRILL_KEY;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MANDRILL_SEND_URL;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_CABS_NOT_AVAILABLE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_NEW_INVOICE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_NEW_USER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_PASSWORD_RESET;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_PROFILE_UPDATE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_SERVICE_NOT_AVAILABLE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MYGATE_APPLICATION_ID;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MYGATE_MERCHANT_ID;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MYGATE_MODE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.OUT_OF_SERVICE_AREA_DISTANCE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.PAYMENT_CONFIRMATION_DURATION;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.RATING_COMPLETION_DURATION;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.REWARD_POINTS_PERECENTAGE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_BOX_KEY;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_BOX_URL;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_ADMIN;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_ADMIN_FOR_DRIVER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_CUSTOMER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_CUSTOMER_FOR_DRIVER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_DRIVER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_COUPON_REDEEMED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_DRIVER_ARRIVED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_DRIVER_PAYMENT_FAILED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_DRIVER_PAYMENT_SUCCESSFUL;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_NEW_USER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_PIN_RESET;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SNAPPMILES_CONVERSION_THRESHOLD;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import za.co.velvetant.taxi.engine.models.Group;
import za.co.velvetant.taxi.engine.models.Role;
import za.co.velvetant.taxi.engine.models.SystemProperty;
import za.co.velvetant.taxi.engine.models.User;
import za.co.velvetant.taxi.engine.persistence.RoleRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.persistence.repositories.SystemPropertyRepository;

public class Bootstrap implements ServletContextListener {

    @Inject
    private SystemPropertyRepository systemPropertyRepository;
    
    @Inject
    private UserRepository userRepository;

    @Inject
    private RoleRepository roleRepository;

    @Override
    public void contextDestroyed(final ServletContextEvent arg0) {
    }

    @Override
    public void contextInitialized(final ServletContextEvent arg0) {
        if (systemPropertyRepository == null) {
            final ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(arg0.getServletContext());
            systemPropertyRepository = (SystemPropertyRepository) ctx.getBean("systemPropertyRepository");
            userRepository = (UserRepository) ctx.getBean("userRepository");
            roleRepository = (RoleRepository) ctx.getBean("roleRepository");
        }

        ensurePropertiesExist();
        ensureUsersExist();
    }

    /**
     * Only used to bootstrap functional/integration tests
     */
    public void testInit() {
        ensurePropertiesExist();
    }

    private void ensurePropertiesExist() {
        // email
        ensurePropertiesExist(MAIL_FROM.getDatabaseName(), "admin@snappCab.com");
        ensurePropertiesExist(MAIL_FROM_NAME.getDatabaseName(), "SnappCab");
        ensurePropertiesExist(MANDRILL_KEY.getDatabaseName(), "CvyoPzoypHMJuEmiGwqu3w");
        ensurePropertiesExist(MANDRILL_SEND_URL.getDatabaseName(), "https://mandrillapp.com/api/1.0/messages/send-template.json");

        // reward points
        ensurePropertiesExist(REWARD_POINTS_PERECENTAGE.getDatabaseName(), "60");

        // BPMN timers
        ensurePropertiesExist(ELIGIBLE_DRIVER_ACCEPTANCE_DURATION.getDatabaseName(), "PT5M");
        ensurePropertiesExist(RATING_COMPLETION_DURATION.getDatabaseName(), "PT5M");
        ensurePropertiesExist(PAYMENT_CONFIRMATION_DURATION.getDatabaseName(), "PT2M");
        ensurePropertiesExist(DRIVER_OFFLINE_INTERVAL.getDatabaseName(), "120");
        ensurePropertiesExist(DRIVER_OFFLINE_POLLING_INTERVAL.getDatabaseName(), "R/PT2M");
        ensurePropertiesExist(DRIVER_FIRST_NOTIFIED_COUNT.getDatabaseName(), "3");
        ensurePropertiesExist(ALLOWED_PAYMENT_CONFIRMATIONS.getDatabaseName(), "6");

        ensurePropertiesExist(OUT_OF_SERVICE_AREA_DISTANCE.getDatabaseName(), "60");
        ensurePropertiesExist(DRIVER_BOX_VERSION.getDatabaseName(), "1.2.0");
        ensurePropertiesExist(SNAPPMILES_CONVERSION_THRESHOLD.getDatabaseName(), "1000");

        // yoursmsbox
        ensurePropertiesExist(SMS_BOX_URL.getDatabaseName(), "http://testapi.yoursmsbox.com");
        ensurePropertiesExist(SMS_BOX_KEY.getDatabaseName(), "20f5298d5c2b8eced505b1b2f06edf1aaffc9491538a968f8cf9365bcdafb92d");

        // mygate
        ensurePropertiesExist(MYGATE_MERCHANT_ID.getDatabaseName(), "fcff3ec5-d6ad-4c6d-9677-2d2062d6eca8");
        ensurePropertiesExist(MYGATE_APPLICATION_ID.getDatabaseName(), "54374b3f-f7da-41a6-b3d0-0e9ac9d4ad38");
        ensurePropertiesExist(MYGATE_MODE.getDatabaseName(), "0");

        // SMS content
        ensurePropertiesExist(SMS_CONTENT_NEW_USER.getDatabaseName(), "Hi %1$s, your account with SnappCab has been successfully created. Your login pin is %3$s. Thank you, SnappCab");
        ensurePropertiesExist(SMS_CONTENT_PIN_RESET.getDatabaseName(), "Hi %1$s, your login pin has been reset to %3$s. Thank you, SnappCab");
        ensurePropertiesExist(SMS_CONTENT_DRIVER_ARRIVED.getDatabaseName(), "Hi %1$s, your driver %3$s has arrived in a %4$s %5$s with registration %6$s. Get in :) SnappCab");
        ensurePropertiesExist(SMS_CONTENT_CANCELLED_BY_ADMIN.getDatabaseName(),
                "Hi %1$s, your fare %3$s has been cancelled by our call centre. Please Snapp again, or contact us on 011 027 6730. Get in :) SnappCab");
        ensurePropertiesExist(SMS_CONTENT_CANCELLED_BY_ADMIN_FOR_DRIVER.getDatabaseName(),
                "Hi %1$s, your fare %3$s has been cancelled by our call centre. Please contact us on 011 027 6730. The SnappCab team.");
        ensurePropertiesExist(SMS_CONTENT_CANCELLED_BY_DRIVER.getDatabaseName(),
                "Hi %s, we are sorry but your fare has been cancelled by your driver. Please Snapp again, or contact us on 011 027 6730. The SnappCab team.");
        ensurePropertiesExist(SMS_CONTENT_CANCELLED_BY_CUSTOMER.getDatabaseName(), "Hi %1$s, you have cancelled fare %3$s. Please Snapp again, or contact us on 011 027 6730. The SnappCab team.");
        ensurePropertiesExist(SMS_CONTENT_CANCELLED_BY_CUSTOMER_FOR_DRIVER.getDatabaseName(),
                "Dear %1$s, the customer has unfortunately cancelled you trip. We hope you get another soon! The SnappCab team.");
        ensurePropertiesExist(SMS_CONTENT_DRIVER_PAYMENT_SUCCESSFUL.getDatabaseName(),
                "Dear %1$s. Credit card payment of R%3$.2f and tip R%4$.2f (Total R%5$.2f) for trip %6$s has been processed successfully. Thanks, the SnappCab team.");
        ensurePropertiesExist(SMS_CONTENT_DRIVER_PAYMENT_FAILED.getDatabaseName(),
                "Dear %1$s. Unfortunately credit card payment for trip %6$s did not process successfully. Please try again or request cash payment. Call us on 011.027.6730 for assistance. Thanks, the SnappCab team.");
        ensurePropertiesExist(SMS_CONTENT_COUPON_REDEEMED.getDatabaseName(), "Hi %1$s. You have redeemed coupon %2$s for R%3$.2f. Your current account balance is R%4$.2f. Thanks, the SnappCab team.");

        // My Message content
        ensurePropertiesExist(MESSAGE_CONTENT_PASSWORD_RESET.getDatabaseName(), "Your password was successfully reset to: %s");
        ensurePropertiesExist(MESSAGE_CONTENT_NEW_USER.getDatabaseName(), "Hi %s, your account with SnappCab has been successfully created. We wish you the best traveling experiences with us");
        ensurePropertiesExist(MESSAGE_CONTENT_NEW_INVOICE.getDatabaseName(), "Hi %s, an invoice for your SnappCab account has been generated and sent to your mailbox");
        ensurePropertiesExist(MESSAGE_CONTENT_PROFILE_UPDATE.getDatabaseName(), "Hi %s, your SnappCab profile has been updated");
        ensurePropertiesExist(MESSAGE_CONTENT_SERVICE_NOT_AVAILABLE.getDatabaseName(), "Hi %s, the service is not currently available in your area");
        ensurePropertiesExist(MESSAGE_CONTENT_CABS_NOT_AVAILABLE.getDatabaseName(), "Hi %s, no cabs are currently available to pickup your request");
    }

    private void ensurePropertiesExist(final String name, final String value) {

        SystemProperty property = systemPropertyRepository.findActiveProperty(name);
        if (property == null) {
            property = new SystemProperty(name, value);
            systemPropertyRepository.save(property);
        }
    }
    
    private void ensureUsersExist() {
        User admin = userRepository.findByEmail("devs@velvetant.co.za");
        if (admin == null) {
            admin = new User("VELVET", "ANT", "0835540735", "devs@velvetant.co.za", BCrypt.hashpw("Acid1974", BCrypt.gensalt()));
            admin.setActive(true);
            final Role  role = new Role(Group.ADMINISTRATOR, admin);
            admin.addGroup(role);
            admin = userRepository.save(admin);
            roleRepository.save(role);
        }
        
        admin = userRepository.findByUserId("dispatch");
        if (admin == null) {
            admin = new User("dispatch", "dispatch", "08355407355", "dispatch", BCrypt.hashpw("disp@tch123!", BCrypt.gensalt()));
            admin.setActive(true);
            final Role  role = new Role(Group.SYSTEM, admin);
            admin.addGroup(role);
            admin = userRepository.save(admin);
            roleRepository.save(role);
        }
    }

}
