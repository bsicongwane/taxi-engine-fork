package za.co.velvetant.taxi.engine.rest;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.api.Corporate;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.Customer;
import za.co.velvetant.taxi.engine.api.FareList;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/corporates")
@Api(value = "/corporates", description = "Services for managing corporates.")
@RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE"})
public interface CorporateService {

    String CREDIT_CARD_CLASS = "za.co.velvetant.taxi.engine.api.CreditCard";
    String CORPORATE_CLASS = "za.co.velvetant.taxi.engine.api.Corporate";

    @POST
    @ApiOperation(value = "Registers a new corporate in the system.", responseClass = CORPORATE_CLASS)
    @ApiErrors({@ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 409, reason = DocumentationMessages.E_409 + " Corporate with same name already exists")})
    Response create(@ApiParam(value = "Corporate to create", required = true) Corporate corporate);

    @POST
    @Path("/admin/corporate")
    @ApiOperation(value = "Registers a new corporate in the system by an administrative user.", responseClass = CORPORATE_CLASS)
    @ApiErrors({@ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 409, reason = DocumentationMessages.E_409 + " Corporate with same name already exists")})
    @RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT"})
    Response createAdministratively(@ApiParam(value = "Administrator of this corporate", required = true) @QueryParam("administrator") String administrator,
            @ApiParam(value = "Corporate to create", required = true) Corporate corporate);

    @PUT
    @Path("/{registration}")
    @ApiOperation(value = "Updates a corporate's details.", responseClass = CORPORATE_CLASS)
    Response edit(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = "Corporate to update", required = true) Corporate corporate);

    @DELETE
    @Path("/{registration}")
    @ApiOperation(value = "Removes a corporate")
    Response remove(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration);

    @PUT
    @Path("/{registration}/administrator/{userid}")
    @ApiOperation(value = "Set a corporate's administrator.", responseClass = CORPORATE_CLASS)
    Response assignAdministrator(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @DELETE
    @Path("/{registration}/administrator/{userid}")
    @ApiOperation(value = "Remove a corporates administrator.")
    Response removeAdministrator(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{registration}")
    @ApiOperation(value = "Finds a corporate given the corporate's company name.", responseClass = CORPORATE_CLASS)
    Corporate find(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration);

    @GET
    @Path("/{registration}/customers")
    @ApiOperation(value = "Finds all the registered customers for the given corporate.", responseClass = "za.co.velvetant.taxi.engine.api.Customer")
    List<Customer> findCustomers(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration);

    @GET
    @ApiOperation(value = "Finds all the corporates in the system.", multiValueResponse = true, responseClass = CORPORATE_CLASS)
    List<Corporate> findAll();

    @Path("/all")
    @GET
    @ApiOperation(value = "Find all corporates.", multiValueResponse = true, responseClass = CORPORATE_CLASS, notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    @RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT"})
    Response findAll(
            @ApiParam(value = "Filter by company name. Partial filtering is allowed.") @QueryParam("companyName") String companyName,
            @ApiParam(value = "Filter by registration. Partial filtering is allowed.") @QueryParam("registration") String registration,
            @ApiParam(value = "Filter by contact email. Partial filtering is allowed.") @QueryParam("contactEmail") String contactEmail,
            @ApiParam(value = "Filter by active/inactive affiliates.", defaultValue = "true") @QueryParam("active") @DefaultValue("true") Boolean active,
            @ApiParam(value = "Page number of affiliates to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number of affiliates to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "companyName", allowableValues = "companyName,registration,contactEmail") @QueryParam("sort") @DefaultValue("companyName") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

    @Path("/{registration}/fares")
    @GET
    @ApiOperation(value = "Finds all the fares for the given corporate.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.FareList")
    FareList findFares(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = DocumentationMessages.M_START_TIME, required = false) @QueryParam("startTime") long startTime,
            @ApiParam(value = DocumentationMessages.M_END_TIME, required = false) @QueryParam("endTime") long endtime,
            @ApiParam(value = "Day of week. Sunday = 1, Monday = 2, Saturday = 7", required = false) @QueryParam("dayOfWeek") int dayOfWeek,
            @ApiParam(value = "Employee userId", required = false) @QueryParam("userId") String userId);

    @PUT
    @Path("/{registration}/customers/{userid}")
    @ApiOperation(value = "Add an existing customer to an existing corporate.")
    Response addEmployee(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("count")
    @Produces("text/plain")
    String count();

    @DELETE
    @Path("/{registration}/customers/{userid}")
    @ApiOperation(value = "Remove a customer from a corporate.")
    Response removeMember(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{registration}/administrators")
    @ApiOperation(value = "Get a corporate's administrators.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.Customer")
    List<Customer> findAdministrators(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration);

    @Path("/{registration}/creditCard")
    @GET
    @ApiOperation(value = "Get a corporate's credit card", responseClass = CREDIT_CARD_CLASS)
    Response getCreditCard(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration);

    @PUT
    @Path("/{registration}/creditCard")
    @ApiOperation(value = "Upload credit card details.", responseClass = CREDIT_CARD_CLASS)
    Response uploadCreditCard(@ApiParam(value = DocumentationMessages.M_COMPANY_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = "Credit Card details.", required = true) CreditCard card);

}
