package za.co.velvetant.taxi.engine.rest.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class NoContentException extends WebApplicationException {

    public NoContentException() {
        super(Response.status(Response.Status.NO_CONTENT).type(MediaType.TEXT_PLAIN_TYPE).build());
    }
}
