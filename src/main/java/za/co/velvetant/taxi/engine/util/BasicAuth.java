package za.co.velvetant.taxi.engine.util;

import javax.xml.bind.DatatypeConverter;

/**
 * Allow to encode/decode the authentification
 * @author Deisss (LGPLv3)
 */
public final class BasicAuth {

    private BasicAuth() {

    }

    /**
     * Decode the basic auth and convert it to array login/password
     * @param auth
     *            The string encoded authentification
     * @return The login (case 0), the password (case 1)
     */
    public static String[] decode(final String auth) {
        // Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
        final String auths = auth.replaceFirst("[B|b]asic ", "");

        // Decode the Base64 into byte[]
        final byte[] decodedBytes = DatatypeConverter.parseBase64Binary(auths);

        // If the decode fails in any case
        if (decodedBytes == null || decodedBytes.length == 0) {
            return null;
        }

        // Now we can convert the byte[] into a splitted array :
        // - the first one is login,
        // - the second one password
        return new String(decodedBytes).split(":", 2);
    }
}
