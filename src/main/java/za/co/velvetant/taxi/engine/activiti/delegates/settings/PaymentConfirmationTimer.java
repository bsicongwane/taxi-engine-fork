package za.co.velvetant.taxi.engine.activiti.delegates.settings;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.PAYMENT_CONFIRMATION_DURATION;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

@Named
public class PaymentConfirmationTimer {

    private static final Logger log = LoggerFactory.getLogger(PaymentConfirmationTimer.class);

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    public String duration() {
        String duration = systemPropertyHelper.getProperty(PAYMENT_CONFIRMATION_DURATION, "PT2M");

        log.debug("Using payment confirmation duration: {}", duration);
        return duration;
    }
}
