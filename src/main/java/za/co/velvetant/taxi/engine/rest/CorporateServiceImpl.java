package za.co.velvetant.taxi.engine.rest;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.mygate.MyGateResponse;
import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.Customer;
import za.co.velvetant.taxi.engine.api.FareList;
import za.co.velvetant.taxi.engine.filters.CorporateFilter;
import za.co.velvetant.taxi.engine.models.Corporate;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.Group;
import za.co.velvetant.taxi.engine.models.Industry;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.models.Role;
import za.co.velvetant.taxi.engine.models.Sector;
import za.co.velvetant.taxi.engine.models.User;
import za.co.velvetant.taxi.engine.persistence.CorporateRepository;
import za.co.velvetant.taxi.engine.persistence.CreditCardRepository;
import za.co.velvetant.taxi.engine.persistence.IndustryRepository;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.engine.persistence.SectorRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.NotAllowedException;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.engine.util.VoConversion;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import static com.google.common.collect.Lists.transform;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;
import static org.apache.commons.collections.CollectionUtils.containsAny;
import static za.co.velvetant.taxi.engine.models.Group.ADMINISTRATOR;
import static za.co.velvetant.taxi.engine.models.Group.AGENT;
import static za.co.velvetant.taxi.engine.models.Group.SUPERVISOR;
import static za.co.velvetant.taxi.engine.models.Group.SYSTEM;
import static za.co.velvetant.taxi.engine.models.MyGateTransaction.Type.STORE_CARD;
import static za.co.velvetant.taxi.engine.models.MyGateTransaction.Type.UPDATE_CARD;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CORPORATE_ACCOUNT;
import static za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines.buildVo;

@Named
@Path("/corporates.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class CorporateServiceImpl implements CorporateService {

    private static final Logger log = LoggerFactory.getLogger(CorporateServiceImpl.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private CorporateRepository corporateRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private FareRepository fareRepository;

    @Inject
    private SectorRepository sectorRepository;

    @Inject
    private IndustryRepository industryRepository;

    @Inject
    private CreditCardRepository creditCardRepository;

    @Inject
    private MyGateTransactionRepository myGateTransactionRepository;

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private PaymentService paymentService;

    @Inject
    private Provider<CorporateFilter> filter;

    @Context
    private HttpHeaders headers;

    @Override
    public Response create(final za.co.velvetant.taxi.engine.api.Corporate corporate) {
        final Corporate existing = corporateRepository.findAllByRegistration(corporate.getRegistration());
        if (existing != null) {
            throw new ConflictException(String.format("Corporate %s is already registered", corporate.getRegistration()));
        }

        final Sector sector = sectorRepository.findByName(corporate.getSectorName());
        final Industry industry = industryRepository.findByName(corporate.getIndustryName());
        Corporate entity = VoConversion.convert(corporate, new Corporate());
        entity.setIndustry(industry);
        entity.setSector(sector);
        entity.setActive(true);
        entity.setHash(UUID.randomUUID().toString());

        entity = corporateRepository.save(entity);
        entity = assign(entity.getRegistration(), CustomerRoutines.getUserId(headers), false);

        return Response.status(CREATED).entity(buildCorporateVo(entity)).build();
    }

    @Override
    public Response createAdministratively(final String administrator, final za.co.velvetant.taxi.engine.api.Corporate corporate) {
        final Corporate existing = corporateRepository.findAllByRegistration(corporate.getRegistration());
        if (existing != null) {
            throw new ConflictException(String.format("Corporate %s is already registered", corporate.getRegistration()));
        }

        final Sector sector = sectorRepository.findByName(corporate.getSectorName());
        final Industry industry = industryRepository.findByName(corporate.getIndustryName());
        Corporate entity = VoConversion.convert(corporate, new Corporate());
        entity.setIndustry(industry);
        entity.setSector(sector);
        entity.setActive(true);
        entity.setHash(UUID.randomUUID().toString());

        entity = corporateRepository.save(entity);
        entity = assign(entity.getRegistration(), administrator, false);

        return Response.status(CREATED).entity(buildCorporateVo(entity)).build();
    }

    @Override
    public Response edit(final String registration, final za.co.velvetant.taxi.engine.api.Corporate corporate) {
        checkAllowed(registration);
        Corporate entity = corporateRepository.findByRegistration(registration);
        ensureExists(entity, registration);

        entity = VoConversion.convert(corporate, entity);
        entity.setRegistration(registration);

        if (corporate.getSectorName() != null) {
            final Sector sector = sectorRepository.findByName(corporate.getSectorName());
            NotFoundException.assertNotNull(sector, String.format("Sector with name %s was not found.", corporate.getSectorName()));
            entity.setSector(sector);
        }

        if (corporate.getIndustryName() != null) {
            final Industry industry = industryRepository.findByName(corporate.getIndustryName());
            NotFoundException.assertNotNull(industry, String.format("Industry with name %s was not found.", corporate.getIndustryName()));
            entity.setIndustry(industry);
        }
        entity.setActive(true);
        entity = corporateRepository.save(entity);
        return Response.status(Response.Status.OK).entity(buildCorporateVo(entity)).build();
    }

    @Override
    public za.co.velvetant.taxi.engine.api.Corporate find(final String registration) {
        final Corporate corporate = corporateRepository.fetchWithAdministrators(registration);
        ensureExists(corporate, registration);
        return buildCorporateVo(corporate);
    }

    @Override
    public List<Customer> findAdministrators(final String registration) {
        final List<Customer> administrators = new ArrayList<>();
        final Corporate corporate = corporateRepository.fetchWithAdministrators(registration);
        ensureExists(corporate, registration);
        if (corporate.getAdministrators() != null) {
            for (final za.co.velvetant.taxi.engine.models.Customer customer : corporate.getAdministrators()) {
                administrators.add(buildVo(customerRepository.fetchWithCreditCard(customer.getUserId()), creditCardRepository, transactionRepository.findCustomersBalance(customer)));
            }
        }
        return administrators;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Corporate> findAll() {
        final List<za.co.velvetant.taxi.engine.api.Corporate> vos = new ArrayList<>();
        for (final Corporate corporate : corporateRepository.fetchAllWithAdministrators()) {
            vos.add(buildCorporateVo(corporate));
        }
        return vos;
    }

    @Override
    public Response findAll(final String companyName, final String registration, final String contactEmail, final Boolean active, final Integer page, final Integer size, final String sort,
            final Sort.Direction order) {
        final CorporateFilter filteredCorporates = filter.get().with(companyName, registration, active).sort(page, size, sort, order).filter();
        final List<za.co.velvetant.taxi.engine.api.Corporate> vos = filteredCorporates.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredCorporates.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public String count() {
        return String.valueOf(corporateRepository.count());
    }

    @Override
    public List<Customer> findCustomers(final String registration) {

        final Corporate entity = corporateRepository.findByRegistration(registration);
        ensureExists(entity, registration);
        final List<Customer> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Customer customer : customerRepository.findByCorporateRegistration(registration)) {
            vos.add(buildVo(customerRepository.fetchWithCreditCard(customer.getUserId()), creditCardRepository, transactionRepository.findCustomersBalance(customer)));
        }
        return vos;
    }

    @Override
    public FareList findFares(final String registration, final long startTime, final long endTime, final int dayOfWeek, final String userId) {
        final Corporate entity = corporateRepository.findByRegistration(registration);
        ensureExists(entity, registration);
        List<Fare> fares;

        final List<Fare> results = new ArrayList<>();

        if (startTime > 0 && endTime > 0) {
            fares = fareRepository.findByCustomerCorporateRegistrationAndRequestTimeBetweenAndPaymentMethod(registration, new Date(startTime), new Date(endTime), CORPORATE_ACCOUNT);
        } else if (startTime > 0) {
            fares = fareRepository.findByCustomerCorporateRegistrationAndRequestTimeGreaterThanAndPaymentMethod(registration, new Date(startTime), CORPORATE_ACCOUNT);
        } else if (endTime > 0) {
            fares = fareRepository.findByCustomerCorporateRegistrationAndRequestTimeLessThanAndPaymentMethod(registration, new Date(endTime), CORPORATE_ACCOUNT);
        } else {
            fares = fareRepository.findByCustomerCorporateRegistrationAndPaymentMethod(registration, CORPORATE_ACCOUNT);
        }
        final double totalAmount = 0.0;
        final int total = 0;
        if (fares != null) {
            for (final Fare fare : fares) {
                if (shouldIncludeFare(fare, dayOfWeek, userId)) {
                    results.add(fare);
                }
            }
            return new FareList(VoConversion.convertFares(results), total, totalAmount);
        }

        return null;
    }

    private boolean shouldIncludeFare(final Fare fare, final int dayOfWeek, final String userId) {
        if (userId != null && (!fare.getCustomer().getUserId().equals(userId))) {
            return false;
        }
        if (dayOfWeek != 0) {
            final GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(fare.getRequestTime());
            if (gregorianCalendar.get(Calendar.DAY_OF_WEEK) != (dayOfWeek - 1)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Response addEmployee(final String registration, final String userid) {
        checkAllowed(registration);

        final za.co.velvetant.taxi.engine.models.Customer user = customerRepository.findByUserId(userid);
        final za.co.velvetant.taxi.engine.models.Corporate corporate = corporateRepository.findByRegistration(registration);
        if (user != null && corporate != null) {
            user.setCorporate(corporate);
            customerRepository.save(user);
            return Response.ok().build();
        }
        if (user == null) {
            throw new ConflictException(String.format("Customer with userid %s has not been created yet", userid));
        }
        throw new ConflictException(String.format("Corporate has not been created yet"));
    }

    @Override
    public Response assignAdministrator(final String registration, final String userid) {
        final Corporate assign = assign(registration, userid, true);

        return Response.ok().entity(VoConversion.convert(assign, new za.co.velvetant.taxi.engine.api.Corporate())).build();
    }

    public Corporate assign(final String registration, final String userid, final boolean check) {
        if (check) {
            checkAllowed(registration);
        }
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findAllowedAdministrator(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found or already administers another company");

        final Corporate corporate = corporateRepository.findByRegistration(registration);
        NotFoundException.assertNotNull(registration, String.format("Corporate with registration %s was not found", registration));
        customer.setAdministered(corporate);
        customerRepository.save(customer);
        return corporateRepository.fetchWithAdministrators(registration);
    }

    @Override
    public Response remove(final String registration) {
        checkAllowed(registration);
        final Corporate corporate = corporateRepository.findByRegistration(registration);
        corporate.setActive(false);
        corporateRepository.save(corporate);
        return Response.ok().build();
    }

    public void ensureExists(final Corporate entity, final String registration) {
        NotFoundException.assertNotNull(entity, String.format("Corporate with registration %s was not found or is not active", registration));
    }

    @Override
    public Response removeAdministrator(final String registration, final String userid) {
        checkAllowed(registration);
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found or already administers another company");

        final Corporate corporate = corporateRepository.fetchWithAdministrators(registration);
        if (corporate.getAdministrators().size() == 1) {
            throw new PreconditionFailedException("There must be at least two administrators for a remove to be allowed");
        }

        NotFoundException.assertNotNull(registration, String.format("Corporate with registration %s was not found", registration));
        corporate.removeAdministrator(customer);
        corporateRepository.save(corporate);

        customer.setAdministered(null);
        customerRepository.save(customer);

        return ok().build();
    }

    @Override
    public Response removeMember(final String registration, final String userid) {
        checkAllowed(registration);
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found or already administers another company");
        final Corporate corporate = corporateRepository.fetchWithAdministrators(registration);
        NotFoundException.assertNotNull(corporate, String.format("Corporate with registration %s was not found", registration));

        customer.setCorporate(null);
        customerRepository.save(customer);

        return ok().build();
    }

    private za.co.velvetant.taxi.engine.api.Corporate buildCorporateVo(final Corporate corporate) {
        final za.co.velvetant.taxi.engine.api.Corporate convert = VoConversion.convert(corporate, new za.co.velvetant.taxi.engine.api.Corporate());

        if (corporate.getAdministrators() != null) {
            final List<String> administrators = new ArrayList<>();
            for (final za.co.velvetant.taxi.engine.models.Customer customer : corporate.getAdministrators()) {
                administrators.add(customer.getEmail());
            }
            convert.setAdministratorEmails(administrators);
        }

        if (corporate.getIndustry() != null) {
            convert.setIndustryName(corporate.getIndustry().getName());
        }

        if (corporate.getSector() != null) {
            convert.setSectorName(corporate.getSector().getName());
        }
        convert.setHash(corporate.getHash());
        return convert;
    }

    @Override
    public Response uploadCreditCard(final String registration, final CreditCard card) {
        Response.ResponseBuilder response = ok();
        za.co.velvetant.taxi.engine.models.CreditCard creditCard = new za.co.velvetant.taxi.engine.models.CreditCard();
        final Corporate corporate = corporateRepository.fetchWithCreditCard(registration);
        if (corporate.getCreditCards() != null && !corporate.getCreditCards().isEmpty()) {
            creditCard = corporate.getCreditCards().get(0);
        }

        creditCard.setCorporate(corporate);
        MyGateResponse myGateREsponse = null;
        MyGateTransaction transaction;
        try {
            if (creditCard.getId() == null) {
                final String tokenId = UUID.randomUUID().toString().replaceAll("-", "");
                creditCard.setGateWayId(tokenId);
                myGateREsponse = paymentService.storeCard(tokenId, card);
                transaction = new MyGateTransaction(myGateREsponse.getDateTime(), null, STORE_CARD, myGateREsponse.getTransactionIndex(), myGateREsponse.getStatus());
            } else {
                myGateREsponse = paymentService.updateCard(creditCard.getGateWayId(), card);
                transaction = new MyGateTransaction(myGateREsponse.getDateTime(), null, UPDATE_CARD, myGateREsponse.getTransactionIndex(), myGateREsponse.getStatus());
            }

            transaction.setCorporate(corporate);
            transaction.setErrorMessage(myGateREsponse.getErrorCode());
            myGateTransactionRepository.save(transaction);
        } catch (final Exception e) {
            log.error("Error calling MyGate", e);
        }

        if (myGateREsponse != null && myGateREsponse.isSuccess()) {
            creditCard.setCvvReference(card.getCvv());
            creditCard = creditCardRepository.save(creditCard);

            corporate.addCreditCard(creditCard);
            corporateRepository.save(corporate);

            response = response.entity(card);
        } else {
            response = status(PRECONDITION_FAILED).entity("There was an error saving your card details with payment gateway.");
        }

        return response.build();
    }

    @Override
    public Response getCreditCard(final String registration) {
        final Corporate corporate = corporateRepository.fetchWithCreditCard(registration);
        NotFoundException.assertNotNull(corporate, String.format("Corporate with registration %s does not exist", registration));

        final List<za.co.velvetant.taxi.engine.models.CreditCard> creditCards = corporate.getCreditCards();
        if (creditCards != null && !creditCards.isEmpty()) {

            final za.co.velvetant.taxi.engine.models.CreditCard creditCard = corporate.getCreditCards().get(0);
            try {
                final CreditCard cc = paymentService.getMaskedCardDetails(creditCard.getGateWayId());
                cc.setCvv(creditCard.getCvvReference());
                return Response.ok().entity(cc).build();
            } catch (RemoteException | ServiceException | ParseException e) {
                log.error(e.getMessage(), e);
            }

        }

        throw new NotFoundException(String.format("Corporate %s has no credit card loaded", registration));
    }

    private void checkAllowed(final String registration) {
        final String userId = CustomerRoutines.getUserId(headers);
        User user = userRepository.findByUserIdAndActive(userId);
        if (user == null || !containsAny(getGroups(user.getGroups()), ImmutableList.of(SYSTEM, ADMINISTRATOR, SUPERVISOR, AGENT))) {
            final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userId);
            NotFoundException.assertNotNull(customer, "Customer with userid " + userId + " was not found");
            if (customer.getAdministered() == null || (!customer.getAdministered().getRegistration().equals(registration))) {
                throw new NotAllowedException("You are not allowed to modify this corporate.");
            }
        }
    }

    private List<Group> getGroups(final List<Role> roles) {
        return transform(roles, new Function<Role, Group>() {

            @Override
            public Group apply(final za.co.velvetant.taxi.engine.models.Role role) {
                return role.getRole();
            }
        });
    }
}
