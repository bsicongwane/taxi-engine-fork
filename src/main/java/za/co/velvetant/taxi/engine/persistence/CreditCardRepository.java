package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.CreditCard;

@Repository
public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
    
    List<CreditCard> findByCustomerUserId(String userId);
    
    List<CreditCard> findByFamilyUuid(String uuid);
    
    List<CreditCard> findByCorporateRegistration(String registration);

}
