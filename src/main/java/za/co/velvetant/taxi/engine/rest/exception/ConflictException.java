package za.co.velvetant.taxi.engine.rest.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ConflictException extends WebApplicationException {

    //TODO suppress stacktrace
    public ConflictException(final String message) {
        super(Response.status(Response.Status.CONFLICT).entity(message).type(MediaType.TEXT_PLAIN_TYPE).build());
    }

    public static void assertEmpty(final Object o, final String notEmptyMessage) {
        if (o != null) {
            throw new ConflictException(notEmptyMessage);
        }

    }
}
