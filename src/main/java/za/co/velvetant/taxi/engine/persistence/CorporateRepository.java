package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Corporate;
import za.co.velvetant.taxi.persistence.repositories.auditing.AuditingRepository;
import za.co.velvetant.taxi.persistence.repositories.metamodel.MetaModelRepository;

@Repository
public interface CorporateRepository extends AuditingRepository<Corporate, Long>, MetaModelRepository<Corporate, Long> {

    @Query("SELECT h FROM Corporate h where h.registration = ?1 and h.active = true")
    Corporate findByRegistration(String registration);

    @Query("SELECT h FROM Corporate h where h.registration = ?1")
    Corporate findAllByRegistration(String registration);

    @Query("SELECT h FROM Corporate h LEFT JOIN FETCH h.administrators where h.registration = ?1 and h.active = true")
    Corporate fetchWithAdministrators(String registration);

    @Query("SELECT h FROM Corporate h LEFT JOIN FETCH h.administrators where h.active = true")
    List<Corporate> fetchAllWithAdministrators();

    @Query("SELECT h FROM Corporate h LEFT JOIN FETCH h.creditCards where h.registration = ?1  and h.active = true")
    Corporate fetchWithCreditCard(String registration);
}
