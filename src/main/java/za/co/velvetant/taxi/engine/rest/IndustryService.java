package za.co.velvetant.taxi.engine.rest;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import za.co.velvetant.taxi.engine.api.Industry;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/industries")
@Api(value = "/industries", description = "Services for managing industries.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({ "CUSTOMER", "DRIVER" })
public interface IndustryService {

    @POST
    @Consumes("application/json")
    @ApiOperation(value = "Registers a new industry in the system.")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 409, reason = DocumentationMessages.E_409 + " Industry with same name already exists") })
    Response create(@ApiParam(value = "Industry to create", required = true) Industry industry);

    @PUT
    @ApiOperation(value = "Updates a industry's details.")
    Response edit(@ApiParam(value = "Industry to update", required = true) Industry industry);

    @GET
    @Path("/{name}")
    @ApiOperation(value = "Finds a industry given the industry's name.")
    Industry find(@ApiParam(value = DocumentationMessages.M_INDUSTRY_NAME, required = true) @PathParam("name") String name);

    @GET
    @ApiOperation(value = "Finds all the industries in the system.")
    List<Industry> findAll();

    @GET
    @Path("count")
    @Produces("text/plain")
    String count();

}


