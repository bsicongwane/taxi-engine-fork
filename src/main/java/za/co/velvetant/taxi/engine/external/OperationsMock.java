package za.co.velvetant.taxi.engine.external;

import static org.apache.commons.lang.builder.ToStringBuilder.reflectionToString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.ops.notifications.Notification;

public class OperationsMock implements Operations {

    private static final Logger log = LoggerFactory.getLogger(DispatchMock.class);

    @Override
    public <T> void notify(final Notification<T> notification) {
        log.debug("Using test operations service to send notification: {}", reflectionToString(notification));
    }
}
