package za.co.velvetant.taxi.engine.messaging.mail.mandrill;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SendTemplate {

    private String key;
    @JsonProperty("template_name")
    private String template;
    @JsonProperty("template_content")
    private String content = StringUtils.EMPTY;
    private Message message;

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(final String template) {
        this.template = template;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(final Message message) {
        this.message = message;
    }

}
