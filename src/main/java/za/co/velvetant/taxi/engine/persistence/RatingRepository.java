package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.velvetant.taxi.engine.models.Rating;

import java.util.List;

@Repository
public interface RatingRepository extends CrudRepository<Rating, Long> {

    List<Rating> findByFareDriverUserId(String userId);

    Page<Rating> findByFareDriverUserId(String userId, Pageable pageable);

    List<Rating> findByFareCustomerUserId(String userId);
}
