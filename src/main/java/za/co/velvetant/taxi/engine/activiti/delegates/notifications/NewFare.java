package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.FARE_REQUEST;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.ops.notifications.NewFareRequestNotification;

@Named
public class NewFare extends ExecutionDelegate {

    @Inject
    private Operations operations;

    @Override
    public void process(final DelegateExecution execution) {
        final FareRequest fareRequest = getVariable(execution, FARE_REQUEST, FareRequest.class);

        operations.notify(new NewFareRequestNotification(execution.getProcessBusinessKey(), NewFare.class, fareRequest));
    }
}
