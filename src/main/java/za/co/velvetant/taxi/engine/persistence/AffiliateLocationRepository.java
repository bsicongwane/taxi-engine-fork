package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.AffiliateLocation;

@Repository
public interface AffiliateLocationRepository extends CrudRepository<AffiliateLocation, Long> {

    List<AffiliateLocation> findByTaxiCompanyRegistration(String registration);

    AffiliateLocation findByTaxiCompanyRegistrationAndLocationAddress(String registration, String address);

}
