package za.co.velvetant.taxi.engine.activiti.util;

public abstract class Variables {

    public static final String FARE_REQUEST = "fareRequest";

    public static final String ELIGIBLE_DRIVERS = "drivers";
    public static final String ACCEPTED_DRIVER = "driver";
    public static final String FIND_DRIVER_RETRY_THRESHOLD = "retryThreshold";
    public static final String PAYMENT_CONFIRMATION_THRESHOLD = "paymentConfirmationThreshold";

    public static final String PAYMENT_CONFIRMED = "paymentConfirmed";
    public static final String PAYMENT_SUCCESSFUL = "paymentSuccessful";
    public static final String PAYMENT_METHOD = "paymentMethod";
    public static final String PAYMENT_AMOUNT = "paymentAmount";
    public static final String CVV = "cvv";
    public static final String USE_ACCOUNT = "useAccount";
    public static final String DRIVER_RATING = "rating";
    public static final String CANCEL_FARE = "cancelFare";
    public static final String PAYMENT_CONFIRMATION_TIMED_OUT = "paymentConfirmationHasTimedOut";
    public static final String TAXIS_IN_SERVICE_RADIUS = "inServiceRadius";
    public static final String OPS_OVERRIDDEN = "opsOverridden";
    public static final String OPS_USER = "ops";
}
