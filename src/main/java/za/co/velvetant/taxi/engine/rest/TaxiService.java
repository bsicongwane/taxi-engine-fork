package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_400;
import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_409;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;
import za.co.velvetant.taxi.engine.api.Taxi;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import za.co.velvetant.taxi.engine.models.TaxiStatus;

@Path("/taxis")
@Api(value = "/taxis", description = "Services for managing taxis.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "DRIVER" })
public interface TaxiService {

    String TAXI_CLASS = "za.co.velvetant.taxi.engine.api.Taxi";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Registers a new taxi in the system.", responseClass = TAXI_CLASS)
    @ApiErrors({ @ApiError(code = 400, reason = E_400), @ApiError(code = 409, reason = E_409 + " Taxi with same registration already exists") })
    Response create(@ApiParam(value = "Taxi to create", required = true) Taxi taxi);

    @PUT
    @Path("/{registration}")
    @ApiOperation(value = "Updates a taxi's details.", responseClass = TAXI_CLASS)
    Response edit(@ApiParam(value = DocumentationMessages.M_TAXI_REG, required = true) @PathParam("registration") String registration,
            @ApiParam(value = "Taxi details to update", required = true) Taxi taxi);

    @GET
    @Path("/{registration}")
    @ApiOperation(value = "Finds a taxi given the taxi's registration.", responseClass = TAXI_CLASS)
    Response find(@ApiParam(value = DocumentationMessages.M_TAXI_REG, required = true) @PathParam("registration") String registration);

    @GET
    @ApiOperation(value = "Finds all the taxis in the system.", multiValueResponse = true, responseClass = TAXI_CLASS)
    Response findAll();

    @GET
    @Path("/all")
    @ApiOperation(value = "Find all taxis.", multiValueResponse = true, responseClass = TAXI_CLASS,
            notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response findAll(
            @ApiParam(value = "Filter by registration. Partial filtering is allowed.") @QueryParam("registration") String registration,
            @ApiParam(value = "Filter by make. Partial filtering is allowed.") @QueryParam("make") String make,
            @ApiParam(value = "Filter by model. Partial filtering is allowed.") @QueryParam("model") String model,
            @ApiParam(value = "Filter by taxi status.", allowableValues = "ONLINE,OFFLINE,BUSY") @QueryParam("status") TaxiStatus status,
            @ApiParam(value = "Filter by active/inactive taxis.", defaultValue = "true") @QueryParam("active") @DefaultValue("true") Boolean active,
            @ApiParam(value = "Page number of taxis to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number of taxis to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "lastName", allowableValues = "registration,make,model") @QueryParam("sort") @DefaultValue("registration") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

    @DELETE
    @Path("/{registration}")
    @ApiOperation(value = "Remove a taxi.", responseClass = TAXI_CLASS)
    Response remove(@ApiParam(value = DocumentationMessages.M_TAXI_REG, required = true) @PathParam("registration") String registration);

}
