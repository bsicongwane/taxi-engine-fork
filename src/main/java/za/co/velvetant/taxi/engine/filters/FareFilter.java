package za.co.velvetant.taxi.engine.filters;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CASH;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CORPORATE_ACCOUNT;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CREDIT_CARD;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.FAMILY_ACCOUNT;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import za.co.velvetant.taxi.engine.models.Corporate_;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Customer_;
import za.co.velvetant.taxi.engine.models.Family_;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.FareState;
import za.co.velvetant.taxi.engine.models.Fare_;
import za.co.velvetant.taxi.engine.persistence.CorporateRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class FareFilter implements Filter<Fare, za.co.velvetant.taxi.engine.api.Fare, FareFilter> {

    private static final Logger log = LoggerFactory.getLogger(FareFilter.class);

    @Inject
    private FareRepository fareRepository;

    @Inject
    private CorporateRepository corporateRepository;

    private Fare example = new Fare();
    private String uuid;
    private String reference;
    private String corporate;
    private String family;
    private Boolean active;
    private Boolean enroute;
    private Boolean exclusive;

    private PageRequest sortable = new PageRequest(0, 10, new Sort(DESC, "requestTime"));

    private Long total;
    private List<Fare> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Fare> vos = new ArrayList<>();

    public FareFilter with(final String uuid, final String reference, final String customerEmail, final String customerCellphone, final String registration, final String family, final Boolean active,
            final Boolean enroute, final Boolean exclusive) {
        this.uuid = uuid;
        this.reference = reference;
        corporate = registration;
        this.family = family;
        this.active = active;
        this.enroute = enroute;
        this.exclusive = exclusive;

        return withCustomerDetails(customerEmail, customerCellphone);
    }

    @Override
    public FareFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public FareFilter filter() {
        Page<Fare> results = fareRepository.findAll(new FareSpecification(example), sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public FareFilter andConvert() {
        for (final Fare fare : filtered) {
            vos.add(convert(fare));
        }

        return this;
    }

    @Override
    public List<Fare> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Fare> vos() {
        return vos;
    }

    private FareFilter withCustomerDetails(final String customerEmail, final String cellphone) {
        Customer customer = null;
        if (isNotBlank(customerEmail)) {
            customer = fromNullable(customer).or(new Customer());
            customer.setEmail(customerEmail);
        }

        if (isNotBlank(cellphone)) {
            customer = fromNullable(customer).or(new Customer());
            customer.setCellphone(cellphone);
        }

        example.setCustomer(customer);

        return this;
    }

    class FareSpecification implements Specification<Fare> {

        public Fare fare;

        FareSpecification(final Fare fare) {
            this.fare = new LikeableFare(fare, exclusive);
        }

        @Override
        public Predicate toPredicate(final Root<Fare> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
            List<Predicate> predicates = new ArrayList<>();

            if (isNotBlank(uuid)) {
                predicates.add(cb.equal(root.get(Fare_.uuid), uuid));
            } else if (isNotBlank(reference)) {
                predicates.add(cb.equal(root.get(Fare_.reference), reference));
            }

            predicates.add(cb.or(cb.or(cb.like(root.get(Fare_.customer).get(Customer_.email), fare.getCustomer().getEmail()),
                    cb.like(root.get(Fare_.customer).get(Customer_.cellphone), fare.getCustomer().getCellphone()))));

            Predicate corporatePredicate = cb.or(cb.equal(root.get(Fare_.customer).get(Customer_.corporate).get(Corporate_.registration), corporate),
                    cb.equal(root.get(Fare_.customer).get(Customer_.administered).get(Corporate_.registration), corporate));
            Predicate familyPredicate = cb.or(cb.equal(root.get(Fare_.customer).get(Customer_.family).get(Family_.uuid), family),
                    cb.equal(root.get(Fare_.customer).get(Customer_.headed).get(Family_.uuid), family));

            if (exclusive) {
                if (isNotBlank(fare.getCustomer().getEmail()) || isNotBlank(fare.getCustomer().getCellphone())) {
                    predicates.add(cb.or(cb.or(cb.like(root.get(Fare_.customer).get(Customer_.email), fare.getCustomer().getEmail()),
                            cb.like(root.get(Fare_.customer).get(Customer_.cellphone), fare.getCustomer().getCellphone()))));
                    if (isBlank(corporate) && isBlank(family)) {
                        predicates.add(cb.and(cb.or(cb.equal(root.get(Fare_.paymentMethod), CASH), cb.equal(root.get(Fare_.paymentMethod), CREDIT_CARD))));
                    }
                }
            } else {
                predicates.add(cb.or(cb.or(cb.like(root.get(Fare_.customer).get(Customer_.email), fare.getCustomer().getEmail()),
                        cb.like(root.get(Fare_.customer).get(Customer_.cellphone), fare.getCustomer().getCellphone()))));
            }

            if (active != null) {
                if (active && enroute != null && enroute) {
                    predicates.add(cb.and(cb.equal(root.get(Fare_.state), FareState.ARRIVED_AT_DESTINATION)));
                } else if (active) {
                    predicates.add(cb.and(cb.and(cb.or(cb.notEqual(root.get(Fare_.state), FareState.COMPLETED)), cb.notEqual(root.get(Fare_.state), FareState.CANCELLED_BY_ADMIN),
                            cb.notEqual(root.get(Fare_.state), FareState.CANCELLED), cb.notEqual(root.get(Fare_.state), FareState.CANCELLED_BY_DRIVER),
                            cb.notEqual(root.get(Fare_.state), FareState.CANCELLED_BY_CUSTOMER), cb.notEqual(root.get(Fare_.state), FareState.TIMED_OUT),
                            cb.notEqual(root.get(Fare_.state), FareState.OUT_OF_SERVICE_AREA))));
                } else if (!active) {
                    predicates.add(cb.equal(root.get(Fare_.state), FareState.COMPLETED));
                }
            }

            if (isNotBlank(corporate)) {
                predicates.add(cb.and(corporatePredicate));
                predicates.add(cb.equal(root.get(Fare_.paymentMethod), CORPORATE_ACCOUNT));
            }

            if (isNotBlank(family)) {
                predicates.add(cb.and(familyPredicate));
                predicates.add(cb.equal(root.get(Fare_.paymentMethod), FAMILY_ACCOUNT));
            }

            return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
        }

        class LikeableFare extends Fare {

            private Boolean unlikable;

            LikeableFare(final Fare fare, final Boolean unlikable) {
                this.unlikable = unlikable;

                try {
                    BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
                    BeanUtils.copyProperties(this, fare);
                } catch (Exception e) {
                    log.error("Could not copy properties from Driver to LikeableSystemProperty", e);
                }
            }

            @Override
            public Customer getCustomer() {
                Customer customer = new Customer();
                if (super.getCustomer() != null) {
                    customer.setEmail(makeLikeable(super.getCustomer().getEmail(), unlikable));
                    customer.setCellphone(makeLikeable(super.getCustomer().getCellphone(), unlikable));
                } else {
                    customer.setEmail(makeLikeable(EMPTY));
                    customer.setCellphone(makeLikeable(EMPTY));
                }

                return customer;
            }

            private String makeLikeable(final String value) {
                return makeLikeable(value, false);
            }

            private String makeLikeable(final String value, final boolean unlikable) {
                String likeable = value;
                if (!unlikable) {
                    likeable = format("%%%s%%", fromNullable(value).or("?"));
                    log.trace("Made this Fare value likeable: {} -> {}", value, likeable);
                }

                return likeable;
            }
        }
    }
}
