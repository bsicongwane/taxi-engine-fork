package za.co.velvetant.taxi.engine.activiti.delegates;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_USER;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.models.ConfirmPaymentOverride;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.User;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.persistence.overrides.ConfirmPaymentOverrideRepository;

@Named
public class AuditConfirmPaymentOverride extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(AuditConfirmPaymentOverride.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private FareRepository fareRepository;

    @Inject
    private ConfirmPaymentOverrideRepository overrideRepository;

    @Override
    public void process(final DelegateExecution execution) {
        log.debug("Auditing Confirm Payment override for trip [{}] by: {}", execution.getProcessBusinessKey(), execution.getVariable(OPS_USER));

        final User opsUser = userRepository.findByUserId(getVariable(execution, OPS_USER, String.class));
        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());

        ConfirmPaymentOverride paymentOverride = overrideRepository.save(new ConfirmPaymentOverride(opsUser, fare));
        fare.addAudit(paymentOverride);
        fareRepository.save(fare);
    }
}
