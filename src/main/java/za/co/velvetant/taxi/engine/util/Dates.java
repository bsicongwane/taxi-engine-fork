package za.co.velvetant.taxi.engine.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class Dates {

    private Dates() {

    }

    // L stops int overflow
    public static final long MONTH_MAX = 1000L * 60L * 60L * 24L * 31L;

    public static String month(final Date date) {
        return new SimpleDateFormat("MMM").format(date);
    }

    public static String day(final Date date) {
        return new SimpleDateFormat("dd").format(date);
    }

}
