package za.co.velvetant.taxi.engine.rest.exception;


public class EngineException extends RuntimeException {

    public EngineException() {
        super();
    }

    public EngineException(final String message) {
        super(message);
    }

    public EngineException(final Exception e) {
        super(e);
    }

}
