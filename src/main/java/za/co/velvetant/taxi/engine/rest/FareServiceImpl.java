package za.co.velvetant.taxi.engine.rest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.api.common.Routes;
import za.co.velvetant.taxi.engine.FareEngine;
import za.co.velvetant.taxi.engine.api.Fare;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.api.PaymentMethod;
import za.co.velvetant.taxi.engine.api.Rating;
import za.co.velvetant.taxi.engine.external.Dispatch;
import za.co.velvetant.taxi.engine.external.google.DirectionsService;
import za.co.velvetant.taxi.engine.filters.FareActiveFilter;
import za.co.velvetant.taxi.engine.filters.FareCompletedFilter;
import za.co.velvetant.taxi.engine.filters.FareFilter;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.FareState;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.models.StopLocation;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.models.driver.DriverShift;
import za.co.velvetant.taxi.engine.persistence.CreditCardRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.engine.persistence.RatingRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.driver.DriverShiftRepository;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE;
import static javax.ws.rs.core.Response.noContent;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.ACCEPTED_DRIVER;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.CANCEL_FARE;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.CVV;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.DRIVER_RATING;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_OVERRIDDEN;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_USER;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_AMOUNT;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_CONFIRMED;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_METHOD;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.USE_ACCOUNT;
import static za.co.velvetant.taxi.engine.models.FareState.ACCEPT_FARES;
import static za.co.velvetant.taxi.engine.models.FareState.ARRIVED_AT_DESTINATION;
import static za.co.velvetant.taxi.engine.models.FareState.ARRIVE_AT_PICKUP;
import static za.co.velvetant.taxi.engine.models.FareState.AWAIT_PAYMENT_CONFIRMATION;
import static za.co.velvetant.taxi.engine.models.FareState.CANCELLED_BY_ADMIN;
import static za.co.velvetant.taxi.engine.models.FareState.CAPTURE_FARE_AMOUNT;
import static za.co.velvetant.taxi.engine.models.FareState.GET_PICKED_UP;
import static za.co.velvetant.taxi.engine.models.FareState.PICK_UP_CUSTOMER;
import static za.co.velvetant.taxi.engine.models.FareState.RATE_DRIVER;
import static za.co.velvetant.taxi.engine.models.FareState.WAIT_FOR_DRIVER;
import static za.co.velvetant.taxi.engine.models.FareState.WAIT_FOR_DRIVERS;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CORPORATE_ACCOUNT;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CREDIT_CARD;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.FAMILY_ACCOUNT;
import static za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines.buildVo;
import static za.co.velvetant.taxi.engine.util.Strings.decode;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;
import static za.co.velvetant.taxi.engine.util.VoConversion.convertHack;

@Named
@Path("/fares.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FareServiceImpl implements FareService {

    private static final Logger log = LoggerFactory.getLogger(FareServiceImpl.class);

    @Inject
    private FareRepository fareRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private CreditCardRepository creditCardRepository;

    @Inject
    private FareEngine fareEngine;

    @Inject
    private RatingRepository ratingRepository;

    @Inject
    private DirectionsService directionsService;

    @Inject
    private DriverShiftRepository driverShiftRepository;

    @Inject
    private Dispatch dispatch;

    @Inject
    private MyGateTransactionRepository transaction;

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private Provider<FareFilter> fareFilter;

    @Inject
    private Provider<FareCompletedFilter> completedFilter;

    @Inject
    private Provider<FareActiveFilter> activeFilter;

    @Context
    private HttpHeaders headers;

    @Override
    @Transactional
    public Response find(final String uuid) {
        Response.ResponseBuilder response = Response.ok();

        za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        if (fare == null) {
            fare = fareRepository.findByReference(uuid);
        }
        NotFoundException.assertNotNull(fare, String.format("Could not find fare with specified uuid -> %s", uuid));

        final Fare trip = convert(fare);
        if (trip.getDriver() != null) {
            trip.getDriver().setRating(DriverServiceImpl.getRating(trip.getDriver().getUserId(), ratingRepository));
        }

        if (fare.getRewardPointLog() != null) {
            trip.setRewards(fare.getRewardPointLog().getAmount());
        }

        trip.setCustomer(buildVo(customerRepository.fetchWithCreditCard(fare.getCustomer().getUserId()), creditCardRepository, transactionRepository.findCustomersBalance(fare.getCustomer())));
        trip.setInvoiceAmount(transactionRepository.findByTripUuid(uuid).getCreditAmount().doubleValue());
        response = response.entity(trip);

        return response.build();
    }

    @Override
    @Transactional
    public Response findActive(final String customer, final String registration, final String family, final long limit) {
        final FareActiveFilter filteredFares = activeFilter.get().with(customer, decode(registration), family).sort(1, (int) limit, "requestTime", Sort.Direction.DESC).check(headers).filter();
        final List<Fare> fares = filteredFares.andConvert().vos();

        Response.ResponseBuilder response = Response.ok().entity(fares).header("X-Total-Entities", filteredFares.totalEntities());
        if (fares.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    @Transactional
    public Response findCompleted(final long startTime, final long endTime, final String customer, final String registration, final String family, final long limit, final Integer size,
            final String sort, final Sort.Direction order) {
        final FareCompletedFilter filteredFares = completedFilter.get().with(customer, decode(registration), family).sort(1, (int) limit, sort, order).check(headers).filter();
        final List<Fare> fares = filteredFares.andConvert().vos();

        Response.ResponseBuilder response = Response.ok().entity(fares).header("X-Total-Entities", filteredFares.totalEntities());
        if (fares.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    @Transactional
    public Response findByCustomer(final String customer, final String grouped, final int limit) {
        return findAll(null, null, customer, null, null, null, null, false, false, 1, limit, grouped, Sort.Direction.DESC);
    }

    @Override
    @Transactional
    public Response findAll(final String uuid, final String reference, final String customerEmail, final String customerCellphone, final String registration, final String family,
            final Boolean active, final Boolean enroute, final Boolean exclusive, final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        final FareFilter filteredFares = fareFilter.get().with(uuid, reference, customerEmail, customerCellphone, decode(registration), family, active, enroute, exclusive)
                .sort(page, size, sort, order).filter();
        final List<Fare> fares = filteredFares.andConvert().vos();

        Response.ResponseBuilder response = Response.ok().entity(fares).header("X-Total-Entities", filteredFares.totalEntities());
        if (fares.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    /** BPMN **/

    @Override
    public Response request(final String userid, final FareRequest fareRequest) {
        final Customer customer = customerRepository.findByUserId(userid);
        NotFoundException.assertNotNull(customer, String.format("Could not find customer with user Id: %s", userid));

        String uuid = null;
        List<String> trips = fareEngine.current(userid);
        if (trips.isEmpty()) {
            fareRequest.setCustomer(userid);

            uuid = UUID.randomUUID().toString();
            log.debug("Fare request received from customer {}, generated UUID: {}", fareRequest.getCustomer(), uuid);

            fareEngine.start(uuid, fareRequest);
        } else {
            for (String trip : trips) {
                log.warn("Customer already has an active trip: {}", trip);
                uuid = trip;
            }
        }

        return Response.ok().entity(uuid).build();
    }

    @Override
    public Response nextTasks(final String uuid, final String userid) {
        try {
            return Response.ok().entity(fareEngine.nextTasks(uuid, userid)).build();
        } catch (final IllegalArgumentException iae) {
            throw new NotFoundException(String.format("No tasks found for [%s] with fare uuid: %s", userid, uuid));
        }
    }

    @Override
    public Response accept(final String uuid, final String userid) {
        final Map<String, Object> variables = new HashMap<>(1);
        variables.put(ACCEPTED_DRIVER, userid);
        completeOnlyAction(uuid, ACCEPT_FARES, userid, variables);

        final Customer customer = fareRepository.findByUUID(uuid).getCustomer();
        fareEngine.completeTask(uuid, WAIT_FOR_DRIVERS, customer.getUserId());

        return ok();
    }

    @Override
    public Response arriveAtPickup(final String uuid, final String userid) {
        completeOnlyAction(uuid, ARRIVE_AT_PICKUP, userid);

        final Customer customer = fareRepository.findByUUID(uuid).getCustomer();
        fareEngine.completeTask(uuid, WAIT_FOR_DRIVER, customer.getUserId());
        return ok();
    }

    @Override
    @Transactional
    public Response pickup(final String uuid, final String userid) {
        completeOnlyAction(uuid, PICK_UP_CUSTOMER, userid);

        final Customer customer = fareRepository.findByUUID(uuid).getCustomer();
        fareEngine.completeTask(uuid, GET_PICKED_UP, customer.getUserId());
        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        fare.setPickUpTime(new Date(System.currentTimeMillis()));
        fareRepository.save(fare);
        return ok();
    }

    @Override
    @Transactional
    public Response cancel(final String uuid, final String userid) {
        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        if (isCancelled(fare)) {
            throw new PreconditionFailedException("Fare is already cancelled");
        }
        fare.setState(FareState.CANCELLED_BY_CUSTOMER);
        fareRepository.save(fare);

        fareEngine.sendSignal(uuid, CANCEL_FARE);

        return Response.ok().entity("OK").build();
    }

    @Override
    @Transactional
    public Response driverCancel(final String uuid, final String userid) {
        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        if (isCancelled(fare)) {
            throw new PreconditionFailedException("Fare is already cancelled");
        }
        fare.setState(FareState.CANCELLED_BY_DRIVER);
        fareRepository.save(fare);

        fareEngine.sendSignal(uuid, CANCEL_FARE);

        return ok();
    }

    @Override
    @Transactional
    public Response opsCancel(final String uuid, final String userid) {
        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        if (isCancelled(fare)) {
            throw new PreconditionFailedException("Fare is already cancelled");
        }
        fare.setState(FareState.CANCELLED_BY_ADMIN);
        fareRepository.save(fare);

        fareEngine.sendSignal(uuid, CANCEL_FARE);

        return ok();
    }

    @Override
    @Transactional
    public Response arrived(final String uuid, final String userid, final Location destination) {
        if (destination != null) {
            final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);

            fare.setDropOff(convert(destination));

            // TODO - PERF Opportunity to optimize here. Address call to Google can be moved to enable async call
            if (destination.getAddress() == null) {
                final String address = directionsService.getAddress(destination);
                if (fare.getDropOff() != null) {
                    fare.getDropOff().setAddress(address);
                }
            }

            final Routes route = directionsService.get(convert(fare.getPickup()), destination).getFirstRoute();
            if (route != null && route.getFirstLeg() != null) {
                fare.setDistance(route.getFirstLeg().getDistance().getValue());
                fare.setDuration(route.getFirstLeg().getDuration().getValue());
            }
            fareRepository.save(fare);
        }
        completeOnlyAction(uuid, ARRIVED_AT_DESTINATION, userid);

        final Customer customer = fareRepository.findByUUID(uuid).getCustomer();
        fareEngine.completeTask(uuid, ARRIVED_AT_DESTINATION, customer.getUserId());

        return ok();
    }

    @Override
    @Transactional
    public Response amountOverride(final String uuid, final String userid) {
        log.debug("[{}] is capturing the fare amount on behalf of the driver for fare [{}]", userid, uuid);

        // These get used to 'break out' of the default process and let an ops user take control
        final Map<String, Object> data = new HashMap<>();
        data.put(OPS_OVERRIDDEN, true);
        data.put(OPS_USER, userid);

        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        try {
            completeOnlyAction(uuid, CAPTURE_FARE_AMOUNT, fare.getDriver().getUserId(), data);
            completeOnlyAction(uuid, CAPTURE_FARE_AMOUNT, fare.getCustomer().getUserId());
        } catch (NotFoundException e) {
            log.warn("Trying to override capture trip amount that is already overridden", e);
        }

        return ok();
    }

    @Override
    @Transactional
    public Response amount(final String uuid, final String userid, final Double amount, PaymentMethod paymentMethod) {
        final Map<String, Object> data = new HashMap<>();
        data.put(PAYMENT_AMOUNT, amount);

        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);

        final Customer customer = customerRepository.findByUserId(fare.getCustomer().getUserId());
        final List<String> allowedMethods = CustomerRoutines.getPaymentMethods(customer, creditCardRepository);

        if (paymentMethod != null) {
            // driver changing to account when it was previously account = keep one that was on fare.
            if (paymentMethod != PaymentMethod.CASH && fare.getPaymentMethod() != za.co.velvetant.taxi.engine.models.PaymentMethod.CASH) {
                paymentMethod = PaymentMethod.valueOf(fare.getPaymentMethod().toString());
            }

            // driver changing to account when it was previously CASH, pick a valid account type.
            if (fare.getPaymentMethod() == za.co.velvetant.taxi.engine.models.PaymentMethod.CASH && paymentMethod != PaymentMethod.CASH) {
                if (allowedMethods.contains(CREDIT_CARD.name())) {
                    paymentMethod = PaymentMethod.CREDIT_CARD;
                } else if (allowedMethods.contains(PaymentMethod.FAMILY_ACCOUNT.name())) {
                    paymentMethod = PaymentMethod.FAMILY_ACCOUNT;
                } else if (allowedMethods.contains(PaymentMethod.CORPORATE_ACCOUNT.name())) {
                    paymentMethod = PaymentMethod.CORPORATE_ACCOUNT;
                }
            }

            // method should be valid now.
            if (!allowedMethods.contains(paymentMethod.name())) {
                throw new PreconditionFailedException(String.format("Customer is not allowed payment method of type %s", paymentMethod.name()));
            }

            data.put(PAYMENT_METHOD, za.co.velvetant.taxi.engine.models.PaymentMethod.valueOf(paymentMethod.toString()));
        }

        if (paymentMethod != null) {
            fare.setPaymentMethod(za.co.velvetant.taxi.engine.models.PaymentMethod.valueOf(paymentMethod.toString()));
            fareRepository.save(fare);
        }

        completeOnlyAction(uuid, CAPTURE_FARE_AMOUNT, userid, data);
        completeOnlyAction(uuid, CAPTURE_FARE_AMOUNT, customer.getUserId());

        return ok();
    }

    @Override
    @Transactional
    public Response confirmPaymentOverride(final String uuid, final String userid) {
        log.debug("[{}] is overriding credit card payment confirmation for fare [{}]", userid, uuid);

        // These get used to 'break out' of the default process and let an ops user take control
        final Map<String, Object> data = new HashMap<>();
        data.put(OPS_OVERRIDDEN, true);
        data.put(OPS_USER, userid);

        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        try {
            completeOnlyAction(uuid, AWAIT_PAYMENT_CONFIRMATION, fare.getDriver().getUserId());
            completeOnlyAction(uuid, AWAIT_PAYMENT_CONFIRMATION, fare.getCustomer().getUserId(), data);
        } catch (NotFoundException e) {
            log.warn("Trying to override confirm payment that is already overridden", e);
        }

        return ok();
    }

    @Override
    @Transactional
    public Response confirmPayment(final String uuid, final String userid, final Boolean confirmed, final PaymentMethod paymentMethod, final Double tip, final String cvv, final Boolean account) {
        log.debug("Confirming [{}] payment using account [{}] for trip [{}] with a tip of {}", confirmed, account, uuid, tip);

        Response.ResponseBuilder response = Response.ok();
        final Map<String, Object> data = new HashMap<>();
        data.put(PAYMENT_CONFIRMED, confirmed);

        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        if (confirmed) {
            final List<String> allowedMethods = CustomerRoutines.getPaymentMethods(fare.getCustomer(), creditCardRepository);

            if (paymentMethod != null) {
                if (!allowedMethods.contains(paymentMethod.name())) {
                    throw new PreconditionFailedException(String.format("You are not allowed payment method of type %s", paymentMethod.name()));
                }

                data.put(PAYMENT_METHOD, za.co.velvetant.taxi.engine.models.PaymentMethod.valueOf(paymentMethod.toString()));
                fare.setPaymentMethod(za.co.velvetant.taxi.engine.models.PaymentMethod.valueOf(paymentMethod.toString()));
            }

            if (fare.getPaymentMethod() == CREDIT_CARD ||
                    fare.getPaymentMethod() == FAMILY_ACCOUNT ||
                    fare.getPaymentMethod() == CORPORATE_ACCOUNT) {
                fare.setTip(tip);
                fareRepository.save(fare);

                data.put(CVV, cvv);
                data.put(USE_ACCOUNT, account);
                final Double amount = (Double) data.get(PAYMENT_AMOUNT);
                if (amount != null) {
                    data.put(PAYMENT_AMOUNT, amount);
                }
            }
        }

        completeOnlyAction(uuid, AWAIT_PAYMENT_CONFIRMATION, userid, data);
        if (fare.isOverriddenOn(AWAIT_PAYMENT_CONFIRMATION)) {
            log.debug("Trip has been overridden on [{}], completing customer task", AWAIT_PAYMENT_CONFIRMATION);
            completeOnlyAction(uuid, AWAIT_PAYMENT_CONFIRMATION, fare.getCustomer().getUserId());
        } else {
            completeOnlyAction(uuid, AWAIT_PAYMENT_CONFIRMATION, fare.getDriver().getUserId());
        }

        log.debug("Checking if processing payment failed for trip: {}", uuid);
        MyGateTransaction lastFailedTransaction = lastFailedTransaction(fare);
        if (lastFailedTransaction != null) {
            log.debug("Transaction failed for trip [{}] with message: {}", uuid, lastFailedTransaction.getErrorMessage());
            response = Response.status(NOT_ACCEPTABLE).entity(lastFailedTransaction.getErrorMessage());
        }

        return response.build();
    }

    @Override
    public Response rate(final String uuid, final String userid, final Rating rating) {
        final Map<String, Object> data = new HashMap<>();
        data.put(DRIVER_RATING, rating);
        completeOnlyAction(uuid, RATE_DRIVER, userid, data);

        return ok();
    }

    @Override
    public Response diagram(final String uuid) {
        Response.ResponseBuilder response = Response.ok();

        final InputStream diagram = fareEngine.diagram(uuid);
        if (diagram == null) {
            response = Response.noContent();
        } else {
            response = response.entity(diagram);
        }

        return response.build();
    }

    @Override
    @Transactional
    public Response removeFare(final String uuid, final String reason) {
        fareEngine.removeFare(uuid, reason);

        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        fare.setState(CANCELLED_BY_ADMIN);
        fareRepository.save(fare);

        dispatch.removeFare(uuid);
        final Driver driver = fare.getDriver();
        if (driver != null) {
            final DriverShift shift = driverShiftRepository.findActiveDriverShiftWithDriverUserId(driver.getUserId());
            if (shift != null) {
                dispatch.makeDriverAvailable(shift.getShiftIdentifier());
            }
        }

        return ok();
    }

    @Override
    @Transactional
    public Response addStop(final String uuid, final Location location) {
        log.debug("Adding stop to fare [{}]: {}", uuid, location);

        final za.co.velvetant.taxi.engine.models.Fare fare = fareRepository.findByUUID(uuid);
        fare.addStop(new StopLocation(location.getLatitude().doubleValue(), location.getLongitude().doubleValue(), location.getAddress()));
        fareRepository.save(fare);

        return ok();
    }

    private Response ok() {
        return Response.ok().entity("{}").build();
    }

    private void completeOnlyAction(final String uuid, final FareState fareState, final String userid) {
        completeOnlyAction(uuid, fareState, userid, null);
    }

    private void completeOnlyAction(final String uuid, final FareState fareState, final String userid, final Map<String, Object> data) {
        try {
            fareEngine.completeTask(uuid, fareState, userid, data);
        } catch (final IllegalArgumentException iae) {
            log.error("Complete action failed", iae);
            throw new NotFoundException(iae.getMessage());
        }
    }

    private boolean isCancelled(final za.co.velvetant.taxi.engine.models.Fare fare) {
        if (fare.getState() == null) {
            return false;
        }
        switch (fare.getState()) {
            case CANCELLED:
            case CANCELLED_BY_DRIVER:
            case CANCELLED_BY_ADMIN:
            case CANCELLED_BY_CUSTOMER:
            case TIMED_OUT:
            case OUT_OF_SERVICE_AREA:
                return true;
            default:
                return false;
        }
    }

    private MyGateTransaction lastFailedTransaction(final za.co.velvetant.taxi.engine.models.Fare fare) {
        log.debug("Checking for failed transaction for trip: {}", fare.getUuid());
        
        MyGateTransaction failedTransaction = null;
        List<MyGateTransaction> lastTransactions = transaction.findLastTransaction(fare);
        if (!lastTransactions.isEmpty()) {
            MyGateTransaction myGateTransaction = lastTransactions.iterator().next();
            log.debug("Found a prior transaction for trip: {}", fare.getUuid());
            if (!myGateTransaction.isSuccessful()) {
                log.debug("Prior transaction for trip [{}] had failed: {}", fare.getUuid(), myGateTransaction.getErrorMessage());
                failedTransaction = myGateTransaction;
            }
        }

        return failedTransaction;
    }
}
