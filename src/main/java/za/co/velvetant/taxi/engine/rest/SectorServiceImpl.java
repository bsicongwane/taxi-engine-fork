package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.api.Sector;
import za.co.velvetant.taxi.engine.persistence.SectorRepository;

@Named
@Path("/sectors.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class SectorServiceImpl implements SectorService {

    @Inject
    private SectorRepository sectorRepository;

    @Override
    public Response create(final Sector sector) {
        za.co.velvetant.taxi.engine.models.Sector entity = convert(sector, new za.co.velvetant.taxi.engine.models.Sector());
        entity = sectorRepository.save(entity);
        return Response.status(Response.Status.CREATED).entity(convert(entity, new Sector())).build();
    }

    @Override
    public Response edit(final String name, final Sector sector) {
        final za.co.velvetant.taxi.engine.models.Sector currentSector = sectorRepository.findByName(name);
        currentSector.setName(sector.getName());
        final za.co.velvetant.taxi.engine.models.Sector updatedSector = sectorRepository.save(currentSector);

        return Response.ok().entity(convert(updatedSector, new Sector())).build();
    }

    @Override
    public Response find(final String name) {
        return Response.ok().entity(convert(sectorRepository.findByName(name), new Sector())).build();
    }

    @Override
    public Response findAll() {
        final List<Sector> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Sector sector : sectorRepository.findAll()) {
            vos.add(convert(sector, new Sector()));
        }
        return Response.ok().entity(vos).build();
    }

    @Override
    public String count() {
        return String.valueOf(sectorRepository.count());
    }

}
