package za.co.velvetant.taxi.engine.rest;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.springframework.data.domain.Sort;
import za.co.velvetant.taxi.engine.api.ChangePasswordDetails;
import za.co.velvetant.taxi.engine.api.User;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
@Api(value = "/users", description = "Operations for managing users.")
@RolesAllowed({ "ADMINISTRATOR", "SUPERVISOR" })
public interface UserService {

    String USER_CLASS = "za.co.velvetant.taxi.engine.api.User";

    @POST
    @ApiOperation(value = "Create a new user", responseClass = USER_CLASS)
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 409, reason = DocumentationMessages.E_409 + "because user with same email already exists") })
    Response create(@ApiParam(value = "User to register", required = true) User user);

    @PUT
    @Path("/{email}")
    @ApiOperation(value = "Update a users details.")
    @ApiErrors({ @ApiError(code = 412, reason = DocumentationMessages.E_412), @ApiError(code = 409, reason = DocumentationMessages.E_409 + "because user with same email already exists") })
    Response edit(@ApiParam(value = "email", required = true) @PathParam("email") String email, @ApiParam(value = "User details update", required = true) User user);

    @DELETE
    @Path("/{email}")
    @ApiOperation(value = "Remove the user", notes = "User details are not permanently removed and can still be retrieved")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400) })
    Response remove(@ApiParam(value = "Email address of user to remove.", required = true) @PathParam("email") String email);

    @HEAD
    @Path("/{email}")
    @ApiOperation(value = "Activate a deactivated user.")
    Response activate(@ApiParam(value = "email", required = true) @PathParam("email") String email);

    @GET
    @Path("/{email}")
    @ApiOperation(value = "Search for a user by email address", responseClass = USER_CLASS)
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400) })
    @RolesAllowed({ "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    User find(@ApiParam(value = "Email address of user to find") @PathParam("email") String email);

    @POST
    @Path("/{email}/resetPassword")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "WEB_USER" })
    @ApiOperation(value = "Reset a user's password. The user is emailed their new password.")
    Response resetPassword(@ApiParam(value = "email", required = true) @PathParam("email") String email);

    @GET
    @ApiOperation(value = "Find all customers.", multiValueResponse = true, responseClass = USER_CLASS, notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    Response findAll(
            @ApiParam(value = "Filter by first name. Partial filtering is allowed.") @QueryParam("firstName") String firstName,
            @ApiParam(value = "Filter by last name. Partial filtering is allowed.") @QueryParam("lastName") String lastName,
            @ApiParam(value = "Filter by cellphone number. Partial filtering is allowed.") @QueryParam("cellphone") String cellphone,
            @ApiParam(value = "Filter by email address. Partial filtering is allowed.") @QueryParam("email") String email,
            @ApiParam(value = "Filter by active/inactive users.", defaultValue = "true") @QueryParam("active") @DefaultValue("true") Boolean active,
            @ApiParam(value = "Page number of users to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number users to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "lastName", allowableValues = "firstName,lastName,cellphone,email") @QueryParam("sort") @DefaultValue("lastName") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

    @GET
    @Path("/{from}/{to}")
    List<User> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to);

    @GET
    @Path("/count")
    @Produces("text/plain")
    String count();

    @PUT
    @Path("/{userid}/password")
    @ApiOperation(value = "Change a User's password.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "DRIVER" })
    Response changePassword(@ApiParam(value = "UserId", required = true) @PathParam("userid") String userid, @ApiParam(value = "New password details.") ChangePasswordDetails details);

}
