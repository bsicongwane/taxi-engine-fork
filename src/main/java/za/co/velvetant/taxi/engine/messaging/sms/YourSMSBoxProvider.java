package za.co.velvetant.taxi.engine.messaging.sms;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_BOX_KEY;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_BOX_URL;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.api.Sms;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

@Named
public class YourSMSBoxProvider implements SmsProvider {

    private static final Logger log = LoggerFactory.getLogger(YourSMSBoxProvider.class);

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    @Override
    public void send(String cellphone, String content, String reference) {
        log.debug("Sending SMS message [{}] to [{}] with reference: {}", content, cellphone, reference);
        final Sms.Message message = new Sms.Message(cellphone, reference, content);
        final Sms smsMessage = new Sms(message);

        if (!Boolean.valueOf(System.getProperty("sms.disabled"))) {
            final String urlProperty = systemPropertyHelper.getProperty(SMS_BOX_URL);
            final String apiKeyProperty = systemPropertyHelper.getProperty(SMS_BOX_KEY);

            final Client client = Client.create();
            client.resource(format("%s%s", urlProperty, "/send")).queryParam("api", apiKeyProperty).accept(APPLICATION_JSON).type(APPLICATION_JSON).entity(smsMessage).post(ClientResponse.class);
            log.debug("Sent SMS: {}", message);
        } else {
            log.warn("SMS messages are disabled. Message will NOT be sent: {}", message);
        }
    }
}
