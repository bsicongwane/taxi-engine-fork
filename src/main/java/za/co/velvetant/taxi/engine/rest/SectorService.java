package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_400;
import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.E_409;
import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.M_SECTOR_NAME;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import za.co.velvetant.taxi.engine.api.Sector;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/sectors")
@Api(value = "/sectors", description = "Services for managing sectors.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({ "CUSTOMER", "DRIVER" })
public interface SectorService {

    String SECTOR_CLASS = "za.co.velvetant.taxi.engine.api.Sector";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Registers a new sector in the system.", responseClass = SECTOR_CLASS)
    @ApiErrors({ @ApiError(code = 400, reason = E_400), @ApiError(code = 409, reason = E_409 + " Sector with same name already exists") })
    Response create(@ApiParam(value = "Sector to create", required = true) Sector sector);

    @PUT
    @Path("/{name}")
    @ApiOperation(value = "Updates a sector's details.", responseClass = SECTOR_CLASS)
    Response edit(@ApiParam(value = M_SECTOR_NAME, required = true) @PathParam("name") String name, @ApiParam(value = "Sector details update", required = true) Sector sector);

    @GET
    @Path("/{name}")
    @ApiOperation(value = "Finds a sector given the sector's name.", responseClass = SECTOR_CLASS)
    Response find(@ApiParam(value = M_SECTOR_NAME, required = true) @PathParam("name") String name);

    @GET
    @ApiOperation(value = "Finds all the sectors in the system.", multiValueResponse = true, responseClass = SECTOR_CLASS)
    Response findAll();

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    String count();

}
