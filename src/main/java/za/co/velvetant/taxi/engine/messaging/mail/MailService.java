package za.co.velvetant.taxi.engine.messaging.mail;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MAIL_FROM;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MAIL_FROM_NAME;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MANDRILL_KEY;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MANDRILL_SEND_URL;
import static za.co.velvetant.taxi.engine.util.Strings.noNullString;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.messaging.mail.mandrill.Mailer;
import za.co.velvetant.taxi.engine.messaging.mail.mandrill.MandrillConfig;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.Person;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

@Named
@Async
public class MailService {

    private static final Logger log = LoggerFactory.getLogger(MailService.class);
    private static final DecimalFormat decimalFormat = new DecimalFormat("#0.00");

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    @Inject
    private PaymentService paymentService;

    public void sendPasswordResetEmail(final Customer customer, final String newPassword) {
        final MandrillConfig loadConfig = loadConfig();
        final Mailer mailer = new Mailer("passwordReset", customer.getEmail(), format("%s %s", customer.getFirstName(), customer.getLastName()), "Password successfully reset", loadConfig);
        mailer.addMergeVar("newPassword", newPassword);
        mailer.send();

    }

    public void sendPasswordResetEmail(final Person user, final String newPassword) {
        final MandrillConfig loadConfig = loadConfig();
        final Mailer mailer = new Mailer("passwordReset", user.getEmail(), format("%s %s", user.getFirstName(), user.getLastName()), "Password successfully reset", loadConfig);
        mailer.addMergeVar("newPassword", newPassword);
        mailer.send();

    }

    public void sendInvoiceEmail(final Fare fare, final String gatewayId) {
        final MandrillConfig loadConfig = loadConfig();
        final String name = format("%s %s", noNullString(fare.getCustomer().getFirstName()), noNullString(fare.getCustomer().getLastName()));
        final Mailer mailer = new Mailer("snappcabinvoice", noNullString(fare.getCustomer().getEmail()), name, "SnappCab Invoice", loadConfig);
        mailer.addMergeVar("SUBJECT", "SnappCab Invoice");
        mailer.addMergeVar("InvoiceNumber", fare.getReference());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        mailer.addMergeVar("InvoiceDateTime", simpleDateFormat.format(new Date(System.currentTimeMillis())));
        mailer.addMergeVar("CustomerName", name);
        mailer.addMergeVar("PickupDate", simpleDateFormat.format(fare.getPickUpTime()));

        mailer.addMergeVar("PickUp", noNullString(fare.getPickup().getAddress()));
        mailer.addMergeVar("DropOff", noNullString(fare.getDropOff().getAddress()));
        mailer.addMergeVar("CreditCardMask", noNullString(getCreditCardNumber(gatewayId)));
        mailer.addMergeVar("Driver", format("%s %s", fare.getDriver().getFirstName(), fare.getDriver().getLastName()));
        mailer.addMergeVar("Vehicle", fare.getTaxi().getRegistration());

        final String billed = formatMoney(fromNullable(fare.getAmount()).or(0.0) + fromNullable(fare.getTip()).or(0.0));
        mailer.addMergeVar("TotalCharge", billed);
        mailer.addMergeVar("TripAmount", formatMoney(fare.getAmount()));
        mailer.addMergeVar("Tip", formatMoney(fare.getTip()));
        mailer.addMergeVar("SubTotal", billed);
        mailer.addMergeVar("Discount", "R0.00");
        mailer.addMergeVar("TripTotal", billed);
        mailer.addMergeVar("BilledAmount", billed);
        mailer.addMergeVar("Oustanding", "R0.00");
        mailer.send();

    }

    protected MandrillConfig loadConfig() {

        final String key = systemPropertyHelper.getProperty(MANDRILL_KEY);
        final String template = systemPropertyHelper.getProperty(MANDRILL_SEND_URL);
        final String mailFrom = systemPropertyHelper.getProperty(MAIL_FROM);
        final String mailFromName = systemPropertyHelper.getProperty(MAIL_FROM_NAME);

        return new MandrillConfig(template, key, mailFrom, mailFromName, null);
    }

    private String getCreditCardNumber(final String gatewayId) {
        String creditCardNumber = EMPTY;
        try {
            creditCardNumber = paymentService.getMaskedCardDetails(gatewayId).getCardNumber();
        } catch (Exception e) {
            log.error("Could not get masked credit card number", e);
        }

        return creditCardNumber;
    }

    private String formatMoney(final Double value) {
        if (value == null) {
            return "R0.00";
        }

        return "R" + decimalFormat.format(value);

    }
}
