package za.co.velvetant.taxi.engine.rest;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;

@Path("/statistics")
@Api(value = "/statistics", description = "Services for retrieving statistics.")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RolesAllowed({"ADMINISTRATOR", "SUPERVISOR"})
public interface StatisticsService {

    @GET
    @ApiOperation(value = "Returns the running totals for various metrics optionally limited to affiliates for a date range.", responseClass = "za.co.velvetant.taxi.statistics.Totals")
    Response totals(@ApiParam(value = "The affiliates registration", required = false) @QueryParam("registration") String registration,
                    @ApiParam(value = "Start date of range", required = false) @QueryParam("from") Long from,
                    @ApiParam(value = "To date of range", required = false) @QueryParam("to") Long to);

    @GET
    @Path("/customer/{userid}")
    @ApiOperation(value = "Returns the running totals for various customer metrics.", responseClass = "za.co.velvetant.taxi.statistics.CustomerTotals")
    Response customerTotals(@ApiParam(value = "The customers userId", required = true) @PathParam("userid") String userid);

}
