package za.co.velvetant.taxi.engine.messaging.sms;

import static java.lang.String.format;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_ADMIN;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_ADMIN_FOR_DRIVER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_CUSTOMER_FOR_DRIVER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_CANCELLED_BY_DRIVER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_COUPON_REDEEMED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_DRIVER_ARRIVED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_DRIVER_PAYMENT_FAILED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_DRIVER_PAYMENT_SUCCESSFUL;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_NEW_USER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_PIN_RESET;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.models.Taxi;
import za.co.velvetant.taxi.engine.models.coupons.Coupon;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

import java.math.BigDecimal;

@Named
@Async
public class SmsMessageService implements SmsService {

    private static final Logger log = LoggerFactory.getLogger(SmsMessageService.class);

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    @Inject
    private SmsProvider smsProvider;

    @Override
    public void addNewUserMessage(final Customer customer) {
        final String content = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_NEW_USER, customer.getFirstName(), customer.getLastName(), customer.getPin());
        smsProvider.send(customer.getCellphone(), content, format("N-%s", customer.getUserId()));
    }

    @Override
    public void sendPinResetMessage(final Customer customer) {
        final String content = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_PIN_RESET, customer.getFirstName(), customer.getLastName(), customer.getPin());
        smsProvider.send(customer.getCellphone(), content, format("R-%s", customer.getUserId()));
    }

    @Override
    public void sendDriverArrivedMessage(final Customer customer, final Driver driver, final Taxi taxi) {
        final String content = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_DRIVER_ARRIVED, customer.getFirstName(), customer.getLastName(), driver.getFirstName(), taxi.getMake(),
                taxi.getModel(), taxi.getRegistration());
        smsProvider.send(customer.getCellphone(), content, format("A-%s", customer.getUserId()));
    }

    @Override
    public void sendCancelledMessage(final Fare fare) {
        final Customer customer = fare.getCustomer();
        final Driver driver = fare.getDriver();
        String customerMessage;
        String driverMessage;
        switch (fare.getState()) {
            case CANCELLED_BY_ADMIN: {
                customerMessage = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_CANCELLED_BY_ADMIN, customer.getFirstName(), customer.getLastName(), fare.getReference());
                smsProvider.send(customer.getCellphone(), customerMessage, format("F-%s", fare.getReference()));
                driverMessage = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_CANCELLED_BY_ADMIN_FOR_DRIVER, driver.getFirstName(), driver.getLastName(), fare.getReference());
                smsProvider.send(driver.getCellphone(), driverMessage, format("F-%s", fare.getReference()));
            }
                break;
            case CANCELLED_BY_DRIVER: {
                customerMessage = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_CANCELLED_BY_DRIVER, customer.getFirstName(), customer.getLastName(), fare.getReference(),
                        driver.getFirstName(), driver.getLastName());
                smsProvider.send(customer.getCellphone(), customerMessage, format("F-%s", fare.getReference()));
            }
                break;
            case CANCELLED_BY_CUSTOMER: {
                driverMessage = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_CANCELLED_BY_CUSTOMER_FOR_DRIVER, driver.getFirstName(), driver.getLastName(), customer.getFirstName(),
                        customer.getLastName(), fare.getReference());
                smsProvider.send(driver.getCellphone(), driverMessage, format("F-%s", fare.getReference()));
            }
                break;
            default:
                log.debug("Fare state does not need SMS. Not sending.");
        }
    }

    @Override
    public void sendPaymentConfirmation(final Fare fare, final MyGateTransaction transaction) {
        log.debug("Sending payment confirmation SMS for fare: {}", fare.getReference());
        String content;
        final Driver driver = fare.getDriver();
        if (transaction.isSuccessful()) {
            content = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_DRIVER_PAYMENT_SUCCESSFUL, driver.getFirstName(), driver.getLastName(), fare.getAmount(), fare.getTip(), (fare.getAmount() + fare.getTip()), fare.getReference());
        } else {
            content = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_DRIVER_PAYMENT_FAILED, driver.getFirstName(), driver.getLastName(), fare.getAmount(), fare.getTip(), (fare.getAmount() + fare.getTip()), fare.getReference());
        }

        smsProvider.send(fare.getDriver().getCellphone(), content, format("P-%s", driver.getUserId()));
    }

    @Override
    public void sendCouponRedeemedMessage(final CustomerCoupon customerCoupon, final BigDecimal balance) {
        log.debug("Sending coupon [{}] redeemed SMS for customer: {}", customerCoupon.getUuid(), customerCoupon.getCustomer().getUserId());

        final Customer customer = customerCoupon.getCustomer();
        final Coupon coupon = customerCoupon.getCoupon();
        final String content = systemPropertyHelper.getAndFormatProperty(SMS_CONTENT_COUPON_REDEEMED, customer.getFirstName(), coupon.getCouponNumber(), coupon.getMonetaryLimit(), balance.doubleValue());
        smsProvider.send(customer.getCellphone(), content, format("C-%s", customer.getUserId()));
    }
}
