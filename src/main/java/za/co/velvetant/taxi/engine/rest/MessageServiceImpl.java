package za.co.velvetant.taxi.engine.rest;

import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.api.common.DTOComparator;
import za.co.velvetant.taxi.engine.api.Message;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.persistence.MessageRepository;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
@Path("/messages.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class MessageServiceImpl implements MessageService {

    @Inject
    private MessageRepository messageRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Override
    public Response find(final String userid, final String uuid) {
        final Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        final za.co.velvetant.taxi.engine.models.Message message = messageRepository.findByUuid(uuid);
        NotFoundException.assertNotNull(message, String.format("Message with uuid %s was not found", uuid));
        final Message convert = convert(message, new Message());
        convert.setCustomer(customer.getUserId());
        convert.setStatus(message.getStatus().name());
        convert.setTag(message.getTag().name());
        convert.setCreatedTimestamp(message.getTime().getTime());
        return Response.ok().entity(convert).build();
    }

    @Override
    public Response findAll(final String userId) {
        final List<Message> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.Message message : messageRepository.findByCustomerUserId(userId)) {
            final Message convert = convert(message, new Message());
            convert.setCustomer(userId);
            convert.setStatus(message.getStatus().name());
            convert.setTag(message.getTag().name());
            convert.setCreatedTimestamp(message.getTime().getTime());
            vos.add(convert);
        }
        Collections.sort(vos, new DTOComparator("createdTimestamp", false));
        return Response.ok().entity(vos).build();
    }

    @Override
    public Response remove(final String userid, final String uuid) {
        final za.co.velvetant.taxi.engine.models.Message message = messageRepository.findByUuid(uuid);
        if (message != null) {
            messageRepository.delete(message);
            return Response.ok().build();
        }
        throw new NotFoundException(String.format("Message with uuid %s was not found", uuid));

    }

}
