package za.co.velvetant.taxi.engine.messaging.mymessages;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_CABS_NOT_AVAILABLE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_NEW_INVOICE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_NEW_USER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_PASSWORD_RESET;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_PROFILE_UPDATE;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MESSAGE_CONTENT_SERVICE_NOT_AVAILABLE;

import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Message;
import za.co.velvetant.taxi.engine.models.MessageStatus;
import za.co.velvetant.taxi.engine.models.MessageTag;
import za.co.velvetant.taxi.engine.persistence.MessageRepository;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

@Named
public class MessageService {

    @Inject
    private MessageRepository messageRepository;

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    public void addPasswordResetMessage(final Customer customer, final String newPassword) {
        final Message message = new Message();
        message.setContent(systemPropertyHelper.getAndFormatProperty(MESSAGE_CONTENT_PASSWORD_RESET, newPassword));
        message.setCustomer(customer);
        message.setStatus(MessageStatus.SENT);
        message.setSubject("Password reset successful");
        message.setTag(MessageTag.PASSWORD_RESET);
        message.setTime(new Date());
        message.setUuid(UUID.randomUUID().toString());
        messageRepository.save(message);
    }

    public void addNewUserMessage(final Customer customer) {
        final Message message = new Message();
        message.setContent(systemPropertyHelper.getAndFormatProperty(MESSAGE_CONTENT_NEW_USER, customer.getFirstName(), customer.getLastName()));
        message.setCustomer(customer);
        message.setStatus(MessageStatus.SENT);
        message.setSubject("Account created successfully");
        message.setTag(MessageTag.NEW_ACCOUNT);
        message.setTime(new Date());
        message.setUuid(UUID.randomUUID().toString());
        messageRepository.save(message);
    }

    public void addNewInvoiceMessage(final Customer customer) {
        final Message message = new Message();
        message.setContent(systemPropertyHelper.getAndFormatProperty(MESSAGE_CONTENT_NEW_INVOICE, customer.getFirstName(), customer.getLastName()));
        message.setCustomer(customer);
        message.setStatus(MessageStatus.SENT);
        message.setSubject("Invoice created");
        message.setTag(MessageTag.INVOICE);
        message.setTime(new Date());
        message.setUuid(UUID.randomUUID().toString());
        messageRepository.save(message);
    }

    public void addProfileUpdateMessage(final Customer customer) {
        final Message message = new Message();
        message.setContent(systemPropertyHelper.getAndFormatProperty(MESSAGE_CONTENT_PROFILE_UPDATE, customer.getFirstName(), customer.getLastName()));
        message.setCustomer(customer);
        message.setStatus(MessageStatus.SENT);
        message.setSubject("Profile updated");
        message.setTag(MessageTag.PROFILE_UPDATE);
        message.setTime(new Date());
        message.setUuid(UUID.randomUUID().toString());
        messageRepository.save(message);
    }

    public void addNoServiceMessage(final Customer customer) {
        final Message message = new Message();
        message.setContent(systemPropertyHelper.getAndFormatProperty(MESSAGE_CONTENT_SERVICE_NOT_AVAILABLE, customer.getFirstName(), customer.getLastName()));
        message.setCustomer(customer);
        message.setStatus(MessageStatus.SENT);
        message.setSubject("Service is not currently available");
        message.setTag(MessageTag.SERVICE_NOT_AVAILABLE);
        message.setTime(new Date());
        message.setUuid(UUID.randomUUID().toString());
        messageRepository.save(message);
    }

    public void addNoCabsMessage(final Customer customer) {
        final Message message = new Message();
        message.setContent(systemPropertyHelper.getAndFormatProperty(MESSAGE_CONTENT_CABS_NOT_AVAILABLE, customer.getFirstName(), customer.getLastName()));
        message.setCustomer(customer);
        message.setStatus(MessageStatus.SENT);
        message.setSubject("No cabs available");
        message.setTag(MessageTag.NO_CABS_AVAILABLE);
        message.setTime(new Date());
        message.setUuid(UUID.randomUUID().toString());
        messageRepository.save(message);
    }

}
