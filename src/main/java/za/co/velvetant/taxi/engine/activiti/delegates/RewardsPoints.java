package za.co.velvetant.taxi.engine.activiti.delegates;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.PaymentMethod;
import za.co.velvetant.taxi.engine.models.RewardPoint;
import za.co.velvetant.taxi.engine.models.RewardPointLog;
import za.co.velvetant.taxi.engine.persistence.RewardPointLogRepository;
import za.co.velvetant.taxi.engine.persistence.RewardPointRepository;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

import javax.inject.Inject;
import javax.inject.Named;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.REWARD_POINTS_PERECENTAGE;

@Named
public class RewardsPoints extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(RewardsPoints.class);

    @Inject
    private FareRepository fareRepository;

    @Inject
    private RewardPointRepository rewardPointRepository;

    @Inject
    private RewardPointLogRepository rewardPointLogRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    @Override
    public void process(final DelegateExecution execution) {
        log.debug("Calculating and allocating rewards points...");
        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        if (fare.getPaymentMethod() == PaymentMethod.CREDIT_CARD || fare.getPaymentMethod() == PaymentMethod.FAMILY_ACCOUNT || fare.getPaymentMethod() == PaymentMethod.CORPORATE_ACCOUNT) {
            Customer customer = fare.getCustomer();
            // TODO get formula
            if (customer.getRewardPoint() == null) {
                final RewardPoint rewardPoint = new RewardPoint();
                rewardPoint.setCustomer(customer);
                customer.setRewardPoint(rewardPointRepository.save(rewardPoint));
                customer = customerRepository.save(customer);
            }

            final String percentage = systemPropertyHelper.getProperty(REWARD_POINTS_PERECENTAGE);
            Double tip = 0.0;
            if (fare.getTip() != null) {
                tip = fare.getTip();
            }
            final long rewards = (long) ((fare.getAmount() + tip) * Double.parseDouble(percentage) * 0.01);
            log.debug("Rewards points: {}", rewards);

            customer.getRewardPoint().earn(rewards, fare);

            for (final RewardPointLog log : customer.getRewardPoint().getRewardPointLogs()) {
                rewardPointLogRepository.save(log);
            }
            rewardPointRepository.save(customer.getRewardPoint());
        }
    }
}
