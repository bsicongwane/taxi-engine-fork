package za.co.velvetant.taxi.engine.messaging.mail.mandrill;

public class MandrillConfig {

    private String mandrillTemplateUrl;
    private String mandrillKey;
    private String mailFrom;
    private String mailFromName;
    private String analyticsDomain;

    public MandrillConfig(final String mandrillTemplateUrl, final String mandrillKey, final String mailFrom, final String mailFromName, final String analyticsDomain) {
        super();
        this.mandrillTemplateUrl = mandrillTemplateUrl;
        this.mandrillKey = mandrillKey;
        this.mailFrom = mailFrom;
        this.mailFromName = mailFromName;
        this.analyticsDomain = analyticsDomain;
    }

    public String getAnalyticsDomain() {
        return analyticsDomain;
    }

    public void setAnalyticsDomain(final String analyticsDomain) {
        this.analyticsDomain = analyticsDomain;
    }

    public String getMandrillTemplateUrl() {
        return mandrillTemplateUrl;
    }

    public void setMandrillTemplateUrl(final String mandrillTemplateUrl) {
        this.mandrillTemplateUrl = mandrillTemplateUrl;
    }

    public String getMandrillKey() {
        return mandrillKey;
    }

    public void setMandrillKey(final String mandrillKey) {
        this.mandrillKey = mandrillKey;
    }


    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(final String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public String getMailFromName() {
        return mailFromName;
    }

    public void setMailFromName(final String mailFromName) {
        this.mailFromName = mailFromName;
    }

}
