package za.co.velvetant.taxi.engine.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.rest.exception.EngineException;

public final class ReflectionUtil {

    private static final Logger log = LoggerFactory.getLogger(ReflectionUtil.class);

    private ReflectionUtil() {
    }

    public static <T> T copySameNameFields(final Object from, final T to) {
        try {

            final List<Field> sourceFields = getAllDeclaredFields(from);
            final List<Field> targetFields = getAllDeclaredFields(to);

            for (final Field source : sourceFields) {
                for (final Field target : targetFields) {
                    if (source.getName().equalsIgnoreCase(target.getName())) {
                        setFieldValue(source, target, from, to);
                    }
                }
            }
        } catch (final Exception e) {
            throw new EngineException(e);
        }
        return to;
    }

    public static void setFieldValue(final Field source, final Field target, final Object from, final Object to) throws IllegalAccessException {
        final boolean sourceAccesible = source.isAccessible();
        final boolean targetAccesible = target.isAccessible();
        try {
            source.setAccessible(true);
            target.setAccessible(true);
            final Object obj = source.get(from);
            if (obj != null) {
                if (target.getType().isAssignableFrom(source.getType())) {

                    target.set(to, obj);
                } else {
                    try {
                        final Object newInstance = target.getType().newInstance();
                        copySameNameFields(obj, newInstance);
                        target.set(to, newInstance);
                    } catch (final InstantiationException ie) {
                        log.trace("skipped");
                    }
                }
            }

        } finally {
            source.setAccessible(sourceAccesible);
            target.setAccessible(targetAccesible);
        }
    }

    public static List<Field> getAllDeclaredFields(final Object o) {

        final List<Field> fields = new ArrayList<>();
        if (o == null) {
            return fields;
        }
        Class<?> clazz = o.getClass();
        while (clazz != null) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

}
