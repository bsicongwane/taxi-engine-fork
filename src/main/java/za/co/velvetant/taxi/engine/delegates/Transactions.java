package za.co.velvetant.taxi.engine.delegates;

import static com.google.common.base.Optional.fromNullable;

import java.math.BigDecimal;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.model.Payment;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.models.transactions.Transaction;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

@Named
public class Transactions {

    private static final Logger log = LoggerFactory.getLogger(Transactions.class);

    @Inject
    private TransactionRepository repository;

    public Transaction find(final String uuid) {
        return repository.findByUuid(uuid);
    }

    public BigDecimal findCustomerBalance(final Customer customer) {
        return fromNullable(repository.findCustomersBalance(customer)).or(new BigDecimal(0.00));
    }

    public BigDecimal determinePayInGiven(final Payment payment) {
        return determinePayInGiven(payment.getTripAmount(), payment.getTrip().getCustomer());
    }

    public BigDecimal determinePayInGiven(final BigDecimal amount, final Customer customer) {
        BigDecimal balance = findCustomerBalance(customer);
        BigDecimal payIn = balance.subtract(amount).negate();
        if (isNegative(payIn)) {
            payIn = new BigDecimal(0.00);
        }

        log.debug("Given [{}]'s balance of [{}], when using amount [{}], the pay in would be: {}", customer.getUserId(), balance, amount, payIn);
        return payIn;
    }

    public Transaction record(final CustomerCoupon coupon) {
        Transaction transaction = new Transaction(coupon);
        transaction.credit(coupon.getCoupon().getMonetaryLimit());

        return repository.save(transaction);
    }

    public Transaction record(final Payment payment) {
        Transaction transaction = new Transaction(payment.getType(), payment.getTrip());
        transaction.debit(payment.getTripAmount());
        transaction.credit(payment.getInvoiceAmount());

        return repository.save(transaction);
    }

    private boolean isNegative(final BigDecimal amount) {
        return amount.signum() == -1;
    }
}
