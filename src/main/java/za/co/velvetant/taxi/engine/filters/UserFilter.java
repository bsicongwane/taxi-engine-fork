package za.co.velvetant.taxi.engine.filters;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convertUsers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.models.User;
import za.co.velvetant.taxi.engine.persistence.UserRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class UserFilter implements Filter<User, za.co.velvetant.taxi.engine.api.User, UserFilter> {

    private static final Logger log = LoggerFactory.getLogger(UserFilter.class);

    @Inject
    private UserRepository userRepository;

    private User example = new User();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "lastName"));

    private Long total;
    private List<User> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.User> vos = new ArrayList<>();

    public UserFilter with(final String firstName, final String lastName, final String cellphone, final String email, final Boolean active) {
        return withNames(firstName, lastName).withCellphone(cellphone).withEmail(email).isActive(active);
    }

    @Override
    public UserFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public UserFilter filter() {
        example.setCreatedDate(null);
        Page<User> results = userRepository.findWithExample(example, sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public UserFilter andConvert() {
        vos.addAll(convertUsers(filtered));

        return this;
    }

    @Override
    public List<User> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.User> vos() {
        return vos;
    }

    private UserFilter withNames(final String firstName, final String lastName) {
        example.setFirstName(firstName);
        example.setLastName(lastName);

        return this;
    }

    private UserFilter withCellphone(final String cellphone) {
        example.setCellphone(cellphone);

        return this;
    }

    private UserFilter withEmail(final String email) {
        example.setEmail(email);

        return this;
    }

    private UserFilter isActive(final Boolean active) {
        example.setActive(active);

        return this;
    }
}
