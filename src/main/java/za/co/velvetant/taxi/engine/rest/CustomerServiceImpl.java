package za.co.velvetant.taxi.engine.rest;

import com.wordnik.swagger.annotations.ApiParam;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.mygate.MyGateResponse;
import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.Cellphone;
import za.co.velvetant.taxi.engine.api.ChangePasswordDetails;
import za.co.velvetant.taxi.engine.api.Corporate;
import za.co.velvetant.taxi.engine.api.CreateCustomer;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.EditCustomer;
import za.co.velvetant.taxi.engine.api.EditFavourite;
import za.co.velvetant.taxi.engine.api.Family;
import za.co.velvetant.taxi.engine.api.Favourite;
import za.co.velvetant.taxi.engine.api.PaymentMethod;
import za.co.velvetant.taxi.engine.api.PaymentType;
import za.co.velvetant.taxi.engine.api.RewardPoint;
import za.co.velvetant.taxi.engine.api.RewardPointList;
import za.co.velvetant.taxi.engine.delegates.Customers;
import za.co.velvetant.taxi.engine.filters.CustomerFilter;
import za.co.velvetant.taxi.engine.messaging.mail.MailService;
import za.co.velvetant.taxi.engine.messaging.mymessages.MessageService;
import za.co.velvetant.taxi.engine.messaging.sms.SmsService;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.models.RewardGroup;
import za.co.velvetant.taxi.engine.models.RewardPointLog;
import za.co.velvetant.taxi.engine.persistence.CreditCardRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.engine.persistence.FavouriteRepository;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.engine.persistence.RewardGroupRepository;
import za.co.velvetant.taxi.engine.persistence.RewardPointLogRepository;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.engine.util.Bytes;
import za.co.velvetant.taxi.engine.util.Dates;
import za.co.velvetant.taxi.engine.util.Strings;
import za.co.velvetant.taxi.engine.util.VoConversion;
import za.co.velvetant.taxi.engine.util.credentials.Password;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;
import javax.xml.rpc.ServiceException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.SERVICE_UNAVAILABLE;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static za.co.velvetant.taxi.engine.models.MyGateTransaction.Type.STORE_CARD;
import static za.co.velvetant.taxi.engine.models.MyGateTransaction.Type.UPDATE_CARD;
import static za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines.buildVo;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
@Path("/customers.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private FavouriteRepository favouriteRepository;

    @Inject
    private CreditCardRepository creditCardRepository;

    @Inject
    private RewardPointLogRepository rewardPointLogRepository;

    @Inject
    private RewardGroupRepository rewardGroupRepository;

    @Inject
    private FareRepository fareRepository;

    @Inject
    private MailService mailService;

    @Inject
    private MessageService messageService;

    @Inject
    private Provider<CustomerFilter> filter;

    @Inject
    private Customers customers;

    @Inject
    private MyGateTransactionRepository myGateTransactionRepository;

    @Inject
    private TransactionRepository transactionRepository;

    @Inject
    private SmsService smsMessageService;

    @Inject
    @Qualifier("SimpleDigitPassword")
    private Password password;

    @Inject
    private PaymentService paymentService;

    @Context
    private SecurityContext securityContext;

    @Override
    public Response create(final CreateCustomer customer) {
        return ok().entity(customers.create(customer)).build();
    }

    @Override
    public Response edit(final String userid, final EditCustomer customer) {
        za.co.velvetant.taxi.engine.models.Customer existing = CustomerRoutines.loadCustomer(userid, customerRepository);
        if (customer.getCellphone() != null && !customer.getCellphone().equals(existing.getCellphone())) {
            CustomerRoutines.checkCellphone(customerRepository, customer.getCellphone());
            existing.setCellphone(Strings.findKeeper(existing.getCellphone(), customer.getCellphone()));
        }

        existing.setFirstName(Strings.findKeeper(existing.getFirstName(), customer.getFirstName()));
        existing.setLastName(Strings.findKeeper(existing.getLastName(), customer.getLastName()));
        existing.setSendNewsLetter(customer.getSendNewsLetter());
        existing = customerRepository.save(existing);
        messageService.addProfileUpdateMessage(existing);
        return ok().entity(convert(existing)).build();
    }

    @Override
    public Response remove(final String userid) {
        final za.co.velvetant.taxi.engine.models.Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        customer.setActive(false);
        customerRepository.save(customer);
        return Response.ok().entity("OK").build();
    }

    @Override
    public Response activate(final String userid) {
        final Customer existingCustomer = customerRepository.findAnyStatusByUserId(userid);
        if (existingCustomer != null) {
            if (!existingCustomer.getActive()) {
                existingCustomer.setActive(true);
                customerRepository.save(existingCustomer);
            } else {
                throw new PreconditionFailedException(format("Customer %s is not deactivated", userid));
            }
        } else {
            throw new PreconditionFailedException(format("Customer %s does no exist", userid));
        }

        return ok().build();
    }

    @Override
    public za.co.velvetant.taxi.engine.api.Customer find(final String userid) {
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.fetchWithCreditCard(userid);
        NotFoundException.assertNotNull(customer, String.format("Customer with userid %s was not found", userid));
        return buildVo(customer, creditCardRepository, transactionRepository.findCustomersBalance(customer));
    }

    @Override
    public Response findAll(final String firstName, final String lastName, final String cellphone, final String email, final Boolean active, final Boolean corporate, final Boolean family,
            final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        final CustomerFilter filteredCustomers = filter.get().with(firstName, lastName, cellphone, email, active, corporate, family).sort(page, size, sort, order).filter();
        final List<za.co.velvetant.taxi.engine.api.Customer> vos = filteredCustomers.andConvert().vos();

        ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredCustomers.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public String count() {
        return String.valueOf(customerRepository.count());
    }

    @Override
    public Corporate findCorporate(@ApiParam(value = "User Id of the customer", required = true) @PathParam("userid") final String userid) {
        final za.co.velvetant.taxi.engine.models.Customer entity = customerRepository.findByUserId(userid);
        if (entity != null && entity.getCorporate() != null) {
            return convert(entity.getCorporate(), new Corporate());
        }
        return null;
    }

    @Override
    public Family findFamily(final String userid) {
        final za.co.velvetant.taxi.engine.models.Customer entity = customerRepository.findByUserId(userid);
        if (entity != null && entity.getFamily() != null) {
            return convert(entity.getFamily(), new Family());
        }
        return null;
    }

    @Override
    public Response uploadImage(final String userid, final InputStream inputStream, final String photoFormat) {
        final za.co.velvetant.taxi.engine.models.Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        customer.setPhoto(Bytes.getBytes(inputStream));
        customer.setPhotoFormat(photoFormat);
        customerRepository.save(customer);

        return Response.ok().entity("OK").build();
    }

    @Override
    public Response getPhoto(final String userid) {
        final za.co.velvetant.taxi.engine.models.Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        if (customer.getPhoto() != null) {
            return ok(new ByteArrayInputStream(customer.getPhoto())).header("Content-Type", customer.getPhotoFormat()).build();
        }
        throw new NotFoundException("Customer with userid " + userid + " has no image loaded");

    }

    @Override
    public Response getEncodedPhoto(final String userid) {

        final za.co.velvetant.taxi.engine.models.Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        if (customer.getPhoto() != null) {
            return ok(new String(Base64.encodeBase64(customer.getPhoto()))).build();
        }

        throw new NotFoundException("Customer with userid " + userid + " has no image loaded");

    }

    @Override
    public Response resetPassword(final String userid) {
        final za.co.velvetant.taxi.engine.models.Customer entity = CustomerRoutines.loadCustomer(userid, customerRepository);
        final String newPassword = password.generate();
        entity.setPassword(password.hash(newPassword));
        customerRepository.save(entity);
        mailService.sendPasswordResetEmail(entity, newPassword);
        messageService.addPasswordResetMessage(entity, newPassword);

        return Response.ok().entity("OK").build();
    }

    @Override
    public Response changeCellphone(final String userid, final Cellphone cellphone) {
        final Customer customer = customerRepository.findAnyStatusByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found");

        final Customer existing = customerRepository.findAnyStatusByCellphone(cellphone.getCellphone());
        if (existing != null) {
            throw new ConflictException(String.format("Cell number %s is already registered.", cellphone.getCellphone()));
        }
        customer.setCellphone(cellphone.getCellphone());
        customerRepository.save(customer);
        return Response.ok().entity("OK").build();
    }

    @Override
    public Response uploadCreditCard(final String userid, final CreditCard card) {
        za.co.velvetant.taxi.engine.models.CreditCard creditCard = new za.co.velvetant.taxi.engine.models.CreditCard();
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.fetchWithCreditCard(userid);
        if (customer.getCreditCards() != null && !customer.getCreditCards().isEmpty()) {
            creditCard = customer.getCreditCards().get(0);
        }
        creditCard.setCustomer(customer);
        MyGateResponse response = null;
        try {
            if (creditCard.getId() == null) {
                final String tokenId = UUID.randomUUID().toString().replaceAll("-", "");
                creditCard.setGateWayId(tokenId);
                response = paymentService.storeCard(tokenId, card);
                final MyGateTransaction transaction = new MyGateTransaction(response.getDateTime(), null, STORE_CARD, response.getTransactionIndex(), response.getStatus());
                transaction.setCustomer(customer);
                transaction.setErrorMessage(response.getErrorCode());
                myGateTransactionRepository.save(transaction);

            } else {
                response = paymentService.updateCard(creditCard.getGateWayId(), card);
                final MyGateTransaction transaction = new MyGateTransaction(response.getDateTime(), null, UPDATE_CARD, response.getTransactionIndex(), response.getStatus());
                transaction.setCustomer(customer);
                transaction.setErrorMessage(response.getErrorCode());
                myGateTransactionRepository.save(transaction);
            }
        } catch (final Exception e) {
            log.error(e.getMessage(), e);
        }

        if (response != null && response.isSuccess()) {
            creditCard.setCvvReference(card.getCvv());

            creditCard = creditCardRepository.save(creditCard);

            customer.addCreditCard(creditCard);
            customerRepository.save(customer);

            return ok().entity(card).build();
        }

        return Response.status(Response.Status.PRECONDITION_FAILED).entity("There was an error saving your card details with payment gateway.").build();
    }

    @Override
    public Response deleteCreditCard(final String userid) {
        ResponseBuilder response = ok().entity("OK");

        final Customer customer = customerRepository.fetchWithCreditCard(userid);
        final List<za.co.velvetant.taxi.engine.models.CreditCard> creditCards = customer.getCreditCards();
        customer.setCreditCards(null);
        customerRepository.save(customer);

        try {
            for (final za.co.velvetant.taxi.engine.models.CreditCard creditCard : creditCards) {
                paymentService.removeCard(creditCard.getGateWayId());
                creditCardRepository.delete(creditCard);
            }
        } catch (final Exception e) {
            log.error("Could not remove customers [{}] credit card", userid);
            log.error("MyGate error occurred", e);
            response = Response.status(SERVICE_UNAVAILABLE).entity(e.getMessage());
        }

        return response.build();
    }

    @Override
    public Response getCreditCard(final String userid) {
        final Customer customer = customerRepository.fetchWithCreditCard(userid);
        NotFoundException.assertNotNull(customer, String.format("Customer with userId %s does not exist", userid));

        final List<za.co.velvetant.taxi.engine.models.CreditCard> creditCards = customer.getCreditCards();
        if (creditCards != null && !creditCards.isEmpty()) {

            final za.co.velvetant.taxi.engine.models.CreditCard creditCard = customer.getCreditCards().get(0);
            try {
                final CreditCard cc = paymentService.getMaskedCardDetails(creditCard.getGateWayId());
                cc.setCvv(creditCard.getCvvReference());
                return Response.ok().entity(cc).build();
            } catch (RemoteException | ServiceException | ParseException e) {
                log.error(e.getMessage(), e);
            }

        }
        throw new NotFoundException(String.format("Customer %s has no credit card loaded", userid));
    }

    @Override
    public Response create(final String userid, final Favourite favourite) {
        final za.co.velvetant.taxi.engine.models.Favourite existing = favouriteRepository.findByNameAndCustomerUserId(favourite.getName(), userid);

        if (existing == null) {
            za.co.velvetant.taxi.engine.models.Favourite entity = convert(favourite, new za.co.velvetant.taxi.engine.models.Favourite());
            entity.setCustomer(customerRepository.findByUserId(userid));
            entity = favouriteRepository.save(entity);
            return Response.status(CREATED).entity(convert(entity, new Favourite())).build();
        }
        throw new ConflictException(String.format("Favourite with name %s already exists for customer %s", favourite.getName(), userid));
    }

    @Override
    public Response edit(final String userid, final String favouriteName, final EditFavourite favourite) {
        za.co.velvetant.taxi.engine.models.Favourite existing = favouriteRepository.findByNameAndCustomerUserId(favouriteName, userid);
        existing.setName(Strings.findKeeper(existing.getName(), favourite.getName()));
        if (favourite.getLocation() != null) {
            existing.getLocation().setAddress(Strings.findKeeper(existing.getLocation().getAddress(), favourite.getLocation().getAddress()));
        }

        existing = favouriteRepository.save(existing);
        return ok().entity(convert(existing, new Favourite())).build();
    }

    @Override
    public Response remove(final String userid, final String favouriteName) {
        favouriteRepository.delete(favouriteRepository.findByNameAndCustomerUserId(favouriteName, userid));
        return ok().entity("OK").build();
    }

    @Override
    public Response find(final String userid, final String favouriteName) {
        ResponseBuilder response = noContent();
        final za.co.velvetant.taxi.engine.models.Favourite favourite = favouriteRepository.findByNameAndCustomerUserId(favouriteName, userid);
        if (favourite != null) {
            response = ok().entity(convert(favourite, new Favourite()));
        }

        return response.build();
    }

    @Override
    public Response findAll(final String userid) {
        ResponseBuilder response = noContent();
        final List<za.co.velvetant.taxi.engine.models.Favourite> favourites = favouriteRepository.findByCustomerUserId(userid);
        if (favourites != null && !favourites.isEmpty()) {
            final List<Favourite> vos = new ArrayList<>();
            for (final za.co.velvetant.taxi.engine.models.Favourite favourite : favourites) {
                vos.add(convert(favourite, new Favourite()));
            }

            response = ok().entity(vos);
        }

        return response.build();
    }

    @Override
    public RewardPoint findRewardsPoints(final String userid) {
        final za.co.velvetant.taxi.engine.models.Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        final za.co.velvetant.taxi.engine.models.RewardPoint rewardPoint = customer.getRewardPoint();
        long total = 0L;
        if (rewardPoint != null) {
            total = customer.getRewardPoint().getTotal();
        }
        final List<RewardGroup> group = rewardGroupRepository.findGroup(total);
        if (group != null && !group.isEmpty()) {
            return new RewardPoint(total, group.get(0).getGroupName());
        }
        return new RewardPoint(total, "NO_REWARD_GROUP_MATCHED");
    }

    @Override
    public RewardPointList findRewardsPointLog(final String userid, final long startTime, final long endTime) {
        final boolean provided = startTime != 0 && endTime != 0;
        final long diff = (endTime - startTime);
        final boolean smallDiff = diff <= Dates.MONTH_MAX;

        final boolean day = provided && smallDiff;

        List<RewardPointLog> points;
        if (provided) {
            points = rewardPointLogRepository.findByFareCustomerUserIdAndTransactionTimeBetween(userid, new Timestamp(startTime), new Timestamp(endTime));
        } else if (startTime != 0) {
            points = rewardPointLogRepository.findByFareCustomerUserIdAndTransactionTimeGreaterThan(userid, new Timestamp(startTime));
        } else if (endTime != 0) {
            points = rewardPointLogRepository.findByFareCustomerUserIdAndTransactionTimeLessThan(userid, new Timestamp(endTime));
        } else {
            points = rewardPointLogRepository.findByFareCustomerUserId(userid);
        }
        final RewardPointList results = new RewardPointList();

        long total = 0;
        for (final RewardPointLog log : points) {
            Long count;
            String key;
            if (day) {
                key = Dates.day(log.getTransactionTime());
            } else {
                key = Dates.month(log.getTransactionTime());
            }
            count = results.getPoints().get(key);
            if (count == null) {
                count = log.getAmount();
            } else {
                count = count + log.getAmount();
            }

            results.getPoints().put(key, count);
            total += log.getAmount();
        }
        results.setTotal(total);

        return results;
    }

    @Override
    public za.co.velvetant.taxi.engine.api.RewardPointLog findBestReward(final String userid) {
        final List<RewardPointLog> logs = rewardPointLogRepository.findByFareCustomerUserId(userid);
        RewardPointLog best = null;
        if (logs != null) {
            for (final RewardPointLog rewardPointLog : logs) {
                if (best == null || best.getAmount() < rewardPointLog.getAmount()) {
                    best = rewardPointLog;
                }
            }
        }
        if (best != null) {
            final za.co.velvetant.taxi.engine.api.RewardPointLog log = new za.co.velvetant.taxi.engine.api.RewardPointLog();
            log.setAmount(best.getAmount());
            log.setTransactionTime(best.getTransactionTime().getTime());
            log.setFrom(new Location(best.getFare().getPickup().getLatitude(), best.getFare().getPickup().getLongitude(), best.getFare().getPickup().getAddress()));
            if (best.getFare().getDropOff() != null) {
                log.setTo(new Location(best.getFare().getDropOff().getLatitude(), best.getFare().getDropOff().getLongitude(), best.getFare().getDropOff().getAddress()));
            }
            return log;
        }
        return null;
    }

    @Override
    public Response findLastReward(final String userid) {
        final List<RewardPointLog> logs = rewardPointLogRepository.findLatest(userid);
        if (logs != null && logs.size() > 0) {
            final za.co.velvetant.taxi.engine.api.RewardPointLog log = new za.co.velvetant.taxi.engine.api.RewardPointLog();
            log.setAmount(logs.get(0).getAmount());
            log.setTransactionTime(logs.get(0).getTransactionTime().getTime());
            log.setFrom(new Location(logs.get(0).getFare().getPickup().getLatitude(), logs.get(0).getFare().getPickup().getLongitude(), logs.get(0).getFare().getPickup().getAddress()));
            if (logs.get(0).getFare().getDropOff() != null) {
                log.setTo(new Location(logs.get(0).getFare().getDropOff().getLatitude(), logs.get(0).getFare().getDropOff().getLongitude(), logs.get(0).getFare().getDropOff().getAddress()));
            }
            return Response.ok(log).build();
        }
        return null;
    }

    @Override
    public Response changePassword(final String userid, final ChangePasswordDetails details) {
        // TODO verify that verifying old password is not required
        final Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        if (details.getNewPassword() != null && details.getConfirmNewPassword() != null && details.getNewPassword().equals(details.getConfirmNewPassword())) {
            customer.setPassword(password.hash(details.getConfirmNewPassword()));
            customerRepository.save(customer);
            messageService.addProfileUpdateMessage(customer);
            return ok().build();
        }
        throw new PreconditionFailedException("Invalid new password and confirm password combination");
    }

    @Override
    public Response findPreviousLocations(final String userid, final int limit, final Integer page, final String sort, final Sort.Direction order) {
        final Page<Fare> fares = fareRepository.findByCustomerUserId(userid, new PageRequest(page - 1, limit, order, sort));
        final Set<Location> locations = new TreeSet<>(new Location.AddressComparator());
        for (final Fare fare : fares.getContent()) {
            locations.add(VoConversion.convert(fare.getPickup()));
            if (fare.getDropOff() != null) {
                locations.add(VoConversion.convert(fare.getDropOff()));
            }
        }

        Response.ResponseBuilder response = Response.ok().entity(locations).header("X-Total-Entities", fares.getTotalElements());
        if (locations.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    @Override
    public Response activateAccount(final String userid, final String pin) {
        final Customer customer = customerRepository.findAnyStatusByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found");
        if (customer.getPin() != null && customer.getPin().equals(pin)) {
            customer.setActive(true);
            customerRepository.save(customer);
            final za.co.velvetant.taxi.engine.api.Customer find = find(userid);

            return Response.ok(find).build();

        }
        throw new PreconditionFailedException("Pin does not match");
    }

    @Override
    public Response getPaymentTypes(final String userid) {
        final List<PaymentType> paymentMethods = new ArrayList<>();
        final Customer customer = CustomerRoutines.loadCustomer(userid, customerRepository);
        paymentMethods.add(new PaymentType(PaymentMethod.CASH, null));
        if (customer.getCorporate() != null) {
            final List<za.co.velvetant.taxi.engine.models.CreditCard> list = creditCardRepository.findByCorporateRegistration(customer.getCorporate().getRegistration());
            if (!list.isEmpty()) {
                paymentMethods.add(new PaymentType(PaymentMethod.CORPORATE_ACCOUNT, list.get(0).getCvvReference()));
            }
        }
        if (customer.getFamily() != null) {
            final List<za.co.velvetant.taxi.engine.models.CreditCard> list = creditCardRepository.findByFamilyUuid(customer.getFamily().getUuid());
            if (!list.isEmpty()) {
                paymentMethods.add(new PaymentType(PaymentMethod.FAMILY_ACCOUNT, list.get(0).getCvvReference()));
            }
        }
        if (customer.getCreditCards() != null && !customer.getCreditCards().isEmpty()) {
            paymentMethods.add(new PaymentType(PaymentMethod.CREDIT_CARD, customer.getCreditCards().get(0).getCvvReference()));
        }
        return Response.ok(paymentMethods).build();
    }

    @Override
    public Response resendPin(final String userid) {
        Customer customer = customerRepository.findAnyStatusByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found");
        customer.setPin(password.generate(5));
        customer = customerRepository.save(customer, securityContext.getUserPrincipal());
        smsMessageService.sendPinResetMessage(customer);
        return Response.ok().build();
    }
}
