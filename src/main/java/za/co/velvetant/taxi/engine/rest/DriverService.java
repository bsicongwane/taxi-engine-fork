package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.rest.DocumentationMessages.M_DRIVER_ID;

import java.io.InputStream;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.api.ChangePasswordDetails;
import za.co.velvetant.taxi.engine.api.Driver;
import za.co.velvetant.taxi.engine.api.Rating;

import com.sun.jersey.multipart.FormDataParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiParamImplicit;
import com.wordnik.swagger.annotations.ApiParamsImplicit;

@Named
@Path("/drivers")
@Api(value = "/drivers", description = "Operations for managing drivers.")
@RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "DRIVER" })
public interface DriverService {

    String DRIVER_CLASS = "za.co.velvetant.taxi.engine.api.Driver";
    String RATING_CLASS = "za.co.velvetant.taxi.engine.api.Rating";

    @POST
    @ApiOperation(value = "Creates a driver", responseClass = DRIVER_CLASS)
    Driver create(@ApiParam(value = "Driver to create", required = true) Driver driver);

    @PUT
    @Path("/{userid}")
    @ApiOperation(value = "Updates a driver", responseClass = DRIVER_CLASS)
    Response edit(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid, @ApiParam(value = "Updated driver details", required = true) Driver driver);

    @GET
    @Path("/{userid}")
    @ApiOperation(value = "Finds a userid's details.", responseClass = DRIVER_CLASS)
    Driver find(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @ApiOperation(value = "Finds all drivers for an affiliate", multiValueResponse = true, responseClass = DRIVER_CLASS)
    List<Driver> findAll(@ApiParam(value = "Affiliate registration") @QueryParam("affiliate") String affiliate);

    @GET
    @Path("/all")
    @ApiOperation(value = "Find all drivers.", multiValueResponse = true, responseClass = DRIVER_CLASS,
    notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response findAll(
            @ApiParam(value = "Filter by first name. Partial filtering is allowed.") @QueryParam("firstName") String firstName,
            @ApiParam(value = "Filter by last name. Partial filtering is allowed.") @QueryParam("lastName") String lastName,
            @ApiParam(value = "Filter by cellphone number. Partial filtering is allowed.") @QueryParam("cellphone") String cellphone,
            @ApiParam(value = "Filter by email address. Partial filtering is allowed.") @QueryParam("email") String email,
            @ApiParam(value = "Filter by active/inactive drivers.", defaultValue = "true") @QueryParam("active") @DefaultValue("true") Boolean active,
            @ApiParam(value = "Affiliate registration identifier.") @QueryParam("affiliate") String affiliate,
            @ApiParam(value = "Whether to include affiliated drivers.") @QueryParam("affiliated") Boolean affiliated,
            @ApiParam(value = "Page number of drivers to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number of drivers to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "lastName", allowableValues = "firstName,lastName,cellphone,email") @QueryParam("sort") @DefaultValue("lastName") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

    @GET
    @Path("/{userid}/ratings")
    @ApiOperation(value = "Finds all ratings for the given driver userid", multiValueResponse = true, responseClass = RATING_CLASS)
    List<Rating> findRatings(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{userid}/ratings/all")
    @ApiOperation(value = "Finds all ratings for the given driver userid", multiValueResponse = true, responseClass = RATING_CLASS)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response findRatings(
            @ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Page number of ratings to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number ratings to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "driver.requestTime", allowableValues = "driver.requestTime,tripRating,driverRating") @QueryParam("sort")
            @DefaultValue("driver.requestTime") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "desc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);

    @GET
    @Path("/{userid}/rating")
    @ApiOperation("Finds the average rating of a driver.")
    String findRating(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid);

    @POST
    @Path("/{userid}/resetPassword")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "WEB_USER", "DRIVER" })
    @ApiOperation(value = "Reset a driver's password. The user is emailed their new password.")
    Response resetPassword(@ApiParam(value = DocumentationMessages.M_DRIVER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("count")
    @Produces("text/plain")
    String count();

    @POST
    @Path("/{userid}/image")
    @ApiOperation(value = "Upload an image for the driver.")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiParamsImplicit(@ApiParamImplicit(dataType = "file", name = "file", paramType = "body"))
    Response uploadImage(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid, @FormDataParam("file") InputStream imageStream,
            @HeaderParam("X-Photo-Format") String photoFormat);

    @GET
    @Path("/{userid}/image")
    @ApiOperation(value = "Get a driver's image")
    Response getPhoto(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid);

    @DELETE
    @Path("/{userid}")
    @ApiOperation(value = "Deactivate a driver")
    Response remove(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid);

    @HEAD
    @Path("/{userid}")
    @ApiOperation(value = "Activate a driverr")
    Response activate(@ApiParam(value = "userid", required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{userid}/image/encoded")
    @ApiOperation(value = "Get a driver's image as base 64 encoded text")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "FAMILY", "DRIVER" })
    @Produces(MediaType.TEXT_PLAIN)
    Response getEncodedPhoto(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid);

    @PUT
    @Path("/{userid}/password")
    @ApiOperation(value = "Change a driver's password.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "DRIVER" })
    Response changePassword(@ApiParam(value = M_DRIVER_ID, required = true) @PathParam("userid") String userid, @ApiParam(value = "New password details.") ChangePasswordDetails details);

}
