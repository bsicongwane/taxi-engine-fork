package za.co.velvetant.taxi.engine.messaging.sms;

public interface SmsProvider {

    void send(String cellphone, String content, String reference);
}
