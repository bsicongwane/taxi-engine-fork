package za.co.velvetant.taxi.engine.rest;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.springframework.data.domain.Sort;
import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.api.PaymentMethod;
import za.co.velvetant.taxi.engine.api.Rating;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path("/fares")
@Api(value = "/fares", description = "Operations for managing fares.")
@RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "DRIVER" })
public interface FareService {

    String FARE_CLASS = "za.co.velvetant.taxi.engine.api.Fare";

    @GET
    @Path("/{uuid}")
    @ApiOperation(value = "Find a fare by fare uuid/reference", responseClass = FARE_CLASS)
    Response find(@ApiParam(value = DocumentationMessages.M_FARE_ID) @PathParam("uuid") String uuid);

    @GET
    @Path("/active")
    @ApiOperation(value = "Find all the active fares, optionally limited to a customer.", multiValueResponse = true, responseClass = FARE_CLASS)
    Response findActive(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = false) @QueryParam("customer") String customer,
            @ApiParam(value = "Corporate registration", required = false) @QueryParam("registration") String registration,
            @ApiParam(value = "Family uuid", required = false) @QueryParam("family") String family,
            @ApiParam(value = "limit ", defaultValue = "10")
    @QueryParam("limit") @DefaultValue("10") long limit);

    @GET
    @ApiOperation(value = "Search a customers fares and returns them grouped by the supplied property.", multiValueResponse = true, responseClass = FARE_CLASS)
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 412, reason = DocumentationMessages.E_412 + " Date supplied was not expected format of yyyy-MM-dd") })
    Response findByCustomer(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @QueryParam("customer") String customer,
            @ApiParam(value = "Returned the fares grouped or not", defaultValue = "requestTime") @QueryParam("grouped") @DefaultValue("requestTime") String grouped,
            @ApiParam(value = "limit ", defaultValue = "10") @QueryParam("limit") @DefaultValue("10") int limit);

    @GET
    @Path("/all")
    @ApiOperation(value = "Find all fares.", multiValueResponse = true, responseClass = FARE_CLASS, notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response findAll(
            @ApiParam(value = "Filter by fare uuid.") @QueryParam("uuid") String uuid,
            @ApiParam(value = "Filter by fare reference.") @QueryParam("reference") String reference,
            @ApiParam(value = "Filter by customer email.") @QueryParam("customerEmail") String customerEmail,
            @ApiParam(value = "Filter by customer cellphone.") @QueryParam("customerCellphone") String customerCellphone,
            @ApiParam(value = "Filter by corporate registration.") @QueryParam("registration") String registration,
            @ApiParam(value = "Filter by family uuid.") @QueryParam("family") String family,
            @ApiParam(value = "Filter by active/inactive fares.", defaultValue = "true", allowableValues = ",true,false") @QueryParam("active") Boolean active,
            @ApiParam(value = "Filter by enroute fares.", defaultValue = "false") @QueryParam("enroute") @DefaultValue("false") Boolean enroute,
            @ApiParam(value = "If filter properties are exclusive or not", defaultValue = "false") @QueryParam("exclusive") @DefaultValue("false") Boolean exclusive,
            @ApiParam(value = "Page number of fares to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number fares to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "requestTime", allowableValues = "requestTime,dropOffTime,customer.email,customer.cellphone")
            @QueryParam("sort") @DefaultValue("requestTime") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "desc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);

    @GET
    @Path("/completed")
    @ApiOperation(value = "Find all the completed fares.", multiValueResponse = true, responseClass = FARE_CLASS)
    Response findCompleted(@ApiParam(value = DocumentationMessages.M_START_TIME, required = false) @QueryParam("startTime") long startTime,
            @ApiParam(value = DocumentationMessages.M_END_TIME, required = false) @QueryParam("endTime") long endtime,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = false) @QueryParam("userid") String userid,
            @ApiParam(value = "Corporate registration", required = false) @QueryParam("registration") String registration,
            @ApiParam(value = "Family uuid", required = false) @QueryParam("family") String family,
            @ApiParam(value = "Number fares to return for this page ", defaultValue = "10") @QueryParam("limit") @DefaultValue("10") long limit,
            @ApiParam(value = "Page number of fares to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Which property to sort by", defaultValue = "requestTime", allowableValues = "requestTime,dropOffTime,customer.email,customer.cellphone") @QueryParam("sort") @DefaultValue("requestTime") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "desc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);

    @POST
    @ApiOperation(value = "Request a new fare.")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400) })
    Response request(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @QueryParam("userid") String userid,
            @ApiParam(value = "Requested fare details", required = true) final FareRequest fareRequest);

    @GET
    @Path("/{uuid}/tasks")
    @ApiOperation(value = "Get the next tasks for the fare with this uuid.", responseClass = "za.co.velvetant.taxi.engine.api.FareTasks")
    @ApiErrors({ @ApiError(code = 404, reason = "Next tasks for this fare and person does not exist") })
    Response nextTasks(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = "Either customer's email address or the drivers userid", required = true) @QueryParam("userid") String userid);

    @POST
    @Path("/{uuid}/accept")
    @ApiOperation(value = "A driver accepts this fare.")
    @ApiErrors({ @ApiError(code = 404, reason = "An action for this driver and person does not exist") })
    Response accept(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_DRIVER_ID, required = true) @QueryParam("userid") String userid);

    @POST
    @Path("/{uuid}/arriveAtPickup")
    @ApiOperation(value = "A driver arrives at the pickup location.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response arriveAtPickup(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_DRIVER_ID, required = true) @QueryParam("userid") String userid);

    @POST
    @Path("/{uuid}/pickup")
    @ApiOperation(value = "A fare picks up the customer.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response pickup(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_DRIVER_ID, required = true) @QueryParam("userid") String userid);

    @POST
    @Path("/{uuid}/cancel")
    @ApiOperation(value = "A customer wishes to cancel the journey with the allocated fare.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response cancel(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @QueryParam("userid") String userid);

    @POST
    @Path("/{uuid}/arrived")
    @ApiOperation(value = "The taxi has arrived at the destination.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response arrived(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_DRIVER_ID, required = true) @QueryParam("userid") String userid, @ApiParam(value = "Destination", required = false) final Location destination);

    @POST
    @Path("/{uuid}/amount/override")
    @ApiOperation(value = "An ops user captures the amount of the fare.")
    @RolesAllowed({ "ADMINISTRATOR", "SUPERVISOR", "AGENT"})
    Response amountOverride(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
                            @ApiParam(value = DocumentationMessages.M_OPS_USER_ID, required = true) @QueryParam("userid") String userId);
    @POST
    @Path("/{uuid}/amount")
    @ApiOperation(value = "The driver captures the amount of the fare.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response amount(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_DRIVER_ID, required = true) @QueryParam("userid") String userid,
            @ApiParam(value = "Cost of the fare", required = true) @QueryParam("amount") @DefaultValue("0.00") Double amount,
            @ApiParam(value = "The payment method that should be used") @QueryParam("paymentMethod") PaymentMethod paymentMethod);

    @POST
    @Path("/{uuid}/payment/confirm/override")
    @ApiOperation(value = "An ops user confirms payment via credit card/account on behalf of a customer.")
    @RolesAllowed({ "ADMINISTRATOR", "SUPERVISOR", "AGENT"})
    Response confirmPaymentOverride(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") final String uuid,
                                    @ApiParam(value = DocumentationMessages.M_OPS_USER_ID, required = true) @QueryParam("userid") final String userid);

    @POST
    @Path("/{uuid}/payment/confirm")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    @ApiOperation(value = "The customer confirms payment via credit card.")
    Response confirmPayment(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") final String uuid,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @QueryParam("userid") final String userid,
            @ApiParam(value = "True (default) to confirm payment, false to decline payment via credit card") @QueryParam("confirmed") @DefaultValue("true") final Boolean confirmed,
            @ApiParam(value = "The payment method that should be used", required = false) @QueryParam("paymentMethod") final PaymentMethod paymentMethod,
            @ApiParam(value = "The tip to add to fare amount", required = false) @QueryParam("tip") final Double tip,
            @ApiParam(value = "Credit card cvv", required = false) @QueryParam("cvv") final String cvv,
            @ApiParam(value = "Pay with customers account", required = false) @QueryParam("account") @DefaultValue("false") final Boolean account);

    @POST
    @Path("/{uuid}/rate")
    @ApiOperation(value = "Rate the driver for this fare.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response rate(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @QueryParam("userid") String userid, @ApiParam(value = "Rating details", required = true) Rating rating);

    @DELETE
    @Path("/{uuid}")
    @ApiOperation(value = "Remove a fare from the system.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response removeFare(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = "Reason for delete.", required = true) @QueryParam("reason") final String reason);

    @GET
    @Path("/{uuid}/diagram")
    @Produces("image/png")
    @ApiOperation(value = "Get the process diagram for this fare")
    @ApiErrors({ @ApiError(code = 404, reason = "This fare does not exist") })
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response diagram(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid);

    @POST
    @Path("/{uuid}/stops")
    @ApiOperation(value = "Add a driver enroute stop point")
    Response addStop(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = "The location they stopped", required = true) final Location location);

    @POST
    @Path("/{uuid}/driverCancel")
    @ApiOperation(value = "A driver wishes to cancel the journey with the allocated fare.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response driverCancel(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_DRIVER_ID, required = true) @QueryParam("userid") String userid);

    @POST
    @Path("/{uuid}/opsCancel")
    @ApiOperation(value = "An ops user wishes to cancel the journey with the allocated fare.")
    @ApiErrors({ @ApiError(code = 404, reason = DocumentationMessages.E_400_F) })
    Response opsCancel(@ApiParam(value = DocumentationMessages.M_FARE_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = "User Id of the person that cancelled the fare", required = true) @QueryParam("userid") String userid);

}
