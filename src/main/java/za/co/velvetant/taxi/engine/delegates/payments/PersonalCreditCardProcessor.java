package za.co.velvetant.taxi.engine.delegates.payments;

import static za.co.velvetant.taxi.engine.model.Payment.PaymentBuilder.creditCardPayment;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.model.Payment;
import za.co.velvetant.taxi.engine.models.CreditCard;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

@Named("CREDIT_CARD")
public class PersonalCreditCardProcessor extends CreditCardProcessor {

    private static final Logger log = LoggerFactory.getLogger(PersonalCreditCardProcessor.class);

    @Inject
    private CustomerRepository customerRepository;

    @Override
    public Payment createPayment(final Fare trip, final String cvv) {
        return creditCardPayment(trip, cvv).forAmount(trip.getAmount()).plusTip(trip.getTip());
    }

    @Override
    protected CreditCard getCreditCard(final String userId) {
        log.debug("Getting personal credit card for: {}", userId);
        
        final Customer customer = customerRepository.findByUserId(userId);
        final List<CreditCard> creditCards = customer.getCreditCards();
        if (creditCards.isEmpty() || creditCards.size() > 1) {
            throw new IllegalStateException("Customer must only have one personal credit card loaded");
        }

        return creditCards.iterator().next();
    }
}
