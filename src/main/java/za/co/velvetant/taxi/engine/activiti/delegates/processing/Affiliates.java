package za.co.velvetant.taxi.engine.activiti.delegates.processing;

import java.util.List;

import za.co.velvetant.taxi.engine.api.FareRequest;

public interface Affiliates {

    public List<String> filterAffiliates(FareRequest fareRequest);
}
