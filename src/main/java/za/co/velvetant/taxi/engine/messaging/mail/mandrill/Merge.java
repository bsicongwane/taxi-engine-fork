package za.co.velvetant.taxi.engine.messaging.mail.mandrill;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Merge {

    @JsonProperty("rcpt")
    private String recipient;

    private List<Var> vars = new ArrayList<>();

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(final String recipient) {
        this.recipient = recipient;
    }

    public List<Var> getVars() {
        return vars;
    }

    public void setVars(final List<Var> vars) {
        this.vars = vars;
    }

}
