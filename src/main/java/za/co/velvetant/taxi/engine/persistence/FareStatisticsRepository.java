package za.co.velvetant.taxi.engine.persistence;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.FareState;
import za.co.velvetant.taxi.engine.models.driver.DriverShift;

@Repository
public interface FareStatisticsRepository extends CrudRepository<DriverShift, Long> {

    @QueryHints(forCounting = true)
    @Query("select count(f) from Fare f where requestTime between :from and :to")
    Long countTotalTrips(@Param("from") Date from, @Param("to") Date to);

    @QueryHints(forCounting = true)
    @Query("select count(f) from Fare f where f.driver.taxiCompany.registration = :registration and requestTime between :from and :to")
    Long countTotalTrips(@Param("registration") String registration, @Param("from") Date from, @Param("to") Date to);

    @QueryHints(forCounting = true)
    @Query("select count(f.id) from Fare f where f.requestTime between :from and :to and f.state = :state")
    Long countCancelled(@Param("from") Date from, @Param("to") Date to, @Param("state") FareState state);

    @QueryHints(forCounting = true)
    @Query("select count(f.id) from Fare f where f.driver.taxiCompany.registration = :registration and f.requestTime between :from and :to and f.state = :state")
    Long countCancelled(@Param("registration") String registration, @Param("from") Date from, @Param("to") Date to, @Param("state") FareState state);

    @Query(nativeQuery = true, value = "select sum(f.amount) as total, " +
            "ROUND(avg(f.duration) / 1000 / 60) as durationAverage, " +
            "ROUND(sum(f.duration) / 1000 / 60) as durationTotal, " +
            "ROUND((avg(f.distance) / 1000)) as distanceAverage, " +
            "ROUND((sum(f.distance) / 1000)) as distanceTotal, " +
            "CAST(sum(f.amount / 10) AS DECIMAL(12,2)) as clipTotal from Fare f " +
            "where f.requestTime between :from and :to and f.amount is not null")
    Object[] findTotalFareCost(@Param("from") Date from, @Param("to") Date to);

    @Query(nativeQuery = true, value = "select sum(f.amount) as total, " +
            "ROUND(avg(f.duration) / 1000 / 60) as durationAverage, " +
            "ROUND(sum(f.duration) / 1000 / 60) as durationTotal, " +
            "ROUND((avg(f.distance) / 1000)) as distanceAverage, " +
            "ROUND((sum(f.distance) / 1000)) as distanceTotal, " +
            "CAST(sum(f.amount / 10) AS DECIMAL(12,2)) as clipTotal from Fare f " +
            "join Person p on (p.id = f.driver_id) join Company c on (c.id = p.taxiCompany_id) " +
            "where f.requestTime between :from and :to and f.amount is not null " +
            "and c.registration = :registration")
    Object[] findTotalFareCost(@Param("registration") String registration, @Param("from") Date from, @Param("to") Date to);
}
