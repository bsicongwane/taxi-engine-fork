package za.co.velvetant.taxi.engine.filters;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.TaxiCompany;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

import com.google.common.base.Function;

@Named
@Scope(SCOPE_PROTOTYPE)
public class AffiliateFilter implements Filter<TaxiCompany, za.co.velvetant.taxi.engine.api.TaxiCompany, AffiliateFilter> {

    private static final Logger log = LoggerFactory.getLogger(AffiliateFilter.class);

    @Inject
    private TaxiCompanyRepository taxiCompanyRepository;

    private TaxiCompany example = new TaxiCompany();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "companyName"));

    private Long total;
    private List<TaxiCompany> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.TaxiCompany> vos = new ArrayList<>();

    public AffiliateFilter with(final String companyName, final String registration, final Boolean active) {
        return withCompanyName(companyName).withRegistration(registration).isActive(active);
    }

    @Override
    public AffiliateFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public AffiliateFilter filter() {
        Page<TaxiCompany> results = taxiCompanyRepository.findWithExample(example, sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    @Transactional
    public AffiliateFilter andConvert() {
        for (final TaxiCompany taxiCompany : filtered) {
            za.co.velvetant.taxi.engine.api.TaxiCompany affiliate = convert(taxiCompany);
            affiliate.setAdministratorEmails(newArrayList(transform(taxiCompany.getAdministrators(), new Function<Customer, String>() {

                @Override
                public String apply(final za.co.velvetant.taxi.engine.models.Customer customer) {
                    log.debug("Adding customer[{}] administrator email: {}", customer.getUserId(), customer.getEmail());
                    return customer.getEmail();
                }
            })));
            affiliate.setCabCount(taxiCompany.getDrivers().size());
            vos.add(affiliate);
        }

        return this;
    }

    @Override
    public List<TaxiCompany> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.TaxiCompany> vos() {
        return vos;
    }

    private AffiliateFilter withCompanyName(final String companyName) {
        example.setCompanyName(companyName);

        return this;
    }

    private AffiliateFilter withRegistration(final String registration) {
        example.setRegistration(registration);

        return this;
    }

    private AffiliateFilter isActive(final Boolean active) {
        example.setActive(active);

        return this;
    }
}
