package za.co.velvetant.taxi.engine.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import za.co.velvetant.taxi.engine.models.Corporate;
import za.co.velvetant.taxi.engine.models.Corporate_;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Customer_;
import za.co.velvetant.taxi.engine.models.Family;
import za.co.velvetant.taxi.engine.models.Family_;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.FareState;
import za.co.velvetant.taxi.engine.models.Fare_;
import za.co.velvetant.taxi.engine.persistence.CorporateRepository;
import za.co.velvetant.taxi.engine.persistence.FamilyRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.HttpHeaders;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Optional.fromNullable;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CASH;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CORPORATE_ACCOUNT;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.CREDIT_CARD;
import static za.co.velvetant.taxi.engine.models.PaymentMethod.FAMILY_ACCOUNT;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
@Scope(SCOPE_PROTOTYPE)
public class FareCompletedFilter implements Filter<Fare, za.co.velvetant.taxi.engine.api.Fare, FareCompletedFilter> {

    private static final Logger log = LoggerFactory.getLogger(FareCompletedFilter.class);

    @Inject
    private FareRepository fareRepository;

    @Inject
    private CorporateRepository corporateRepository;

    @Inject
    private FamilyRepository familyRepository;

    @Inject
    private CustomerRepository customerRepository;

    private String userId;
    private String registration;
    private String family;
    private HttpHeaders headers;

    private PageRequest sortable = new PageRequest(0, 10, new Sort(DESC, "requestTime"));

    private Long total;
    private List<Fare> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Fare> vos = new ArrayList<>();

    public FareCompletedFilter with(final String userId, final String registration, final String family) {
        this.userId = userId;
        this.registration = registration;
        this.family = family;

        return this;
    }

    @Override
    public FareCompletedFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public FareCompletedFilter filter() {
        Specification<Fare> specification;
        if (isNotBlank(userId)) {
            specification = new FareSpecification();
        } else if (isNotBlank(family) || isNotBlank(registration)) {
            specification = new AdminFareSpecification();
        } else {
            this.userId = CustomerRoutines.getUserId(headers);
            specification = new AllFareSpecification();
        }

        Page<Fare> results = fareRepository.findAll(specification, sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public FareCompletedFilter andConvert() {
        for (final Fare fare : filtered) {
            vos.add(convert(fare));
        }

        return this;
    }

    @Override
    public List<Fare> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Fare> vos() {
        return vos;
    }

    public FareCompletedFilter check(final HttpHeaders headers) {
        this.headers = headers;

        return this;
    }

    class FareSpecification implements Specification<Fare> {

        @Override
        public Predicate toPredicate(final Root<Fare> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
            List<Predicate> predicates = new ArrayList<>();
            if (isNotBlank(registration)) {
                predicates.add(cb.equal(root.get(Fare_.paymentMethod), CORPORATE_ACCOUNT));
                predicates.add(cb.and(cb.equal(root.get(Fare_.customer).get(Customer_.corporate).get(Corporate_.registration), registration)));
            } else if (isNotBlank(family)) {
                predicates.add(cb.equal(root.get(Fare_.paymentMethod), FAMILY_ACCOUNT));
                predicates.add(cb.and(cb.equal(root.get(Fare_.customer).get(Customer_.family).get(Family_.uuid), family)));
            } else {
                predicates.add(cb.or(cb.equal(root.get(Fare_.paymentMethod), CASH), cb.equal(root.get(Fare_.paymentMethod), CREDIT_CARD)));
            }

            predicates.add(cb.and(cb.equal(root.get(Fare_.customer).get(Customer_.userId), userId)));
            predicates.add(cb.and(cb.equal(root.get(Fare_.state), FareState.COMPLETED)));
            return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
        }
    }

    class AdminFareSpecification implements Specification<Fare> {

        @Override
        public Predicate toPredicate(final Root<Fare> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
            List<Customer> customers = new ArrayList<>();
            List<Predicate> predicates = new ArrayList<>();
            if (isNotBlank(registration)) {
                customers.addAll(customerRepository.findByCorporateRegistration(registration));
                customers.addAll(fromNullable(corporateRepository.fetchWithAdministrators(registration)).or(new Corporate()).getAdministrators());
                predicates.add(cb.equal(root.get(Fare_.paymentMethod), CORPORATE_ACCOUNT));
                predicates.add(root.get(Fare_.customer).in(customers));
            } else if (isNotBlank(family)) {
                customers.addAll(customerRepository.findByFamilyUuid(family));
                customers.addAll(fromNullable(familyRepository.fetchWithAdministrators(family)).or(new Family()).getAdministrators());
                predicates.add(cb.equal(root.get(Fare_.paymentMethod), FAMILY_ACCOUNT));
                predicates.add(root.get(Fare_.customer).in(customers));
            }

            predicates.add(cb.and(cb.equal(root.get(Fare_.state), FareState.COMPLETED)));
            return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
        }
    }

    class AllFareSpecification implements Specification<Fare> {

        @Override
        public Predicate toPredicate(final Root<Fare> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.and(cb.equal(root.get(Fare_.customer).get(Customer_.userId), userId)));
            predicates.add(cb.and(cb.equal(root.get(Fare_.state), FareState.COMPLETED)));
            return query.where(predicates.toArray(new Predicate[predicates.size()])).getRestriction();
        }
    }

}
