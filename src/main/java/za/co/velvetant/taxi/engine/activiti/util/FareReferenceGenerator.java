package za.co.velvetant.taxi.engine.activiti.util;

import javax.inject.Inject;
import javax.inject.Named;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.FareReference;
import za.co.velvetant.taxi.engine.persistence.FareReferenceRepository;

@Named
public class FareReferenceGenerator {

    private static final Logger log = LoggerFactory.getLogger(FareReferenceGenerator.class);

    @Inject
    private FareReferenceRepository fareReferenceRepository;

    public String generate(final Fare fare) {
        String reference = fare.getUuid();

        try {
            final FareReference fareReference = new FareReference();
            fareReference.setFare(fare);
            fareReference.setCreatedTime(fare.getRequestTime());
            final Long referenceId = fareReferenceRepository.save(fareReference).getId();

            final DateTime dateTime = new DateTime();
            Long countSince = fareReferenceRepository.findFirst(dateTime.dayOfMonth().withMinimumValue().withTimeAtStartOfDay().toDate());
            reference = String.format("%s-%04d", dateTime.toString("yyMMdd"), Math.abs(referenceId - --countSince));
        } catch (Exception e) {
            log.error("Could not generate fare reference, using fare uuid", e);
        }

        return reference;
    }

    public static void main(String[] args) {
        System.out.println(new DateTime().dayOfMonth().withMinimumValue().withTimeAtStartOfDay().toDate());
    }
}
