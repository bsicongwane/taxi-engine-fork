package za.co.velvetant.taxi.engine.external;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MYGATE_APPLICATION_ID;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MYGATE_MERCHANT_ID;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.MYGATE_MODE;

import java.rmi.RemoteException;
import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.xml.rpc.ServiceException;

import za.co.velvetant.mygate.BillingDetails;
import za.co.velvetant.mygate.MyGateResponse;
import za.co.velvetant.mygate.MyGateServiceImpl;
import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

public class MyGateSOAP implements PaymentService {

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    private MyGateServiceImpl myGateService;

    @PostConstruct
    public void init() {
        String merchantId = systemPropertyHelper.getProperty(MYGATE_MERCHANT_ID);
        String applicationId = systemPropertyHelper.getProperty(MYGATE_APPLICATION_ID);
        String mode = systemPropertyHelper.getProperty(MYGATE_MODE);

        myGateService = new MyGateServiceImpl(merchantId, applicationId, mode);
    }

    @Override
    public MyGateResponse storeCard(final String clientToken, final CreditCard creditCard) throws RemoteException, ServiceException, ParseException {
        return myGateService.storeCard(clientToken, creditCard);
    }

    @Override
    public MyGateResponse updateCard(final String clientToken, final CreditCard creditCard) throws RemoteException, ServiceException, ParseException {
        return myGateService.updateCard(clientToken, creditCard);
    }

    @Override
    public MyGateResponse processPayment(final String clientToken, final BillingDetails billingDetails) throws RemoteException, ServiceException, ParseException {
        return myGateService.processPayment(clientToken, billingDetails);
    }

    @Override
    public CreditCard getMaskedCardDetails(final String clientToken) throws RemoteException, ServiceException, ParseException {
        return myGateService.getMaskedCardDetails(clientToken);
    }

    @Override
    public MyGateResponse removeCard(final String clientToken) throws RemoteException, ServiceException, ParseException {
        return myGateService.removeCard(clientToken);
    }

    @Override
    public MyGateResponse verifyExists(final String clientToken) throws RemoteException, ServiceException, ParseException {
        return myGateService.verifyExists(clientToken);
    }
}
