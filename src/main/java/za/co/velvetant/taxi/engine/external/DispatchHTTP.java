package za.co.velvetant.taxi.engine.external;

import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static za.co.velvetant.taxi.engine.util.TaxiUrlConfigurations.getTaxiDispatchDriverShiftAvailableUrl;
import static za.co.velvetant.taxi.engine.util.TaxiUrlConfigurations.getTaxiDispatchDriverShiftBusyUrl;
import static za.co.velvetant.taxi.engine.util.TaxiUrlConfigurations.getTaxiDispatchDriverShiftEndUrl;
import static za.co.velvetant.taxi.engine.util.TaxiUrlConfigurations.getTaxiDispatchExpiredShiftsUrl;
import static za.co.velvetant.taxi.engine.util.TaxiUrlConfigurations.getTaxiDispatchFaresNotificationUrl;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

import za.co.velvetant.taxi.api.common.TaxiDistance;
import za.co.velvetant.taxi.dispatch.api.driver.FullDriverShift;
import za.co.velvetant.taxi.engine.api.Fare;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.sun.jersey.core.util.Base64;

@Named
public class DispatchHTTP implements Dispatch {

    private static final Logger log = LoggerFactory.getLogger(DispatchHTTP.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    @Inject
    private RestTemplate restTemplate;

    @SuppressWarnings("unchecked")
    @Override
    public Collection<TaxiDistance> fare(final Fare fare, final int closest, final int radius, final int window) {
        try {
            final HttpHeaders headers = createHeaders();
            headers.setAccept(ImmutableList.<MediaType> of(APPLICATION_JSON));

            final HttpEntity<Fare> httpEntity = new HttpEntity<>(fare, headers);
            final ResponseEntity<List> response = restTemplate.exchange(format("%s?radius=%d&closest=%d&window=%d", getTaxiDispatchFaresNotificationUrl(), radius, closest, window), HttpMethod.PUT,
                    httpEntity, List.class, fare.getUuid());
            log.debug("Notified dispatch of new fare request: {}", fare);

            return mapper.convertValue(response.getBody(), new TypeReference<List<TaxiDistance>>() {
            });
        } catch (final Exception e) {
            log.error("Error invoking dispatch", e);
            throw new PreconditionFailedException(e, "Dispatch not available over HTTP." + e.getMessage());
        }
    }

    @Override
    public Collection<FullDriverShift> expired(final Long expiration) {
        try {
            final HttpHeaders headers = createHeaders();
            headers.setAccept(ImmutableList.<MediaType> of(APPLICATION_JSON));

            final HttpEntity<Fare> httpEntity = new HttpEntity<>(headers);
            final ResponseEntity<List> response = restTemplate.exchange(format("%s?expiration=%d", getTaxiDispatchExpiredShiftsUrl(), expiration), HttpMethod.GET, httpEntity, List.class);

            return mapper.convertValue(response.getBody(), new TypeReference<List<FullDriverShift>>() {
            });
        } catch (final Exception e) {
            log.error("Error invoking dispatch", e);
            throw new PreconditionFailedException(e, "Dispatch not available over HTTP." + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @Async
    public void removeFare(final String uuid) {
        try {
            final HttpEntity<String> httpEntity = new HttpEntity<>(createHeaders());
            restTemplate.exchange(getTaxiDispatchFaresNotificationUrl(), HttpMethod.DELETE, httpEntity, (Class) null, uuid);
        } catch (final Exception e) {
            log.error("Error invoking dispatch", e);
            throw new PreconditionFailedException(e, "Dispatch not available over HTTP." + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @Async
    public void makeDriverBusy(final String shiftIdentifier) {
        try {
            final HttpEntity<String> httpEntity = new HttpEntity<>(createHeaders());
            restTemplate.exchange(getTaxiDispatchDriverShiftBusyUrl(), HttpMethod.POST, httpEntity, (Class) null, shiftIdentifier);
        } catch (final Exception e) {
            log.error("Error invoking dispatch", e);
            throw new PreconditionFailedException(e, "Dispatch not available over HTTP." + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @Async
    public void makeDriverAvailable(final String shiftIdentifier) {
        try {
            final HttpEntity<String> httpEntity = new HttpEntity<>(createHeaders());
            restTemplate.exchange(getTaxiDispatchDriverShiftAvailableUrl(), HttpMethod.POST, httpEntity, (Class) null, shiftIdentifier);
        } catch (final Exception e) {
            log.error("Error invoking dispatch", e);
            throw new PreconditionFailedException(e, "Dispatch not available over HTTP." + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    @Async
    public void endDriverShift(final String shiftIdentifier) {
        try {
            final HttpEntity<String> httpEntity = new HttpEntity<>(createHeaders());
            restTemplate.exchange(format("%s?system=true", getTaxiDispatchDriverShiftEndUrl()), HttpMethod.POST, httpEntity, (Class) null, shiftIdentifier);
        } catch (final Exception e) {
            log.error("Error invoking dispatch", e);
            throw new PreconditionFailedException(e, "Dispatch not available over HTTP." + e.getMessage());
        }
    }

    private HttpHeaders createHeaders() {

        final HttpHeaders httpHeaders = new HttpHeaders();

        // Basic Authentication
        final String authorisation = "dispatch" + ":" + "disp@tch123!";
        final byte[] encodedAuthorisation = Base64.encode(authorisation.getBytes());
        httpHeaders.add("Authorization", "Basic " + new String(encodedAuthorisation));

        return httpHeaders;
    }
}
