package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.RewardGroup;

@Repository
public interface RewardGroupRepository extends CrudRepository<RewardGroup, Long> {

    @Query("select h from RewardGroup h where h.amount >= ?1 order by amount asc")
    List<RewardGroup> findGroup(Long points);

    RewardGroup findByGroupName(String groupName);
}
