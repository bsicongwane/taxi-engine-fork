package za.co.velvetant.taxi.engine.external;

import static za.co.velvetant.taxi.engine.util.TaxiUrlConfigurations.getTaxiOpsNotificationsUrl;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

import za.co.velvetant.taxi.ops.notifications.Notification;

public class OperationsHTTP implements Operations {

    private static final Logger log = LoggerFactory.getLogger(OperationsHTTP.class);

    @Inject
    private RestTemplate restTemplate;

    @Override
    @Async
    public <T> void notify(final Notification<T> notification) {
        log.debug("Notifying operations: {}", notification);
        try {
            restTemplate.postForLocation(getTaxiOpsNotificationsUrl(), notification);
            log.debug("Notified operations");
        } catch (Throwable t) {
            log.error("Error notifying operations", t);
        }
    }
}
