package za.co.velvetant.taxi.engine.delegates.payments;

import static za.co.velvetant.taxi.engine.model.Payment.PaymentBuilder.creditCardPayment;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.model.Payment;
import za.co.velvetant.taxi.engine.models.CreditCard;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

@Named("FAMILY_ACCOUNT")
public class FamilyCreditCardProcessor extends CreditCardProcessor {

    private static final Logger log = LoggerFactory.getLogger(FamilyCreditCardProcessor.class);

    @Inject
    private CustomerRepository customerRepository;

    @Override
    public Payment createPayment(final Fare trip, final String cvv) {
        return creditCardPayment(trip, cvv).forAmount(trip.getAmount()).plusTip(trip.getTip());
    }

    @Override
    protected CreditCard getCreditCard(final String userId) {
        log.debug("Getting family credit card for: {}", userId);

        final Customer customer = customerRepository.findByUserId(userId);
        final List<CreditCard> creditCards = customer.getFamily().getCreditCards();
        if (creditCards.isEmpty() || creditCards.size() > 1) {
            throw new IllegalStateException("Family must only have one credit card loaded");
        }

        return creditCards.iterator().next();
    }

    @Override
    protected MyGateTransaction linkTransaction(final MyGateTransaction transaction, final Fare trip) {
        MyGateTransaction linkedTransaction = super.linkTransaction(transaction, trip);
        linkedTransaction.setFamily(trip.getCustomer().getFamily());

        return linkedTransaction;
    }
}
