package za.co.velvetant.taxi.engine.rest;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.api.CreateCustomer;
import za.co.velvetant.taxi.engine.api.CreateFamily;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.Customer;
import za.co.velvetant.taxi.engine.api.Family;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/families")
@Api(value = "/families", description = "Services for managing family accounts.")
@RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "FAMILY"})
public interface FamilyService {

    String CREDIT_CARD_CLASS = "za.co.velvetant.taxi.engine.api.CreditCard";
    String FAMILY_CLASS = "za.co.velvetant.taxi.engine.api.Family";

    @POST
    @ApiOperation(value = "Create a new family.", responseClass = FAMILY_CLASS)
    Response create(@ApiParam(value = "Family to create", required = true) CreateFamily family);

    @POST
    @Path("/admin")
    @ApiOperation(value = "Create a new family by an administrative user.", responseClass = FAMILY_CLASS)
    @RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT"})
    Response createAdministratively(@ApiParam(value = "Userid of the family head", required = true) @QueryParam("familyHead") String familyHead,
            @ApiParam(value = "Family to create", required = true) CreateFamily family);

    @PUT
    @Path("/{uuid}")
    @ApiOperation(value = "Change a family name.", responseClass = FAMILY_CLASS)
    Response edit(@ApiParam(value = "Family to edit", required = true) Family family, @ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid);

    @Path("/{uuid}/members")
    @POST
    @ApiOperation(value = "Add member to family", responseClass = "za.co.velvetant.taxi.engine.api.CreateCustomer")
    CreateCustomer addFamilyMember(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = "Customer toC add to family", required = true) CreateCustomer customer);

    @Path("/{uuid}/members/{userid}")
    @PUT
    @ApiOperation(value = "Add an existing customer to family")
    Response addFamilyMember(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{uuid}/members")
    @ApiOperation(value = "Finds all members of a family.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.Customer")
    List<Customer> findMembers(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid);

    @GET
    @Path("/{uuid}")
    @ApiOperation(value = "Get all family account's details including all members.", responseClass = FAMILY_CLASS)
    Family find(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid);

    @PUT
    @Path("/{uuid}/administrator/{userid}")
    @ApiOperation(value = "Set a family's administrator.")
    Response assignAdministrator(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @ApiOperation(value = "Finds all the family accounts in the system.", multiValueResponse = true, responseClass = FAMILY_CLASS)
    List<Family> findAll();

    @Path("/all")
    @GET
    @ApiOperation(value = "Find all families.", multiValueResponse = true, responseClass = FAMILY_CLASS, notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    @RolesAllowed({"SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT"})
    Response findAll(@ApiParam(value = "Filter by uuid.") @QueryParam("uuid") String uuid,
            @ApiParam(value = "Filter by family name. Partial filtering is allowed.") @QueryParam("familyName") String familyName,
            @ApiParam(value = "Page number of customers to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number customers to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "familyName", allowableValues = "familyName") @QueryParam("sort") @DefaultValue("familyName") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

    @DELETE
    @Path("/{uuid}/administrator/{userid}")
    @ApiOperation(value = "Remove a family's administrator.")
    Response removeAdministrator(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @DELETE
    @Path("/{uuid}/members/{userid}")
    @ApiOperation(value = "Remove a customer from a family.")
    Response removeMember(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{uuid}/administrators")
    @ApiOperation(value = "Get a family's administrators.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.Customer")
    List<Customer> findAdministrators(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid);

    @Path("/{uuid}/creditCard")
    @GET
    @ApiOperation(value = "Get a family's credit card", responseClass = CREDIT_CARD_CLASS)
    Response getCreditCard(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid);

    @PUT
    @Path("/{uuid}/creditCard")
    @ApiOperation(value = "Upload credit card details.", responseClass = CREDIT_CARD_CLASS)
    Response uploadCreditCard(@ApiParam(value = DocumentationMessages.M_FAMILY_ID, required = true) @PathParam("uuid") String uuid,
            @ApiParam(value = "Credit Card details.", required = true) CreditCard card);

}
