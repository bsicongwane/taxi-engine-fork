package za.co.velvetant.taxi.engine.rest;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.noContent;
import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.api.SystemProperty;
import za.co.velvetant.taxi.engine.filters.SystemPropertyFilter;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.persistence.repositories.SystemPropertyRepository;

@Named
@Path("/systemProperties.json")
@Transactional
public class SystemPropertyServiceImpl implements SystemPropertyService {

    @Inject
    private SystemPropertyRepository systemPropertyRepository;

    @Inject
    private Provider<SystemPropertyFilter> filter;

    @Override
    public Response create(final SystemProperty systemProperty) {
        za.co.velvetant.taxi.engine.models.SystemProperty entity = convert(systemProperty, new za.co.velvetant.taxi.engine.models.SystemProperty());
        entity.setStartTime(new Date());
        entity = systemPropertyRepository.save(entity);

        return status(CREATED).entity(convert(entity, new SystemProperty())).build();
    }

    @Override
    public Response edit(final String name, final SystemProperty systemProperty) {
        final za.co.velvetant.taxi.engine.models.SystemProperty currentSystemProperty = loadActiveProperty(name);
        remove(name);

        SystemProperty editableSystemProperty = convert(currentSystemProperty, new SystemProperty());
        editableSystemProperty.setValue(systemProperty.getValue());
        editableSystemProperty.activate();
        create(editableSystemProperty);

        return ok().entity(convert(loadActiveProperty(name), new SystemProperty())).build();
    }

    @Override
    public Response find(final String name) {
        final za.co.velvetant.taxi.engine.models.SystemProperty property = loadActiveProperty(name);

        return ok().entity(convert(property, new SystemProperty())).build();
    }

    @Override
    public Response remove(final String name) {
        final za.co.velvetant.taxi.engine.models.SystemProperty currentSystemProperty = systemPropertyRepository.findActiveProperty(name);
        currentSystemProperty.setEndTime(new Date());
        systemPropertyRepository.save(currentSystemProperty);

        return ok().build();
    }

    @Override
    public Response activate(final String name) {
        final za.co.velvetant.taxi.engine.models.SystemProperty currentSystemProperty = systemPropertyRepository.findInactiveProperty(name);
        currentSystemProperty.setEndTime(null);
        systemPropertyRepository.save(currentSystemProperty);

        return ok().build();
    }

    @Override
    public Response findAll(final String name, final Boolean active, final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        final SystemPropertyFilter filteredProperties = filter.get().with(name, active).sort(page, size, sort, order).filter();
        final List<SystemProperty> vos = filteredProperties.andConvert().vos();

        Response.ResponseBuilder response = ok().entity(vos).header("X-Total-Entities", filteredProperties.totalEntities());
        if (vos.isEmpty()) {
            response = noContent();
        }

        return response.build();
    }

    private za.co.velvetant.taxi.engine.models.SystemProperty loadActiveProperty(final String name) {
        final za.co.velvetant.taxi.engine.models.SystemProperty systemProperty = systemPropertyRepository.findActiveProperty(name);
        NotFoundException.assertNotNull(systemProperty, format("SystemProperty with name %s was not found", name));

        return systemProperty;
    }

}
