package za.co.velvetant.taxi.engine.rest;

import static com.google.common.base.Optional.fromNullable;
import static javax.ws.rs.core.Response.ok;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static za.co.velvetant.taxi.engine.models.FareState.CANCELLED;
import static za.co.velvetant.taxi.engine.models.FareState.CANCELLED_BY_ADMIN;
import static za.co.velvetant.taxi.engine.models.FareState.CANCELLED_BY_CUSTOMER;
import static za.co.velvetant.taxi.engine.models.FareState.CANCELLED_BY_DRIVER;
import static za.co.velvetant.taxi.engine.models.driver.DriverShiftState.AVAILABLE;
import static za.co.velvetant.taxi.engine.models.driver.DriverShiftState.BUSY;
import static za.co.velvetant.taxi.engine.models.driver.DriverShiftState.ENDED;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.PaymentMethod;
import za.co.velvetant.taxi.engine.models.Rating;
import za.co.velvetant.taxi.engine.models.RewardPoint;
import za.co.velvetant.taxi.engine.persistence.DriverShiftStatisticsRepository;
import za.co.velvetant.taxi.engine.persistence.FareStatisticsRepository;
import za.co.velvetant.taxi.engine.persistence.RatingRepository;
import za.co.velvetant.taxi.engine.persistence.RewardPointRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.statistics.CustomerTotals;
import za.co.velvetant.taxi.statistics.Totals;

@Named
@Path("/statistics.json")
public class StatisticsServiceResource implements StatisticsService {

    private static final Logger log = LoggerFactory.getLogger(StatisticsServiceResource.class);

    @Inject
    private DriverShiftStatisticsRepository driverShiftStatisticsRepository;

    @Inject
    private FareStatisticsRepository fareStatisticsRepository;

    @Inject
    private FareRepository fareRepository;

    @Inject
    private RewardPointRepository rewardPointRepository;

    @Inject
    private RatingRepository ratingRepository;

    @Override
    public Response totals(final String registration, final Long from, final Long to) {
        za.co.velvetant.taxi.statistics.Totals totals = new Totals();

        Date fromDate = new Date(from);
        Date toDate = new Date(to);
        log.debug("Getting totals for affiliate [{}] from [{}] to [{}]", registration, fromDate, toDate);

        if (isBlank(registration)) {
            totals.setOnlineDrivers(driverShiftStatisticsRepository.countByStateNot(ENDED));
            totals.setAvailableDrivers(driverShiftStatisticsRepository.countByState(AVAILABLE));
            totals.setEngagedDrivers(driverShiftStatisticsRepository.countByState(BUSY));

            totals.setTrips(fareStatisticsRepository.countTotalTrips(fromDate, toDate));
            totals.setCancelledByCustomer(fareStatisticsRepository.countCancelled(fromDate, toDate, CANCELLED_BY_CUSTOMER));
            totals.setCancelledByDriver(fareStatisticsRepository.countCancelled(fromDate, toDate, CANCELLED_BY_DRIVER));
            totals.setCancelledByOps(fareStatisticsRepository.countCancelled(fromDate, toDate, CANCELLED_BY_ADMIN));
            totals.setNotAcceptedTrips(fareStatisticsRepository.countCancelled(fromDate, toDate, CANCELLED));

            Object[] fareTotals = fareStatisticsRepository.findTotalFareCost(fromDate, toDate);
            assignTotals(totals, fareTotals);
        } else {
            totals.setOnlineDrivers(driverShiftStatisticsRepository.countByStateNot(ENDED, registration));
            totals.setAvailableDrivers(driverShiftStatisticsRepository.countByState(AVAILABLE, registration));
            totals.setEngagedDrivers(driverShiftStatisticsRepository.countByState(BUSY, registration));

            totals.setTrips(fareStatisticsRepository.countTotalTrips(registration, fromDate, toDate));
            totals.setCancelledByCustomer(fareStatisticsRepository.countCancelled(registration, fromDate, toDate, CANCELLED_BY_CUSTOMER));
            totals.setCancelledByDriver(fareStatisticsRepository.countCancelled(registration, fromDate, toDate, CANCELLED_BY_DRIVER));
            totals.setCancelledByOps(fareStatisticsRepository.countCancelled(registration, fromDate, toDate, CANCELLED_BY_ADMIN));
            totals.setNotAcceptedTrips(fareStatisticsRepository.countCancelled(fromDate, toDate, CANCELLED));

            Object[] fareTotals = fareStatisticsRepository.findTotalFareCost(registration, fromDate, toDate);
            assignTotals(totals, fareTotals);
        }

        return ok().entity(totals).build();
    }

    @Override
    public Response customerTotals(final String userid) {
        CustomerTotals customerTotals = new CustomerTotals();

        Double totalSpent = 0.0;
        Double totalCreditCardSpend = 0.0;

        Long totalSnappMiles = 0L;
        Long totalDriverRatings = 0L;
        Long totalTripRatings = 0L;
        Long averageDriverRating = 0L;
        Long averageTripRating = 0L;

        List<Fare> customerFares = fareRepository.findByCustomerUserId(userid);
        List<RewardPoint> rewardPoints = rewardPointRepository.findByCustomerUserId(userid);
        List<Rating> ratings = ratingRepository.findByFareCustomerUserId(userid);

        // Work out average ratings
        if (!ratings.isEmpty()) {
            for (Rating candidate : ratings) {
                if (candidate.getDriverRating() != null) {
                    totalDriverRatings++;
                    averageDriverRating += candidate.getDriverRating().getRating();
                }

                if (candidate.getTripRating() != null) {
                    averageTripRating += candidate.getTripRating().getRating();
                    totalTripRatings++;
                }
            }
        }

        if (averageDriverRating != 0)
            averageDriverRating /= totalDriverRatings;

        if (averageTripRating != 0)
            averageTripRating /= totalTripRatings;

        // Work out the total value a customer has spent on trips and how much has been paid for using a credit card
        if (!customerFares.isEmpty()) {
            for (Fare candidate : customerFares) {
                if (candidate.getAmount() != null) {
                    totalSpent += candidate.getAmount();

                    if (candidate.getPaymentMethod().equals(PaymentMethod.CREDIT_CARD))
                        totalCreditCardSpend += candidate.getAmount();

                    if (candidate.getTip() != null) {
                        totalSpent += candidate.getTip();

                        if (candidate.getPaymentMethod().equals(PaymentMethod.CREDIT_CARD))
                            totalCreditCardSpend += candidate.getTip();
                    }
                }
            }
        }

        if (!rewardPoints.isEmpty()) {
            for (RewardPoint candidate : rewardPoints) {
                if (candidate != null) {
                    totalSnappMiles += candidate.getTotal();
                }
            }
        }

        customerTotals.setSnappMiles(totalSnappMiles);
        customerTotals.setTotalCreditCardPayment(totalCreditCardSpend);
        customerTotals.setTotalDriverRatings(totalDriverRatings);
        customerTotals.setTotalTripRatings(totalTripRatings);
        customerTotals.setTotalSpent(totalSpent);
        customerTotals.setTotalCompletedTrips(fareRepository.countCustomerCompletedTrips(userid));
        customerTotals.setTotalTrips(fareRepository.countCustomerTrips(userid));
        customerTotals.setTotalNonCompletedTrips(customerTotals.getTotalTrips() - customerTotals.getTotalCompletedTrips());
        customerTotals.setAverageDriverRating(averageDriverRating);
        customerTotals.setAverageTripRating(averageTripRating);

        return ok().entity(customerTotals).build();
    }

    private void assignTotals(final Totals totals, final Object[] fareTotals) {
        if (fareTotals != null && fareTotals.length > 0) {
            totals.setTotalFareCosts((Double) fromNullable(fareTotals[0]).or(0.00));
            totals.setAverageDuration(((BigDecimal) fromNullable((fareTotals[1])).or(new BigDecimal("0"))).longValue());
            totals.setTotalDuration(((BigDecimal) fromNullable((fareTotals[2])).or(new BigDecimal("0"))).longValue());
            totals.setAverageDistance(((BigDecimal) fromNullable((fareTotals[3])).or(new BigDecimal("0"))).longValue());
            totals.setTotalDistance(((BigDecimal) fromNullable((fareTotals[4])).or(new BigDecimal("0"))).longValue());
            totals.setTotalClipCost(((BigDecimal) fromNullable((fareTotals[5])).or(new BigDecimal("0.00"))).doubleValue());
        }
    }
}
