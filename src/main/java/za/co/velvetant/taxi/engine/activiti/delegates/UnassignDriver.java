package za.co.velvetant.taxi.engine.activiti.delegates;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.external.Dispatch;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.models.driver.DriverShift;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.driver.DriverShiftRepository;

@Named
public class UnassignDriver extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(UnassignDriver.class);

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private DriverShiftRepository driverShiftRepository;

    @Inject
    private Dispatch dispatch;

    @Override
    public void process(final DelegateExecution execution) {
        log.debug("Completing fare: {}", execution.getProcessBusinessKey());

        final Driver driver = driverRepository.findByUserId(getVariable(execution, "driver", String.class));
        if (driver != null) {
            final DriverShift shift = driverShiftRepository.findActiveDriverShiftWithDriverUserId(driver.getUserId());
            if(shift != null) {
                dispatch.makeDriverAvailable(shift.getShiftIdentifier());
            }
        }
    }
}
