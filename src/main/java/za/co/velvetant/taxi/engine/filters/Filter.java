package za.co.velvetant.taxi.engine.filters;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.models.BaseEntityModel;

public interface Filter<E extends BaseEntityModel, V extends Serializable, T extends Filter> {

    T sort(Integer page, Integer size, String sort, Sort.Direction order);

    T filter();

    T andConvert();

    List<E> entities();

    Long totalEntities();

    List<V> vos();
}
