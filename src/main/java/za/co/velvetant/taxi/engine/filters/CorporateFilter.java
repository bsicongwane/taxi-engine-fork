package za.co.velvetant.taxi.engine.filters;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.transform;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.models.Corporate;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.persistence.CorporateRepository;

import com.google.common.base.Function;

@Named
@Scope(SCOPE_PROTOTYPE)
public class CorporateFilter implements Filter<Corporate, za.co.velvetant.taxi.engine.api.Corporate, CorporateFilter> {

    private static final Logger log = LoggerFactory.getLogger(CorporateFilter.class);

    @Inject
    private CorporateRepository corporateRepository;

    private Corporate example = new Corporate();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "companyName"));

    private Long total;
    private List<Corporate> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Corporate> vos = new ArrayList<>();

    public CorporateFilter with(final String companyName, final String registration, final Boolean active) {
        return withCompanyName(companyName).withRegistration(registration).isActive(active);
    }

    @Override
    public CorporateFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public CorporateFilter filter() {
        Page<Corporate> results = corporateRepository.findWithExample(example, sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    @Transactional
    public CorporateFilter andConvert() {
        for (final Corporate taxiCompany : filtered) {
            za.co.velvetant.taxi.engine.api.Corporate corporate = convert(taxiCompany, new za.co.velvetant.taxi.engine.api.Corporate());
            corporate.setAdministratorEmails(newArrayList(transform(taxiCompany.getAdministrators(), new Function<Customer, String>() {

                @Override
                public String apply(final Customer customer) {
                    log.debug("Adding customer[{}] administrator email: {}", customer.getUserId(), customer.getEmail());
                    return customer.getEmail();
                }
            })));
            vos.add(corporate);
        }

        return this;
    }

    @Override
    public List<Corporate> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Corporate> vos() {
        return vos;
    }

    private CorporateFilter withCompanyName(final String companyName) {
        example.setCompanyName(companyName);

        return this;
    }

    private CorporateFilter withRegistration(final String registration) {
        example.setRegistration(registration);

        return this;
    }

    private CorporateFilter isActive(final Boolean active) {
        example.setActive(active);

        return this;
    }
}
