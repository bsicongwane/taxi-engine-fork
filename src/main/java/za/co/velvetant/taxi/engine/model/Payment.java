package za.co.velvetant.taxi.engine.model;

import static com.google.common.base.Optional.fromNullable;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;
import static za.co.velvetant.taxi.engine.models.transactions.TransactionType.CASH;
import static za.co.velvetant.taxi.engine.models.transactions.TransactionType.CREDIT_CARD;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.ToStringBuilder;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.transactions.TransactionType;

public class Payment {

    private Fare trip;
    private String cvv;
    private TransactionType type;
    private BigDecimal tripAmount;
    private BigDecimal invoiceAmount;

    private Payment() {
    }

    public Payment forAmount(final BigDecimal amount) {
        this.tripAmount = fromNullable(amount).or(new BigDecimal(0.00));

        return this;
    }

    public Payment plusTip(final BigDecimal tip) {
        this.tripAmount = this.tripAmount.add(fromNullable(tip).or(new BigDecimal(0.00)));

        return this;
    }

    public Payment forAmount(final Double amount) {
        forAmount(new BigDecimal(fromNullable(amount).or(0.00)));

        return this;
    }

    public Payment plusTip(final Double tip) {
        plusTip(new BigDecimal(fromNullable(tip).or(0.00)));

        return this;
    }

    public Fare getTrip() {
        return trip;
    }

    public String getCvv() {
        return cvv;
    }

    public TransactionType getType() {
        return type;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, SHORT_PREFIX_STYLE);
    }

    public BigDecimal getTripAmount() {
        return tripAmount;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(final BigDecimal invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public static class PaymentBuilder {

        public static Payment cashPayment(final Fare trip) {
            Payment payment = new Payment();
            payment.trip = trip;
            payment.type = CASH;

            return payment;
        }

        public static Payment creditCardPayment(final Fare trip, final String cvv) {
            Payment payment = new Payment();
            payment.trip = trip;
            payment.type = CREDIT_CARD;
            payment.cvv = cvv;

            return payment;
        }
    }
}
