package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.api.common.DriverLocation;
import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.ops.notifications.DriverArrivedAtDestinationNotification;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class ArrivedAtDestination extends ExecutionDelegate {

    @Inject
    private FareRepository fareRepository;

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private Operations operations;

    @Override
    public void process(final DelegateExecution execution) {
        String driverId = getVariable(execution, "driver", String.class);
        Driver driver = driverRepository.findByUserId(driverId);

        Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());

        operations.notify(new DriverArrivedAtDestinationNotification(execution.getProcessBusinessKey(), ArrivedAtDestination.class, new DriverLocation(convert(driver), convert(fare.getDropOff()))));
    }
}