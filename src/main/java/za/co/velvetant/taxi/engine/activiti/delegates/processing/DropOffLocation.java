package za.co.velvetant.taxi.engine.activiti.delegates.processing;

import static com.google.common.base.Optional.fromNullable;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.api.common.Leg;
import za.co.velvetant.taxi.api.common.Routes;
import za.co.velvetant.taxi.engine.external.google.DirectionsService;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.Location;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
@Transactional
public class DropOffLocation {

    private static final Logger log = LoggerFactory.getLogger(DropOffLocation.class);

    @Inject
    private DirectionsService directionsService;

    @Inject
    private FareRepository fareRepository;

    public void process(final String uuid, final za.co.velvetant.taxi.api.common.Location pickup, final za.co.velvetant.taxi.api.common.Location dropoff) {
        log.debug("Processing drop off location for fare [{}] with drop off location: {}", uuid, dropoff);
        final Fare fare = fareRepository.findByUUID(uuid);
        if (dropoff != null) {
            final Location from = fare.getPickup();
            final Location to = new Location(dropoff.getLatitude().doubleValue(), dropoff.getLongitude().doubleValue());

            final Routes route = directionsService.get(convert(from), convert(to)).getFirstRoute();
            from.setAddress(getFromAddress(route, pickup.getAddress()));
            to.setAddress(getToAddress(route, dropoff.getAddress()));
            fare.setDropOff(to);
            fare.setDistance(route.getFirstLeg().getDistance().getValue());
            fare.setDuration(route.getFirstLeg().getDuration().getValue());
        }

        fareRepository.save(fare);
    }

    private String getFromAddress(final Routes route, final String address) {
        String fromAddress = address;
        if (route != null && isBlank(fromAddress)) {
            fromAddress = fromNullable(route.getFirstLeg()).or(new Leg()).getStartAddress();
        }

        return fromAddress;
    }

    private String getToAddress(final Routes route, final String address) {
        String toAddress = address;
        if (isBlank(toAddress)) {
            toAddress = fromNullable(route.getFirstLeg()).or(new Leg()).getEndAddress();
        }

        return toAddress;
    }
}
