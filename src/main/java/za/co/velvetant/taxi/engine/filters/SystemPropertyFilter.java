package za.co.velvetant.taxi.engine.filters;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.models.SystemProperty;
import za.co.velvetant.taxi.engine.models.SystemProperty_;
import za.co.velvetant.taxi.persistence.repositories.SystemPropertyRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class SystemPropertyFilter implements Filter<SystemProperty, za.co.velvetant.taxi.engine.api.SystemProperty, SystemPropertyFilter> {

    private static final Logger log = LoggerFactory.getLogger(SystemPropertyFilter.class);

    @Inject
    private SystemPropertyRepository repository;

    private SystemProperty example = new SystemProperty();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "name"));

    private Long total;
    private List<SystemProperty> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.SystemProperty> vos = new ArrayList<>();

    public SystemPropertyFilter with(final String name, final Boolean active) {
        return withName(name).isActive(active);
    }

    @Override
    public SystemPropertyFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public SystemPropertyFilter filter() {
        Page<SystemProperty> results = repository.findAll(new SystemPropertySpecification(example), sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    @Transactional
    public SystemPropertyFilter andConvert() {
        for (final SystemProperty systemProperty : filtered) {
            vos.add(convert(systemProperty, new za.co.velvetant.taxi.engine.api.SystemProperty()));
        }

        return this;
    }

    @Override
    public List<SystemProperty> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.SystemProperty> vos() {
        return vos;
    }

    private SystemPropertyFilter withName(final String name) {
        example.setName(name);

        return this;
    }

    private SystemPropertyFilter isActive(final Boolean active) {
        if (active) {
            example.setEndTime(null);
        } else {
            example.setEndTime(new Date());
        }

        return this;
    }

    class SystemPropertySpecification implements Specification<SystemProperty> {

        public SystemProperty property;

        SystemPropertySpecification(final SystemProperty property) {
            this.property = new LikeableSystemProperty(property);
        }

        @Override
        public Predicate toPredicate(final Root<SystemProperty> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
            Predicate activePredicate;
            Path<Date> activePath = root.get(SystemProperty_.endTime);
            if (property.getEndTime() != null) {
                activePredicate = cb.isNotNull(activePath);
            } else {
                activePredicate = cb.isNull(activePath);
            }

            query.where(cb.and(activePredicate), cb.and(cb.like(root.get(SystemProperty_.name), property.getName())));

            return query.getRestriction();
        }

        class LikeableSystemProperty extends SystemProperty {

            LikeableSystemProperty(SystemProperty driver) {
                try {
                    BeanUtilsBean.getInstance().getConvertUtils().register(false, false, 0);
                    BeanUtils.copyProperties(this, driver);
                } catch (Exception e) {
                    log.error("Could not copy properties from SystemProperty to LikeableSystemProperty", e);
                }
            }

            @Override
            public String getName() {
                return makeLikeable(super.getName());
            }

            private String makeLikeable(final String value) {
                String likeable = format("%%%s%%", fromNullable(value).or(EMPTY));

                log.trace("Made this SystemProperty value likeable: {} -> {}", value, likeable);
                return likeable;
            }
        }
    }
}
