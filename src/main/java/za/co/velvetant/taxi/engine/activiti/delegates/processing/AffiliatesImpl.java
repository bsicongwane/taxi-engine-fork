package za.co.velvetant.taxi.engine.activiti.delegates.processing;

import static com.google.common.collect.Lists.transform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.models.TaxiCompany;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

import com.google.common.base.Function;

@Named
public class AffiliatesImpl implements Affiliates {

    private static final Logger log = LoggerFactory.getLogger(AffiliatesImpl.class);

    @Inject
    private TaxiCompanyRepository affiliatesRepository;

    public List<String> filterAffiliates(final FareRequest fareRequest) {
        List<String> affiliates = new ArrayList<>();
        if (fareRequest.getAffiliateIds().isEmpty()) {
            Location pickup = fareRequest.getPickup();
            List<TaxiCompany> closestAffiliates = affiliatesRepository.findAllCloseTo(new BigDecimal(pickup.getLatitude().doubleValue()), new BigDecimal(pickup.getLongitude().doubleValue()));

            for (TaxiCompany affiliate : closestAffiliates) {
                affiliates.add(affiliate.getId().toString());
            }

            log.debug("Drivers from all the affiliates (within the operating area [{}, {}]) will be eligible: {}", pickup.getLatitude(), pickup.getLongitude(), affiliates);
        } else {
            affiliates = transform(affiliatesRepository.findAllAffiliateIds(fareRequest.getAffiliateIds()), new LongToStringFunction());
            log.debug("Only drivers from the following affiliates will be eligible: {}", affiliates);
        }

        return affiliates;
    }

    private class LongToStringFunction implements Function<Long, String> {

        @Override
        public String apply(final Long affiliateId) {
            return affiliateId.toString();
        }
    }
}
