package za.co.velvetant.taxi.engine.util;

import java.text.DecimalFormat;

public abstract class Formatter {

    public static String formatDistance(final Long distance) {
        if (distance == null) {
            return "";
        }
        return (new DecimalFormat("#,###,##0.0").format(distance / 1000) + " km");
    }

    public static String formatDuration(final Long duration) {
        if (duration == null) {
            return "";
        }
        return (new DecimalFormat("#,###,##0.0").format(duration) + " seconds");
    }

    public static String formatCurrency(final Double amount) {
        if (amount == null) {
            return "";
        }
        return ("R " + new DecimalFormat("#,###,##0.00").format(amount));
    }
}
