package za.co.velvetant.taxi.engine.activiti.delegates;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.external.Dispatch;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;

@Named
public class MarkExpired extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(MarkExpired.class);

    @Inject
    private Dispatch dispatch;

    @Override
    public void process(final DelegateExecution execution) {
        try {
            String expiredShift = getVariable(execution, "expiredShift", String.class);
            log.debug("Marking shift [{}] as OFFLINE", expiredShift);

            dispatch.endDriverShift(expiredShift);
        } catch (PreconditionFailedException e) {
            log.error("Error expiring shifts", e);
        }
    }
}
