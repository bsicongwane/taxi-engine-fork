package za.co.velvetant.taxi.engine.rest.routine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.velvetant.taxi.engine.api.PaymentMethod;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.persistence.CreditCardRepository;
import za.co.velvetant.taxi.engine.persistence.PersonRepository;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.NotFoundException;
import za.co.velvetant.taxi.engine.util.BasicAuth;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

import javax.ws.rs.core.HttpHeaders;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static za.co.velvetant.taxi.engine.api.PaymentMethod.CORPORATE_ACCOUNT;
import static za.co.velvetant.taxi.engine.api.PaymentMethod.CREDIT_CARD;
import static za.co.velvetant.taxi.engine.api.PaymentMethod.FAMILY_ACCOUNT;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

public final class CustomerRoutines {

    private static final Logger log = LoggerFactory.getLogger(CustomerRoutines.class);

    private CustomerRoutines() {
    }

    public static List<String> getPaymentMethods(final Customer customer, final CreditCardRepository creditCardRepository) {
        final List<String> paymentMethods = new ArrayList<>();
        paymentMethods.add(PaymentMethod.CASH.name());
        if (customer.getCorporate() != null) {
            final List<za.co.velvetant.taxi.engine.models.CreditCard> list = creditCardRepository.findByCorporateRegistration(customer.getCorporate().getRegistration());
            if (!list.isEmpty()) {
                paymentMethods.add(CORPORATE_ACCOUNT.name());
            }
        }
        if (customer.getFamily() != null) {

            final List<za.co.velvetant.taxi.engine.models.CreditCard> list = creditCardRepository.findByFamilyUuid(customer.getFamily().getUuid());
            if (!list.isEmpty()) {
                paymentMethods.add(FAMILY_ACCOUNT.name());
            }
        }
        if (customer.getCreditCards() != null && !customer.getCreditCards().isEmpty()) {
            paymentMethods.add(CREDIT_CARD.name());
        }

        log.debug("Payment methods for customer [{}]: {}", customer.getUserId(), paymentMethods);
        return paymentMethods;
    }

    public static za.co.velvetant.taxi.engine.models.Customer loadCustomer(final String userid, final CustomerRepository customerRepository) {
        final za.co.velvetant.taxi.engine.models.Customer customer = customerRepository.findByUserId(userid);
        NotFoundException.assertNotNull(customer, "Customer with userid " + userid + " was not found");
        return customer;
    }

    public static String getUserId(final HttpHeaders headers) {
        final List<String> requestHeaders = headers.getRequestHeader("authorization");
        final String basic = requestHeaders.get(0);
        final String user = BasicAuth.decode(basic)[0];

        log.debug("Got user from auth header: {}", user);
        return user;

    }

    public static za.co.velvetant.taxi.engine.api.Customer buildVo(final Customer customer, final CreditCardRepository creditCardRepository, final BigDecimal balance) {
        final za.co.velvetant.taxi.engine.api.Customer vo = convert(customer, new za.co.velvetant.taxi.engine.api.Customer());
        vo.setCreatedDate(customer.getCreatedDate());
        if (customer.getCorporate() != null) {
            vo.setCorporateRegistration(customer.getCorporate().getRegistration());
        }
        if (customer.getFamily() != null) {
            vo.setFamilyName(customer.getFamily().getFamilyName());
            vo.setFamilyUuid(customer.getFamily().getUuid());
        }
        if (customer.getAdministered() != null) {
            vo.setAdministeredCorporate(customer.getAdministered().getRegistration());
        }
        if (customer.getHeaded() != null) {
            vo.setAdministeredFamily(customer.getHeaded().getUuid());
        }
        vo.setPaymentMethods(CustomerRoutines.getPaymentMethods(customer, creditCardRepository));
        vo.setBalance(balance);

        final za.co.velvetant.taxi.engine.models.RewardPoint rewardPoint = customer.getRewardPoint();
        if (rewardPoint != null) {
            vo.setSnappMiles(customer.getRewardPoint().getTotal());
        }

        vo.setHash(customer.getHash());
        vo.setPassword(null);
        return vo;
    }

    public static void checkUserId(final PersonRepository personRepository, final String userId) {
        if (userId != null) {
            final za.co.velvetant.taxi.engine.models.Person existing = personRepository.findByUserId(userId);
            if (existing != null) {
                throw new ConflictException(String.format("Someone with userId %s already exists", userId));
            }
        }
    }

    public static void checkCellphone(final CustomerRepository customerRepository, final String cellphone) {

        final Customer existing = customerRepository.findAnyStatusByCellphone(cellphone);

        if (existing != null) {
            throw new ConflictException(String.format("A customer with cellphone %s already exists.", cellphone));
        }

    }


}
