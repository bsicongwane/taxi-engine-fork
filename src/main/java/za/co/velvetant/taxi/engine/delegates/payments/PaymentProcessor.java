package za.co.velvetant.taxi.engine.delegates.payments;

public interface PaymentProcessor {

    void process(String tripUuid, String cvv, Boolean useAccount);
}
