package za.co.velvetant.taxi.engine.delegates;

import static java.lang.String.format;
import static za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException.assertNotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.messaging.sms.SmsService;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.coupons.Coupon;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.models.exceptions.InvalidEntityException;
import za.co.velvetant.taxi.engine.models.transactions.Transaction;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.persistence.coupons.CouponRepository;
import za.co.velvetant.taxi.persistence.coupons.CustomerCouponRepository;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

import com.google.common.collect.ImmutableSet;

@Named
public class Coupons {

    private static final Logger log = LoggerFactory.getLogger(Coupons.class);

    @Inject
    private CouponRepository couponRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private CustomerCouponRepository customerCouponRepository;

    @Inject
    private Transactions transactions;

    @Inject
    private SmsService smsService;

    public Coupon create(final za.co.velvetant.taxi.engine.api.coupon.Coupon coupon) {
        assertNotNull(coupon, "Coupon details cannot be null");

        Coupon newCoupon;
        if (couponRepository.findByCouponNumber(coupon.getCouponNumber()) == null) {
            try {
                newCoupon = couponRepository.save(buildCoupon(coupon));
            } catch (InvalidEntityException e) {
                throw new PreconditionFailedException(e, "Could not create coupon");
            }
        } else {
            throw new ConflictException(format("Coupon already exists: %s", coupon));
        }

        return newCoupon;
    }

    public Coupon findByCouponNumber(final String couponNumber) {
        return couponRepository.findByCouponNumber(couponNumber);
    }

    public CustomerCoupon findCustomerCouponByUuid(final String uuid) {
        return customerCouponRepository.findByUuid(uuid);
    }

    public CustomerCoupon assign(final String couponNumber, final String userId) {
        Coupon coupon = couponRepository.findByCouponNumber(couponNumber);
        Customer customer = customerRepository.findByUserId(userId);

        CustomerCoupon customerCoupon = checkCustomerCoupon(coupon, customer, customerCouponRepository.findByCustomerAndCoupon(customer, coupon));
        checkCouponIsNotRedeemed(customerCoupon);
        checkIfCouponNotExpired(coupon);
        checkIfCustomerCouponNotExpired(customerCoupon);
        checkUsageLimit(customerCoupon);

        customerCoupon = customerCouponRepository.save(customerCoupon);
        coupon.addCustomerCoupon(customerCoupon);
        couponRepository.save(coupon);

        return customerCoupon;
    }

    public Coupon assignAll(final String couponNumber, final List<String> customers) {
        Set<CustomerCoupon> assignedCoupons = new HashSet<>();
        for (String userId : customers) {
            assignedCoupons.add(assign(couponNumber, userId));
        }

        Coupon coupon = couponRepository.findByCouponNumber(couponNumber);
        List<CustomerCoupon> customerCoupons = ImmutableSet.copyOf(coupon.getCustomerCoupons()).asList();
        for (CustomerCoupon customerCoupon : customerCoupons) {
            if (!assignedCoupons.contains(customerCoupon)) {
                unassign(couponNumber, customerCoupon.getCustomer().getUserId());
            }
        }

        return couponRepository.findByCouponNumber(couponNumber);
    }

    public Coupon unassign(final String couponNumber, final String userid) {
        Customer customer = customerRepository.findByUserId(userid);
        Coupon coupon = couponRepository.findByCouponNumber(couponNumber);
        List<CustomerCoupon> customerCoupons = customerCouponRepository.findByCustomerAndCoupon(customer, coupon);
        for (CustomerCoupon customerCoupon : customerCoupons) {
            if (!customerCoupon.isRedeemed()) {
                customerCouponRepository.delete(customerCoupon);
            } else {
                log.warn("Customer coupon [{}] has been redeemed and cannot be unassigned", customerCoupon.getUuid());
                throw new PreconditionFailedException(format("Customer coupon [%s] has been redeemed and cannot be unassigned", customerCoupon.getUuid()));
            }
        }

        coupon.getCustomerCoupons().removeAll(customerCoupons);
        return couponRepository.save(coupon);
    }

    public Coupon expire(final String couponNumber) {
        Coupon coupon = couponRepository.findByCouponNumber(couponNumber);
        for (CustomerCoupon customerCoupon : coupon.getCustomerCoupons()) {
            expireCustomerCoupon(couponNumber, customerCoupon.getUuid());
        }

        return couponRepository.save(coupon.expire());
    }

    public CustomerCoupon expireCustomerCoupon(final String couponNumber, final String uuid) {
        CustomerCoupon customerCoupon = customerCouponRepository.findByUuid(uuid);
        if (!customerCoupon.isRedeemed()) {
            customerCoupon = customerCouponRepository.save(customerCoupon.expire());
        }

        return customerCoupon;
    }

    public CustomerCoupon redeem(final String couponNumber, final String userId) {
        CustomerCoupon customerCoupon = customerCouponRepository.save(assign(couponNumber, userId).redeem());
        Transaction transaction = transactions.record(customerCoupon);

        smsService.sendCouponRedeemedMessage(customerCoupon, transactions.findCustomerBalance(customerCoupon.getCustomer()));

        return transaction.getCoupon();
    }

    private CustomerCoupon checkCustomerCoupon(final Coupon coupon, final Customer customer, final List<CustomerCoupon> customerCoupons) {
        CustomerCoupon customerCoupon;
        if (customerCoupons.size() > 1) {
            throw new IllegalStateException(format("Usage based coupon [%s] has been assigned/redeemed [%d] times for customer: %s", coupon.getCouponNumber(), customerCoupons.size(), customer.getUserId()));
        } else if (customerCoupons.size() == 1) {
            customerCoupon = customerCoupons.iterator().next();
        } else {
            customerCoupon = new CustomerCoupon(coupon, customer);
        }

        return customerCoupon;
    }

    private void checkCouponIsNotRedeemed(final CustomerCoupon customerCoupon) {
        if(customerCoupon.isRedeemed()) {
            throw new ConflictException(format("This coupon has already been redeemed at: %s", new DateTime(customerCoupon.getRedemptionDate()).toString("yyyy-MM-dd")));
        }
    }

    private void checkIfCouponNotExpired(final Coupon coupon) {
        if (coupon.hasExpired()) {
            throw new PreconditionFailedException(format("This coupon has already expired at %s", new DateTime(coupon.getValidTo()).toString("yyyy-MM-dd")));
        }
    }

    private void checkIfCustomerCouponNotExpired(final CustomerCoupon customerCoupon) {
        if (customerCoupon.hasExpired()) {
            throw new PreconditionFailedException("This customer coupon has been expired");
        }
    }

    private void checkUsageLimit(final CustomerCoupon customerCoupon) {
        Coupon coupon = customerCoupon.getCoupon();
        if (customerCoupon.getId() == null && coupon.getCustomerCoupons().size() >= coupon.getUsageLimit()) {
            throw new PreconditionFailedException(format("This coupon has reached it's usage limit of %s", coupon.getUsageLimit()));
        }
    }

    private Coupon buildCoupon(final za.co.velvetant.taxi.engine.api.coupon.Coupon coupon) {
        return new Coupon(coupon.getCouponNumber(), coupon.getDescription(), coupon.getValidFrom(), coupon.getValidTo(), coupon.getMonetaryLimit(), coupon.getUsageLimit());
    }
}
