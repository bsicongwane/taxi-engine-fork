package za.co.velvetant.taxi.engine.activiti.delegates;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.external.Dispatch;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.models.driver.DriverShift;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.driver.DriverShiftRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class AssignDriver extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(AssignDriver.class);

    @Inject
    private FareRepository fareRepository;

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private DriverShiftRepository driverShiftRepository;

    @Inject
    private Dispatch dispatch;

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        log.debug("Assigning driver [{}] to fare: {}", execution.getVariable("driver"), execution.getProcessBusinessKey());

        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        final Driver driver = driverRepository.findByUserId(getVariable(execution, "driver", String.class));
        fare.setDriver(driver);

        final DriverShift shift = driverShiftRepository.findActiveDriverShiftWithDriverUserId(driver.getUserId());
        fare.setTaxi(shift.getTaxi());
        fare.setAcceptedTime(new Date());
        fareRepository.save(fare);

        dispatch.removeFare(fare.getUuid());
        dispatch.makeDriverBusy(shift.getShiftIdentifier());
    }
}
