package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_AMOUNT;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_METHOD;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.models.PaymentMethod;
import za.co.velvetant.taxi.ops.notifications.FareAmountCapturedNotification;

@Named
public class FareAmountCaptured extends ExecutionDelegate {

    @Inject
    private Operations operations;

    @Override
    public void process(final DelegateExecution execution) {
        final String driver = getVariable(execution, "driver", String.class);
        final String paymentMehod = getVariable(execution, PAYMENT_METHOD, PaymentMethod.class).name();
        final Double paymentAmount = getVariable(execution, PAYMENT_AMOUNT, Double.class);

        operations.notify(new FareAmountCapturedNotification(execution.getProcessBusinessKey(), FareAmountCaptured.class, driver, paymentMehod, paymentAmount));
    }
}
