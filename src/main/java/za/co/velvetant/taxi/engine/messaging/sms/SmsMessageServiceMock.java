package za.co.velvetant.taxi.engine.messaging.sms;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_COUPON_REDEEMED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_DRIVER_ARRIVED;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_NEW_USER;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.SMS_CONTENT_PIN_RESET;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Async;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.models.Taxi;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.models.driver.Driver;

import java.math.BigDecimal;

@Async
public class SmsMessageServiceMock implements SmsService {

    private static final Logger log = LoggerFactory.getLogger(SmsMessageServiceMock.class);

    @Override
    public void addNewUserMessage(final Customer customer) {
        log.debug("[MOCK] Sending [{}] SMS to customer: {}", SMS_CONTENT_NEW_USER, customer.getUserId());
    }

    @Override
    public void sendDriverArrivedMessage(final Customer customer, final Driver driver, final Taxi taxi) {
        log.debug("[MOCK] Sending [{}] SMS to customer: {}", SMS_CONTENT_DRIVER_ARRIVED, customer.getUserId());
    }

    @Override
    public void sendPinResetMessage(final Customer customer) {
        log.debug("[MOCK] Sending [{}] SMS to customer: {}", SMS_CONTENT_PIN_RESET, customer.getUserId());
    }

    @Override
    public void sendCancelledMessage(final Fare fare) {
        switch (fare.getState()) {
            case CANCELLED_BY_ADMIN:
                log.debug("[MOCK] Sending [CANCELLED_BY_ADMIN] SMS to customer: {}", fare.getCustomer().getUserId());

                break;
            case CANCELLED_BY_CUSTOMER:
                log.debug("[MOCK] Sending [CANCELLED_BY_CUSTOMER] SMS to customer: {}", fare.getCustomer().getUserId());

                break;
            case CANCELLED_BY_DRIVER:
                log.debug("[MOCK] Sending [CANCELLED_BY_DRIVER] SMS to customer: {}", fare.getCustomer().getUserId());

                break;
            default:
                log.debug("[MOCK] Fare state does not need SMS. Not sending.");
        }
    }

    @Override
    public void sendPaymentConfirmation(final Fare fare, final MyGateTransaction transaction) {
        log.debug("[MOCK] Sending [DRIVER_PAYMENT_*] SMS to driver: {}", fare.getDriver().getUserId());
    }

    @Override
    public void sendCouponRedeemedMessage(final CustomerCoupon customerCoupon, final BigDecimal balance) {
        log.debug("[MOCK] Sending [{}] SMS to customer: {}", SMS_CONTENT_COUPON_REDEEMED, customerCoupon.getCustomer().getFirstName());
    }

}
