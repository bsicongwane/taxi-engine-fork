package za.co.velvetant.taxi.engine.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.models.Industry;
import za.co.velvetant.taxi.engine.persistence.IndustryRepository;
import za.co.velvetant.taxi.engine.util.VoConversion;

@Named
@Path("/industries.json")
@Transactional
public class IndustryServiceImpl implements IndustryService {

    @Inject
    private IndustryRepository industryRepository;

    @Override
    public Response create(final za.co.velvetant.taxi.engine.api.Industry industry) {
        Industry entity = VoConversion.convert(industry, new Industry());
        entity = industryRepository.save(entity);
        return Response.status(Response.Status.CREATED).entity(VoConversion.convert(entity, new za.co.velvetant.taxi.engine.api.Industry())).build();
    }

    @Override
    public Response edit(final za.co.velvetant.taxi.engine.api.Industry industry) {
        Industry entity = VoConversion.convert(industry, new Industry());
        entity = industryRepository.save(entity);
        return Response.status(Response.Status.OK).entity(VoConversion.convert(entity, new za.co.velvetant.taxi.engine.api.Industry())).build();
    }

    @Override
    public za.co.velvetant.taxi.engine.api.Industry find(final String name) {
        return VoConversion.convert(industryRepository.findByName(name), new za.co.velvetant.taxi.engine.api.Industry());
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Industry> findAll() {
        final List<za.co.velvetant.taxi.engine.api.Industry> vos = new ArrayList<>();
        for (final Industry industry : industryRepository.findAll()) {
            vos.add(VoConversion.convert(industry, new za.co.velvetant.taxi.engine.api.Industry()));
        }
        return vos;
    }

    @Override
    public String count() {
        return String.valueOf(industryRepository.count());
    }

}
