package za.co.velvetant.taxi.engine.filters;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convertDrivers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import za.co.velvetant.taxi.engine.models.TaxiCompany;
import za.co.velvetant.taxi.engine.models.driver.Driver;
import za.co.velvetant.taxi.engine.models.driver.Driver_;
import za.co.velvetant.taxi.persistence.driver.DriverRepository;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class DriverFilter implements Filter<Driver, za.co.velvetant.taxi.engine.api.Driver, DriverFilter> {

    private static final Logger log = LoggerFactory.getLogger(DriverFilter.class);

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private TaxiCompanyRepository taxiCompanyRepository;

    private Boolean affiliated;
    private Driver example = new Driver();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "lastName"));

    private Long total;
    private List<Driver> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Driver> vos = new ArrayList<>();

    public DriverFilter with(final String firstName, final String lastName, final String cellphone, final String email, final Boolean active, final String affiliate, final Boolean affiliated) {
        return withNames(firstName, lastName).withCellphone(cellphone).withEmail(email).isActive(active).withAffiliate(affiliate).isAffiliated(affiliated);
    }

    @Override
    public DriverFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public DriverFilter filter() {
        Page<Driver> results;

        example.setCreatedDate(null);
        if (affiliated != null) {
            log.debug("Checking for affiliated drivers...");
            results = driverRepository.findAll(new AffiliatedSpecification(example), sortable);
        } else {
            results = driverRepository.findWithExample(example, sortable);
        }

        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public DriverFilter andConvert() {
        vos.addAll(convertDrivers(filtered));

        return this;
    }

    @Override
    public List<Driver> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Driver> vos() {
        return vos;
    }

    private DriverFilter withNames(final String firstName, final String lastName) {
        example.setFirstName(firstName);
        example.setLastName(lastName);

        return this;
    }

    private DriverFilter withCellphone(final String cellphone) {
        example.setCellphone(cellphone);

        return this;
    }

    private DriverFilter withEmail(final String email) {
        example.setEmail(email);

        return this;
    }

    private DriverFilter isActive(final Boolean active) {
        example.setActive(active);

        return this;
    }

    private DriverFilter withAffiliate(final String affiliate) {
        example.setTaxiCompany(taxiCompanyRepository.findByRegistrationAndActive(affiliate));

        return this;
    }

    private DriverFilter isAffiliated(final Boolean affiliated) {
        this.affiliated = affiliated;

        return this;
    }

    class AffiliatedSpecification implements Specification<Driver> {

        public Driver driver;

        AffiliatedSpecification(final Driver driver) {
            this.driver = new LikeableDriver(driver);
        }

        @Override
        public Predicate toPredicate(final Root<Driver> root, final CriteriaQuery<?> query, final CriteriaBuilder cb) {
            Predicate affiliatedPredicate;
            Path<TaxiCompany> affiliatedPath = root.get(Driver_.taxiCompany);
            if (affiliated) {
                affiliatedPredicate = cb.isNotNull(affiliatedPath);
            } else {
                affiliatedPredicate = cb.isNull(affiliatedPath);
            }

            query.where(
                    cb.and(affiliatedPredicate),
                    cb.and(cb.or((cb.like(root.get(Driver_.firstName), driver.getFirstName())), cb.or(cb.like(root.get(Driver_.lastName), driver.getLastName())),
                            cb.or(cb.like(root.get(Driver_.cellphone), driver.getCellphone())), cb.or(cb.like(root.get(Driver_.email), driver.getEmail()))),
                            cb.and(cb.equal(root.get(Driver_.active), driver.getActive()))));

            return query.getRestriction();
        }

        class LikeableDriver extends Driver {

            LikeableDriver(Driver driver) {
                try {
                    BeanUtils.copyProperties(this, driver);
                    this.setCreatedDate(null);
                } catch (Exception e) {
                    log.error("Could not copy properties from Driver to LikeableSystemProperty", e);
                }
            }

            @Override
            public String getFirstName() {
                return makeLikeable(super.getFirstName());
            }

            @Override
            public String getLastName() {
                return makeLikeable(super.getLastName());
            }

            @Override
            public String getCellphone() {
                return makeLikeable(super.getCellphone());
            }

            @Override
            public String getEmail() {
                return makeLikeable(super.getEmail());
            }

            private String makeLikeable(final String value) {
                String likeable = format("%%%s%%", fromNullable(value).or(""));

                log.trace("Made this Driver value likeable: {} -> {}", value, likeable);
                return likeable;
            }
        }
    }
}
