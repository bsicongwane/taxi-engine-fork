package za.co.velvetant.taxi.engine.filters;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.models.Rating;
import za.co.velvetant.taxi.engine.persistence.RatingRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class RatingFilter implements Filter<Rating, za.co.velvetant.taxi.engine.api.Rating, RatingFilter> {

    private static final Logger log = LoggerFactory.getLogger(RatingFilter.class);

    @Inject
    private RatingRepository ratingRepository;

    private String userId;
    private PageRequest sortable = new PageRequest(0, 10, new Sort(DESC, "fare.requestTime"));

    private Long total;
    private List<Rating> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Rating> vos = new ArrayList<>();

    public RatingFilter with(final String userId) {
        return withUserId(userId);
    }

    @Override
    public RatingFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        String adjustedSort = sort;
        if(StringUtils.equals("date", sort)) {
            adjustedSort = "fare.dropOffTime";
        }
        sortable = new PageRequest(page - 1, size, new Sort(order, adjustedSort));

        return this;
    }

    @Override
    public RatingFilter filter() {
        Page<Rating> results = ratingRepository.findByFareDriverUserId(userId, sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public RatingFilter andConvert() {
        for (final Rating rating : filtered) {
            vos.add(convert(rating));
        }

        return this;
    }

    @Override
    public List<Rating> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Rating> vos() {
        return vos;
    }

    private RatingFilter withUserId(final String userId) {
        this.userId = userId;

        return this;
    }
}
