package za.co.velvetant.taxi.engine.rest;

import static javax.ws.rs.core.Response.Status.SERVICE_UNAVAILABLE;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Named
@Path("/health.json")
public class HeathServiceImpl implements HeathService {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Response check() {
        Response.ResponseBuilder response = Response.ok();
        if (entityManager.createNativeQuery("SELECT 1").getSingleResult() == null) {
            response = Response.status(SERVICE_UNAVAILABLE);
        }

        return response.build();
    }

    @Override
    public Response monitoring() {
        return check();
    }

}
