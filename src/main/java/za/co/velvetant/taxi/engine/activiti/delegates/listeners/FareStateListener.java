package za.co.velvetant.taxi.engine.activiti.delegates.listeners;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.FormService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.form.FormProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.activiti.delegates.LoggingDelegate;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.FareState;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class FareStateListener extends LoggingDelegate {

    private static final Logger log = LoggerFactory.getLogger(FareStateListener.class);

    @Inject
    private FareRepository repository;

    @Inject
    private FormService formService;

    private Expression state;

    @Override
    public void process(final DelegateExecution execution) {
        log.debug("Updating fare [{}] state to: {}", execution.getProcessBusinessKey(), state.getValue(execution));

        Fare fare = repository.findByUUID(execution.getProcessBusinessKey());
        fare.setState(FareState.valueOf((String) state.getValue(execution)));
        repository.save(fare);
    }

    @Override
    public void process(final DelegateTask task) {
        FareState state = null;
        for (final FormProperty formProperty : formService.getTaskFormData(task.getId()).getFormProperties()) {
            if (formProperty.getId().equals("task.type")) {
                state = FareState.valueOf(formProperty.getValue());
                break;
            }
        }

        log.debug("Updating fare [{}] state to: {}", task.getExecution().getProcessBusinessKey(), state);

        Fare fare = repository.findByUUID(task.getExecution().getProcessBusinessKey());
        fare.setState(state);
        repository.save(fare);
    }
}
