package za.co.velvetant.taxi.engine.messaging.sms;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.models.Taxi;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.models.driver.Driver;

import java.math.BigDecimal;

public interface SmsService {

    void addNewUserMessage(final Customer customer);

    void sendDriverArrivedMessage(Customer customer, Driver driver, Taxi taxi);

    void sendPinResetMessage(Customer customer);

    void sendCancelledMessage(Fare fare);

    void sendPaymentConfirmation(Fare fare, MyGateTransaction transaction);

    void sendCouponRedeemedMessage(CustomerCoupon customerCoupon, BigDecimal balance);
}
