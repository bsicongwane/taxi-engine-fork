package za.co.velvetant.taxi.engine.rest;

import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.api.TaxiCompany;
import za.co.velvetant.taxi.engine.util.VoConversion;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

@Named
@Path("/taxiCompanies.json")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class TaxiCompanyServiceImpl implements TaxiCompanyService {

    @Inject
    private TaxiCompanyRepository taxiCompanyRepository;

    @Override
    public Response create(final TaxiCompany taxiCompany) {
        za.co.velvetant.taxi.engine.models.TaxiCompany entity = convert(taxiCompany, new za.co.velvetant.taxi.engine.models.TaxiCompany());
        entity = taxiCompanyRepository.save(entity);
        return Response.status(Response.Status.CREATED).entity(convert(entity, new TaxiCompany())).build();
    }

    @Override
    public Response edit(final String registration, final TaxiCompany taxiCompany) {
        za.co.velvetant.taxi.engine.models.TaxiCompany currentTaxiCompany = taxiCompanyRepository.findByRegistrationAndActive(registration);
        currentTaxiCompany = VoConversion.convert(taxiCompany, currentTaxiCompany);
        final za.co.velvetant.taxi.engine.models.TaxiCompany updatedTaxiCompany = taxiCompanyRepository.save(currentTaxiCompany);

        return Response.ok().entity(convert(updatedTaxiCompany, new TaxiCompany())).build();
    }

    @Override
    public Response find(final String registration) {
        return Response.ok().entity(convert(taxiCompanyRepository.findByRegistrationAndActive(registration), new TaxiCompany())).build();
    }

    @Override
    public Response findAll() {
        final List<TaxiCompany> vos = new ArrayList<>();
        for (final za.co.velvetant.taxi.engine.models.TaxiCompany taxiCompany : taxiCompanyRepository.findAll()) {
            vos.add(convert(taxiCompany, new TaxiCompany()));
        }
        return Response.ok().entity(vos).build();
    }



}
