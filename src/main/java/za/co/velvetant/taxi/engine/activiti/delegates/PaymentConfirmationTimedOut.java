package za.co.velvetant.taxi.engine.activiti.delegates;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_CONFIRMATION_THRESHOLD;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_CONFIRMATION_TIMED_OUT;
import static za.co.velvetant.taxi.engine.models.FareState.AWAIT_PAYMENT_CONFIRMATION;
import static za.co.velvetant.taxi.engine.models.FareState.CAPTURE_FARE_AMOUNT;
import static za.co.velvetant.taxi.engine.models.FareState.TIMED_OUT;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.FormService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.FareState;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class PaymentConfirmationTimedOut extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(PaymentConfirmationTimedOut.class);

    @Inject
    private FareRepository fareRepository;

    @Inject
    private TaskService taskService;

    @Inject
    private FormService formService;

    @Override
    @Transactional
    public void process(final DelegateExecution execution) {
        Integer threshold = getVariable(execution, PAYMENT_CONFIRMATION_THRESHOLD, Integer.class);
        if (threshold == null) {
            threshold = 0;
        }

        log.debug("Payment confirmation timed out, the has happened [{}] times", threshold + 1);
        execution.setVariable(PAYMENT_CONFIRMATION_THRESHOLD, ++threshold);
        execution.setVariable(PAYMENT_CONFIRMATION_TIMED_OUT, true);

        Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        fare.setState(TIMED_OUT);
        fareRepository.save(fare);
        log.debug("Setting fare [{}] to TIMED_OUT because threshold", execution.getProcessBusinessKey());

        String driver = getVariable(execution, "driver", String.class);
        Task task = getTask(execution, driver, AWAIT_PAYMENT_CONFIRMATION);
        if (task != null) {
            taskService.complete(task.getId());
            log.debug("Cancelled task [{}] for driver: {}", task.getName(), driver);
        }

        String customer = getVariable(execution, "customer", String.class);
        task = getTask(execution, customer, CAPTURE_FARE_AMOUNT);
        if (task != null) {
            taskService.complete(task.getId());
            log.debug("Cancelled task [{}] for customer: {}", task.getName(), customer);
        }

        execution.setVariable(PAYMENT_CONFIRMATION_TIMED_OUT, false);
    }

    private Task getTask(final DelegateExecution execution, final String assignee, final FareState state) {
        return taskPropertyEquals(taskService.createTaskQuery().processInstanceBusinessKey(execution.getProcessBusinessKey()).taskAssignee(assignee).singleResult(), state);
    }

    private Task taskPropertyEquals(final Task task, final FareState state) {
        Task matchedTask = null;
        if (task != null) {
            for (final FormProperty formProperty : formService.getTaskFormData(task.getId()).getFormProperties()) {
                if (formProperty.getId().equals("task.type") && FareState.valueOf(formProperty.getValue()) == state) {
                    matchedTask = task;
                    break;
                }
            }
        }

        return matchedTask;
    }
}
