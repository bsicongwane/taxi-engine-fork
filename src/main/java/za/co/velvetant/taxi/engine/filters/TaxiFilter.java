package za.co.velvetant.taxi.engine.filters;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.models.Taxi;
import za.co.velvetant.taxi.engine.models.TaxiStatus;
import za.co.velvetant.taxi.persistence.taxi.TaxiRepository;

@Named
@Scope(SCOPE_PROTOTYPE)
public class TaxiFilter implements Filter<Taxi, za.co.velvetant.taxi.engine.api.Taxi, TaxiFilter> {

    private static final Logger log = LoggerFactory.getLogger(TaxiFilter.class);

    @Inject
    private TaxiRepository taxiRepository;

    private Taxi example = new Taxi();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "registration"));

    private Long total;
    private List<Taxi> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Taxi> vos = new ArrayList<>();

    public TaxiFilter with(final String registration, final String make, final String model, final TaxiStatus status, final Boolean active) {
        withDetails(registration, make, model).withStatus(status).isActive(active);

        return this;
    }

    @Override
    public TaxiFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public TaxiFilter filter() {
        Page<Taxi> results = taxiRepository.findWithExample(example, sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public TaxiFilter andConvert() {
        for (final Taxi taxi : filtered) {
            vos.add(convert(taxi));
        }

        return this;
    }

    @Override
    public List<Taxi> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Taxi> vos() {
        return vos;
    }

    private TaxiFilter withDetails(final String registration, final String make, final String model) {
        example = new Taxi(null, registration, make, model, null, null);

        return this;
    }

    private TaxiFilter withStatus(final TaxiStatus status) {
        example.setStatus(status);

        return this;
    }

    private TaxiFilter isActive(final Boolean active) {
        example.setActive(active);

        return this;
    }
}
