package za.co.velvetant.taxi.engine.activiti.delegates.settings;

import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.ELIGIBLE_DRIVER_ACCEPTANCE_DURATION;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;

@Named
public class EligibleDriverAcceptanceTimer {

    private static final Logger log = LoggerFactory.getLogger(EligibleDriverAcceptanceTimer.class);

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    public String duration() {
        String duration = systemPropertyHelper.getProperty(ELIGIBLE_DRIVER_ACCEPTANCE_DURATION, "PT5M");

        log.debug("Using eligible fare acceptance duration: {}", duration);
        return duration;
    }
}
