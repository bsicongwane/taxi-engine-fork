package za.co.velvetant.taxi.engine.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import za.co.velvetant.taxi.engine.rest.exception.EngineException;

public final class Bytes {

    private Bytes() {

    }

    public static byte[] getBytes(final InputStream is) {
        try {
            int len;
            int size = 1024;
            byte[] buf;

            if (is instanceof ByteArrayInputStream) {
                size = is.available();
                buf = new byte[size];
                is.read(buf, 0, size);
            } else {
                final ByteArrayOutputStream bos = new ByteArrayOutputStream();
                buf = new byte[size];
                while ((len = is.read(buf, 0, size)) != -1) {
                    bos.write(buf, 0, len);
                }
                buf = bos.toByteArray();
            }
            return buf;
        } catch (final Exception e) {
            throw new EngineException(e);
        }
    }

}
