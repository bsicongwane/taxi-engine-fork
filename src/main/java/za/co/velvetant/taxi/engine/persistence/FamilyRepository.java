package za.co.velvetant.taxi.engine.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Family;
import za.co.velvetant.taxi.persistence.repositories.auditing.AuditingRepository;
import za.co.velvetant.taxi.persistence.repositories.metamodel.MetaModelRepository;

@Repository
public interface FamilyRepository extends AuditingRepository<Family, Long>, MetaModelRepository<Family, Long> {

    @Query("SELECT h FROM Family h LEFT JOIN FETCH h.members where h.uuid = ?1")
    Family findByUuid(String uuid);

    @Query("SELECT h FROM Family h LEFT JOIN FETCH h.administrators where h.uuid = ?1")
    Family fetchWithAdministrators(String uuid);

    @Query("SELECT h FROM Family h LEFT JOIN FETCH h.administrators")
    List<Family> fetchAllWithAdministrators();

    @Query("SELECT h FROM Family h LEFT JOIN FETCH h.creditCards where h.uuid = ?1")
    Family fetchWithCreditCard(String uuid);
}
