package za.co.velvetant.taxi.engine.rest;

import com.sun.jersey.multipart.FormDataParam;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiError;
import com.wordnik.swagger.annotations.ApiErrors;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiParamImplicit;
import com.wordnik.swagger.annotations.ApiParamsImplicit;
import org.springframework.data.domain.Sort;
import za.co.velvetant.taxi.engine.api.Cellphone;
import za.co.velvetant.taxi.engine.api.ChangePasswordDetails;
import za.co.velvetant.taxi.engine.api.Corporate;
import za.co.velvetant.taxi.engine.api.CreateCustomer;
import za.co.velvetant.taxi.engine.api.CreditCard;
import za.co.velvetant.taxi.engine.api.Customer;
import za.co.velvetant.taxi.engine.api.EditCustomer;
import za.co.velvetant.taxi.engine.api.EditFavourite;
import za.co.velvetant.taxi.engine.api.Family;
import za.co.velvetant.taxi.engine.api.Favourite;
import za.co.velvetant.taxi.engine.api.RewardPoint;
import za.co.velvetant.taxi.engine.api.RewardPointList;
import za.co.velvetant.taxi.engine.api.RewardPointLog;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

@Path("/customers")
@Api(value = "/customers", description = "Operations for managing customers.")
public interface CustomerService {

    String CREDIT_CARD_CLASS = "za.co.velvetant.taxi.engine.api.CreditCard";
    String CUSTOMER_CLASS = "za.co.velvetant.taxi.engine.api.Customer";

    @POST
    @ApiOperation(value = "Create a new customer", responseClass = "za.co.velvetant.taxi.engine.api.CreateCustomer")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 409, reason = DocumentationMessages.E_409 + " User with same userid already exists") })
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "WEB_USER" })
    Response create(@ApiParam(value = "Customer to create", required = true) CreateCustomer customer);

    @Path("/{userid}")
    @PUT
    @ApiOperation(value = "Update a customers details.", responseClass = "za.co.velvetant.taxi.engine.api.EditCustomer")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response edit(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Customer to update", required = true) EditCustomer customer);

    @DELETE
    @Path("/{userid}")
    @ApiOperation(value = "Removes a customer", notes = "Customer details are not permanently removed and can still be retrieved")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response remove(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @HEAD
    @Path("/{userid}")
    @ApiOperation(value = "Activate a deactivated customer.")
    Response activate(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{userid}")
    @ApiOperation(value = "Finds a customer by userid address", responseClass = CUSTOMER_CLASS)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "FAMILY", "DRIVER" })
    Customer find(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @ApiOperation(value = "Find all customers.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.Customer",
    notes = "Used by Sn@ppCab Operations application. Security authorisation roles apply.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
    Response findAll(
            @ApiParam(value = "Filter by first name. Partial filtering is allowed.") @QueryParam("firstName") String firstName,
            @ApiParam(value = "Filter by last name. Partial filtering is allowed.") @QueryParam("lastName") String lastName,
            @ApiParam(value = "Filter by cellphone number. Partial filtering is allowed.") @QueryParam("cellphone") String cellphone,
            @ApiParam(value = "Filter by email address. Partial filtering is allowed.") @QueryParam("email") String email,
            @ApiParam(value = "Filter by active/inactive customers.", defaultValue = "true") @QueryParam("active") @DefaultValue("true") Boolean active,
            @ApiParam(value = "Whether to include customers that belong to a corporate.") @QueryParam("corporate") Boolean corporate,
            @ApiParam(value = "Whether to include customers that belong to a family.") @QueryParam("family") Boolean family,
            @ApiParam(value = "Page number of customers to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number customers to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "lastName", allowableValues = "firstName,lastName,cellphone,email") @QueryParam("sort") @DefaultValue("lastName") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "asc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("asc") Sort.Direction order);

    @GET
    @Path("/{userid}/corporate")
    @ApiOperation(value = "Finds a customer's corporate if they are linked to one.", responseClass = "za.co.velvetant.taxi.engine.api.Corporate")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "DRIVER" })
    Corporate findCorporate(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{userid}/family")
    @ApiOperation(value = "Finds a customer's family if they are linked to one.", responseClass = "za.co.velvetant.taxi.engine.api.Family")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "FAMILY", "DRIVER" })
    Family findFamily(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/count")
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CORPORATE", "FAMILY" })
    String count();

    @POST
    @Path("/{userid}/image")
    @ApiOperation(value = "Upload an image for the customer.")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiParamsImplicit(@ApiParamImplicit(dataType = "file", name = "file", paramType = "body"))
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response uploadImage(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid, @FormDataParam("file") InputStream imageStream,
            @HeaderParam("X-Photo-Format") String photoFormat);

    @POST
    @Path("/{userid}/resetPassword")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "FAMILY", "WEB_USER" })
    @ApiOperation(value = "Reset a user's password. The user is emailed their new password.")
    Response resetPassword(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @POST
    @Path("/{userid}/resendPin")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "FAMILY", "WEB_USER" })
    @ApiOperation(value = "Resend a user's pin via sms.")
    Response resendPin(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @PUT
    @Path("/{userid}/cellphone")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "FAMILY", "WEB_USER" })
    @ApiOperation(value = "Change a user's cellphone.")
    Response changeCellphone(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "New cell number.", required = true) Cellphone cellphone);

    @POST
    @Path("/{userid}/activate")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "FAMILY", "WEB_USER" })
    @ApiOperation(value = "Activate a new account.", responseClass = CUSTOMER_CLASS)
    Response activateAccount(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Customer pin.", required = true) @QueryParam("pin") String pin);

    @PUT
    @Path("/{userid}/password")
    @ApiOperation(value = "Change a user's password.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response changePassword(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "New password details.") ChangePasswordDetails details);

    @Path("/{userid}/creditCard")
    @GET
    @ApiOperation(value = "Get a customer's credit card", responseClass = CREDIT_CARD_CLASS)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response getCreditCard(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @PUT
    @Path("/{userid}/creditCard")
    @ApiOperation(value = "Upload credit card details.", responseClass = CREDIT_CARD_CLASS)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response uploadCreditCard(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Credit Card details.", required = true) CreditCard card);

    @DELETE
    @Path("/{userid}/creditCard")
    @ApiOperation(value = "Delete a customers credit card.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response deleteCreditCard(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    /** Favourites **/

    @POST
    @Path("/{userid}/favourites")
    @ApiOperation(value = "Create a new favourite", responseClass = "za.co.velvetant.taxi.engine.api.Favourite")
    @ApiErrors({ @ApiError(code = 400, reason = DocumentationMessages.E_400), @ApiError(code = 409, reason = DocumentationMessages.E_409 + "Favourite with same name already exists") })
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response create(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Favourite to create", required = true) Favourite favourite);

    @PUT
    @Path("/{userid}/favourites/{favouriteName}")
    @ApiOperation(value = "Update a favourites details.", responseClass = "za.co.velvetant.taxi.engine.api.EditFavourite")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response edit(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Favourite name", required = true) @PathParam("favouriteName") String favouriteName, @ApiParam(value = "Favourite to update", required = true) EditFavourite favourite);

    @DELETE
    @Path("/{userid}/favourites/{favouriteName}")
    @ApiOperation(value = "Removes a favourite")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response remove(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Name of the favourite", required = true) @PathParam("favouriteName") String favouriteName);

    @GET
    @Path("/{userid}/favourites/{favouriteName}")
    @ApiOperation(value = "Finds a favourite by name", responseClass = "za.co.velvetant.taxi.engine.api.Favourite")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response find(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
                  @ApiParam(value = "Name of the favourite", required = true) @PathParam("favouriteName") String favouriteName);

    @GET
    @Path("/{userid}/favourites")
    @ApiOperation(value = "Find all the favourites for a customer", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.Favourite")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response findAll(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{userid}/image")
    @ApiOperation(value = "Get a customer's image")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "FAMILY", "DRIVER" })
    Response getPhoto(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    /** reward points **/

    @GET
    @Path("/{userid}/rewardPoints/log")
    @ApiOperation(value = "Get a customer's reward point details.", responseClass = "za.co.velvetant.taxi.engine.api.RewardPointList")
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    RewardPointList findRewardsPointLog(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = DocumentationMessages.M_START_TIME) @QueryParam("startTime") long startTime, @ApiParam(value = DocumentationMessages.M_END_TIME) @QueryParam("endTime") long endTime);

    @GET
    @Path("/{userid}/rewardPoints/best")
    @ApiOperation(value = "Get a customer's best reward point earning.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    RewardPointLog findBestReward(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{userid}/rewardPoints/last")
    @ApiOperation(value = "Get a customer's last reward point earned.")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response findLastReward(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @GET
    @Path("/{userid}/rewardPoints")
    @ApiOperation(value = "Get a customer's total reward points and reward status.", responseClass = "za.co.velvetant.taxi.engine.api.RewardPoint")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    RewardPoint findRewardsPoints(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);

    @Path("/{userid}/locations")
    @GET
    @ApiOperation(value = "Get all past locations.", multiValueResponse = true, responseClass = "za.co.velvetant.taxi.engine.api.common.Location")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response findPreviousLocations(
            @ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid,
            @ApiParam(value = "Number customers to return for this page", defaultValue = "10") @QueryParam("limit") @DefaultValue("10") int limit,
            @ApiParam(value = "Page number of customers to return", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Which property to sort by", defaultValue = "requestTime", allowableValues = "requestTime,dropOffTime,customer.email,customer.cellphone") @QueryParam("sort") @DefaultValue("requestTime") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "desc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);


    @GET
    @Path("/{userid}/image/encoded")
    @ApiOperation(value = "Get a customer's image")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER", "CORPORATE", "FAMILY", "DRIVER" })
    @Produces(MediaType.TEXT_PLAIN)
    Response getEncodedPhoto(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);


    @GET
    @Path("/{userid}/paymentTypes")
    @ApiOperation(value = "Get a customer's payment types", responseClass = "za.co.velvetant.taxi.engine.api.PaymentType")
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response getPaymentTypes(@ApiParam(value = DocumentationMessages.M_CUSTOMER_ID, required = true) @PathParam("userid") String userid);


}
