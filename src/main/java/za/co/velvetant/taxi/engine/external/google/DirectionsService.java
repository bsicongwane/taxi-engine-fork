package za.co.velvetant.taxi.engine.external.google;

import za.co.velvetant.taxi.api.common.Directions;
import za.co.velvetant.taxi.api.common.Location;

public interface DirectionsService {

    Directions get(Location from, Location to);

    String getAddress(final Location location);
}
