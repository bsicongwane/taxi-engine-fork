package za.co.velvetant.taxi.engine.delegates.payments;

import static com.google.common.base.Optional.fromNullable;
import static org.apache.commons.lang.builder.ToStringBuilder.reflectionToString;

import java.math.BigDecimal;
import java.util.Date;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.mygate.BillingDetails;
import za.co.velvetant.mygate.MyGateResponse;
import za.co.velvetant.mygate.PaymentService;
import za.co.velvetant.taxi.engine.messaging.mail.MailService;
import za.co.velvetant.taxi.engine.messaging.mymessages.MessageService;
import za.co.velvetant.taxi.engine.messaging.sms.SmsService;
import za.co.velvetant.taxi.engine.model.Payment;
import za.co.velvetant.taxi.engine.model.exceptions.PaymentException;
import za.co.velvetant.taxi.engine.models.CreditCard;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.MyGateTransaction;
import za.co.velvetant.taxi.engine.persistence.MyGateTransactionRepository;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

public abstract class CreditCardProcessor extends TransactionalTripPaymentProcessor {

    private static final Logger log = LoggerFactory.getLogger(CreditCardProcessor.class);

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private PaymentService paymentService;

    @Inject
    private MyGateTransactionRepository myGateTransactionRepository;

    @Inject
    private SmsService smsService;

    @Inject
    private MailService mailService;

    @Inject
    private MessageService messageService;

    protected abstract CreditCard getCreditCard(final String userId);

    @Override
    public void process(final Payment payment, final Boolean useAccount) {
        log.debug("Processing credit card payment with account [{}]: {}", useAccount, payment);

        if (payment.getInvoiceAmount().signum() > 0) {
            final CreditCard creditCard = getCreditCard(payment.getTrip().getCustomer().getUserId());
            log.debug("Processing payment with credit card [{}]-[{}] for: {} (including tip)", creditCard.getGateWayId(), payment.getCvv(), payment.getInvoiceAmount());

            MyGateTransaction transaction = null;
            Fare trip = payment.getTrip();

            try {
                final BillingDetails billingDetails = new BillingDetails(payment.getCvv(), payment.getInvoiceAmount().toString(), "0", "", "", "", "", payment.getTrip().getReference());
                final MyGateResponse response = paymentService.processPayment(creditCard.getGateWayId(), billingDetails);

                transaction = buildTransaction(response, trip);
            } catch (final Exception e) {
                log.error("Error processing MyGate payment", e);
                transaction = buildTransaction("-1", -1, e.getMessage(), "-1", trip);
            } finally {
                if (transaction != null) {
                    log.debug("Processing successful transaction: {}", reflectionToString(transaction));
                    transaction = myGateTransactionRepository.save(transaction);
                    smsService.sendPaymentConfirmation(trip, transaction);
                    if (transaction.isSuccessful()) {
                        messageService.addNewInvoiceMessage(trip.getCustomer());
                        mailService.sendInvoiceEmail(trip, creditCard.getGateWayId());
                    } else {
                        throw new PaymentException("Payment for trip [%s] failed, see MyGate transaction: %s", trip.getUuid(), transaction.getId());
                    }
                }
            }
        } else {
            BigDecimal balance = transactions.findCustomerBalance(customerRepository.findByUserId(payment.getTrip().getCustomer().getUserId()));
            log.debug("Trip invoice amount is [{}], customers credit [{}] was enough to cover the trip amount: {}", payment.getInvoiceAmount(), balance, payment.getTripAmount());
        }
    }

    protected MyGateTransaction linkTransaction(final MyGateTransaction transaction, final Fare trip) {
        transaction.setCustomer(trip.getCustomer());

        return transaction;
    }

    private MyGateTransaction buildTransaction(final MyGateResponse response, final Fare trip) {
        return buildTransaction(response.getTransactionIndex(), response.getStatus(), response.getErrorCode(), response.getAuthorizationId(), trip);
    }

    private MyGateTransaction buildTransaction(final String index, final Integer status, final String errorCode, final String authorizationId, final Fare trip) {
        MyGateTransaction transaction = new MyGateTransaction(new Date(), trip, MyGateTransaction.Type.PAY, index, status);
        transaction.setErrorMessage(fromNullable(errorCode).or("UNKNOWN"));
        transaction.setAuthorisationId(authorizationId);

        transaction = linkTransaction(transaction, trip);

        return transaction;
    }
}
