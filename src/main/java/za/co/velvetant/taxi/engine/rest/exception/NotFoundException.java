package za.co.velvetant.taxi.engine.rest.exception;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class NotFoundException extends WebApplicationException {

    public NotFoundException(final String message) {
        super(Response.status(Response.Status.NOT_FOUND).entity(message).type(MediaType.TEXT_PLAIN_TYPE).build());
    }

    public static void assertNotNull(final Object o) {
        assertNotNull(o, null);
    }

    public static void assertNotNull(final Object o, final String notFoundMessage) {
        if (o == null && isNotBlank(notFoundMessage)) {
            throw new NotFoundException(notFoundMessage);
        }
    }
}
