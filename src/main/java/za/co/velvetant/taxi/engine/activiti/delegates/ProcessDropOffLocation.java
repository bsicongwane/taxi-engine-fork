package za.co.velvetant.taxi.engine.activiti.delegates;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.FARE_REQUEST;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.activiti.delegates.processing.DropOffLocation;
import za.co.velvetant.taxi.engine.api.FareRequest;

@Named
public class ProcessDropOffLocation extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(ProcessDropOffLocation.class);

    @Inject
    private DropOffLocation processor;

    @Override
    public void process(final DelegateExecution execution) {
        final FareRequest fareRequest = getVariable(execution, FARE_REQUEST, FareRequest.class);
        processor.process(execution.getProcessBusinessKey(), fareRequest.getPickup(), fareRequest.getDropOff());
    }
}
