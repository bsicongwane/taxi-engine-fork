package za.co.velvetant.taxi.engine.activiti.delegates.notifications;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;

import za.co.velvetant.taxi.engine.activiti.delegates.ExecutionDelegate;
import za.co.velvetant.taxi.engine.external.Operations;
import za.co.velvetant.taxi.engine.messaging.mymessages.MessageService;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.persistence.trips.FareRepository;
import za.co.velvetant.taxi.ops.notifications.NoEligibleDriverAssignedNotification;

@Named
public class NoEligibleDriver extends ExecutionDelegate {

    @Inject
    private Operations operations;

    @Inject
    private MessageService messageService;

    @Inject
    private FareRepository fareRepository;

    @Override
    public void process(final DelegateExecution execution) {
        operations.notify(new NoEligibleDriverAssignedNotification(execution.getProcessBusinessKey(), NoEligibleDriver.class));
        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());
        messageService.addNoCabsMessage(fare.getCustomer());
    }
}
