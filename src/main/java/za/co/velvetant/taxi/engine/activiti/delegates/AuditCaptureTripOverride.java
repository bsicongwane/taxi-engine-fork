package za.co.velvetant.taxi.engine.activiti.delegates;

import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_USER;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.models.CaptureTripAmountOverride;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.User;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.persistence.overrides.CaptureTripAmountOverrideRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@Named
public class AuditCaptureTripOverride extends ExecutionDelegate {

    private static final Logger log = LoggerFactory.getLogger(AuditCaptureTripOverride.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private FareRepository fareRepository;

    @Inject
    private CaptureTripAmountOverrideRepository overrideRepository;

    @Override
    public void process(final DelegateExecution execution) {
        log.debug("Auditing Capture Trip Amount override for trip [{}] by: {}", execution.getProcessBusinessKey(), execution.getVariable(OPS_USER));

        final User opsUser = userRepository.findByUserId(getVariable(execution, OPS_USER, String.class));
        final Fare fare = fareRepository.findByUUID(execution.getProcessBusinessKey());

        CaptureTripAmountOverride amountOverride = overrideRepository.save(new CaptureTripAmountOverride(opsUser, fare));
        fare.addAudit(amountOverride);
        fareRepository.save(fare);
    }
}
