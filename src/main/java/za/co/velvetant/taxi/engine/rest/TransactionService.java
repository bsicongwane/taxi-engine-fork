package za.co.velvetant.taxi.engine.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Sort;

import za.co.velvetant.taxi.engine.models.transactions.TransactionType;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

@Path("/transactions")
@Api(value = "/transactions", description = "Service for managing customer transactions")
@RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT" })
public interface TransactionService {

    String TRANSACTION_CLASS = "za.co.velvetant.taxi.api.transactions.Transaction";

    @GET
    @Path("/")
    @ApiOperation(value = "Find all transactions", multiValueResponse = true, responseClass = TRANSACTION_CLASS)
    Response findAll(@ApiParam(value = "Filter by uuid.") @QueryParam("uuid") String uuid,
            @ApiParam(value = "Filter by transaction date.") @QueryParam("date") Long date,
            @ApiParam(value = "Filter by transaction type.") @QueryParam("transactionType") TransactionType transactionType,
            @ApiParam(value = "Filter by the customer who created the transaction.") @QueryParam("userid") String userid,
            @ApiParam(value = "Filter for transactions for a specific trip.") @QueryParam("reference") String reference,
            @ApiParam(value = "Page number of transactions to return.", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number transactions to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "date", allowableValues = "date,transactionType,userid,reference") @QueryParam("sort") @DefaultValue("date") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "desc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);

    @GET
    @Path("/customers/{userid}")
    @ApiOperation(value = "Find all transactions for a customer", multiValueResponse = true, responseClass = TRANSACTION_CLASS)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response findAllForCustomer(@ApiParam(value = "Filter by uuid.") @QueryParam("uuid") String uuid,
            @ApiParam(value = "Filter by transaction date.") @QueryParam("date") Long date,
            @ApiParam(value = "Filter by transaction type.") @QueryParam("transactionType") TransactionType transactionType,
            @ApiParam(value = "Filter by the customer who created the transaction.") @PathParam("userid") String userid,
            @ApiParam(value = "Filter for transactions for a specific trip.") @QueryParam("reference") String reference,
            @ApiParam(value = "Page number of transactions to return.", defaultValue = "1") @QueryParam("page") @DefaultValue("1") Integer page,
            @ApiParam(value = "Number transactions to return for this page", defaultValue = "10") @QueryParam("size") @DefaultValue("10") Integer size,
            @ApiParam(value = "Which property to sort by", defaultValue = "date", allowableValues = "date,transactionType,userid,reference") @QueryParam("sort") @DefaultValue("date") String sort,
            @ApiParam(value = "Direction to order sort property", defaultValue = "desc", allowableValues = "asc,desc") @QueryParam("order") @DefaultValue("desc") Sort.Direction order);

    @GET
    @Path("/{uuid}")
    @ApiOperation(value = "Find transaction for this uuid", responseClass = TRANSACTION_CLASS)
    @RolesAllowed({ "SYSTEM", "ADMINISTRATOR", "SUPERVISOR", "AGENT", "CUSTOMER" })
    Response find(@ApiParam(value = "Transaction uuid.") @PathParam("uuid") String uuid);
}
