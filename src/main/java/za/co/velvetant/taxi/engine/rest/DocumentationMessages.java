package za.co.velvetant.taxi.engine.rest;

public final class DocumentationMessages {

    private DocumentationMessages() {

    }

    public static final String E_400 = "Not a valid request";
    public static final String E_403 = "Not authorised to access this request";
    public static final String E_409 = "Conflict occurred.";
    public static final String E_412 = "A precondition failed.";
    public static final String E_400_F = "An action for this fare and person does not exist";

    public static final String M_CUSTOMER_ID = "Email address of the customer";
    public static final String M_FARE_ID = "Fare unique identifier";
    public static final String M_INDUSTRY_NAME = "The name of the industry";
    public static final String M_SECTOR_NAME = "Identifies the sector by name";
    public static final String M_COMPANY_REG = "The registration of the company.";
    public static final String M_TAXI_REG = "The registration of the taxi.";
    public static final String M_DRIVER_ID = "The drivers userid.";
    public static final String M_UUID = "Message unique identifier";
    public static final String M_OPS_USER_ID = "The ops users Id";

    public static final String M_START_TIME = "Start time timestamp.";
    public static final String M_END_TIME = "End time timestamp.";
    public static final String M_FAMILY_ID = "Family unique identifier";

}
