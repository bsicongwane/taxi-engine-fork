package za.co.velvetant.taxi.engine.rest.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class NotAllowedException extends WebApplicationException {

    public NotAllowedException(final String message) {
        super(Response.status(Response.Status.FORBIDDEN).entity(message).type(MediaType.TEXT_PLAIN_TYPE).build());
    }

    public NotAllowedException(final Exception e, final String message) {
        super(Response.status(Response.Status.FORBIDDEN).entity(message + " - " + e.getMessage()).type(MediaType.TEXT_PLAIN_TYPE).build());
    }

    public NotAllowedException() {
        super(Response.status(Response.Status.FORBIDDEN).type(MediaType.TEXT_PLAIN_TYPE).build());
    }
}
