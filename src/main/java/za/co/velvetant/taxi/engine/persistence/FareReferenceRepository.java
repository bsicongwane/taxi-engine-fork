package za.co.velvetant.taxi.engine.persistence;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.FareReference;

@Repository
public interface FareReferenceRepository extends CrudRepository<FareReference, Long> {

    @Query("SELECT min(h.id) FROM FareReference h where h.createdTime >= ?1")
    Long findFirst(Date startTime);
}
