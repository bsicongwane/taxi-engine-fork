package za.co.velvetant.taxi.engine.filters;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import za.co.velvetant.taxi.engine.models.Corporate;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Customer_;
import za.co.velvetant.taxi.engine.models.Family;
import za.co.velvetant.taxi.engine.persistence.CreditCardRepository;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static za.co.velvetant.taxi.engine.rest.routine.CustomerRoutines.getPaymentMethods;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
@Scope(SCOPE_PROTOTYPE)
public class CustomerFilter implements Filter<Customer, za.co.velvetant.taxi.engine.api.Customer, CustomerFilter> {

    private static final Logger log = LoggerFactory.getLogger(CustomerFilter.class);

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private CreditCardRepository creditCardRepository;

    private Boolean corporate;
    private Boolean family;
    private Customer example = new Customer();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(ASC, "lastName"));

    private Long total;
    private List<Customer> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.Customer> vos = new ArrayList<>();

    public CustomerFilter with(final String firstName, final String lastName, final String cellphone, final String email, final Boolean active, final Boolean corporate, final Boolean family) {
        return withNames(firstName, lastName).withCellphone(cellphone).withEmail(email).isActive(active).hasCorporate(corporate).hasFamily(family);
    }

    @Override
    public CustomerFilter sort(final Integer page, final Integer size, final String sort, final Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));

        return this;
    }

    @Override
    public CustomerFilter filter() {
        Page<Customer> results;

        example.setCreatedDate(null);
        if ((corporate != null) || (family!= null)) {
            log.debug("Checking for customers without a corporate/family ...");
            results = customerRepository.findAll(new CustomerSpecification(example), sortable);
        } else {
            results = customerRepository.findWithExample(example, sortable);
        }

        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    public CustomerFilter andConvert() {
        for (final Customer customer : filtered) {
            final za.co.velvetant.taxi.engine.api.Customer vo = convert(customer);

            if (customer.getCorporate() != null) {
                vo.setCorporateRegistration(customer.getCorporate().getRegistration());
            }

            if (customer.getFamily() != null) {
                vo.setFamilyName(customer.getFamily().getFamilyName());
                vo.setFamilyUuid(customer.getFamily().getUuid());
            }

            vo.setPassword(null);
            vo.setPaymentMethods(getPaymentMethods(customer, creditCardRepository));

            vos.add(vo);
        }

        return this;
    }

    @Override
    public List<Customer> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.Customer> vos() {
        return vos;
    }

    private CustomerFilter withNames(final String firstName, final String lastName) {
        example.setFirstName(firstName);
        example.setLastName(lastName);

        return this;
    }

    private CustomerFilter withCellphone(final String cellphone) {
        example.setCellphone(cellphone);

        return this;
    }

    private CustomerFilter withEmail(final String email) {
        example.setEmail(email);

        return this;
    }

    private CustomerFilter isActive(final Boolean active) {
        example.setActive(active);

        return this;
    }

    private CustomerFilter hasCorporate(final Boolean corporate) {
        this.corporate = corporate;

        return this;
    }

    private CustomerFilter hasFamily(final Boolean family) {
        this.family = family;

        return this;
    }

    class CustomerSpecification implements Specification<Customer> {

        public Customer customer;

        CustomerSpecification(final Customer customer) {
            this.customer = new LikeableCustomer(customer);
        }

        @Override
        public Predicate toPredicate(Root<Customer> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            Predicate familyPredicate;
            Predicate corporatePredicate;
            Path<Corporate> corporatePath = root.get(Customer_.corporate);
            Path<Family> familyPath = root.get(Customer_.family);

            if (family != null && family) {
                familyPredicate = cb.isNotNull(familyPath);
            } else {
                familyPredicate = cb.isNull(familyPath);
            }

            if (corporate != null && corporate) {
                corporatePredicate = cb.isNotNull(corporatePath);
            } else {
                corporatePredicate = cb.isNull(corporatePath);
            }

            Predicate customerPredicate = null;
            if (family != null) {
                customerPredicate = familyPredicate;
            }

            if (corporate != null) {
                customerPredicate = corporatePredicate;
            }

            query.where(cb.and(customerPredicate),
                    cb.and(cb.or((cb.like(root.get(Customer_.firstName), customer.getFirstName())), cb.or(cb.like(root.get(Customer_.lastName), customer.getLastName())),
                            cb.or(cb.like(root.get(Customer_.cellphone), customer.getCellphone())), cb.or(cb.like(root.get(Customer_.email), customer.getEmail()))),
                            cb.and(cb.equal(root.get(Customer_.active), customer.getActive()))));

            return query.getRestriction();
        }

        class LikeableCustomer extends Customer {

            LikeableCustomer(Customer customer) {
                try {
                    BeanUtils.copyProperties(this, customer);
                } catch (Exception e) {
                    log.error("Could not copy properties from Customer to LikeableSystemProperty", e);
                }
            }

            @Override
            public String getFirstName() {
                return makeLikeable(super.getFirstName());
            }

            @Override
            public String getLastName() {
                return makeLikeable(super.getLastName());
            }

            @Override
            public String getCellphone() {
                return makeLikeable(super.getCellphone());
            }

            @Override
            public String getEmail() {
                return makeLikeable(super.getEmail());
            }

            private String makeLikeable(final String value) {
                String likeable = format("%%%s%%", fromNullable(value).or(""));

                log.trace("Made this Customer value likeable: {} -> {}", value, likeable);
                return likeable;
            }
        }
    }
}
