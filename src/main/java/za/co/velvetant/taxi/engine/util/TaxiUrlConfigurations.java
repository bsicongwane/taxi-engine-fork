package za.co.velvetant.taxi.engine.util;

import static java.lang.String.format;

public final class TaxiUrlConfigurations {

    private TaxiUrlConfigurations() {
    }

    public static String getTaxiOpsNotificationsUrl() {
        return System.getProperty("taxi-ops-notification-url", "http://localhost:9000/notifications");
    }

    public static String getTaxiDispatchFaresUrl() {
        return System.getProperty("taxi-dispatch-url", "http://localhost:8080/taxi-dispatch");
    }

    public static String getTaxiDispatchFaresNotificationUrl() {
        return format("%s/%s", getTaxiDispatchFaresUrl(), "notifications/fares/{fare-uuid}");
    }

    public static String getTaxiDispatchDriverShiftBusyUrl() {
        return format("%s/driver/shift/%s", getTaxiDispatchFaresUrl(), "{shift-identifier}/busy");
    }

    public static String getTaxiDispatchDriverShiftAvailableUrl() {
        return format("%s/driver/shift/%s", getTaxiDispatchFaresUrl(), "{shift-identifier}/available");
    }

    public static String getTaxiDispatchDriverShiftEndUrl() {
        return format("%s/driver/shift/%s", getTaxiDispatchFaresUrl(), "{shift-identifier}/end");
    }

    public static String getTaxiDispatchExpiredShiftsUrl() {
        return format("%s/driver/shift/expired", getTaxiDispatchFaresUrl());
    }

}
