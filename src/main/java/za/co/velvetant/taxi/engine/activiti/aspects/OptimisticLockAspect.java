package za.co.velvetant.taxi.engine.activiti.aspects;

import javax.inject.Named;

import org.activiti.engine.ActivitiOptimisticLockingException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;

@Aspect
@Named
public class OptimisticLockAspect {

    private static final Logger log = LoggerFactory.getLogger(OptimisticLockAspect.class);

    @Around("execution(* za.co.velvetant.taxi.engine.FareEngine.*(..))")
    public Object aroundInvoke(final ProceedingJoinPoint point) {
        log.debug("Proxying ActivitiFare invocations for ActivitiOptimisticLockingException's");

        Object value = null;
        try {
            value = point.proceed();
        } catch (final Throwable t) {
            if (t instanceof ActivitiOptimisticLockingException) {
                log.warn("Optimistic lock exception occurred, swallowing: {}", t.getMessage());
            } else {
                Throwables.propagate(t);
            }
        }

        return value;
    }
}
