package za.co.velvetant.taxi.engine.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;
import za.co.velvetant.taxi.engine.models.coupons.Coupon;
import za.co.velvetant.taxi.engine.models.coupons.Coupon_;
import za.co.velvetant.taxi.persistence.coupons.CouponRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.google.common.base.Optional.fromNullable;
import static java.lang.String.format;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.data.domain.Sort.Direction.DESC;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@Named
@Scope(SCOPE_PROTOTYPE)
public class CouponFilter implements Filter<Coupon, za.co.velvetant.taxi.engine.api.coupon.Coupon, CouponFilter> {

    private static final Logger log = LoggerFactory.getLogger(CouponFilter.class);

    @Inject
    private CouponRepository couponRepository;

    private Coupon example = new Coupon();
    private PageRequest sortable = new PageRequest(0, 10, new Sort(DESC, "validFrom"));

    private Long total;
    private List<Coupon> filtered = new ArrayList<>();
    private List<za.co.velvetant.taxi.engine.api.coupon.Coupon> vos = new ArrayList<>();

    @Override
    public CouponFilter sort(Integer page, Integer size, String sort, Sort.Direction order) {
        sortable = new PageRequest(page - 1, size, new Sort(order, sort));
        return this;
    }

    @Override
    public CouponFilter filter() {
        Page<Coupon> results = couponRepository.findAll(new CouponSpecification(example), sortable);
        filtered = results.getContent();
        total = results.getTotalElements();

        return this;
    }

    @Override
    @Transactional
    public CouponFilter andConvert() {
        for (final Coupon coupon : filtered) {
            vos.add(convert(coupon));
        }

        return this;
    }

    @Override
    public List<Coupon> entities() {
        return filtered;
    }

    @Override
    public Long totalEntities() {
        return total;
    }

    @Override
    public List<za.co.velvetant.taxi.engine.api.coupon.Coupon> vos() {
        return vos;
    }

    public CouponFilter with(final String couponNumber, final Long validFrom, final Long validTo, final BigDecimal monetaryLimit, final Boolean activated) {
        Date validFromDate = null;
        Date validToDate = null;
        if (validFrom != null) {
            validFromDate = new Date(validFrom);
        }
        if (validTo != null) {
            validToDate = new Date(validTo);
        }

        return withCouponNumber(couponNumber).withValidFromDate(validFromDate).withValidToDate(validToDate).withMonetaryLimit(monetaryLimit).isActive(activated);
    }

    private CouponFilter withCouponNumber(String couponNumber){
        example.setCouponNumber(couponNumber);
        return this;
    }

    private CouponFilter withValidFromDate(Date validFrom){
        example.setValidFrom(validFrom);
        return this;
    }

    private CouponFilter withValidToDate(Date validTo){
        example.setValidTo(validTo);
        return this;
    }

    private CouponFilter withMonetaryLimit(BigDecimal monetaryLimit){
        example.setMonetaryLimit(monetaryLimit);
        return this;
    }

    private CouponFilter isActive(Boolean active){
        example.setActive(active);
        return this;
    }

    class CouponSpecification implements Specification<Coupon> {

        public Coupon coupon;

        CouponSpecification(final Coupon coupon) {
            this.coupon = new LikableCoupon(coupon);
        }

        @Override
        public Predicate toPredicate(Root<Coupon> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            List<Predicate> predicates = new ArrayList<>();

            Path<Date> validFromPath = root.get(Coupon_.validFrom);
            Path<Date> validToPath = root.get(Coupon_.validTo);

            if (example.getValidFrom() != null && example.getValidTo() != null) {
                predicates.add(cb.and(cb.greaterThanOrEqualTo(validFromPath, coupon.getValidFrom()), cb.lessThanOrEqualTo(validToPath, coupon.getValidTo())));
            } else {
                if (example.getValidFrom() != null) {
                    predicates.add(cb.or(cb.greaterThan(validFromPath, coupon.getValidFrom())));
                }

                if (example.getValidTo() != null) {
                    predicates.add(cb.or(cb.lessThan(validToPath, coupon.getValidTo())));
                }
            }

            if (example.getMonetaryLimit() != null) {
                predicates.add(cb.or(cb.equal(root.get(Coupon_.monetaryLimit), coupon.getMonetaryLimit())));
            }

            predicates.add(cb.or(cb.like(root.get(Coupon_.couponNumber), coupon.getCouponNumber())));
            predicates.add((cb.equal(root.get(Coupon_.active), coupon.getActive())));
            query.where(predicates.toArray(new Predicate[predicates.size()]));

            return query.getRestriction();
        }

        class LikableCoupon extends Coupon {

            LikableCoupon(Coupon coupon) {
                this.setActive(coupon.getActive());
                this.setCouponNumber(coupon.getCouponNumber());
                this.setMonetaryLimit(coupon.getMonetaryLimit());
                this.setValidFrom(coupon.getValidFrom());
                this.setValidTo(coupon.getValidTo());
            }

            @Override
            public String getCouponNumber() {
                return makeLikeable(super.getCouponNumber());
            }

            @Override
            public String getUuid() {
                return makeLikeable(super.getUuid());
            }

            private String makeLikeable(final String value) {
                String likeable = format("%%%s%%", fromNullable(value).or(""));

                log.trace("Made this Coupon value likeable: {} -> {}", value, likeable);
                return likeable;
            }

        }
    }
}
