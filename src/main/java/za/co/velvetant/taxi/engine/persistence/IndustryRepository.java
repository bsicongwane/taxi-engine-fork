package za.co.velvetant.taxi.engine.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.velvetant.taxi.engine.models.Industry;

@Repository
public interface IndustryRepository extends CrudRepository<Industry, Long> {

    Industry findByName(String name);
}
