package za.co.velvetant.taxi.engine.activiti;

import static java.lang.String.format;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.FARE_REQUEST;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.ActivitiOptimisticLockingException;
import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.impl.bpmn.diagram.ProcessDiagramGenerator;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.FareEngine;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.api.FareTask;
import za.co.velvetant.taxi.engine.api.FareTasks;
import za.co.velvetant.taxi.engine.models.FareState;

@Named
public class ActivitiFareEngine implements FareEngine {

    private static final Logger log = LoggerFactory.getLogger(ActivitiFareEngine.class);

    @Inject
    private RepositoryService repositoryService;

    @Inject
    private ProcessEngine processEngine;

    @Inject
    private RuntimeService runtimeService;

    @Inject
    private IdentityService identityService;

    @Inject
    private TaskService taskService;

    @Inject
    private FormService formService;

    @Override
    public String start(final String uuid, final FareRequest request) {
        log.debug("Starting new fare[{}] with request: {}", uuid, request);

        final Map<String, Object> variables = new HashMap<>();
        variables.put(FARE_REQUEST, request);

        identityService.setAuthenticatedUserId(request.getCustomer());
        final ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("taxi", uuid, variables);

        return processInstance.getProcessInstanceId();
    }

    @Override
    public FareTasks nextTasks(final String uuid, final String person) {
        log.debug("Finding next tasks for fare[{}] and person: {}", uuid, person);

        final List<Task> tasks = getTasks(uuid, person);
        if (tasks == null) {
            throw new IllegalArgumentException();
        }

        final FareTasks fareTasks = new FareTasks(uuid);
        for (final Task task : tasks) {
            fareTasks.addTask(new FareTask(uuid, getFareState(formService, task).toString()));
        }

        return fareTasks;
    }

    @Override
    public void completeTask(final String uuid, final String person) {
        log.debug("Completing task for fare[{}] by person: {}", uuid, person);

        completeTask(uuid, person, null);
    }

    @Override
    public void completeTask(final String uuid, final String person, final Map<String, Object> data) {
        log.debug("Completing task for fare[{}] by person[{}] with the following data: {}", uuid, person, data);

        final List<Task> tasks = getTasks(uuid, person);
        if (tasks != null && tasks.size() == 1) {
            final String taskId = tasks.get(0).getId();
            taskService.claim(taskId, person);
            taskService.complete(taskId, data);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void completeTask(final String uuid, final FareState fareState, final String person) {
        log.debug("Completing task {} for fare[{}] by person: {}", fareState, uuid, person);

        completeTask(uuid, fareState, person, null);
    }

    @Override
    public void completeTask(final String uuid, final FareState fareState, final String person, final Map<String, Object> data) {
        log.debug("Completing task {} for fare[{}] by person[{}] with the following data: {}", fareState, uuid, person, data);

        final List<Task> tasks = getTasks(uuid, person);
        boolean completed = false;
        if (tasks != null) {
            for (final Task task : tasks) {
                if (getFareState(formService, task) == fareState) {
                    try {
                        taskService.claim(task.getId(), person);
                        taskService.complete(task.getId(), data);
                        completed = true;
                    } catch (ActivitiOptimisticLockingException e) {
                        log.warn("Trip [{}] task [{}] already completed concurrently", uuid, task.getName());
                        throw new IllegalArgumentException(format("The task of state [%s] has already been completed", fareState));
                    }

                    break;
                }
            }

            if (!completed) {
                throw new IllegalArgumentException(format("There was no task of state [%s] to complete", fareState));
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void sendSignal(final String uuid, final String signal) {
        final ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(uuid).singleResult();
        final Execution execution = runtimeService.createExecutionQuery().signalEventSubscriptionName(signal).processInstanceId(processInstance.getProcessInstanceId()).singleResult();

        log.debug("Signalling execution[{}]: {}", execution.getId(), execution.getActivityId());
        runtimeService.signalEventReceived(signal, execution.getId());
    }

    @Override
    public List<String> current(final String customer) {
        List<ProcessInstance> fareInstances;
        if (customer != null) {
            fareInstances = runtimeService.createProcessInstanceQuery().variableValueEquals("customer", customer).active().list();
        } else {
            fareInstances = runtimeService.createProcessInstanceQuery().active().list();
        }
        final List<String> fares = new ArrayList<>(fareInstances.size());
        for (final ProcessInstance fareInstance : fareInstances) {
            fares.add(fareInstance.getBusinessKey());
        }
        return fares;
    }

    @Override
    public InputStream diagram(final String uuid) {
        InputStream diagram = null;
        final ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(uuid).singleResult();

        if (processInstance != null) {
            final List<String> executionIds = new ArrayList<>();
            final List<Execution> executions = runtimeService.createExecutionQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
            for (final Execution execution : executions) {
                executionIds.addAll(runtimeService.getActiveActivityIds(execution.getId()));
            }

            diagram = ProcessDiagramGenerator.generateDiagram(repositoryService.getBpmnModel(processInstance.getProcessDefinitionId()), "png", executionIds);
        }

        return diagram;
    }

    private List<Task> getTasks(final String uuid, final String person) {
        List<Task> tasks = taskService.createTaskQuery().processInstanceBusinessKey(uuid).taskAssignee(person).list();
        if (tasks.isEmpty()) {
            tasks = taskService.createTaskQuery().processInstanceBusinessKey(uuid).taskCandidateUser(person).list();
        }

        return tasks;
    }

    private FareState getFareState(final FormService formService, final Task task) {
        FareState fareState = null;

        final TaskFormData formData = formService.getTaskFormData(task.getId());
        for (final FormProperty formProperty : formData.getFormProperties()) {
            if (formProperty.getId().equals("task.type")) {
                fareState = FareState.valueOf(formProperty.getValue());
                break;
            }
        }

        return fareState;
    }

    @Override
    public List<String> getFaresAwaitingFormTask(final FareState fareState) {
        final List<String> fares = new ArrayList<>();

        final List<Task> list = taskService.createTaskQuery().active().list();
        for (final Task task : list) {
            for (final FormProperty formProperty : formService.getTaskFormData(task.getId()).getFormProperties()) {
                if (formProperty.getId().equals("task.type") && FareState.valueOf(formProperty.getValue()) == fareState) {
                    fares.add(task.getProcessInstanceId());
                }
            }
        }
        return fares;

    }

    @Override
    public List<String> completed(final Date start, final Date end, final String customer) {
        final List<String> fares = new ArrayList<>();
        List<HistoricProcessInstance> list = null;
        if (customer == null) {
            list = processEngine.getHistoryService().createHistoricProcessInstanceQuery().finished().list();
        } else {
            list = processEngine.getHistoryService().createHistoricProcessInstanceQuery().finished().variableValueEquals("customer", customer).list();
        }

        for (final HistoricProcessInstance instance : list) {
            if (start != null && end != null && instance.getEndTime().after(start) && instance.getEndTime().before(end)) {
                fares.add(instance.getBusinessKey());
            } else if (start == null && end == null) {
                fares.add(instance.getBusinessKey());
            } else if (end != null && instance.getEndTime().before(end)) {
                fares.add(instance.getBusinessKey());
            } else if (start != null && instance.getEndTime().after(start)) {
                fares.add(instance.getBusinessKey());
            }
        }
        return fares;

    }

    @Override
    public void removeFare(final String uuid, final String reason) {
        final ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(uuid).singleResult();
        if(processInstance != null) {
            runtimeService.deleteProcessInstance(processInstance.getProcessInstanceId(), reason);
        }
    }
}
