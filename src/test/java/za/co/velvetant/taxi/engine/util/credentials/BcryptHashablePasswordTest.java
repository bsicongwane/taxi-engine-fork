package za.co.velvetant.taxi.engine.util.credentials;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;

public class BcryptHashablePasswordTest {

    private Password password;

    @Before
    public void setup() {
        password = new BcryptHashablePassword() {

            @Override
            public String generate() {
                return null;
            }

            @Override
            public String generate(final int digits) {
                return null;
            }
        };
    }

    @Test
    public void hashes_password_successfully() {
        String hash = password.hash("test");

        assertThat(BCrypt.checkpw("test", hash)).isTrue();
    }
}
