package za.co.velvetant.taxi.engine.delegates;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.coupons.Coupon;
import za.co.velvetant.taxi.engine.models.coupons.CustomerCoupon;
import za.co.velvetant.taxi.engine.rest.exception.ConflictException;
import za.co.velvetant.taxi.engine.rest.exception.PreconditionFailedException;
import za.co.velvetant.taxi.persistence.coupons.CouponRepository;
import za.co.velvetant.taxi.persistence.coupons.CustomerCouponRepository;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;

import java.util.ArrayList;
import java.util.Date;

import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CouponsTest {

    @Mock
    private Coupon coupon;

    @Mock
    private CustomerCoupon customerCoupon;

    @Mock
    private Customer customer;

    @Mock
    private CouponRepository couponRepository;

    @Mock
    private CustomerCouponRepository customerCouponRepository;

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private Coupons coupons;

    @Before
    public void setup() {
        given(couponRepository.findByCouponNumber("123-456-789")).willReturn(coupon);
        given(customerRepository.findByUserId("bob@velvetant.co.za")).willReturn(customer);
        given(customerCouponRepository.findByCustomerAndCoupon(customer, coupon)).willReturn(ImmutableList.of(customerCoupon));
    }

    @Test(expected = PreconditionFailedException.class)
    public void check_that_null_coupon_is_caught() {
        coupons.create(null);
    }

    @Test(expected = PreconditionFailedException.class)
    public void try_assign_an_already_assigned_customer_coupon_that_has_expired() {
        given(customerCoupon.hasExpired()).willReturn(true);

        coupons.assign("123-456-789", "bob@velvetant.co.za");
    }

    @SuppressWarnings("unchecked")
    @Test(expected = PreconditionFailedException.class)
    public void try_assign_coupon_that_has_reached_its_usage_limit() {
        given(customerCouponRepository.findByCustomerAndCoupon(customer, coupon)).willReturn(new ArrayList());
        given(customerCoupon.getCoupon()).willReturn(coupon);
        given(coupon.getUsageLimit()).willReturn(1);
        given(coupon.getCustomerCoupons()).willReturn(ImmutableSet.of(customerCoupon));

        coupons.assign("123-456-789", "bob@velvetant.co.za");
    }

    @Test(expected = IllegalStateException.class)
    public void try_assign_coupon_that_has_already_been_assigned() {
        given(customerCouponRepository.findByCustomerAndCoupon(customer, coupon)).willReturn(ImmutableList.of(customerCoupon, new CustomerCoupon()));
        given(coupon.getCouponNumber()).willReturn("123-456-789");
        given(customer.getUserId()).willReturn("bob@velvetant.co.za");

        coupons.assign("123-456-789", "bob@velvetant.co.za");
    }

    @Test(expected = ConflictException.class)
    public void try_redeem_a_coupon_that_has_already_been_redeemed() {
        given(customerCoupon.isRedeemed()).willReturn(true);
        given(customerCoupon.getRedemptionDate()).willReturn(new Date());
        given(customerCouponRepository.findByCustomerAndCoupon(customer, coupon)).willReturn(ImmutableList.of(customerCoupon));

        coupons.redeem("123-456-789", "bob@velvetant.co.za");
    }
}
