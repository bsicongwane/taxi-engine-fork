package za.co.velvetant.taxi.engine.activiti;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.ACCEPTED_DRIVER;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.CANCEL_FARE;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.CVV;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.DRIVER_RATING;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.FARE_REQUEST;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_OVERRIDDEN;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.OPS_USER;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_AMOUNT;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_CONFIRMED;
import static za.co.velvetant.taxi.engine.activiti.util.Variables.PAYMENT_METHOD;
import static za.co.velvetant.taxi.engine.api.PaymentMethod.CASH;
import static za.co.velvetant.taxi.engine.api.PaymentMethod.CORPORATE_ACCOUNT;
import static za.co.velvetant.taxi.engine.api.PaymentMethod.CREDIT_CARD;
import static za.co.velvetant.taxi.engine.models.FareState.ACCEPT_FARES;
import static za.co.velvetant.taxi.engine.models.FareState.ARRIVED_AT_DESTINATION;
import static za.co.velvetant.taxi.engine.models.FareState.ARRIVE_AT_PICKUP;
import static za.co.velvetant.taxi.engine.models.FareState.AWAIT_PAYMENT_CONFIRMATION;
import static za.co.velvetant.taxi.engine.models.FareState.CAPTURE_FARE_AMOUNT;
import static za.co.velvetant.taxi.engine.models.FareState.GET_PICKED_UP;
import static za.co.velvetant.taxi.engine.models.FareState.PICK_UP_CUSTOMER;
import static za.co.velvetant.taxi.engine.models.FareState.RATE_DRIVER;
import static za.co.velvetant.taxi.engine.models.FareState.WAIT_FOR_DRIVER;
import static za.co.velvetant.taxi.engine.models.FareState.WAIT_FOR_DRIVERS;
import static za.co.velvetant.taxi.engine.models.SystemProperty.Name.ALLOWED_PAYMENT_CONFIRMATIONS;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.engine.api.PaymentMethod;
import za.co.velvetant.taxi.engine.api.Rating;
import za.co.velvetant.taxi.engine.models.FareState;
import za.co.velvetant.taxi.engine.util.SystemPropertyHelper;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

import com.google.common.collect.ImmutableList;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = "test")
@ContextConfiguration("classpath:spring-context.xml")
@Ignore
public class TaxiFareBPMNFunctionalTestCase {

    private static final Logger log = LoggerFactory.getLogger(TaxiFareBPMNFunctionalTestCase.class);

    @Inject
    @Rule
    public ActivitiRule activitiRule;

    @Inject
    private TaxiCompanyRepository affiliateRepository;

    @Inject
    private SystemPropertyHelper systemPropertyHelper;

    private RuntimeService runtimeService;
    private IdentityService identityService;
    private TaskService taskService;
    private FormService formService;
    private ManagementService managementService;

    private FareRequest request;
    private String driver;
    private ProcessInstance processInstance;

    @Before
    public void setup() {
        runtimeService = activitiRule.getRuntimeService();
        identityService = activitiRule.getIdentityService();
        taskService = activitiRule.getTaskService();
        formService = activitiRule.getFormService();
        managementService = activitiRule.getManagementService();

        driver = "driver1";
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_credit_card_and_affiliate() {
        requestFareWithAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allAffiliatedDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(99.99);
        // Customer confirms amount for credit card payment
        customerConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_credit_card() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(99.99);
        // Customer confirms amount for credit card payment
        customerConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_cash() {
        requestFareWithoutAffiliate(CASH);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(99.99);
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_corporate_account() {
        requestFareWithoutAffiliate(CORPORATE_ACCOUNT);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(99.99);
        // Customer confirms amount for credit card payment
        customerConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void cancel_fare_while_waiting_for_eligible_drivers() {
        requestFareWithoutAffiliate(CORPORATE_ACCOUNT);
        customerWaitingForDriverToBeAssigned();
        // No drivers are dispatched yet
        customerCancelsFare();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void cancel_fare_while_waiting_for_driver() {
        requestFareWithoutAffiliate(CORPORATE_ACCOUNT);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        customerCancelsFare();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void cancel_fare_while_waiting_for_driver_to_pickup_customer() {
        requestFareWithoutAffiliate(CORPORATE_ACCOUNT);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerCancelsFare();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_one_declined_credit_card_payment() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(88.88);
        // Customer declines credit card payment
        customerDeclinesCreditCardPayment();
        // Driver re-captures amount of the fare, keeping payment type the same
        driverCapturesCostOfFare(99.99);
        customerConfirmsCreditCardPayment();
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_one_declined_credit_card_payment_changed_to_cash_payment() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(88.88);
        // Customer declines credit card payment
        customerDeclinesCreditCardPayment();
        // Driver re-captures amount of the fare, changing payment type to CASH
        driverCapturesCostOfFare(99.99, CASH);
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void if_no_driver_accepts_fare_within_time_then_cancel_fare() {
        requestFareWithoutAffiliate(CASH);
        customerWaitingForDriverToBeAssigned();

        for (int x = 0; x < 3; x++) {
            final Job timer = managementService.createJobQuery().singleResult();
            managementService.executeJob(timer.getId());
        }

        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void if_customer_does_not_rate_driver_within_time_then_complete_fare() {
        requestFareWithoutAffiliate(CASH);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(99.99);
        // Customer has paid
        // Customer does not rate fare within time limit
        final Job timer = managementService.createJobQuery().singleResult();
        managementService.executeJob(timer.getId());
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_credit_card_failed_and_fallback_to_cash() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(88.88);
        // Customer declines credit card payment
        customerDeclinesCreditCardPayment();
        // Driver re-captures amount of the fare, changing payment type to CASH
        driverCapturesCostOfFare(99.99, CASH);
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void successfully_time_out_fare_when_payment_not_captured() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();

        // Driver does not capture payment for all attempts
        int attempts = Integer.parseInt(systemPropertyHelper.getProperty(ALLOWED_PAYMENT_CONFIRMATIONS));
        for (int x = 0; x < attempts; x++) {
            completeTimer();
        }

        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void successfully_complete_fare_where_one_time_out_when_payment_not_captured() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver does not capture payment
        completeTimer();
        // Driver re-captures amount of the fare, changing payment type to CASH
        driverCapturesCostOfFare(99.99, CASH);
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void successfully_time_out_fare_when_payment_not_confirmed() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();

        int attempts = Integer.parseInt(systemPropertyHelper.getProperty(ALLOWED_PAYMENT_CONFIRMATIONS));
        log.debug("Attempting to capture fare amount [{}] times", attempts);
        for (int x = 0; x < attempts; x++) {
            // Driver captures amount of the fare
            driverCapturesCostOfFare(99.99);
            // Customer does not confirm payment within time limit
            completeTimer();
        }

        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void successfully_complete_fare_where_one_time_out_when_payment_not_confirmed() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(99.99);
        // Customer does not confirm payment within time limit
        completeTimer();
        // Driver re-captures amount of the fare, changing to CASH
        driverCapturesCostOfFare(99.99, CASH);
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_credit_card_with_trip_amount_overridden() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Ops user captures amount of the fare
        opsUserOverridesCaptureTripAmount();
        opsUserCapturesTripAmount(99.99);
        customerConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_credit_card_with_ops_confirming_payment() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(99.99);
        // Ops overrides confirm payment
        opsUserOverridesConfirmCreditCardPayment();
        // Ops user confirms amount for credit card payment
        opsUserConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_credit_card_with_ops_overriding_trip_amount_and_confirming_payment() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Ops captures amount of the fare
        opsUserOverridesCaptureTripAmount();
        opsUserCapturesTripAmount(99.99);
        // Ops overrides confirm payment
        opsUserOverridesConfirmCreditCardPayment();
        // Ops user confirms amount for credit card payment
        opsUserConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_one_overridden_trip_amount_declined_overridden_credit_card_payment() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Ops user captures amount of the fare
        opsUserOverridesCaptureTripAmount();
        opsUserCapturesTripAmount(99.99);
        // Ops overrides confirm payment
        opsUserOverridesConfirmCreditCardPayment();
        opsUserDeclinesCreditCardPayment();
        // Ops user captures amount of the fare
        opsUserOverridesCaptureTripAmount();
        opsUserCapturesTripAmount(99.99);
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        // Ops overrides confirm payment
        opsUserOverridesConfirmCreditCardPayment();
        opsUserConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void complete_successful_fare_with_one_declined_overridden_credit_card_payment() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures amount of the fare
        driverCapturesCostOfFare(88.88);
        // Ops overrides confirm payment
        opsUserOverridesConfirmCreditCardPayment();
        opsUserDeclinesCreditCardPayment();
        // Driver re-captures amount of the fare, changing payment type to CASH
        driverCapturesCostOfFare(99.99, CASH);
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    @Test
    @Deployment(resources = "bpmn/activiti/taxi-1.bpmn")
    public void fix_1251() {
        requestFareWithoutAffiliate(CREDIT_CARD);
        customerWaitingForDriverToBeAssigned();
        // Drivers are dispatched
        allDriversAcceptFare();
        customerWaitsForDriver();
        driverArrivesAtPickup();
        customerGetsPickedUp();
        driverPicksUpCustomer();
        // Customer is en route
        // Customer has arrived at their destination
        driverArrivesAtDestination();
        customerArrivesAtDestination();
        // Driver captures zero amount by mistake
        driverCapturesCostOfFare(0.00);
        // Customer declines credit card payment so that driver can change amount
        customerDeclinesCreditCardPayment();
        // Driver re-captures amount of the trip
        driverCapturesCostOfFare(99.99);
        checkProcessValue(PAYMENT_AMOUNT, 99.99);
        customerConfirmsCreditCardPayment();
        // Customer has paid
        customerRatesDriver();
        fareShouldBeFinished();
    }

    private void requestFareWithAffiliate(final PaymentMethod paymentMethod) {
        requestFare(paymentMethod, true);
    }

    private void requestFareWithoutAffiliate(final PaymentMethod paymentMethod) {
        requestFare(paymentMethod, false);
    }

    private void requestFare(final PaymentMethod paymentMethod, final boolean withAffiliate) {
        final String uuid = UUID.randomUUID().toString();
        request = fareRequest(paymentMethod, withAffiliate);

        final Map<String, Object> variables = new HashMap<>();
        variables.put(FARE_REQUEST, request);
        identityService.setAuthenticatedUserId(request.getCustomer());
        processInstance = runtimeService.startProcessInstanceByKey("taxi", uuid, variables);

        assertNotNull(processInstance.getProcessInstanceId());
        assertEquals(uuid, processInstance.getBusinessKey());
    }

    private void allDriversAcceptFare() {
        driverAcceptsFare(ImmutableList.of(driver, "driver2", "driver3"));
    }

    private void allAffiliatedDriversAcceptFare() {
        driverAcceptsFare(ImmutableList.of(driver, "driver2"));
    }

    private void driverAcceptsFare(List<String> expectedDrivers) {
        Task task = taskService.createTaskQuery().taskCandidateUser(driver).singleResult();
        logFormProperties(task);

        final List<String> candidates = new ArrayList<>();
        for (final IdentityLink link : taskService.getIdentityLinksForTask(task.getId())) {
            log.debug("Task [{}] has available candidate: {}", task.getName(), link.getUserId());
            candidates.add(link.getUserId());
        }

        assertThat(candidates).containsAll(expectedDrivers);
        assertEquals("Task type should match", ACCEPT_FARES, fareState(task));

        final Map<String, Object> variables = new HashMap<>();
        variables.put(ACCEPTED_DRIVER, driver);
        claimAndComplete(task, driver, variables);

        task = checkTaskAssigneeAndType(request.getCustomer(), WAIT_FOR_DRIVERS);

        claimAndComplete(task, request.getCustomer());
    }

    private void customerWaitingForDriverToBeAssigned() {
        checkTaskAssigneeAndType(request.getCustomer(), WAIT_FOR_DRIVERS);
    }

    private void driverArrivesAtPickup() {
        final Task task = checkTaskAssigneeAndType(driver, ARRIVE_AT_PICKUP);
        claimAndComplete(task, driver);
    }

    private void customerGetsPickedUp() {
        final Task task = checkTaskAssigneeAndType(request.getCustomer(), WAIT_FOR_DRIVER);
        claimAndComplete(task, request.getCustomer());
    }

    private void driverPicksUpCustomer() {
        Task task = checkTaskAssigneeAndType(request.getCustomer(), GET_PICKED_UP);

        claimAndComplete(task, request.getCustomer());

        task = checkTaskAssigneeAndType(driver, PICK_UP_CUSTOMER);

        claimAndComplete(task, driver);
    }

    private void customerWaitsForDriver() {
        checkTaskAssigneeAndType(request.getCustomer(), WAIT_FOR_DRIVER);
    }

    private void driverArrivesAtDestination() {
        final Task task = checkTaskAssigneeAndType(driver, ARRIVED_AT_DESTINATION);

        claimAndComplete(task, driver);
    }

    private void customerArrivesAtDestination() {
        final Task task = checkTaskAssigneeAndType(request.getCustomer(), ARRIVED_AT_DESTINATION);

        claimAndComplete(task, request.getCustomer());
    }

    private void driverCapturesCostOfFare(final Double amount) {
        driverCapturesCostOfFare(amount, null);
    }

    private void opsUserOverridesCaptureTripAmount() {
        final Task task = checkTaskAssigneeAndType(driver, CAPTURE_FARE_AMOUNT);

        final Map<String, Object> variables = new HashMap<>();
        variables.put(OPS_USER, OPS_USER);
        variables.put(OPS_OVERRIDDEN, true);
        claimAndComplete(task, driver, variables);

        Task customerTask = checkTaskAssigneeAndType(request.getCustomer(), CAPTURE_FARE_AMOUNT);
        claimAndComplete(customerTask, request.getCustomer());

        checkTaskAssigneeAndType(OPS_USER, CAPTURE_FARE_AMOUNT);
        checkTaskAssigneeAndType(request.getCustomer(), CAPTURE_FARE_AMOUNT);
    }

    private void opsUserOverridesConfirmCreditCardPayment() {
        final Task task = checkTaskAssigneeAndType(driver, AWAIT_PAYMENT_CONFIRMATION);

        final Map<String, Object> variables = new HashMap<>();
        variables.put(OPS_USER, OPS_USER);
        variables.put(OPS_OVERRIDDEN, true);
        claimAndComplete(task, driver, variables);

        Task customerTask = checkTaskAssigneeAndType(request.getCustomer(), AWAIT_PAYMENT_CONFIRMATION);
        claimAndComplete(customerTask, request.getCustomer());

        checkTaskAssigneeAndType(OPS_USER, AWAIT_PAYMENT_CONFIRMATION);
    }

    private void driverCapturesCostOfFare(final Double amount, final PaymentMethod paymentMethod) {
        Task task = checkTaskAssigneeAndType(driver, CAPTURE_FARE_AMOUNT);

        final Map<String, Object> variables = new HashMap<>();
        variables.put(PAYMENT_AMOUNT, amount);
        if (paymentMethod != null) {
            variables.put(PAYMENT_METHOD, za.co.velvetant.taxi.engine.models.PaymentMethod.valueOf(paymentMethod.toString()));
        }
        claimAndComplete(task, driver, variables);

        task = checkTaskAssigneeAndType(request.getCustomer(), CAPTURE_FARE_AMOUNT);
        claimAndComplete(task, request.getCustomer());
    }

    private void opsUserCapturesTripAmount(final double amount) {
        opsUserCapturesTripAmount(amount, null);
    }

    private void opsUserCapturesTripAmount(final double amount, final PaymentMethod paymentMethod) {
        Task task = checkTaskAssigneeAndType(OPS_USER, CAPTURE_FARE_AMOUNT);

        final Map<String, Object> variables = new HashMap<>();
        variables.put(PAYMENT_AMOUNT, amount);
        if (paymentMethod != null) {
            variables.put(PAYMENT_METHOD, za.co.velvetant.taxi.engine.models.PaymentMethod.valueOf(paymentMethod.toString()));
        }
        claimAndComplete(task, OPS_USER, variables);

        Task customerTask = checkTaskAssigneeAndType(request.getCustomer(), CAPTURE_FARE_AMOUNT);
        claimAndComplete(customerTask, request.getCustomer());

    }

    private void customerConfirmsCreditCardPayment() {
        confirmCreditCardPayment(true);
    }

    private void opsUserConfirmsCreditCardPayment() {
        opsConfirmCreditCardPayment(true);
    }

    private void opsUserDeclinesCreditCardPayment() {
        opsConfirmCreditCardPayment(false);
    }

    private void customerDeclinesCreditCardPayment() {
        confirmCreditCardPayment(false);
    }

    private void confirmCreditCardPayment(final boolean confirmed) {
        final Task driverTask = checkTaskAssigneeAndType(driver, AWAIT_PAYMENT_CONFIRMATION);
        claimAndComplete(driverTask, driver);

        final Task task = checkTaskAssigneeAndType(request.getCustomer(), AWAIT_PAYMENT_CONFIRMATION);

        final Map<String, Object> variables = new HashMap<>();
        variables.put(PAYMENT_CONFIRMED, confirmed);
        if (confirmed) {
            variables.put(CVV, "123");
        }
        claimAndComplete(task, request.getCustomer(), variables);
    }

    public void opsConfirmCreditCardPayment(final boolean confirmed) {
        final Task task = checkTaskAssigneeAndType(request.getCustomer(), AWAIT_PAYMENT_CONFIRMATION);
        claimAndComplete(task, request.getCustomer());

        final Map<String, Object> variables = new HashMap<>();
        variables.put(PAYMENT_CONFIRMED, confirmed);
        if (confirmed) {
            variables.put(CVV, "123");
        }
        variables.put(OPS_OVERRIDDEN, true);

        final Task opsTask = checkTaskAssigneeAndType(OPS_USER, AWAIT_PAYMENT_CONFIRMATION);
        claimAndComplete(opsTask, OPS_USER, variables);
    }

    private void customerRatesDriver() {
        final Task task = checkTaskAssigneeAndType(request.getCustomer(), RATE_DRIVER);

        final Map<String, Object> variables = new HashMap<>();
        final Rating rating = new Rating(5, 5, processInstance.getBusinessKey());
        variables.put(DRIVER_RATING, rating);
        claimAndComplete(task, request.getCustomer(), variables);
    }

    private void customerCancelsFare() {
        sendSignal(CANCEL_FARE);
    }

    private void fareShouldBeFinished() {
        for (Execution execution : runtimeService.createExecutionQuery().processInstanceId(processInstance.getProcessInstanceId()).list()) {
            log.warn("Active execution: {}", execution);
        }

        assertThat(runtimeService.createProcessInstanceQuery().processInstanceId(processInstance.getProcessInstanceId()).active().singleResult()).isNull();
    }

    private Task checkTaskAssigneeAndType(final String assignee, final FareState fareState) {

        log.debug("Available tasks for assignee: {}", assignee);
        for (Task task : taskService.createTaskQuery().taskAssignee(assignee).list()) {
            log.debug("\t-> Task: {}", task);
        }

        final Task task = taskService.createTaskQuery().taskAssignee(assignee).singleResult();
        logFormProperties(task);

        assertEquals("Assignee should match", assignee, task.getAssignee());
        assertEquals("Task type should match", fareState, fareState(task));

        return task;
    }

    private FareRequest fareRequest(final PaymentMethod paymentMethod, final boolean withAffiliate) {
        final FareRequest request = new FareRequest(new Date().getTime(), new Location(-26.092594, 28.129807), "bob@velvetant.co.za", paymentMethod.name());

        if (withAffiliate) {
            request.addAffiliateId(affiliateRepository.findByCompanyName("Ant Farmers Pty Ltd").getRegistration());
        }

        return request;
    }

    private void claimAndComplete(final Task task, final String assignee) {
        claimAndComplete(task, assignee, null);
    }

    private void claimAndComplete(final Task task, final String assignee, final Map<String, Object> variables) {
        taskService.claim(task.getId(), assignee);
        taskService.complete(task.getId(), variables);
    }

    private void sendSignal(final String name) {
        final Execution execution = runtimeService.createExecutionQuery().signalEventSubscriptionName(name).processInstanceId(processInstance.getProcessInstanceId()).singleResult();
        try {
            log.debug("Signalling execution[{}]: {}", execution.getId(), execution.getActivityId());
            runtimeService.signalEventReceived(name, execution.getId());
        } catch (final Exception e) {
            log.warn("Could not signal [{}] for execution: {}", name, execution.getId());
        }

        final List<Execution> executions = runtimeService.createExecutionQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
        for (final Execution activeExecution : executions) {
            log.error("Executions still active[{}]: {}", activeExecution.getId(), activeExecution.getActivityId());
        }
    }

    private void logFormProperties(final Task task) {
        final TaskFormData formData = formService.getTaskFormData(task.getId());
        log.debug("Task[{}] assigned to {} has the following properties:", task.getName(), task.getAssignee());
        for (final FormProperty formProperty : formData.getFormProperties()) {
            log.debug("\t{}", ToStringBuilder.reflectionToString(formProperty, ToStringStyle.MULTI_LINE_STYLE));
        }
    }

    private FareState fareState(final Task task) {
        FareState fareState = null;

        final TaskFormData formData = formService.getTaskFormData(task.getId());
        for (final FormProperty formProperty : formData.getFormProperties()) {
            if (formProperty.getId().equals("task.type")) {
                fareState = FareState.valueOf(formProperty.getValue());
                break;
            }
        }

        return fareState;
    }

    private void checkProcessValue(final String name, final Object value) {
        assertThat(runtimeService.createProcessInstanceQuery().processInstanceId(processInstance.getProcessInstanceId()).variableValueEquals(name, value).singleResult()).isNotNull();
    }

    private void completeTimer() {
        Job timer = managementService.createJobQuery().singleResult();
        managementService.executeJob(timer.getId());
    }

}
