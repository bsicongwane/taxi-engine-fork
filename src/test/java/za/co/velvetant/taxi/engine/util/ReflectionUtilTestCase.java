package za.co.velvetant.taxi.engine.util;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.engine.api.Favourite;
import za.co.velvetant.taxi.engine.api.User;
import za.co.velvetant.taxi.engine.models.Corporate;
import za.co.velvetant.taxi.engine.models.Customer;

@Ignore
public class ReflectionUtilTestCase {

    private static final Logger log = LoggerFactory.getLogger(ReflectionUtilTestCase.class);

    @Test
    public void simpleCopy() throws IllegalAccessException, InstantiationException {
        User userVo = new User("bob", "ant", "12345", "test@test.com");

        User u = new User();
        // vo to entity
        ReflectionUtil.copySameNameFields(userVo, u);

        assertEquals(userVo.getCellphone(), u.getCellphone());
        za.co.velvetant.taxi.engine.models.User entity = new za.co.velvetant.taxi.engine.models.User("bobEntity", "antEntity", "12345", "test@test.com", null);
        userVo = new User();
        // entity to vo
        ReflectionUtil.copySameNameFields(entity, userVo);

        assertEquals(entity.getLastName(), userVo.getLastName());
    }

    @Test
    public void complexCopy() throws IllegalAccessException, InstantiationException {
        Corporate entity = new Corporate();
        entity.setBillingAddress1("entitybilling");
        Customer customer = new Customer("test1", "test1Last", "0835540735", "test@email.com", null);
        customer.setCorporate(entity);

        Favourite api = new Favourite();
        api.setLocation(new Location(0.3, 0.4, "Home"));

        za.co.velvetant.taxi.engine.models.Favourite convert = VoConversion.convert(api, new za.co.velvetant.taxi.engine.models.Favourite());
        log.debug("Converted model: {}", convert);

        assertEquals("Home", convert.getLocation().getAddress());

    }

}
