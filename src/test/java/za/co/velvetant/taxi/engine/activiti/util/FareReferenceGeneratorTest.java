package za.co.velvetant.taxi.engine.activiti.util;

import static java.lang.String.format;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.FareReference;
import za.co.velvetant.taxi.engine.persistence.FareReferenceRepository;

@RunWith(MockitoJUnitRunner.class)
public class FareReferenceGeneratorTest {

    @Mock
    private FareReferenceRepository fareReferenceRepository;

    @InjectMocks
    private FareReferenceGenerator generator;

    private Fare fare;
    private FareReference reference;

    @Before
    public void setup() {
        reference = new FareReference();
        fare = mock(Fare.class);

        when(fareReferenceRepository.save(any(FareReference.class))).thenReturn(reference);
    }

    @Test
    public void fare_reference_at_beginning_of_month_generated_successfully() throws Exception {
        reference.setId(0L);
        when(fareReferenceRepository.findFirst(any(Date.class))).thenReturn(0L);

        String reference = generator.generate(fare);

        assertThat(reference).isEqualTo(format("%s-0001", new DateTime().toString("yyMMdd")));
    }

    @Test
    public void fare_reference_at_random_day_generated_successfully() throws Exception {
        reference.setId(42L);
        when(fareReferenceRepository.findFirst(any(Date.class))).thenReturn(13L);

        String reference = generator.generate(fare);

        assertThat(reference).isEqualTo(format("%s-0030", new DateTime().toString("yyMMdd")));
    }
}
