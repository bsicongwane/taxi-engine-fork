package za.co.velvetant.taxi.engine.util.credentials;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class AlphaNumericPasswordTest {

    private Password password;

    @Before
    public void setup() throws Exception {
        password = new AlphaNumericPassword();
    }

    @Test
    public void generates_random_alphanumeric_password_successfully() {
        String password1 = password.generate();
        String password2 = password.generate();

        assertThat(password1).doesNotMatch(password2);
        assertThat(password1).hasSize(10);
    }

    @Test
    public void generates_random_alphanumeric_pin_with_5_digits_successfully() {
        String password1 = password.generate(5);
        String password2 = password.generate(5);

        assertThat(password1).doesNotMatch(password2);
        assertThat(password1).hasSize(5);
    }
}
