package za.co.velvetant.taxi.engine.util;

import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;

import za.co.velvetant.taxi.engine.messaging.mail.MailService;
import za.co.velvetant.taxi.engine.messaging.mail.mandrill.MandrillConfig;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.engine.models.Location;
import za.co.velvetant.taxi.engine.models.driver.Driver;

@Ignore
public class MailerTestCase {

    @Test
    public void sendInvoice() {
        final MailService service  = new MailService() {

            @Override
            protected MandrillConfig loadConfig() {
                return new MandrillConfig("https://mandrillapp.com/api/1.0/messages/send-template.json", "CvyoPzoypHMJuEmiGwqu3w", "evans@snappcab.com", "EvansArmitage", null);
            }
        };
        final Fare fare = new Fare();
        fare.setReference("12233");
        fare.setPickedUpTime(new Date(System.currentTimeMillis()));
        fare.setDropOffTime(new Date(System.currentTimeMillis()));
        fare.setPickup(new Location(null, null, "6 Pinard road"));
        fare.setDropOff(new Location(null, null, "8 Pinard road"));
        fare.setPickUpTime(new Date(System.currentTimeMillis()));
        fare.setAmount(42.0);
        fare.setTip(34.0);
        fare.setDriver(new Driver("Test", "Tester", "0835540735", "evans@velvetant.co.za", "", "001", "123"));
        final Customer customer = new Customer("Evans", "Armitage", "0835540735", "evans@velvetant.co.za", "r035198x");
        fare.setCustomer(customer);
        final String crediCardMask = "12132***";
        service.sendInvoiceEmail(fare , crediCardMask);
    }

}
