package za.co.velvetant.taxi.engine.util.credentials;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

public class SimpleDigitPasswordTest {

    private Password password;

    @Before
    public void setup() {
        password = new SimpleDigitPassword();
    }

    @Test
    public void generates_random_4_digit_password_successfully() {
        String password1 = password.generate();
        String password2 = password.generate();

        assertThat(password1).doesNotMatch(password2);
        assertThat(password1).hasSize(4);
    }

    @Test
    public void generates_random_8_digit_password_successfully() {
        String password1 = password.generate(8);
        String password2 = password.generate(8);

        assertThat(password1).doesNotMatch(password2);
        assertThat(password1).hasSize(8);
    }
}
