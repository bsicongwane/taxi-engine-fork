package za.co.velvetant.taxi.engine.persistence;

import static com.google.common.collect.Lists.transform;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

import za.co.velvetant.taxi.api.common.Location;
import za.co.velvetant.taxi.api.common.TaxiDistance;
import za.co.velvetant.taxi.dispatch.api.TaxiTracking;
import za.co.velvetant.taxi.engine.models.Taxi;
import za.co.velvetant.taxi.persistence.taxi.TaxiLocationRepositoryCustom;
import za.co.velvetant.taxi.persistence.taxi.TaxiRepository;

import com.google.common.base.Function;

@Named("FakeTaxiLocationRepository")
public class FakeTaxiLocationRepository implements TaxiLocationRepositoryCustom {

    private static final Logger log = LoggerFactory.getLogger(FakeTaxiLocationRepository.class);

    @Inject
    private TaxiRepository taxiRepository;

    @Override
    public Collection<TaxiDistance> findInVicinity(final BigDecimal latitude, final BigDecimal longitude, final int numberOfTaxis, final List<String> affiliates, final int radius) {
        log.debug("[FAKE] Finding [{}] taxis in vicinity [{}] for affiliates: {}", numberOfTaxis, radius, affiliates);
        return transform(taxiRepository.findAll(new PageRequest(0, numberOfTaxis)).getContent(), new Function<Taxi, TaxiDistance>() {

            @Override
            public TaxiDistance apply(final Taxi taxi) {
                return new TaxiDistance(new za.co.velvetant.taxi.api.common.Taxi(taxi.getRegistration()), Math.random() * 10);
            }
        });
    }

    @Override
    public Long countBusyInRadius(final BigDecimal latitude, final BigDecimal longitude, final int radius) {
        return 0L;
    }

    @Override
    public Collection<TaxiTracking> findClosest(final BigDecimal latitude, final BigDecimal longitude, final int numberOfTaxis, final float radius) {
        log.debug("[FAKE] Finding [{}] closest taxis in radius [{}]: {}", numberOfTaxis, radius);
        return transform(taxiRepository.findAll(new PageRequest(0, numberOfTaxis)).getContent(), new Function<Taxi, TaxiTracking>() {

            @Override
            public TaxiTracking apply(final Taxi taxi) {
                return new TaxiTracking(taxi.getRegistration(), new Location(), null, null);
            }
        });
    }
}
