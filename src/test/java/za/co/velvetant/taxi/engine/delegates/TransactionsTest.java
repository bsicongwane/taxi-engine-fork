package za.co.velvetant.taxi.engine.delegates;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

@RunWith(MockitoJUnitRunner.class)
public class TransactionsTest {

    @Mock
    private TransactionRepository repository;

    @Mock
    private Customer customer;

    @InjectMocks
    private Transactions transactions;

    @Before
    public void setup() {
        given(customer.getUserId()).willReturn("test@velvetant.co.za");
    }

    @Test
    public void determine_payIn_given_a_balance_that_covers_half_the_trip() {
        given(repository.findCustomersBalance(any(Customer.class))).willReturn(new BigDecimal(100.00));

        BigDecimal payIn = transactions.determinePayInGiven(new BigDecimal(200.00), customer);

        assertThat(payIn).isEqualTo(new BigDecimal(100.00));
    }

    @Test
    public void determine_payIn_given_a_balance_that_covers_the_whole_trip() {
        given(repository.findCustomersBalance(any(Customer.class))).willReturn(new BigDecimal(300.00));

        BigDecimal payIn = transactions.determinePayInGiven(new BigDecimal(200.00), customer);

        assertThat(payIn).isEqualTo(new BigDecimal(0.00));
    }

    @Test
    public void determine_payIn_when_balance_is_zero() {
        given(repository.findCustomersBalance(any(Customer.class))).willReturn(new BigDecimal(0.00));

        BigDecimal payIn = transactions.determinePayInGiven(new BigDecimal(200.00), customer);

        assertThat(payIn).isEqualTo(new BigDecimal(200.00));
    }
}
