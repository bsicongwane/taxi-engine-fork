package za.co.velvetant.taxi.engine.persistence;

import static com.google.common.collect.Lists.transform;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import za.co.velvetant.taxi.engine.activiti.delegates.processing.Affiliates;
import za.co.velvetant.taxi.engine.api.FareRequest;
import za.co.velvetant.taxi.persistence.taxi.TaxiCompanyRepository;

import com.google.common.base.Function;

@Named("AffiliatesMock")
public class AffiliatesMock implements Affiliates {

    private static final Logger log = LoggerFactory.getLogger(FakeTaxiLocationRepository.class);

    @Inject
    private TaxiCompanyRepository affiliatesRepository;

    @Override
    public List<String> filterAffiliates(final FareRequest fareRequest) {
        log.debug("[MOCK] Filtering affiliates for customer: {}", fareRequest.getCustomer());
        List<Long> affiliates = affiliatesRepository.findAllAffiliateIds(fareRequest.getAffiliateIds());
        if (affiliates.isEmpty()) {
            affiliates = affiliatesRepository.findAllAffiliateIds();
        }
        return transform(affiliates, new LongToStringFunction());
    }

    private class LongToStringFunction implements Function<Long, String> {

        @Override
        public String apply(final Long affiliateId) {
            return affiliateId.toString();
        }
    }
}
