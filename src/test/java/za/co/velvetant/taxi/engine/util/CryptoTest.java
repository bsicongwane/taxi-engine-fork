package za.co.velvetant.taxi.engine.util;

import static org.fest.assertions.api.Assertions.assertThat;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

public class CryptoTest {

    @Test
    public void hash_is_applied_properly() throws UnsupportedEncodingException, NoSuchAlgorithmException {

        String hash = Crypto.hash("test");

        assertThat(hash).isNotNull();
    }

    @Test
    public void hash_is_applied_consistently() throws UnsupportedEncodingException, NoSuchAlgorithmException {

        String hash1 = Crypto.hash("test");
        String hash2 = Crypto.hash("test");

        assertThat(hash1).isEqualTo(hash2);
    }
}
