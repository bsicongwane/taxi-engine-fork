package za.co.velvetant.taxi.engine.persistence;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Fare;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.trips.FareRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-context-repositories.xml")
public class FareRepositoryFunctionalTestCase {

    @Inject
    private FareRepository fareRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Before
    public void setup() {
        Customer customer = customerRepository.findByEmail("bob@velvetant.co.za");
        customer.setSendNewsLetter(true);
        // save 10 fares where the last fare saved (but latest) is the one we want and there are 5 other active fares
        for (int x = 9; x >= 0; x--) {
            Fare fare = new Fare();
            fare.setCustomer(customer);

            if (x == 0) {
                fare.setUuid("1");
                fare.setRequestTime(new Date());
            } else {
                fare.setRequestTime(new DateTime().minusDays(1).toDate());
                fare.setUuid(UUID.randomUUID().toString());
            }

            fareRepository.save(fare);
        }
    }

    @Test
    public void testFindByCustomerEmail() throws Exception {
        Page<Fare> pageableFares = fareRepository.findByCustomerEmail("bob@velvetant.co.za", new PageRequest(0, 1));

        assertEquals(1, pageableFares.getSize());
        assertEquals("1", pageableFares.getContent().get(0).getUuid());
    }
}
