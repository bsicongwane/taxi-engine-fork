package za.co.velvetant.taxi.engine.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;
import za.co.velvetant.taxi.engine.api.CreateCustomer;
import za.co.velvetant.taxi.engine.delegates.Customers;
import za.co.velvetant.taxi.engine.filters.CustomerFilter;
import za.co.velvetant.taxi.engine.messaging.mymessages.MessageService;
import za.co.velvetant.taxi.engine.messaging.sms.SmsMessageService;
import za.co.velvetant.taxi.engine.models.Customer;
import za.co.velvetant.taxi.engine.models.Group;
import za.co.velvetant.taxi.engine.models.Role;
import za.co.velvetant.taxi.engine.persistence.PersonRepository;
import za.co.velvetant.taxi.engine.persistence.RoleRepository;
import za.co.velvetant.taxi.engine.persistence.UserRepository;
import za.co.velvetant.taxi.engine.util.credentials.Password;
import za.co.velvetant.taxi.persistence.customers.CustomerRepository;
import za.co.velvetant.taxi.persistence.transactions.TransactionRepository;

import javax.inject.Provider;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static za.co.velvetant.taxi.engine.util.VoConversion.convert;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceImplTestCase {

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Mock
    private CustomerRepository repository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private MessageService messageService;

    @Mock
    private Provider<CustomerFilter> customerFilter;

    @Mock
    private CustomerFilter filter;

    @Mock
    private UserRepository userRepository;
    
    @Mock
    private PersonRepository personRepository;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private SmsMessageService smsMessageService;

    @Mock
    private Password password;

    @InjectMocks
    private Customers customers;

    @Before
    public void before() {
        given(roleRepository.save(any(Role.class))).willReturn(testCustomerRole());
        given(repository.save(any(Customer.class), any(Principal.class))).willReturn(testEntityCustomer());
        given(customerFilter.get()).willReturn(filter);

    }

    @Test
    public void testCreate() throws Exception {
        final Customer testEntityCustomer = testEntityCustomer();
        given(repository.findByUserIdOrCellphone(testEntityCustomer.getEmail(), testEntityCustomer.getCellphone())).willReturn(null);
        given(userRepository.findByUserId(testEntityCustomer.getEmail())).willReturn(null);
        given(personRepository.findByUserId(testEntityCustomer.getEmail())).willReturn(null);
        Mockito.doNothing().when(messageService).addNewUserMessage(testEntityCustomer);
        Mockito.doNothing().when(smsMessageService).addNewUserMessage(testEntityCustomer);

        final CreateCustomer createdCustomer = customers.create(convert(testEntityCustomer, new CreateCustomer()));
        assertNotNull(createdCustomer);

       // verify(repository).findByUserIdOrCellphone(testEntityCustomer.getEmail(), testEntityCustomer.getCellphone());
        verify(repository).save(any(Customer.class));
        verify(roleRepository).save(any(Role.class));
    }

    @Test
    public void testFind() throws Exception {
        // just sanity
        final String existingEmail = testCustomerEmail();
        when(repository.fetchWithCreditCard(existingEmail)).thenReturn(testEntityCustomer());
        final za.co.velvetant.taxi.engine.api.Customer found = customerService.find(testCustomerEmail());
        assertNotNull(found);
        assertEquals(found.getEmail(), existingEmail);

        verify(repository).fetchWithCreditCard(existingEmail);
    }

    @Test
    public void testGET() throws Exception {
        final List<Customer> customers = new ArrayList<>();
        customers.add(testEntityCustomer());
        given(repository.findActive()).willReturn(customers);
        given(filter.with(anyString(), anyString(), anyString(), anyString(), anyBoolean(), anyBoolean(), anyBoolean())).willReturn(filter);
        given(filter.sort(anyInt(), anyInt(), anyString(), any(Sort.Direction.class))).willReturn(filter);
        given(filter.filter()).willReturn(filter);
        given(filter.andConvert()).willReturn(filter);
        final Response response = customerService.findAll(null, null, null, null, true, false, false, 1, 10, null, null);
        assertNotNull(response);
        // GET calls findAll
        verify(filter).filter();
    }

    @Test
    public void testCount() throws Exception {
        given(repository.count()).willReturn(42L);
        assertEquals(customerService.count(), "42");
        verify(repository).count();

    }

    public static Customer testEntityCustomer() {
        final Customer customer = new Customer("test1", "test1Last", "0835540735", "bob@velvetant.co.za", "r035198x");
        customer.setId(42L);
        return customer;
    }

    public static String testCustomerEmail() {
        return "bob@velvetant.co.za";
    }

    public static Role testCustomerRole() {
        return new Role(Group.CUSTOMER, testEntityCustomer());
    }
}
